<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes(['register' => false]); //disabled register routes
Route::get('/home', 'HomeController@index')->name('home');

/****************************************
**** Start Inbound Collection Routes  ***
****************************************/
//inbound view
Route::get('/inbound','InboundController@inbound');
Route::get('/inbound/{status}','InboundController@inbound_by_status');
Route::get('/action/inbound/{action}','InboundController@inbound_action');
Route::get('/scan/inbound/{action}','InboundController@inbound_scan');
Route::get('/inbound-handover-branch/{id}','InboundController@inbound_handover_branch');
Route::get('/inbound-from-city/{id}','InboundController@inbound_from_city');
Route::get('/handover-by-branch/{id}','InboundController@handover_by_branch');
Route::get('/handover-by-transit/{id}','InboundController@handover_by_transit');
Route::get('/rejected/{status}','InboundController@rejected_status');
Route::get('/group-by-inbound-receiver/{branch_id}','InboundController@group_by_inbound_receiver');
Route::get('/rejected-waybills-by-city/{city_id}','InboundController@rejected_waybills_by_city');
Route::get('/accepted-by/{user_id}','InboundController@accepted_by');
Route::get('/waybill/{waybill_no}/logs','InboundController@waybill_logs');
Route::get('/coming-waybills','InboundController@coming_waybills');
Route::get('/coming-waybills/{city_id}','InboundController@coming_waybills_from_city');
Route::get('/transit-coming-waybills/{city_id}','InboundController@transit_coming_waybills_from_city');
Route::post('/inbound/{id}/view','InboundController@view_waybill');
Route::get('/inbound/view/{id}/logs','InboundController@view_logs');
Route::get('/to-receive-lists/inbound','InboundController@to_receive_lists');
Route::get('/inbound/received-by/{user_id}','InboundController@received_by');
Route::get('/inbound-summary','InboundController@inbound_summary');

//inbound json
Route::get('/json/handover-by-branch/{id}','InboundController@json_handover_by_branch');
Route::get('/json/handover-by-transit/{id}','InboundController@json_handover_by_transit');
Route::get('/json/inbound-handover-branch/{id}','InboundController@json_inbound_handover_branch');
Route::get('/json/inbound-from-city/{id}','InboundController@json_inbound_from_city');
Route::get('/json/accepted-by/{user_id}','InboundController@json_accepted_by');
Route::get('/json/inbound/{status}','InboundController@json_inbound_by_status');
Route::get('/json/rejected-waybills-by-city/{city_id}','InboundController@json_rejected_waybills_by_city');
Route::get('/json/rejected/{status}','InboundController@json_rejected_status');
Route::get('/json/coming-waybills','InboundController@json_coming_waybills');
Route::get('/json/coming-waybills/{city_id}','InboundController@json_coming_waybills_from_city');
Route::get('/json/transit-coming-waybills','InboundController@json_transit_coming_waybills');
Route::get('/json/transit-coming-waybills/{city_id}','InboundController@json_transit_coming_waybills_from_city');
Route::get('/json/inbound/received-by/{user_id}','InboundController@json_received_by');

/****************************************
**** Start Outbound Collection Routes ***
****************************************/
//outbound view
Route::post('/outbound/{id}/view','OutboundController@view_waybill');
Route::get('/outbound','OutboundController@outbound');
Route::get('/outbound/{status}','OutboundController@outbound_by_status');
Route::get('/action/outbound/{action}','OutboundController@outbound_action');
Route::get('/scan/outbound/{action}','OutboundController@outbound_scan');
Route::get('/outbound/view/{id}','OutboundController@view_waybill');
Route::get('/outbound/view/{id}/logs','OutboundController@view_logs');
Route::get('/delivery-by/{id}','OutboundController@delivery_by');
Route::get('/branch-out-by-city/{id}','OutboundController@branch_out_by_city');
Route::get('/transit-out-by-city/{id}','OutboundController@transit_out_by_city');
Route::get('/inform-to-mail/{id}','OutboundController@inform_to_mail');
Route::post('/outbound/fetch-info','OutboundController@fetch_waybills');
Route::get('/filtered-outbound/by-branches','OutboundController@filtered_outbound_by_branches');
Route::get('/to-receive-lists/outbound','OutboundController@to_receive_lists');
Route::get('/branch-out-summary','OutboundController@branch_out_summary');
//Route::get('/outbound/transit/{status}','OutboundController@outbound_transit_status');
Route::get('/branch-out-summary/{branch_id}','OutboundController@branch_out_summary_by_branch');
Route::get('/branch-out-summary/{branch_id}/to/{city_id}','OutboundController@branch_out_to_city_by_branch');
Route::get('/single-scan/outbound/{action}','OutboundController@single_outbound_scan');

//outbound json
Route::get('/json/branch-out-by-city/{id}','OutboundController@json_branch_out_by_city');
Route::get('/json/transit-out-by-city/{id}','OutboundController@json_transit_out_by_city');
Route::get('/json/outbound/{status}','OutboundController@json_outbound_by_status');
Route::get('/transit-routes','TransitController@transit_routes');
Route::get('/json/users','UserController@json_users');
Route::get('/json/delivery-by/{id}','OutboundController@json_delivery_by');
Route::get('/json/branch-out-summary/{branch_id}/{filtered}','OutboundController@json_branch_out_summary_by_branch');
Route::get('/json/branch-out-summary/{branch_id}/to/{city_id}','OutboundController@json_branch_out_to_city_by_branch');

//Transit routes
Route::get('/transit/{status}','InboundController@transit_status');
Route::get('/json/transit/{status}','InboundController@json_transit_status');
Route::get('/transit-from-city/{id}','InboundController@transit_from_city');
Route::get('/json/transit-from-city/{id}','InboundController@json_transit_from_city');

//stock routes
Route::get('/stock-statistics','StockController@stock_statistics');
Route::get('/stock-statistics/{status}','StockController@stock_statistics_by_status');
Route::get('/json/stock-statistics/{status}','StockController@json_stock_statistics_by_status');
Route::get('/stock/by-branches','HomeController@stock_by_branches');
Route::get('/stock/check-stock/{branch_id}','HomeController@check_stock_by_branches');


/*****************************************************
** @start - report route listing
*****************************************************/
/* view routes */
Route::get('/search','ReportController@search');
Route::get('/reports','ReportController@reports');
Route::post('/reports/search/action-1','ReportController@search_action_one')->name('search-1');
Route::post('/reports/search/action-2','ReportController@search_action_two')->name('search-2');
Route::post('/reports/search/action-3','ReportController@search_action_three')->name('search-3');
Route::post('/reports/search/action-4','ReportController@search_action_four')->name('search-4');
Route::post('/reports/search/action-5','ReportController@search_action_five')->name('search-5');
Route::post('/reports/search/action-6','ReportController@search_action_six')->name('search-6');
Route::post('/reports/search/action-7','ReportController@search_action_seven')->name('search-7');
Route::post('/reports/search/action-8','ReportController@search_action_eight')->name('search-8');

/* search result routes */
Route::get('/json/reports/action-1/{params}','ReportController@json_search_action_one');
Route::get('/json/reports/action-2/{params}','ReportController@json_search_action_two');
Route::get('/json/reports/action-3/{params}','ReportController@json_search_action_three');
Route::get('/json/reports/action-4/{params}','ReportController@json_search_action_four');
Route::get('/json/reports/action-5/{params}','ReportController@json_search_action_five');
Route::get('/json/reports/action-6/{params}','ReportController@json_search_action_six');
Route::get('/json/reports/action-7/{params}','ReportController@json_search_action_seven');

/* export button routes */
Route::post('/export/reports/action-1/','ReportController@export_search_action_one');
Route::post('/export/reports/action-2/','ReportController@export_search_action_two');
Route::post('/export/reports/action-3/','ReportController@export_search_action_three');
Route::post('/export/reports/action-4/','ReportController@export_search_action_four');
Route::post('/export/reports/action-5/','ReportController@export_search_action_five');
Route::post('/export/reports/action-6/','ReportController@export_search_action_six');
Route::post('/export/reports/action-7/','ReportController@export_search_action_seven');
//Route::post('/json/reports/search-7','HomeController@json_search_action_seven');
/*****************************************************
** @end - report route listing
*****************************************************/


//advanced search routes
Route::get('/advanced-search','HomeController@advanced_search');
Route::post('/advanced-search/search-1','HomeController@advanced_search_1')->name('advanced-search-1');
Route::post('/advanced-search/search-2','HomeController@advanced_search_2')->name('advanced-search-2');
Route::post('/advanced-search/search-3','HomeController@advanced_search_3')->name('advanced-search-3');
Route::post('/advanced-search/search-4','HomeController@advanced_search_4');
Route::post('/advanced-search/search-5','HomeController@advanced_search_5');
Route::post('/advanced-search/search-6','HomeController@advanced_search_6');
Route::get('/view/waybill/{waybill_no}','HomeController@view_waybill');


Route::get('/help','HelpController@help');
Route::post('/help/ask','HelpController@ask');
Route::resource('/cities','CityController');
Route::resource('/branches','BranchController');
Route::resource('/users','UserController');
Route::post('changed-password','UserController@changed_password')->name('changed.password');
Route::get('/my-profile','UserController@my_profile');

Route::get('/config-routes/{city_id}','SettingController@config_routes');
Route::get('/set-routes','SettingController@set_routes');
Route::post('/set-routes','HomeController@config_routes');

Route::get('sending','HomeController@to_send_email');
Route::post('changed-date','SettingController@changed_date');

Route::prefix('setting')->group(function () {
	Route::get('/','SettingController@setting');
	Route::get('/waybill','SettingController@setting_waybill');
	Route::post('/setup-routes','SettingController@setting_routes');

	Route::get('/setting-times','SettingController@setting_times');
	Route::post('/setup-times','SettingController@setup_times')->name('setup-times');

	Route::get('/change-city','SettingController@change_city');
	Route::post('/change-city','SettingController@change_access_city')->name('change-city');

	Route::get('/run-raw-query','SettingController@run_raw_query');
	Route::post('/run-query-1','SettingController@run_query1')->name('query.run1');
	Route::post('/run-query-2','SettingController@run_query2')->name('query.run2');

	Route::get('/edit-waybill','SettingController@edit_waybill')->name('edit.waybill');
	Route::post('/update-waybill','SettingController@update_waybill')->name('update.waybill');

	Route::post('/waybill/delete','SettingController@waybill_delete')->name('delete-waybill');

	Route::post('/marked-branch','SettingController@marked_branch');
	Route::post('/clear-branch','SettingController@clear_branch');
});




Route::prefix('documentation/')->group(function () {
	Route::get('/api','DocController@index');
	Route::get('/api/{item}','DocController@api_lists');
});


Route::get('daily-check-lists/{list}','HomeController@daily_check_lists');
Route::get('daily-check-lists/{list}/{status}','HomeController@daily_check_lists_status');

Route::get('inbound-summary/filtered/{filtered}','InboundController@inbound_summary_filtered');
/** Live Search Routes **/
Route::get('outbound-search','OutboundController@outbound_search');
Route::get('delivery-by-search','OutboundController@search_json_delivery_by');
Route::get('branch-out-by-city','OutboundController@search_branch_out_by_city');


//sent cargo to odoo
Route::post('sent-odoo-api-callback','ApiController@sent_odoo_api_callback');



Route::get('/testing','HomeController@testing');
Route::get('/home/{section}','HomeController@fetch_section');
Route::get('/collected-by-branch/{branch_id}','OutboundController@collected_by_branch');

Route::get('/board','HomeController@board');
Route::get('/board/inbound',function(){
	return view('operator.pages.inbound');
});
Route::get('/board/outbound',function(){
	return view('operator.pages.outbound');
});
Route::get('json/stock/check-stock/{branch_id}','HomeController@json_check_stock_by_branches');

/* AI Outbound Received */
Route::get('ai-outbound-received','AiController@ai_outbound_received');
Route::post('ai-outbound-received','AiController@scanned_outbound_received');

Route::get('scanned-handover',function(){

	return view('operator.pages.scanned-handover');
});
Route::post('scanned-handover','ApiController@scanned_handover');

Route::get('/test',function(){
	$cargos = array('B78787878');
	sent_to_odoo($cargos,'YGN','in');

	return response()->json(['status' => 'sent']);
});


Route::get('/handover',function(){
	$date = '2021-03-01';

	$waybills = App\Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
		->join('branches AS b1','b1.id','=','branch_action_logs.from_branch')
		->join('branches AS b2','b2.id','=','branch_action_logs.to_branch')
		->select('branch_action_logs.action_date','waybill_no','waybills.current_status','b1.name AS from_branch','b2.name AS to_branch','branch_action_logs.active')
		->where('branch_action_logs.created_at','like','%'.$date.'%')
		->where('branch_action_logs.action_id','>=',6)
		->where('branch_action_logs.to_branch',1)
		->where('branch_action_logs.active',1)
		->where('branch_action_logs.action_type','inbound')
		->get();

	return $waybills;
});

Route::get('/branch-out',function(){
	$date = '2021-02-14 ';

	$waybills = App\Waybill::join('action_logs','action_logs.waybill_id','=','waybills.id')
		->join('cities AS c1','c1.id','=','waybills.origin')
		->leftjoin('users AS u','u.id','=','waybills.user_id')
		->leftjoin('branches AS b','b.id','=','u.branch_id')
		->leftjoin('cities AS c2','c2.id','=','waybills.destination')
		->select('action_logs.action_date','waybill_no','waybills.current_status','c1.shortcode AS origin','c2.shortcode AS destination','u.name as delivery','b.name as branch')
		->where('action_logs.action_date','like','%'.$date.'%')
		->where('action_logs.action_id',4)
		->where('action_logs.action_type','outbound')
		->where('waybills.origin',102)
		//->where('waybills.destination',45)
		->get();

	return $waybills;
});
