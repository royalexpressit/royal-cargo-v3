<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/action/inbound','ApiController@inbound_action');
Route::post('/action/outbound','ApiController@outbound_action');


Route::get('/cities','ApiController@cities');
Route::get('/cities/{id}/branches','ApiController@city_branches');
Route::get('/branches','ApiController@branches');
Route::get('/cities/{id}/branches','ApiController@city_branches');
Route::get('/delivery-by/{id}','ApiController@json_delivery_by');

Route::post('/check-transit','ApiController@check_transit');
Route::get('/transit-routes','ApiController@transit_routes');
Route::post('/search-transit','ApiController@search_transit');
Route::get('/view/{id}/logs/','ApiController@view_logs');

Route::get('/search-waybill','ApiController@search_waybill');


Route::prefix('v2')->group(function () {
    Route::post('/login','MobileApiController@login');
    Route::post('/change-password','MobileApiController@api_change_password');
    Route::post('/cities','MobileApiController@get_cities');
    Route::post('/branches','MobileApiController@get_branches');
    Route::post('/delivery-lists','MobileApiController@get_delivery_lists');
    Route::post('/transit-routes','MobileApiController@get_transit_routes');
    Route::post('/tracking','MobileApiController@tracking');

    //action routes
    Route::post('/outbound/action','MobileApiController@outbound_action');
    Route::post('/inbound/action','MobileApiController@inbound_action');
    
    //waybill status
    Route::post('/inbound/status','MobileApiController@inbound_status');
    Route::post('/outbound/status','MobileApiController@outbound_status');
    Route::post('/rejected/status','MobileApiController@rejected_status');
    Route::post('/search','MobileApiController@search');
    Route::post('/coming-waybill','MobileApiController@coming_waybill');

    //sent cargo to odoo
    Route::post('sent-odoo-api-callback','MobileApiController@sent_odoo_api_callback');
});
