<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBranchActionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_action_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('waybill_id')->nullable();
            $table->integer('from_branch')->nullable();
            $table->integer('to_branch')->nullable();
            $table->integer('action_id')->nullable();
            $table->string('action_type')->nullable();
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('branch_action_logs');
    }
}
