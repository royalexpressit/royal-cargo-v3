<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWaybillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('waybills', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('waybill_no')->nullable();
            $table->string('batch_id')->nullable();
            $table->integer('origin')->nullable();
            $table->integer('transit')->nullable();
            $table->integer('destination')->nullable();
            $table->integer('current_status')->nullable();
            $table->integer('transit_status')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('outbound_date')->nullable();
            $table->string('inbound_date')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('waybills');
    }
}
