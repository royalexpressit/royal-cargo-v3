<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateActionLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('action_logs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('waybill_id')->nullable();
            $table->integer('action_id')->nullable();
            $table->integer('action_by')->nullable();
            $table->string('action_date')->nullable();
            $table->string('action_type')->nullable();
            $table->integer('branch_id')->nullable();
            $table->integer('city_id')->nullable();
            $table->text('action_log')->nullable();
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('action_logs');
    }
}
