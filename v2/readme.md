## Royal Express Cargo

Cargo is a system for oubound and inbound waybills management of Royal Express Services.
The goal is to save and search transferred waybills between city to city.

- Outbound Waybills Management
- Inbound Waybills Management
- Postponed Waybills Management

## Version

- 2.0.1 (New Version)

## Features
- **Scan Feature (Used by scanner)**

## Copyright

Royal Express Services. Powered By IT Department. [Royal Express](http://royalx.net).
