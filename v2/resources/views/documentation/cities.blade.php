<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Royal Cargo | Config Routes</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">


	<link href="{{ asset('assets/css/jquery-ui.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">	
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title"><a href="{{ url('documentation/api') }}">Api Collections</a> # City Lists API</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
								<div class="row" id="kt_sortable_portlets">
									<div class="col-lg-9">	
										<div class="kt-portlet">
											<div class="kt-portlet__body">
										   		<table class="table table-bordered m-table">
												  	<tbody>
												    	<tr>
												      		<th class="fs-16 text-primary">API URL</th>
												      		<td><code class="fs-16">{{ server_address() }}/api/v2/cities</code></td>
												    	</tr>
												    	<tr>
													      	<th class="fs-16 text-primary">Method</th>
													      	<td><span class="badge badge-pill badge-primary">POST</span></td>
												    	</tr>
												    	<tr>
												      		<th class="fs-16 text-primary">Request (JSON)</th>
												      		<td>
												      			-
												      		</td>
												    	</tr>
												    	<tr>
												      		<th class="fs-16 text-primary">Header Content</th>
												      		<td>
												      			x-api-key :
												      			d53c832d-67c1-44bc-96fc-187a0f1c6b90
												      		</td>
												    	</tr>
												    	<tr>
												      		<th class="fs-16 text-primary">Response  (JSON)</th>
												      		<td>
												      			<code class="pre pre-l1">{</code>
																<code class="pre pre-l2">"success": 1,</code>
																<code class="pre pre-l2">"data": [</code>
																	<code class="pre pre-l3">{</code>
																		<code class="pre pre-l4">"id": 1,</code>
																		<code class="pre pre-l4">"name": "Ahtaung",</code>
																		<code class="pre pre-l4">"mm_name": "အထောင်",</code>
																		<code class="pre pre-l4">"shortcode": "ATG",</code>
																		<code class="pre pre-l4">"is_service_point": 1</code>
																	<code class="pre pre-l3">},</code>
																	<code class="pre pre-l3">{</code>
																		<code class="pre pre-l4">"id": 2,</code>
																		<code class="pre pre-l4">"name": "Aunglan",</code>
																		<code class="pre pre-l4">"mm_name": "အောင်လံ",</code>
																		<code class="pre pre-l4">"shortcode": "ALN",</code>
																		<code class="pre pre-l4">"is_service_point": 1</code>
																	<code class="pre pre-l3">},</code>
																	<code class="pre pre-l3">...</code>
																	<code class="pre pre-l3">{</code>
																		<code class="pre pre-l4">"id": 108,</code>
																		<code class="pre pre-l4">"name": "Zalun",</code>
																		<code class="pre pre-l4">"mm_name": "ဇလွန်",</code>
																		<code class="pre pre-l4">"shortcode": "ZLN",</code>
																		<code class="pre pre-l4">"is_service_point": 1</code>
																	<code class="pre pre-l3">}</code>
																<code class="pre pre-l2">]</code>
																<code class="pre pre-l1">}</code>
												      		</td>
												    	</tr>
												  	</tbody>
												</table>
											</div>
										</div>
									</div>

									<div class="col-lg-6">	
									</div>
								</div>				
							</div>
							<!-- end:: Content -->	
						</div>
					</div>						
				</div>
			</div>
		</div>
		
		<!-- begin:: Footer -->
		@include('layouts.footer')
		<!-- end:: Footer -->	
		<!-- end:: Page -->
	</div>
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
        
    <script src="{{ asset('assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('assets/js/draggable.js') }}" type="text/javascript"></script>
</body>
</html>