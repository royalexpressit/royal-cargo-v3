<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Config Routes</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">


	<link href="{{ asset('_assets/css/jquery-ui.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Active Transit Routes (<span class="text-primary">{{ city($origin)['name'] }}</span> - <span class="text-success">{{ city($destination)['name'] }}</span>) </h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								@if(allowed_config_route(Auth::id()) == 1)
								<!--begin::Row--> 
								<div class="row" id="kt_sortable_portlets">
									<div class="col-lg-6" >
										<h5 class="text-success">
											Active Transit
										</h5>
										<div class="h-s-600 pa-10 bg-white border-dashed scroll-hide-y" id="blockui_content_1">
											@foreach($routes as $route)	
									   		<div class="kt-portlet kt-portlet--sortable mb-2 drag-item-{{ $route->transit }} drag-item-active" id="{{ $route->transit }}">
												<div class="kt-portlet__head h-40" title="{{ $route->transit }}">
													<div class="kt-portlet__head-label">
														<span class="kt-portlet__head-icon">
															<i class="flaticon2-resize"></i>
														</span>
														<h3 class="kt-portlet__head-title">
															{{ city($route->transit)['shortcode'] }}
															<small>({{ city($route->transit)['name'] }} - {{ city($route->transit)['mm_name'] }})</small>
														</h3>			
													</div>
												</div>
											</div>
											@endforeach		

											<!-- begin:Empty Portlet: sortable porlet required for each columns! -->
									      	<div class="kt-portlet kt-portlet--sortable-empty">
									      	</div>
									      	<!--end::Empty Portlet-->
										</div>
									</div>

									<div class="col-lg-6">	
										<h5 class="text-danger">
											Inactive Transit
										</h5>
										<div class="h-s-600 bg-white pa-10 border-dashed">
											@foreach(App\City::orderBy('shortcode','asc')->get() as $city)
											<!--begin::Portlet-->
											@if(!App\Transit::where('origin',$origin)->where('transit',$city->id)->where('destination',$destination)->where('active',1)->first())
											<div class="kt-portlet kt-portlet--sortable mb-2 drag-item-{{ $city->id }} drag-item-inactive" id="{{ $city->id }}">
												<div class="kt-portlet__head h-40" title="{{ $city->id }}">
													<div class="kt-portlet__head-label">
														<span class="kt-portlet__head-icon">
															<i class="flaticon2-resize"></i>
														</span>
														<h3 class="kt-portlet__head-title">
															{{ $city->shortcode }}
															<small>({{ $city->name }} - {{ $city->mm_name }})</small>
														</h3>			
													</div>
												</div>
											</div>	
											@endif
											<!--end::Portlet-->
											@endforeach
										</div>
										

										<!-- begin:Empty Portlet: sortable porlet required for each columns! -->
								      	<div class="kt-portlet kt-portlet--sortable-empty">
								      	</div>
								      	<!--end::Empty Portlet-->	
									</div>
								</div>
								<!--end::Row--> 
								@else
								<!--begin::Row--> 
								<div class="row" id="kt_sortable_portlets">
									<div class="col-lg-6" >
										<h5 class="text-success">
											Active Transit
										</h5>
										<div class="h-s-400 pa-10 bg-white border-dashed scroll-hide-y">
											@foreach($routes as $route)	
									   		<div class="kt-portlet kt-portlet--sortable mb-2 drag-item-{{ $route->transit }} drag-item-active" id="{{ $route->transit }}">
												<div class="kt-portlet__head h-40" title="{{ $route->transit }}">
													<div class="kt-portlet__head-label">
														<span class="kt-portlet__head-icon">
															<i class="flaticon2-resize"></i>
														</span>
														<h3 class="kt-portlet__head-title">
															{{ city($route->transit)['shortcode'] }}
															<small>({{ city($route->transit)['name'] }} - {{ city($route->transit)['mm_name'] }})</small>
														</h3>			
													</div>
												</div>
											</div>
											@endforeach		

											<!-- begin:Empty Portlet: sortable porlet required for each columns! -->
									      	<div class="kt-portlet kt-portlet--sortable-empty">
									      	</div>
									      	<!--end::Empty Portlet-->
										</div>
									</div>
									<div class="col-lg-6" >
										<h5 class="text-danger">
											Inactive Transit
										</h5>
										<div class="h-s-400 pa-10 bg-white border-dashed scroll-hide-y">
											<p class="fs-22 text-warning">Permission denied to setup config routes.</p>
										</div>
									</div>
								</div>
								<!--end::Row--> 
								@endif			
							</div>
							<!-- end:: Content -->
						</div>
					</div>
														
				</div>
			</div>
		</div>
		
		<!-- begin:: Footer -->
		@include('layouts.footer')
		<input type="hidden" id="origin" value="{{ $origin }}">
		<input type="hidden" id="destination" value="{{ $destination }}">
		<!-- end:: Footer -->	
		<!-- end:: Page -->
	</div>
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
        
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/draggable.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		
   	</script>
</body>
</html>