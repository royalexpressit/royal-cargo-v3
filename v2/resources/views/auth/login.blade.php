<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Login</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/login-v1.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.form-control{
    		font-size: 1.2em;
    	}
    </style>
</head>
<body style="background-image: url('/_assets/images/bg.jpg');background-size: cover;"  class="kt-login-v1--enabled kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >

<!-- begin::Page loader -->
	
<!-- end::Page Loader -->        
    	<!-- begin:: Page -->
	<div class="kt-grid kt-grid--ver kt-grid--root">
		<div class="kt-grid__item  kt-grid__item--fluid kt-grid kt-grid--hor kt-login-v1" id="kt_login_v1">
	<!--begin::Item-->
	<div class="kt-grid__item">
		<!--begin::Heade-->
		<div class="kt-login-v1__head">
			<div class="kt-login-v1__logo">
				<a href="#">
					<img src="{{ asset('_assets/images/logo.png') }}" alt=""/>
				</a>
			</div>
			<div class="kt-login-v1__signup">
				<h4 class="kt-login-v1__heading">Don't have an account?</h4>
				<a href="tel:+959450049715">Contact to IT</a>
			</div>
		</div>
		<!--begin::Head-->
	</div>
	<!--end::Item-->

	<!--begin::Item-->
	<div class="kt-grid__item  kt-grid kt-grid--ver  kt-grid__item--fluid">
		<!--begin::Body-->
		<div class="kt-login-v1__body">
			<!--begin::Section-->
			<div class="kt-login-v1__section">
				<div class="kt-login-v1__info">
					<h3 class="kt-login-v1__intro">Royal Cargo Dashboard</h3>
					<p>Inbound & Outbound Waybills</p>
				</div>
			</div>
			<!--begin::Section-->

			<!--begin::Separator-->
			<div class="kt-login-v1__seaprator"></div>
			<!--end::Separator-->

		 	<!--begin::Wrapper-->
			<div class="kt-login-v1__wrapper">
				<div class="kt-login-v1__container">
					<h3 class="kt-login-v1__title">
						Sign In Account
					</h3>

					<!--begin::Form-->
					<form class="kt-login-v1__form kt-form" action="{{ route('login') }}" method="POST" autocomplete="off" id="kt_login_form">
						@csrf
						<div class="form-group">
							<input class="form-control" type="email" placeholder="Email" name="email" autocomplete="off" autofocus="">
							@error('email')
                            <span>
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
						</div>
						<div class="form-group">
							<input class="form-control" type="password" placeholder="Password" name="password" autocomplete="off">
							@error('password')
                            <span>
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
						</div>

						<div class="kt-login-v1__actions">
							<span class="kt-login-v1__forgot">
								&nbsp;
							</span>
							<button type="submit" class="btn btn-pill btn-elevate pulse" >Sign In</button>
						</div>
					</form>
					<!--end::Form-->

					<!--begin::Divider-->
					<div class="kt-login-v1__divider">
						<div class="kt-divider">
							<span></span>
							<span>OR</span>
							<span></span>
						</div>
					</div>
					<!--end::Divider-->

					<!--begin::Options-->
					<div class="kt-login-v1__options">
						<a href="#" class="btn">
							<i class="fab fa-facebook-f"></i>
							Fcebook
						</a>
						<a href="#" class="btn">
							<i class="fab fa-twitter"></i>
							Twitter
						</a>
						<a href="#" class="btn">
							<i class="fab fa-google"></i>
							Google
						</a>
					</div>
					<!--end::Options-->
				</div>
			</div>
			<!--end::Wrapper-->
		</div>
		<!--begin::Body-->
	</div>
	<!--end::Item-->

	<!--begin::Item-->
	<div class="kt-grid__item">
		<div class="kt-login-v1__footer">
			<div class="kt-login-v1__menu">
				<a href="#">Privacy</a>
				<a href="#">Legal</a>
				<a href="#">Contact</a>
			</div>

			<div class="kt-login-v1__copyright">
				<a href="#">&copy; 2020 Royal Express Services Co,Ltd.</a>
			</div>
		</div>
	</div>
	<!--end::Item-->
</div>
	</div>
	
<!-- end:: Page -->


    <script>
        var KTAppOptions = {
		    "colors": {
		        "state": {
		            "brand": "#385aeb",
		            "metal": "#c4c5d6",
		            "light": "#ffffff",
		            "accent": "#00c5dc",
		            "primary": "#5867dd",
		            "success": "#34bfa3",
		            "info": "#36a3f7",
		            "warning": "#ffb822",
		            "danger": "#fd3995",
		            "focus": "#9816f4"
		        },
		        "base": {
		            "label": [
		                "#c5cbe3",
		                "#a1a8c3",
		                "#3d4465",
		                "#3e4466"
		            ],
		            "shape": [
		                "#f0f3ff",
		                "#d9dffa",
		                "#afb4d4",
		                "#646c9a"
		            ]
		        }
		    }
		};
	</script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/login.js') }}" type="text/javascript"></script>
</body>
</html>