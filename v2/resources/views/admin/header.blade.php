<!-- begin:: Header -->
<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed "  data-ktheader-minimize="on" >
	<div class="kt-header__top">	
		<div class="kt-container ">
			<!-- begin:: Brand -->
            <div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
            	<div class="kt-header__brand-logo">
            		<a href="{{ url('home') }}">
            			<img alt="Logo" src="{{ asset('_assets/images/logo.png') }}" class=""/>
            			<img alt="Logo" src="" class="kt-header__brand-logo-sticky"/>			
            		</a>		
            	</div> 
            	<div class="kt-header__brand-title">
                    <ul class="user-info">
                        <li class="small"><i class="flaticon-medal text-danger"></i> <strong class="text-danger">{{ Auth::user()->name }}</strong> (Admin)</li>
                        <li class="small text-primary"><i class="flaticon2-calendar-5"></i> {{ get_date() }}</li>
                    </ul>
                    
                </div>
            </div>
            <!-- end:: Brand -->

			<!-- begin:: Topbar -->
            <div class="kt-header__topbar">
                <!--begin: Search -->
        		<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown">
            		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
            			<span class="kt-header__topbar-icon" ><i class="flaticon2-search-1"></i></span>
            		</div>
            		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
            			<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
                            <form method="get" class="kt-quick-search__form">
                                <div class="input-group">
                                    <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                                    <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                                    <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
                               	</div>
                            </form>
                            <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200">

                            </div>
                        </div>
            		</div>
            	</div>
                <!--end: Search -->

            	<!--begin: Notifications -->
            	<div class="kt-header__topbar-item dropdown">
            		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
            			<span class="kt-header__topbar-icon"><i class="flaticon2-notification"></i></span>
            			<span class="kt-badge kt-badge--danger"></span>
            		</div>
            		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
            			<div class="kt-head kt-head--sm kt-head--skin-light">
                        <h3 class="kt-head__title">User Notifications</h3>
                    </div>
                        <div class="kt-notification kt-margin-t-10 kt-margin-b-10 kt-scroll" data-scroll="true" data-height="270">
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon-time-2"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title">
                                        New order has been received
                                    </div>
                                    <div class="kt-notification__item-time">
                                        2 hrs ago
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon-file"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title">
                                        New file has been uploaded
                                    </div>
                                    <div class="kt-notification__item-time">
                                        5 hrs ago
                                    </div>
                                </div>
                            </a>
                            <a href="#" class="kt-notification__item">
                                <div class="kt-notification__item-icon">
                                    <i class="flaticon-user"></i>
                                </div>
                                <div class="kt-notification__item-details">
                                    <div class="kt-notification__item-title">
                                        New user feedback received
                                    </div>
                                    <div class="kt-notification__item-time">
                                        8 hrs ago
                                    </div>
                                </div>
                            </a>
                        </div>		
                    </div>
            	</div>
            	<!--end: Notifications -->	

            	<!--begin: Quick Actions -->
            	<div class="kt-header__topbar-item dropdown">
            		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
            			<span class="kt-header__topbar-icon"><i class="fa fa-qrcode"></i></span>
            		</div>
            		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-xl">
            			<div class="kt-head bg-danger">
                            <h3 class="kt-head__title">Quick Actions</h3>
                        </div>
                        <div class="kt-grid-nav">
                            <a href="{{ url('scan/outbound/collected') }}" class="kt-grid-nav__item">
                                <div class="kt-grid-nav__item-icon"><i class="fa fa-qrcode fs-40"></i></div>
                                <div class="kt-grid-nav__item-title text-primary">Outbound Scan</div>
                                <div class="kt-grid-nav__item-desc text-danger">စာထွက်အတွက် scan ဖတ်ရန်</div>
                            </a>
                            <a href="{{ url('scan/inbound/branch-in') }}" class="kt-grid-nav__item">
                                <div class="kt-grid-nav__item-icon"><i class="fa fa-qrcode fs-40"></i></div>
                                <div class="kt-grid-nav__item-title text-primary">Inbound Scan</div>
                                <div class="kt-grid-nav__item-desc text-danger">စာဝင်အတွက် scan ဖတ်ရန်</div>
                            </a>
                            <a href="{{ url('scan/inbound/postponed') }}" class="kt-grid-nav__item">
                                <div class="kt-grid-nav__item-icon"><i class="fa fa-qrcode fs-40"></i></div>
                                <div class="kt-grid-nav__item-title text-primary">Postponed Scan</div>
                                <div class="kt-grid-nav__item-desc text-danger">ဆိုင်းငံ့စာအတွက် scan ဖတ်ရန်</div>
                            </a>
                            <a href="{{ url('scan/inbound/transferred') }}" class="kt-grid-nav__item">
                                <div class="kt-grid-nav__item-icon"><i class="fa fa-qrcode fs-40"></i></div>
                                <div class="kt-grid-nav__item-title text-primary">Transferred Scan</div>
                                <div class="kt-grid-nav__item-desc text-danger">စာပြန်ပို့အတွက် scan ဖတ်ရန်</div>
                            </a>
                        </div>		
                    </div>
            	</div>
            	<!--end: Quick Actions -->	

            	<!--begin: Quick Panel Toggler -->
            	<div class="kt-header__topbar-item" data-toggle="kt-tooltip" title="Quick panel" data-placement="top">
            		<div class="kt-header__topbar-wrapper">
            			<span class="kt-header__topbar-icon" id="kt_quick_panel_toggler_btn"><i class="flaticon2-menu"></i></span>
            		</div>
            	</div>
            	<!--end: Quick Panel Toggler -->	

	
            	<!--begin: User -->
            	<div class="kt-header__topbar-item kt-header__topbar-item--user">
            		<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
                        <img alt="Pic" src="{{ asset('_assets/images/users/'.Auth::user()->image) }}"/>
                    </div>
            		<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-md">
            			<div class="kt-user-card-v4 kt-user-card-v4--skin-light kt-notification-item-padding-x">
                            <div class="kt-user-card-v4__avatar">
                                <!--use "kt-rounded" class for rounded avatar style-->
                                <img class="kt-rounded- sm-pic" alt="Pic" src="{{ asset('_assets/images/users/'.Auth::user()->image) }}"/>
                                <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
                                <span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold kt-hidden">S</span>
                            </div>
                            <div class="kt-user-card-v4__name">
                                {{ Auth::user()->name }}
                                <small>{{ user_role(role()) }} ({{ branch(Auth::user()->branch_id)['name'] }})</small>
                            </div>
                            <div class="kt-user-card-v4__badge kt-hidden">
                                <span class="btn btn-label-primary btn-sm btn-bold btn-font-md">23 messages</span>
                            </div>
                        </div>

                        <ul class="kt-nav kt-margin-b-10">
                            <li class="kt-nav__item">
                                <a href="/keen/preview/demo4/custom/profile/personal-information.html" class="kt-nav__link">
                                    <span class="kt-nav__link-icon"><i class="flaticon2-schedule"></i></span>
                                    <span class="kt-nav__link-text">My Profile</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="/keen/preview/demo4/custom/profile/overview-1.html" class="kt-nav__link">
                                    <span class="kt-nav__link-icon"><i class="flaticon2-writing"></i></span>
                                    <span class="kt-nav__link-text">Tasks</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="/keen/preview/demo4/custom/profile/overview-2.html" class="kt-nav__link">
                                    <span class="kt-nav__link-icon"><i class="flaticon2-mail-1"></i></span>
                                    <span class="kt-nav__link-text">Messages</span>
                                </a>
                            </li>
                            <li class="kt-nav__item">
                                <a href="/keen/preview/demo4/custom/profile/account-settings.html" class="kt-nav__link">
                                    <span class="kt-nav__link-icon"><i class="flaticon2-drop"></i></span>
                                    <span class="kt-nav__link-text">Settings</span>
                                </a>
                            </li>

                            <li class="kt-nav__separator kt-nav__separator--fit"></li>

                            <li class="kt-nav__custom kt-space-between">
                                <a href="{{ route('logout') }}" target="_blank" class="btn btn-danger btn-upper btn-sm btn-bold" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Log Out</a>
                                <i class="flaticon2-information kt-label-font-color-2" data-toggle="kt-tooltip" data-placement="right" title="" data-original-title="Click to learn more..."></i>
                            </li>
                        </ul>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
            		</div>
            	</div>
            	<!--end: User -->		
	 
            </div>
            <!-- end:: Topbar -->		
        </div>
    </div>

	<div class="kt-header__bottom">
		<div class="kt-container ">
			<!-- begin: Header Menu -->
            <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
            <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
                <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile "  >
                    <ul class="kt-menu__nav ">
                        <li class="kt-menu__item  kt-menu__item--submenu kt-menu__item--rel">
                            <a href="{{ url('home') }}" class="kt-menu__link">
                                <span class="kt-menu__link-text">Dashboard</span>
                            </a>
                        </li>
                        <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <span class="kt-menu__link-text">Outbound</span>
                                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('outbound/collected') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Collected Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('outbound/handover') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Handover Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('outbound/received') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Received Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('outbound/branch-out') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Branch Out Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('outbound/transit') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Transit Waybills</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li>   
                        <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <span class="kt-menu__link-text">Inbound</span>
                                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('inbound/branch-in') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Branch In Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('inbound/handover') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Handover Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('inbound/received') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Received Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('inbound/transit') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Transit Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('inbound/postponed') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Postponed Waybills</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('inbound/rejected') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Rejected Waybills</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li> 
                        <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <span class="kt-menu__link-text">Rejected</span>
                                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('rejected/pending') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Pending</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('rejected/transferred') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Transferred</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item" aria-haspopup="true">
                                        <a href="{{ url('rejected/accepted') }}" class="kt-menu__link ">
                                            <span class="kt-menu__link-text">Accepted</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </li> 
                        <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                            <a href="javascript:;" class="kt-menu__link kt-menu__toggle">
                                <span class="kt-menu__link-text">More</span>
                                <i class="kt-menu__hor-arrow la la-angle-down"></i>
                                <i class="kt-menu__ver-arrow la la-angle-right"></i>
                            </a>
                            <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--left">
                                <ul class="kt-menu__subnav">
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ url('users') }}" class="kt-menu__link">
                                            <span class="kt-menu__link-text">All Users</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ url('cities') }}" class="kt-menu__link">
                                            <span class="kt-menu__link-text">Available Cities</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ url('branches') }}" class="kt-menu__link">
                                            <span class="kt-menu__link-text">Available Branches</span>
                                        </a>
                                    </li>
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ url('transit-routes') }}" class="kt-menu__link">
                                            <span class="kt-menu__link-text">Transit Routes</span>
                                        </a>
                                    </li>  
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ url('advanced-search') }}" class="kt-menu__link">
                                            <span class="kt-menu__link-text">Advanced Search</span>
                                        </a>
                                    </li> 
                                    <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                                        <a href="{{ url('reports') }}" class="kt-menu__link">
                                            <span class="kt-menu__link-text">Waybill Reports</span>
                                        </a>
                                    </li>   
                                </ul>
                            </div>
                        </li> 
                        <li class="kt-menu__item kt-menu__item--submenu kt-menu__item--rel" data-ktmenu-submenu-toggle="click" aria-haspopup="true">
                            <a href="{{ url('setting') }}" class="kt-menu__link">
                                <span class="kt-menu__link-text">Setting</span>
                            </a>
                        </li>           
                    </ul>
                </div>
                <div class="kt-header-toolbar">
                    <div>
                        <a href="{{ url('coming-waybills') }}" class="btn btn-primary btn-bold btn-sm" data-toggle="kt-tooltip" title="" data-placement="top" data-original-title="ဝင်လာမည့်စာများ"><i class="flaticon2-hourglass-1"></i> Coming Waybills</a>
                        <a href="{{ url('stock-statistics') }}" class="btn btn-danger btn-bold btn-sm" data-toggle="kt-tooltip" title="" data-placement="top" data-original-title="စာဝေလက်ကျန် အခြေအနေ"><i class="flaticon2-pie-chart-1"></i> Stock Statistics</a>
                    </div>
                </div>
            </div>
            <!-- end: Header Menu -->		
        </div>
	</div>
</div>
<!-- end:: Header -->

<!-- begin:: Quick Panel -->
        <div id="kt_quick_panel" class="kt-offcanvas-panel">
            <div class="kt-offcanvas-panel__nav">
                <ul class="nav nav-pills" role="tablist">
                    <li class="nav-item active">
                        <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">dNotifications</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_actions" role="tab">Actions</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
                    </li>
                </ul>

                <button class="kt-offcanvas-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></button>
            </div>

            <div class="kt-offcanvas-panel__body">
                <div class="tab-content">
                    <div class="tab-pane fade show kt-offcanvas-panel__content kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
                        <!--Begin::Timeline -->
                        <div class="kt-timeline">
                            <!--Begin::Item -->                      
                            <div class="kt-timeline__item kt-timeline__item--success">
                                <div class="kt-timeline__item-section">
                                    <div class="kt-timeline__item-section-border">
                                        <div class="kt-timeline__item-section-icon">
                                            <i class="flaticon-feed kt-font-success"></i>
                                        </div>
                                    </div>
                                    <span class="kt-timeline__item-datetime">02:30 PM</span>
                                </div>

                                <a href="" class="kt-timeline__item-text">
                                    KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                             
                                </a>
                                <div class="kt-timeline__item-info">
                                    HTML,CSS,VueJS                                                                                            
                                </div>
                            </div>
                            <!--End::Item -->  

                            <!--Begin::Item --> 
                            <div class="kt-timeline__item kt-timeline__item--danger">
                                <div class="kt-timeline__item-section">
                                    <div class="kt-timeline__item-section-border">
                                        <div class="kt-timeline__item-section-icon">
                                            <i class="flaticon-safe-shield-protection kt-font-danger"></i>
                                        </div>
                                    </div>
                                    <span class="kt-timeline__item-datetime">01:20 AM</span>
                                </div>

                                <a href="" class="kt-timeline__item-text">
                                    New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
                                </a>
                                <div class="kt-timeline__item-info">
                                    Security, Fieewall                                                                                         
                                </div>
                            </div>  
                            <!--End::Item --> 

                            <!--Begin::Item --> 
                            <div class="kt-timeline__item kt-timeline__item--brand">
                                <div class="kt-timeline__item-section">
                                    <div class="kt-timeline__item-section-border">
                                        <div class="kt-timeline__item-section-icon">
                                            <i class="flaticon2-box kt-font-brand"></i>
                                        </div>
                                    </div>
                                    <span class="kt-timeline__item-datetime">Yestardey</span>
                                </div>

                                <a href="" class="kt-timeline__item-text">
                                    FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard                                                                                            
                                </a>
                                <div class="kt-timeline__item-info">
                                    PSD, Sketch, AJ                                                                                       
                                </div>
                            </div>  
                            <!--End::Item --> 

                            <!--Begin::Item --> 
                            <div class="kt-timeline__item kt-timeline__item--warning">
                                <div class="kt-timeline__item-section">
                                    <div class="kt-timeline__item-section-border">
                                        <div class="kt-timeline__item-section-icon">
                                            <i class="flaticon-pie-chart-1 kt-font-warning"></i>
                                        </div>
                                    </div>
                                    <span class="kt-timeline__item-datetime">Aug 13,2018</span>
                                </div>

                                <a href="" class="kt-timeline__item-text">
                                    Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br> England, BA12FJ                                                                                           
                                </a>
                                <div class="kt-timeline__item-info">
                                    Meeting, Customer                                                                                          
                                </div>
                            </div> 
                            <!--End::Item --> 

                            <!--Begin::Item --> 
                            <div class="kt-timeline__item kt-timeline__item--info">
                                <div class="kt-timeline__item-section">
                                    <div class="kt-timeline__item-section-border">
                                        <div class="kt-timeline__item-section-icon">
                                            <i class="flaticon-notepad kt-font-info"></i>
                                        </div>
                                    </div>
                                    <span class="kt-timeline__item-datetime">May 09, 2018</span>
                                </div>

                                <a href="" class="kt-timeline__item-text">
                                    KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                                
                                </a>
                                <div class="kt-timeline__item-info">
                                    HTML,CSS,VueJS                                                                                            
                                </div>
                            </div> 
                            <!--End::Item --> 

                            <!--Begin::Item --> 
                            <div class="kt-timeline__item kt-timeline__item--accent">
                                <div class="kt-timeline__item-section">
                                    <div class="kt-timeline__item-section-border">
                                        <div class="kt-timeline__item-section-icon"                                        >
                                            <i class="flaticon-bell kt-font-success"></i>
                                        </div>
                                    </div>
                                    <span class="kt-timeline__item-datetime">01:20 AM</span>
                                </div>

                                <a href="" class="kt-timeline__item-text">
                                    New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
                                </a>
                                <div class="kt-timeline__item-info">
                                    Security, Fieewall                                                                                         
                                </div>
                            </div>   
                            <!--End::Item -->                    

                        </div> 
                        <!--End::Timeline --> 
                    </div>
                    <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_actions" role="tabpanel">
                        <!--begin::Portlet-->
                        <div class="kt-portlet kt-portlet--solid-success">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
                                    <h3 class="kt-portlet__head-title">Recent Bills</h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <div class="kt-portlet__head-group">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-portlet__content">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
                                </div>
                            </div>  
                            <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                            </div>
                        </div>
                        <!--end::Portlet-->


                        <!--begin::Portlet-->
                        <div class="kt-portlet kt-portlet--solid-info">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Latest Invoices</h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <div class="kt-portlet__head-group">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-portlet__content">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
                                </div>
                            </div>  
                            <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                            </div>
                        </div>
                        <!--end::Portlet-->

                        <!--begin::Portlet-->
                        <div class="kt-portlet kt-portlet--solid-warning">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">New Comments</h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <div class="kt-portlet__head-group">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-portlet__content">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
                                </div>
                            </div>  
                            <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                            </div>
                        </div>
                        <!--end::Portlet-->

                        <!--begin::Portlet-->
                        <div class="kt-portlet kt-portlet--solid-brand">
                            <div class="kt-portlet__head">
                                <div class="kt-portlet__head-label">
                                    <h3 class="kt-portlet__head-title">Recent Posts</h3>
                                </div>
                                <div class="kt-portlet__head-toolbar">
                                    <div class="kt-portlet__head-group">
                                        <div class="dropdown dropdown-inline">
                                            <button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                                                <i class="flaticon-more"></i>
                                            </button>
                                            <div class="dropdown-menu dropdown-menu-right">
                                                <a class="dropdown-item" href="#">Action</a>
                                                <a class="dropdown-item" href="#">Another action</a>
                                                <a class="dropdown-item" href="#">Something else here</a>
                                                <div class="dropdown-divider"></div>
                                                <a class="dropdown-item" href="#">Separated link</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="kt-portlet__body">
                                <div class="kt-portlet__content">
                                    Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
                                </div>
                            </div>  
                            <div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
                                <a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                    <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
                        <form class="kt-form">
                            <div class="kt-heading kt-heading--space-sm">Notifications</div>

                            <div class="form-group form-group-xs row">
                                <label class="col-8 col-form-label">Enable notifications:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm">
                                        <label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_1">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <label class="col-8 col-form-label">Enable audit log:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm">
                                        <label>
                                            <input type="checkbox"  name="quick_panel_notifications_2">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-last form-group-xs row">
                                <label class="col-8 col-form-label">Notify on new orders:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm">
                                        <label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_2">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            
                            <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                            <div class="kt-heading kt-heading--space-sm">Orders</div>

                            <div class="form-group form-group-xs row">
                                <label class="col-8 col-form-label">Enable order tracking:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm kt-switch--danger">
                                        <label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_3">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <label class="col-8 col-form-label">Enable orders reports:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm kt-switch--danger">
                                        <label>
                                            <input type="checkbox"  name="quick_panel_notifications_3">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-last form-group-xs row">
                                <label class="col-8 col-form-label">Allow order status auto update:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm kt-switch--danger">
                                        <label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_4">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>

                            <div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

                            <div class="kt-heading kt-heading--space-sm">Customers</div>

                            <div class="form-group form-group-xs row">
                                <label class="col-8 col-form-label">Enable customer singup:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm kt-switch--success">
                                        <label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_5">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-xs row">
                                <label class="col-8 col-form-label">Enable customers reporting:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm kt-switch--success">
                                        <label>
                                            <input type="checkbox"  name="quick_panel_notifications_5">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>
                            <div class="form-group form-group-last form-group-xs row">
                                <label class="col-8 col-form-label">Notifiy on new customer registration:</label>
                                <div class="col-4 kt-align-right">
                                    <span class="kt-switch kt-switch--sm kt-switch--success">
                                        <label>
                                            <input type="checkbox" checked="checked" name="quick_panel_notifications_6">
                                            <span></span>
                                        </label>
                                    </span>
                                </div>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- end:: Quick Panel -->