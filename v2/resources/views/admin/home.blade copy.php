<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Royal Cargo | Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('admin.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Dashboard</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	<div class="input-group date">
												<input type="text" class="form-control " readonly  placeholder="Select date" id="kt_datepicker_2" value="{{ get_date() }}"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
												</div>
											</div>
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"><strong>{{ (get_date() == date('Y-m-d')? 'Today':get_date()) }}</strong> Outbound Status </h3>									
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--primary">
														<label>
														<input type="checkbox" id="outbound-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>
										   
										    <div class="kt-none">
										        <div class="kt-widget-1 outbound-switch">
										        	<a href="{{ url('outbound/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Outbound Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ outbound_dashboard_status(Auth::user()->city_id)['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/collected') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Collected Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာကောက်ထားသောအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['collected'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/handover') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
											                    Today Handover Waybills
											                </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာလွှဲပြောင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['handover'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/received') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                    	Today Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာလက်ခံအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['received'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/branch-out') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Branch Out Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာထွက်ပြီးသောအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['branch_out'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 outbound-transit-switch">
										        	<a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Outbound <span class="text-danger">Transit</span> Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['outbound'] }}</div>
										                </div>
										            </a>
										        	<a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today <span class="text-danger">Transit</span> Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာထွက်</span>- လက်ခံပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['received'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today <span class="text-danger">Transit</span> Branch Out Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာထွက်</span>- စာထွက်ပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['branch_out'] }}</div>
										                </div>
										            </a>
										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] > 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													
													<div class="alert alert-outline-warning mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text">
														  	နယ်စာထွက်ရန်အတွက် အရေအတွက် <strong>{{ transit_dashboard_status(Auth::user()->city_id)['balance'] }}</strong> စောင် လိုနေပါသည်။!
														  	<a href="#">စာရင်းကြည့်ရန်</a>
														  </div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@else
													<div class="alert alert-outline-success mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon2-check-mark"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် ကိုက်ညီမှုရှိသည်။</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@endif
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->

										
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"><strong>{{ (get_date() == date('Y-m-d')? 'Today':get_date()) }}</strong> Inbound Status</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="inbound-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>
										    <div class="kt-none">
										        <div class="kt-widget-1 inbound-switch">
										        	<a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ inbound_dashboard_status()['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/branch-in') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Branch-In Waybills 
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['branch_in'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/handover') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Handover Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['handover'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/received') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Received Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['received'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/postponed') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Postponed/Rejected Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- စာဆိုင်းငံ့/ငြင်းပယ် အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['postponed'] + inbound_dashboard_status()['rejected'] + inbound_dashboard_status()['transferred'] + inbound_dashboard_status()['accepted'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 inbound-transit-switch">
										        	<a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Inbound Transit Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['inbound'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Transit Branch In Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာဝင်</span>- စာဝင်ထားဆဲအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['branch_in'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/all') }}"  class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Transit Handover Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာဝင်</span>- စာလွှဲပြောင်းပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['handover'] }}</div>
										                </div>
										            </a>
										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] > 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
										            @endif	
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
								
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာပို့သမား (သို့) ကောင်တာမှ ကောက်ထားသောစာများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
										            	<span class="removed-outbound-filtered hide">
											            	<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" >
																<i class="flaticon-close"></i>
															</button>
														</span>
														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i> 
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav {{ (collected_outbound_by_branches()->count() > 10 ? 'fixed-mini-scroll':'') }}">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Filtered By Branch </span>
																	</li>
																	@if(!collected_outbound_by_branches()->isEmpty())
																		@foreach(collected_outbound_by_branches() as $branch)
																		<li class="kt-nav__item">
																			<span class="kt-nav__link filtered-outbound-by-branch" id="{{ $branch->branch_id }}">
																				<i class="kt-nav__link-icon flaticon-home-2"></i>
																				<span class="kt-nav__link-text" >{{ branch($branch->branch_id)['name'] }} <i class="flaticon2-check-mark text-success checked-item checked-item-{{ $branch->branch_id }} hide"></i></span>
																			</span>
																		</li>
																		@endforeach
																	@else
																		<li class="kt-nav__item">
																			<span class="kt-nav__link">
																				<i class="kt-nav__link-icon flaticon-close"></i>
																				<span class="kt-nav__link-text" >မရှိသေးပါ</span>
																			</span>
																		</li>
																	@endif
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body fixed-height">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>စာပို့သမား</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြီး</th>
														        <th>စာလက်ခံပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@if(!group_by_delivery(Auth::user()->city_id)->isEmpty())
															    	@foreach(group_by_delivery(Auth::user()->city_id) as $key => $delivery)
															      	<tr class="branch branch-{{ user($delivery->user_id)['branch_id'] }}">
																        <th scope="row"><a href="{{ url('collected-by/'.user($delivery->user_id)['id']) }}" class="text-danger"><i class="flaticon2-user"></i> {{ user($delivery->user_id)['name'] }} <span class="text-primary">({{ branch(user($delivery->user_id)['branch_id'])['name'] }})</span></a></th>
																        <td><span class="badge badge-pill badge-primary badge-sm">{{ delivery_man_collected($delivery->user_id)['total'] }}</span></td>
																        <td><span class="badge badge-pill badge-danger badge-sm">{{ delivery_man_collected($delivery->user_id)['collected'] }}</span></td>
																        <td><span class="badge badge-pill badge-warning badge-sm">{{ delivery_man_collected($delivery->user_id)['handover'] }}</span></td>
																        <td><span class="badge badge-pill badge-success badge-sm">{{ delivery_man_collected($delivery->user_id)['received'] }}</span> {!! (delivery_man_collected($delivery->user_id)['total'] == delivery_man_collected($delivery->user_id)['received']? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															      	</tr>
															      	@endforeach
															      	<input type="hidden" id="check-tb-height" value="{{ $key }}">
														    	@endif
														    </tbody>
													  	</table>
													</div>
												</div>
										    </div>
										    <div class="kt-portlet__foot kt-portlet__foot--md">
												<div class="">
													<a href="{{ url('to-receive-lists/outbound') }}" class="btn btn-danger btn-bold">စာထွက်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
													<a href="{{ url('filtered-outbound/by-branches') }}" class="btn btn-warning">ရုံးအလိုက်စာကောက်စာရင်းကြည့်ရန်</a>
												</div>
											</div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာထွက်လုပ်ထားသော မြို့များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">

														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Export Tools</span>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-print"></i>
																			<span class="kt-nav__link-text">Print</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-copy"></i>
																			<span class="kt-nav__link-text">Copy</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-excel-o"></i>
																			<span class="kt-nav__link-text">Excel</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-text-o"></i>
																			<span class="kt-nav__link-text">CSV</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																			<span class="kt-nav__link-text">PDF</span>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>မြို့သို့ (စာထွက်)</th>
														        <th>စုစုပေါင်း စာထွက်</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@foreach(group_by_to_city(Auth::user()->city_id) as $city)
														      	<tr>
														        	<th scope="row"><a href="{{ url('branch-out-by-city/'.$city->destination) }}" class="text-danger">{{ city($city->destination)['shortcode'].' <'.city($city->destination)['mm_name'].'>' }}</a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ to_city_status_count($city->destination)['total'] }}</span></td>
														      	</tr>
														      	@endforeach
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">မိမိမြို့သို့ မြို့အလိုက် စာဝင်စာရင်များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
															<input type="checkbox" id="city-transit">
															<span></span>
															<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>

										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>စာဝင်လာသည့်မြို့ </th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြောင်းပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    @foreach(group_by_from_city(Auth::user()->city_id) as $city)
														    	<tr>
														        	<th scope="row"><a href="{{ url('inbound-from-city/'.$city->origin) }}" class="text-danger">{{ city($city->origin)['shortcode'] }} <span class="text-primary"><{{ city($city->origin)['mm_name'] }}></span></a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ from_city_status_count($city->origin)['total'] }}</span></td>
														        	<td><span class="badge badge-pill badge-danger badge-sm">{{ from_city_status_count($city->origin)['branch_in'] }}</span></td>
														        	<td><span class="badge badge-pill badge-{{ (from_city_status_count($city->origin)['branch_in'] == 0? 'success':'warning') }} badge-sm">{{ from_city_status_count($city->origin)['handover'] }}</span> {!! (from_city_status_count($city->origin)['branch_in'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
														      	</tr>
														    @endforeach	
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">ရုံးအလိုက် လွှဲပြောင်းထားသော စာရင်းများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="city-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>

										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      	<tr class="text-warning bg-whitesmoke">
														        	<th>ရုံးသို့စာလွှဲ</th>
														        	<th>စုစုပေါင်း</th>
														        	<th>စာလက်ခံကျန်</th>
														        	<th>စာလက်ခံပြီး</th>
														      	</tr>
														    </thead>
														    <tbody>
														    	@foreach(inbound_handover_branches(Auth::user()->city_id) as $branch)
														    	<tr>
														    		<th scope="row"><a href="{{ url('handover-by-branch/'.$branch->to_branch) }}">{{ branch($branch->to_branch)['name'] }}</a></th>
														    		<td><span class="badge badge-pill badge-primary badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['total'] }}</span></td>
														    		<td><span class="badge badge-pill badge-warning badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['remain'] }}</span></td>
														    		<td><span class="badge badge-pill badge-success badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['received'] }}</span> {!! (inbound_handover_count_by_branch($branch->to_branch)['remain'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
														    	</tr>
														    	@endforeach
														    </tbody>
														    <tbody class="hide">
														    	@foreach(transit_handover_branches(Auth::user()->city_id) as $branch)
														    	<tr>
														    		<th scope="row"><a href="{{ url('handover-by-branch/'.$branch->to_branch) }}">{{ branch($branch->to_branch)['name'] }}</a></th>
														    		<td></td>
														    		<td></td>
														    		<td></td>
														    	</tr>
														    	@endforeach
														    </tbody>
													  	</table>
													  	<button type="button" class="btn btn-danger btn-bold">စာဝင်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</button>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Change Date For Dashboard</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h4>Cargo Dashboard အား <span id="set_date"></span> ရက်စွဲပြောင်းမည်။ </h4>
					<p>Dashboard တွင်မြင်ရသော waybills များသည် ထိုရက်စွဲ အတိုင်းမြင်ရမည်ဖြစ်သည်။</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-brand" data-dismiss="modal">ပိတ်မည်</button>
					<button type="button" class="btn btn-brand set-date" data-dismiss="modal">ပြောင်းမည်</button>
					<input type="hidden" id="config_date" value="">
				</div>
			</div>
		</div>
	</div>

	<!-- Modal -->
						<div class="modal fade" id="no-secure-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-centered" role="document">
								<div class="modal-content">
									<div class="modal-header bg-danger">
										<h5 class="modal-title text-white"><i class="flaticon-lock"></i> စကားဝှက်(password) လုံခြုံမှုဆိုင်ရာ အသိပေးခြင်း</h5>
										<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<p class="fs-16">လူကြီးမင်း အသုံးပြုထားသော စကားဝှက်(password) မှာ လုံခြုံမှု မရှိကြောင်းတွေ့ရပါသည်။ တခြားသူမှ အလွယ်တကူ အသုံးမပြုနိုင်အောင် စကားဝှက်(password)အား အသစ်ပြောင်းလဲရန် အကြံပြုပါသည်။ </p>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-outline-brand">Change Password</button>
										<button type="button" class="btn btn-outline-brand" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>

    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		/*
   		var no_secure = $("#no_secure").val();
   		if(no_secure == 1){
   			$('#no-secure-alert').modal({show:true});
   		}
   		*/

   		$("#kt_datepicker_2").datepicker({
		    format: 'yyyy-mm-dd'
		})
   		$("#kt_datepicker_2").on("change", function(e) {
   			var date = $("#kt_datepicker_2").val();
   			$("#set_date").text(date);
   			$("#config_date").val(date);
		    $('#exampleModal').modal({show:true});
		});

		$(".set-date").on("click",function search(e) {
        	set_date 	= $('#config_date').val();
        	_token 		= $('#_token').val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                
            $.ajax({
                type: 'post',
                url: "{{ url('changed-date') }}",
                dataType:'json',
                data: {
                    'set_date'	:set_date,
                    '_token' 	: _token
            	},
                success: function(data) { 
                	console.log(data);
                	location.reload();
                },
            });
        });
   	</script>
   	
</body>
</html>