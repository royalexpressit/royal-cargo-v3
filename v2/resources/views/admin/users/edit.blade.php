<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Users</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('admin.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Users - Edit User (အသုံးပြုသူအချက်အလက်ပြင်ရန်)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container kt-grid__item kt-grid__item--fluid">
								<!--begin::Portlet-->
								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">({{ $user->name }}) Edit User</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('users.update',$user->id) }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">Username (အမည်)<span class="text-danger fs-18">*</span></label>
															<input type="text" class="form-control"  name="name" value="{{ $user->name }}" required="" />
														</div>
														<div class="form-group">
															<label class="form-label">Email (အီးမေလ်း)<span class="text-danger fs-18">*</span></label>
															<input type="email" class="form-control" value="aungaung@royalx.net" disabled="" />
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-lg-6">
																	<label class="form-label">City (မြို့)<span class="text-danger fs-18">*</span></label>
																	<select class="form-control kt-select2" id="city" name="city" >
			                                                            @foreach(App\City::orderBy('name','asc')->get() as $city)
			                                                            <option value="{{ $city->id }}">{{ $city->shortcode.' - '.$city->name.' ('.$city->mm_name.')' }}</option>
			                                                            @endforeach
			                                                        </select>
																</div>
																<div class="col-lg-6">
																	<label class="form-label">Branch (ရုံး)<span class="text-danger fs-18">*</span></label>
																	<select class="form-control kt-select2" id="branch" name="branch">
			                                                            @foreach(App\Branch::orderBy('name','asc')->get() as $branch)
			                                                            <option value="{{ $branch->id }}">{{ $branch->name.' ('.city($branch->city_id)['shortcode'].')' }}</option>
			                                                            @endforeach
			                                                        </select>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="role_id" value="2" {{ ($user->role==2? 'checked':'') }}> Operator
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="role_id"  value="3" {{ ($user->role==3? 'checked':'') }}> Delivery/Counter
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="role_id"  value="4" {{ ($user->role==4? 'checked':'') }}> Cash On Delivery
																	<span></span>
																</label>
															</div>
														</div>
														<div class="form-group">
															@csrf
															@method('put')
															<button type="submit" class="btn btn-primary btn-wide">ပြင်ဆင်မည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>

					 				<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">({{ $user->name }}) Change Password</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('changed.password') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">Old Password (လက်ရှိ စကားဝှက်)<span class="text-danger fs-18">*</span></label>
															<input type="password" class="form-control" name="old_password" placeholder="Old Password" required="" />
														</div>
														<div class="form-group">
															<label class="form-label">New Password (စကားဝှက် အသစ်)<span class="text-danger fs-18">*</span></label>
															<input type="password" class="form-control" name="new_password" placeholder="New Password" required=""/>
														</div>
														<div class="form-group">
															<input type="hidden" name="user_id" value="{{ $user->id }}">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide">ပြောင်းမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
	

	@include('operator.quick-panel')


    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 

	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	$('#city').select2({
            placeholder: "Select From City"
        });
        $('#branch').select2({
            placeholder: "Select From Branch"
        });
    </script>
</body>
</html>