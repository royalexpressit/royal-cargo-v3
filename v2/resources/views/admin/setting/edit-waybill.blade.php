<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Edit Waybill</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.times{
            height:80px;
            width:80px;
            background:url('{{ asset("_assets/5sec.gif") }}');
            background-size: cover;
            background-position: left;
            background-repeat: no-repeat;
        }
        .continue-action{
        	display: none;
        }
        .continue-action{
        	margin-top:10px;
        }
        .invalid-no{

        }
        .scanned-panel{
        	height: 460px;
        	overflow-y: auto;
        }
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading" id="blockui_content_1">
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Outbound (စာထွက်)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												<strong class="badge badge-primary text-uppercase">Outbound</strong> - Collected Wabyill (စာထွက် - စာကောက်စာရင်းသွင်းရန်) 
											</h3>
										</div>
									</div>
									<!--begin::Form-->
									<form action="{{ route('update.waybill') }}" class="kt-form kt-form--fit kt-form--label-right" method="POST">
										<div class="kt-portlet__body">
											<div class="row">
												<div class="col-lg-4 col-md-6 col-sm-12">
													<div class="form-group">
														<label class="form-label text-success">Waybill No (စာအမှတ်) <strong class="text-danger check-number hide">(နံပါတ်မှားနေသည်)</strong></label>
														<input type="text" id="waybill" name="waybill_no" class="form-control" value="{{ $waybill->waybill_no }}" autofocus="">
													</div>
													<div class="form-group">
														<label class="form-label">Origin</label>
														<input type="text" class="form-control" value="{{ city($waybill->origin)['shortcode'].' ('.city($waybill->origin)['name'].')' }}" disabled="">
													</div>
													<div class="form-group">
														<label class="form-label">Transit</label>
														<select class="form-control kt-select2" id="transit" name="transit">
															<option >-</option>
															@foreach(App\City::orderBy('name','asc')->get() as $city)
															<option value="{{ $city->id }}" {{ ($city->id == $waybill->transit? 'selected':'')  }}>{{ $city->shortcode }} ({{ $city->name }})</option>
															@endforeach
														</select>
													</div>
													<div class="form-group ">
														<label class="form-label text-success">Destination</label>
														<select class="form-control kt-select2" id="destination" name="destination">
															<option >Not Set</option>
															@foreach(App\City::orderBy('name','asc')->get() as $city)
															<option value="{{ $city->id }}" {{ ($city->id == $waybill->destination? 'selected':'') }}>{{ $city->shortcode }} ({{ $city->name }})</option>
															@endforeach
														</select>
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-lg-6 col-md-6 col-sm-12">
																<label class="form-label">Branch Status({{ $waybill->current_status }})</label>
																<select class="form-control kt-select2" id="branch_status" name="branch_status">
																	<option value="1" {{ ($waybill->current_status == 1? 'selected':'' ) }}>Collected (Out)</option>
																	<option value="2" {{ ($waybill->current_status == 2? 'selected':'' ) }}>Handover (Out)</option>
																	<option value="3" {{ ($waybill->current_status == 3? 'selected':'' ) }}>Received (Out)</option>
																	<option value="4" {{ ($waybill->current_status == 4? 'selected':'' ) }}>Branch Out (Out)</option>
																	<option value="5" {{ ($waybill->current_status == 5? 'selected':'' ) }}>Branch In (In)</option>
																	<option value="6" {{ ($waybill->current_status == 6? 'selected':'' ) }}>Handover (In)</option>
																	<option value="7" {{ ($waybill->current_status == 7? 'selected':'' ) }}>Received (In)</option>
																	<option value="8" {{ ($waybill->current_status == 8? 'selected':'' ) }}>Postponed (In)</option>
																	<option value="9" {{ ($waybill->current_status == 9? 'selected':'' ) }}>Rejected (In)</option>
																</select>
															</div>
															<div class="col-lg-6 col-md-6 col-sm-12">
																<label class="form-label">Transit Status({{ $waybill->transit_status }})</label>
																<select class="form-control kt-select2" id="transit_status" name="transit_status">
																	<option value="1" {{ ($waybill->transit_status == 1? 'selected':'' ) }}>Collected (Out)</option>
																	<option value="2" {{ ($waybill->transit_status == 2? 'selected':'' ) }}>Handover (Out)</option>
																	<option value="3" {{ ($waybill->transit_status == 3? 'selected':'' ) }}>Received (Out)</option>
																	<option value="4" {{ ($waybill->transit_status == 4? 'selected':'' ) }}>Branch Out (Out)</option>
																	<option value="5" {{ ($waybill->transit_status == 5? 'selected':'' ) }}>Branch In (In)</option>
																	<option value="6" {{ ($waybill->transit_status == 6? 'selected':'' ) }}>Handover (In)</option>
																	<option value="7" {{ ($waybill->transit_status == 7? 'selected':'' ) }}>Received (In)</option>
																	<option value="8" {{ ($waybill->transit_status == 8? 'selected':'' ) }}>Postponed (In)</option>
																	<option value="9" {{ ($waybill->transit_status == 9? 'selected':'' ) }}>Rejected (In)</option>
																</select>
															</div>
														</div>
													</div>
													<div class="form-group">
														<label class="form-label">Delivery/Counter</label>
														<select class="form-control kt-select2" id="delivery" name="delivery">
															@foreach(App\User::where('role','3')->where('branch_id',Auth::user()->branch_id)->orderBy('name','asc')->get() as $delivery)
															<option value="{{ $delivery->id }}">{{ $delivery->name.' ('.branch($delivery->branch_id)['name'].')' }}</option>
															@endforeach
														</select>
													</div>
													<div class="form-group">
														@csrf
														<button type="submit" class="btn btn-primary btn-wide scan-btn" style="display: block;">ပြင်မည်</button>
													</div>
												</div>
												<div class="col-lg-8 col-md-6 col-sm-12">
													
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
				<input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
				<input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
				<input type="hidden" id="handover_branch_id" value="{{ main_office(Auth::user()->city_id) }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')

    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')   
	@include('layouts.action-modal')                   	     
        
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    $(document).ready(function(){
    	var base_url = $("#url").val();
	    var scanned = 0;
	    var success = 0;
	    var failed  = 0;
	    var raw     = '';
	    var voice   = 'alert-1.mp3';

	    $('#transit,#destination').select2({
	        placeholder: "Select City"
	    });

	    $('#branch_status,#transit_status').select2({
	        placeholder: "Select Status"
	    });

	    $("#form").submit(function(event){
	    	event.preventDefault();  
	    });
	});
    </script>
</body>
</html>