<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Setting</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		border:1px solid #f5f5f5;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile " >
        <div class="kt-header-mobile__logo">
            <a href="{{ url('') }}">
                <img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('admin.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
                                            <strong class="badge badge-danger text-uppercase">Setting</strong> - Run Raw Query
                                        </h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
							            	
							            </div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->
 
							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid" id="blockui_content_1">
											<div class="kt-portlet__head bg-danger">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">(QUERY) Models & Query Collections</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<div class="table-responsive">
                            <table class="table table-bordered">
                                <thead>
                                  <tr>
                                    <th>#</th>
                                    <th>Model</th>
                                    <th>Table Name</th>
                                  </tr>
                                </thead>
                                <tbody>
                                  <tr>
                                    <th scope="row">1</th>
                                    <td>Action</td>
                                    <td><code>actions</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">2</th>
                                    <td>ActionLog</td>
                                    <td><code>action_logs</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">3</th>
                                    <td>Branch</td>
                                    <td><code>branches</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">4</th>
                                    <td>BranchActionLog</td>
                                    <td><code>branch_action_logs</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">5</th>
                                    <td>City</td>
                                    <td><code>cities</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">6</th>
                                    <td>SetupTime</td>
                                    <td><code>setup_times</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">7</th>
                                    <td>Transit</td>
                                    <td><code>transits</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">8</th>
                                    <td>User</td>
                                    <td><code>users</code></td>
                                  </tr>
                                  <tr>
                                    <th scope="row">9</th>
                                    <td>Waybill</td>
                                    <td><code>waybills</code></td>
                                  </tr>
                                </tbody>
                            </table>
                        </div>
												</div>
											</div>	 
										</div>
									</div>

									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet">
											<div class="kt-portlet__head bg-danger">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">(QUERY) Run Query table, field, limit & order</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('query.run1') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Table Name</label>
                                                                    <input type="text" class="form-control" id="table" value="users" name="table"/>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Field</label>
                                                                    <input type="text" class="form-control" id="field" value="id" name="field"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Limit</label>
                                                                    <input type="text" class="form-control" id="limit" value="10" name="limit"/>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Order</label>
                                                                    <input type="text" class="form-control" id="order" value="ASC" name="order"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="kt-switch kt-switch--sm">
                                                                <label>
                                                                    <input type="checkbox" name="">
                                                                    <span></span>
                                                                    <strong class="switch-label switch-lbl text-primary">%LIKE%</strong>
                                                                </label>
                                                            </span>
                                                        </div>
														<div class="form-group">
                                                            <input type="hidden" name="option" value="1">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide delete-by-number" >RUN</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>

                                        <div class="kt-portlet">
                                            <div class="kt-portlet__head bg-danger">
                                                <div class="kt-portlet__head-label">            
                                                    <h3 class="kt-portlet__head-title text-white">(QUERY) Run Query table, field & order</h3>
                                                </div>
                                            </div>
                                            <div class="kt-portlet__body">
                                                <div class="kt-widget-18">
                                                    <form action="{{ route('query.run2') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Table Name</label>
                                                                    <input type="text" class="form-control" id="table2" value="users" name="table2"/>
                                                                </div>
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Where(Field)</label>
                                                                    <input type="text" class="form-control" id="field2" value="email" name="field2"/>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="form-group">
                                                            <div class="row">
                                                                <div class="col-lg-6">
                                                                    <label for="exampleTextarea">Where(Value)</label>
                                                                    <input type="text" class="form-control" id="search2" value="yekyawaung@royalx.net" name="search2"/>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <label for="exampleTextarea">Order</label>
                                                                    <input type="text" class="form-control" id="order2" value="ASC" name="order2"/>
                                                                </div>
                                                                <div class="col-lg-3">
                                                                    <label for="exampleTextarea">Limit</label>
                                                                    <input type="text" class="form-control" id="limit" value="10" name="limit2"/>
                                                                </div>
                                                            </div>
                                                            
                                                        </div>
                                                        <div class="form-group">
                                                            <span class="kt-switch kt-switch--sm">
                                                                <label>
                                                                    <input type="checkbox" name="">
                                                                    <span></span>
                                                                    <strong class="switch-label switch-lbl text-primary">%LIKE%</strong>
                                                                </label>
                                                            </span>
                                                        </div>
                                                        <div class="form-group">

                                                            <input type="hidden" name="option2" value="2">
                                                            @csrf
                                                            <button type="submit" class="btn btn-primary btn-wide delete-by-number" >RUN</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>   
                                        </div>
									</div>
								</div>

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->


     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
	<!--
	<script src="{{ asset('assets/js/blockui.js') }}" type="text/javascript"></script>
   	-->
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		var _token = $("#_token").val();
   		$('#to_delete_date, #kt_datepicker_1_validate').datepicker({
			format: 'yyyy-mm-dd'
		});

		$('#delete-action-1').click(function() {
            KTApp.block('#blockui_content_1', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'danger',
                message: 'လုပ်ဆောင်နေပါသည် ...'
            });
        });
        $('#delete-action-2').click(function() {
            KTApp.block('#blockui_content_2', {
                overlayColor: '#000000',
                type: 'v2',
                state: 'danger',
                message: 'လုပ်ဆောင်နေပါသည် ...'
            });
        });
   		
   		$(".delete-by-date").on("click",function search(e) {
        	var date   = $("#to_delete_date").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                
            $.ajax({
                type: 'post',
                url: "{{ url('setting/waybill/delete') }}",
                dataType:'json',
                data: {
                    'date'		:date,
                    '_token'	:_token,
                    'filtered' 	:'date'
            	},
                success: function(data) {
                	console.log(data); 
                	setTimeout(function() {
		                KTApp.unblock('#blockui_content_1');
		            }, 1000);
                },
            });
        });

        $(".delete-by-number").on("click",function search(e) {
        	var waybill_no   = $("#waybill_no").val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                
            $.ajax({
                type: 'post',
                url: "{{ url('setting/waybill/delete') }}",
                dataType:'json',
                data: {
                    'waybill_no':waybill_no,
                    '_token'	:_token,
                    'filtered' 	:'num'
            	},
                success: function(data) {
                	console.log(data); 
                	$("#waybill_no").val('');
                	setTimeout(function() {
		                KTApp.unblock('#blockui_content_2');
		            }, 1000);
                },
            });
        });
   		
   	</script>
</body>
</html>