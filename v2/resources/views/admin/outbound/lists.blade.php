<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Royal Cargo | Outbound [{{ $keyword }}]</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
    
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title"><strong class="badge badge-primary text-uppercase">Outbound</strong> - <span class="keyword">{{ $keyword }}</span><span class="search">Search with '<strong class="text-danger" id="term"></strong>'</span> Waybills (စာထွက် {{ $mm_keyword }}စာရင်းများ)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        <div class="typeahead">
								        <span class="searching kt-spinner kt-spinner--md kt-spinner--danger search-spinner"></span><input class="form-control" id="search-waybill" type="text" dir="ltr" placeholder="Search Waybill">
								    </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-solid-warning alert-bold alert-box" role="alert">
		                            <div class="alert-text">{{ $mm_keyword }} စာရင်းများ မရှိသေးပါ</div>
		                        </div>
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="list-group" id="fetched-data">
									</div>
									<div class="list-group" id="search-data">
									</div>
								</div>
								<div class="pagination">
									<div class="btn-group" role="group" aria-label="Basic example">
										<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
										<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
										<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')
	@include('operator.outbound.view-modal')
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 
        
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	
    	$(document).ready(function(){
    		//declared first loaded json data
			var load_url = '{{ url('').'/'.$json }}';
			var waybills = [];
			var item     = 0;
			var _token   = $("#_token").val();


    		var load_data = function() { 
    		 	$.ajax({
				    url: load_url,
				    type: 'GET',
				    data: {},
				    success: function(data){
				    	if(data.total > 0){
				    		$(".alert-box").hide();
				    		$('#fetched-data').empty();
					        $.each( data.data, function( key, value ) {
					        	++item;
					        	waybills.push(value.waybill_no);
								$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
				    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
				    			+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
				    			+ '<small>'+value.action_date+'</small></div>'
				    			+ '<small> စာပို့သမား/ကောင်တာ - '+value.delivery+' , စာရင်းသွင်းသူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
							});

							//console.log(waybills);
							//$(".pagination").show();
					        $("#to-records").text(data.to);
							$("#total-records").text(data.total);
								
							if(data.prev_page_url === null){
								$("#prev-btn").attr('disabled',true);
							}else{
								$("#prev-btn").attr('disabled',false);
							}
							if(data.next_page_url === null){
								$("#next-btn").attr('disabled',true);
							}else{
								$("#next-btn").attr('disabled',false);
							}
							$("#prev-btn").val(data.prev_page_url);
							$("#next-btn").val(data.next_page_url);
				    	}else{
				    		$(".pagination").hide();
				    		$(".alert-box").show();
				    	}
				    }
				});
    		};

    		var load_pagination_data = function() { 
    		 	//clicked url json data
				var clicked_url = $(this).val();
				

				$(this).siblings().removeClass('active')
				$(this).addClass('active');
				$.ajax({
				    url: clicked_url,
				    type: 'GET',
				    data: {},
				    success: function(data){
				        //console.log(data);
				        $("#fetched-data").empty();
				        $.each( data.data, function( key, value ) {
					        ++item;
					        waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
				    		+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-success"></i>'+value.branch+')</h5>'
				    		+ '<small>'+value.action_date+'</small></div>'
				    		+ '<small> စာပို့သမား/ကောင်တာ - '+value.name+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
						});	

						

				        $("#to-records").text(data.to);
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
				    }
				});
    		};

    		var load_search_data = function(data) {
    			
    			var waybill = data;
			    if(waybill.length > 5){
			    	$('.search-spinner').show();
			    	$('.keyword').hide();
			    	$('.search').css('display', 'inline-block');
			    	$('#term').text(data);
			    	$('#fetched-data').empty();
			    	$.ajax({
					    url: '{{ url("api/search-waybill") }}',
					    type: 'GET',
					    data: {
					    	'term':waybill
					    },
					    success: function(data){
					        $('.search-spinner').hide();
					        if(data.total > 0){
					        	$("#search-data").empty();
						        $.each( data.data, function( key, value ) {
						        	++item;
						        	waybills.push(value.waybill_no);
									$("#search-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start search-border" data-toggle="modal" data-target="#exampleModalLong">'
					    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-success"></i>'+value.branch+')</h5>'
					    			+ '<small>'+value.action_date+'</small></div>'
					    			+ '<small> စာပို့သမား/ကောင်တာ - '+value.name+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
								});
								console.log(item);
							}else{
				    			$(".alert-box").show();
							}
						}
					});
			    }else{
			    	$("#search-data").empty();
			    	$('#fetched-data').show();
			    	$('.keyword').show();
			    	$('.search').hide();
			    	load_data();
			    }
    		}


    		load_data();



			
			

			$('.pagination-btn').click(function(){
				load_pagination_data();
			});

			$('#search-waybill').keyup(function(e) {
				load_search_data(e.target.value);
			});

			$('body').delegate(".load-modal","click",function () {
				var id = $(this).attr('id');
				$.ajax({
					url: '{{ url("outbound") }}/'+id+'/view',
					type: 'POST',
					data: {
					    'id':id,
					    '_token': _token
					},
					success: function(data){
					    console.log(data);
					    $("#waybill_label").text(data.waybill_no);
					    $("#created_at").text(data.created_at);
					    $("#from_city").text('_ '+data.origin);
					    $("#transit_city").text('_ '+data.transit);
					    $("#to_city").text('_ '+data.destination);
					    $("#delivery").text('_ '+data.delivery);
					    $("#current_status").text(data.current_status);
					    $(".view-logs").attr("href",'/view/'+data.id+'/logs')
					}
				});
			});
		});
    </script>
</body>
</html>