<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		border:1px solid #f5f5f5;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('cod.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Dashboard</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
							            	<a href="#" class="btn btn-danger">ဆိုင်းငံ့ထားသော စာရင်းများ</a>
							            </div>

							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
								<div class="row">
									<div class="col-lg-3 col-xl-3 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
											<div class="kt-portlet__body kt-portlet__body--fluid">
												<div class="kt-widget-3 kt-widget-3--brand">
													<div class="kt-widget-3__content">
														<div class="kt-widget-3__content-info">
															<div class="kt-widget-3__content-section">
																<div class="kt-widget-3__content-title">Total</div>
																<div class="kt-widget-3__content-desc">Rejected Wabyills</div>
															</div>
															<div class="kt-widget-3__content-section">
																<span class="kt-widget-3__content-number"><i class="fa flaticon-open-box"></i> {{ rejected_status_count('int')['total'] }}</span>				
															</div>
														</div>	
														<div class="kt-widget-3__content-stats">				
															<div class="kt-widget-3__content-progress">
																<div class="progress">
																	<!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
																	<div class="progress-bar bg-primary progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ rejected_status_count('percent')['total'] }}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
															</div>
															<div class="kt-widget-3__content-action">
																<div class="kt-widget-3__content-text">စုစုပေါင်း</div>
																<div class="kt-widget-3__content-value">{{ rejected_status_count('percent')['total'] }}%</div>
															</div>
														</div>
													</div>
												</div>		
											</div>
										</div>
										<!--end::Portlet-->	
									</div>
									<div class="col-lg-3 col-xl-3 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<a href="{{ url('rejected/pending') }}" class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
											<div class="kt-portlet__body kt-portlet__body--fluid">
												<div class="kt-widget-3 kt-widget-3--danger">
													<div class="kt-widget-3__content">
														<div class="kt-widget-3__content-info">
															<div class="kt-widget-3__content-section">
																<div class="kt-widget-3__content-title">Pending</div>
																<div class="kt-widget-3__content-desc">Rejected Waybills</div>
															</div>
															<div class="kt-widget-3__content-section">
																<span class="kt-widget-3__content-number"><i class="fa flaticon-open-box"></i> {{ rejected_status_count('int')['pending'] }}</span>				
															</div>
														</div>	

														<div class="kt-widget-3__content-stats">				
															<div class="kt-widget-3__content-progress">
																<div class="progress">
																	<!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
																	<div class="progress-bar bg-danger progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ rejected_status_count('percent')['pending'] }}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
															</div>
															<div class="kt-widget-3__content-action">
																<div class="kt-widget-3__content-text">ပြန်ပို့ရန်ကျန်နေ</div>
																<div class="kt-widget-3__content-value">{{ rejected_status_count('percent')['pending'] }}%</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
										</a>
										<!--end::Portlet-->	
									</div>
									<div class="col-lg-3 col-xl-3 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<a href="{{ url('rejected/transferred') }}" class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
											<div class="kt-portlet__body kt-portlet__body--fluid">
												<div class="kt-widget-3 kt-widget-3--warning">
													<div class="kt-widget-3__content">
														<div class="kt-widget-3__content-info">
															<div class="kt-widget-3__content-section">
																<div class="kt-widget-3__content-title">Transferred</div>
																<div class="kt-widget-3__content-desc">Rejected Waybills</div>
															</div>
															<div class="kt-widget-3__content-section">
																<span class="kt-widget-3__content-number"><i class="fa flaticon-open-box"></i> {{ rejected_status_count('int')['transferred'] }}</span>				
															</div>
														</div>	

														<div class="kt-widget-3__content-stats">
															<div class="kt-widget-3__content-progress">
																<div class="progress">
																	<!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
																	<div class="progress-bar bg-warning progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ rejected_status_count('percent')['transferred'] }}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
															</div>
															<div class="kt-widget-3__content-action">
																<div class="kt-widget-3__content-text">ပြန်ပို့ထားပြီးလွှဲ</div>
																<div class="kt-widget-3__content-value">{{ rejected_status_count('percent')['transferred'] }}%</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
										</a>
										<!--end::Portlet-->	
									</div>
									<div class="col-lg-3 col-xl-3 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<a href="{{ url('rejected/accepted') }}" class="kt-portlet kt-portlet--fit kt-portlet--height-fluid">
											<div class="kt-portlet__body kt-portlet__body--fluid">
												<div class="kt-widget-3 kt-widget-3--success">
													<div class="kt-widget-3__content">
														<div class="kt-widget-3__content-info">
															<div class="kt-widget-3__content-section">
																<div class="kt-widget-3__content-title">Accepted</div>
																<div class="kt-widget-3__content-desc">Rejected Waybills</div>
															</div>
															<div class="kt-widget-3__content-section">
																<span class="kt-widget-3__content-number"><i class="fa flaticon-open-box"></i> {{ rejected_status_count('int')['accepted'] }}</span>				
															</div>
														</div>	

														<div class="kt-widget-3__content-stats">
															<div class="kt-widget-3__content-progress">
																<div class="progress">
																	<!-- Doc: A bootstrap progress bar can be used to show a user how far along it's in a process, see https://getbootstrap.com/docs/4.1/components/progress/ -->
																	<div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: {{ rejected_status_count('percent')['accepted'] }}%" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100"></div>
																</div>
															</div>
															<div class="kt-widget-3__content-action">
																<div class="kt-widget-3__content-text">လက်ခံရရှိပြီး</div>
																<div class="kt-widget-3__content-value">{{ rejected_status_count('percent')['accepted'] }}%</div>
															</div>
														</div>
													</div>
												</div>	
											</div>
										</a>
										<!--end::Portlet-->	
									</div>
								</div>

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-8 col-xl-4 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">မြို့အလိုက် စာငြင်းပယ်(rejected) စာရင်းများအားလုံး</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
														<button type="button" class="btn btn-warning">ယခုလအတွက်ကြည့်ရန်</button>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>မြို့အမည်</th>
														        <th>စုစုပေါင်း</th>
														        <th>ပြန်ပို့ရန်ကျန်</th>
														        <th>ပြန်ပို့ထားပြီး</th>
														        <th>စာလက်ခံရရှိပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@foreach(rejected_waybills_by_city() as $city)
														      	<tr>
															        <th scope="row"><a href="{{ url('rejected-waybills-by-city/'.$city->destination) }}"><i class="flaticon-placeholder-3"></i> {{ city($city->destination)['shortcode'] }} <span class="text-danger">({{ city($city->destination)['name'].')' }}</span></a></th>
															        <td><span class="badge badge-pill badge-primary badge-sm">{{ rejected_waybills_status_by_city($city->destination)['total'] }}</span></td>
															        <td><span class="badge badge-pill badge-danger badge-sm">{{ rejected_waybills_status_by_city($city->destination)['pending'] }}</span></td>
															        <td><span class="badge badge-pill badge-warning badge-sm">{{ rejected_waybills_status_by_city($city->destination)['transferred'] }}</span></td>
															        <td><span class="badge badge-pill badge-success badge-sm">{{ rejected_waybills_status_by_city($city->destination)['accepted'] }}</span></td>
														      	</tr>
														      	@endforeach
														    </tbody>
													  	</table>
													  	
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာလက်ခံလုပ်ဆောင်သူ စာရင်းများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            
										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>အမည်</th>
														        <th>လက်ခံစာစောင်</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@foreach(cod_users() as $user)
														      	<tr>
														        	<th scope="row"><a href="{{ url('accepted-by/'.$user->id) }}"><i class="flaticon2-user"></i> {{ $user->name }}</a></th>
														        	<td><span class="badge badge-pill badge-success badge-sm">{{ accepted_count_by_user($user->id) }}</span></td>
														      	</tr>
														      	@endforeach
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
								
							</div>						
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->

	@include('operator.quick-panel')

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->


	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
</body>
</html>