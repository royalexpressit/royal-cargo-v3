<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Rejected Waybills</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.alert-box{
    		display: none;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
			<!-- header -->
			@include('cod.header')
			<!-- header -->	


			<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
				<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
					<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
						<!-- begin:: Subheader -->
						<div class="kt-subheader kt-grid__item" id="kt_subheader">
						    <div class="kt-container ">
						        <div class="kt-subheader__main">
									<h3 class="kt-subheader__title">Rejected - {{ $keyword }} Waybills (စာငြင်းပယ် {{ $mm_keyword }}စာရင်းများ)</h3>
						            <span class="kt-subheader__separator kt-hidden"></span>
						        </div>
						        <div class="typeahead">
									<input class="form-control" id="kt_typeahead_1" type="text" dir="ltr" placeholder="Search Waybill">
								</div>
						    </div>
						</div>
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-grid__item kt-grid__item--fluid">
							
							<div class="alert alert-solid-warning alert-bold alert-box" role="alert">
	                            <div class="alert-text">{{ $mm_keyword }} စာငြင်းပယ် မရှိသေးပါ</div>
	                        </div>
							<!--begin::Portlet-->
							<div class="kt-portlet">
								<div class="list-group" id="fetched-data">
								</div>
							</div>
							<div class="pagination">
								<div class="btn-group" role="group" aria-label="Basic example">
									<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
									<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
									<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
								</div>
							</div>
							<!--end::Portlet-->



	</div>
<!-- end:: Content -->							</div>
											</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
<input type="hidden" id="user_city" value="{{ Auth::user()->city }}">
<input type="hidden" id="user_branch" value="{{ Auth::user()->branch }}">
<!-- end:: Footer -->			</div>
		</div>
	</div>
	
<!-- end:: Page -->

	<!-- begin:: Topbar Offcanvas Panels -->
	
	
			<!-- begin::Offcanvas Toolbar Quick Actions -->
<div id="kt_offcanvas_toolbar_quick_actions" class="kt-offcanvas-panel">
	<div class="kt-offcanvas-panel__head">
		<h3 class="kt-offcanvas-panel__title">
			Quick Actions
		</h3>
		<a href="#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_quick_actions_close"><i class="flaticon2-delete"></i></a>
	</div>
	<div class="kt-offcanvas-panel__body">
		<div class="kt-grid-nav-v2">
			<a href="#" class="kt-grid-nav-v2__item">
				<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-box"></i></div>
				<div class="kt-grid-nav-v2__item-title">Orders</div>				
			</a>
			<a href="#" class="kt-grid-nav-v2__item">
				<div class="kt-grid-nav-v2__item-icon"><i class="flaticon-download-1"></i></div>
				<div class="kt-grid-nav-v2__item-title">Uploades</div>				
			</a>
			<a href="#" class="kt-grid-nav-v2__item">
				<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-supermarket"></i></div>
				<div class="kt-grid-nav-v2__item-title">Products</div>				
			</a>
			<a href="#" class="kt-grid-nav-v2__item">
				<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-avatar"></i></div>
				<div class="kt-grid-nav-v2__item-title">Customers</div>				
			</a>
			<a href="#" class="kt-grid-nav-v2__item">
				<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-list"></i></div>
				<div class="kt-grid-nav-v2__item-title">Blog Posts</div>				
			</a>
			<a href="#" class="kt-grid-nav-v2__item">
				<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-settings"></i></div>
				<div class="kt-grid-nav-v2__item-title">Settings</div>				
			</a>			
		</div>
	</div>
</div>
<!-- end::Offcanvas Toolbar Quick Actions -->	
		<!-- end:: Topbar Offcanvas Panels -->

@include('operator.quick-panel')


<!-- Modal -->
						<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
							<div class="modal-dialog modal-dialog-scrollable" role="document">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="exampleModalLongTitle">View Waybill</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="kt-list kt-list--badge">
						                    <div class="kt-list__item">
						                        <span class="kt-list__badge"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-browser-2 kt-font-success"></i></span>
						                        <span class="kt-list__text">New booking(<a href="#" class="kt-link">ID:23232323</a>) has been placed and awaiting for approval</span>
						                        <span class="kt-list__time">Just now</span>
						                    </div>
						                    <div class="kt-list__item">
						                        <span class="kt-list__badge"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-graphic kt-font-brand"></i></span>
						                        <span class="kt-list__text">Booking(<a href="#" class="kt-link">ID:34523433</a>) has been accepted. Processing is pending</span>
						                        <span class="kt-list__time">5 mins</span>
						                    </div>
						                    <div class="kt-list__item">
						                        <span class="kt-list__badge kt-font-danger"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-user kt-font-focus"></i></span>
						                        <span class="kt-list__text">Branch In <br> Branch In By</span>
						                        <span class="kt-list__time">3 days</span>
						                    </div>
						                    <div class="kt-list__item">
						                        <span class="kt-list__badge"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-user kt-font-focus"></i></span>
						                        <span class="kt-list__text">Handover <br> Handover By</span>
						                        <span class="kt-list__time">3 weeks</span>
						                    </div>
						                    <div class="kt-list__item">
						                        <span class="kt-list__badge"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-user kt-font-focus"></i></span>
						                        <span class="kt-list__text">Received <br> Received By</span>
						                        <span class="kt-list__time">24 Jan, 2018</span>
						                    </div>
						                    <div class="kt-list__item kt-font-info">
						                        <span class="kt-list__badge"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-medical-records kt-font-dark"></i></span>
						                        <span class="kt-list__text kt-font-info">New booking(<a href="#" class="kt-link">ID:23232323</a>) has been placed and awaiting for approval</span>
						                        <span class="kt-list__time">14 Feb, 2018</span>
						                    </div>
						                    <div class="kt-list__item">
						                        <span class="kt-list__badge"></span>
						                        <span class="kt-list__icon"><i class="flaticon2-chat-1 kt-font-success"></i></span>
						                        <span class="kt-list__text">Booking(<a href="#" class="kt-link">ID:23232323</a>) has been successully processed</span>
						                        <span class="kt-list__time">Last year</span>
						                    </div>
						                </div>
									</div>
									<div class="modal-footer">
										<button type="button" class="btn btn-brand" data-dismiss="modal">Close</button>
									</div>
								</div>
							</div>
						</div>
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

                              
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>    
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	$(document).ready(function(){

			//declared first loaded json data
			var load_url = '{{ url('').'/'.$json }}';
			var waybills = [];
			var item     = 0;
			$.ajax({
			    url: load_url,
			    type: 'GET',
			    data: {},
			    success: function(data){
			    	if(data.total > 0){
			    		$(".alert-box").hide();
			    		//console.log(data);
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
			    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<span class=""><i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+'</span> - <i class="flaticon-placeholder-3 text-success"></i>'+value.destination+' )</h5> <h5><i class="flaticon2-location text-danger"></i>'+value.branch+'</h5>'
			    			+ '<small>'+value.action_date+'</small></div>'
			    			+ '<small> စာလက်ခံရရှိသူ - '+value.branch_in_by+' ('+value.branch+')</small><span class="badge badge-pill badge-'+(value.current_status == 9? 'warning':(value.current_status == 10? 'primary':'success'))+' pull-right">'+(value.current_status == 9? 'Pending':(value.current_status == 10? 'Transferred':'Accepted'))+'</span></a>');
						});

						//console.log(waybills);
						//$(".pagination").show();
				        $("#to-records").text(data.to);
						$("#total-records").text(data.total);
							
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
			    	}else{
			    		$(".pagination").hide();
			    		$(".alert-box").show();
			    	}
			    }
			});

			$('.pagination-btn').click(function(){
				//clicked url json data
				var clicked_url = $(this).val();
				

				$(this).siblings().removeClass('active')
				$(this).addClass('active');
				$.ajax({
				    url: clicked_url,
				    type: 'GET',
				    data: {},
				    success: function(data){
				        //console.log(data);
				        $("#fetched-data").empty();
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
			    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<span class="text-danger"><i class="flaticon-placeholder-3 "></i>'+value.branch+'</span>)</h5>'
			    			+ '<small>'+value.action_date+'</small></div>'
			    			+ '<small> စာဝင်လုပ်ဆောင်သူ - '+value.branch_in_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 5? 'danger':(value.current_status == 6? 'warning':(value.current_status == 7? 'success':'primary')))+' pull-right">'+(value.current_status == 5? 'Branch In':(value.current_status == 6? 'Handover':(value.current_status == 7? 'Received':'Postponed')))+'</span></a>');
						});

						

				        $("#to-records").text(data.to);
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
				    }
				});
			});
		});

		
    </script>
</body>
</html>