<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Accept Waybills</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .scan-btn{
            display: none;
            margin-top:10px;
        }
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
    <!-- begin::Page loader -->
    @include('layouts.loading')
    <!-- end::Page Loader -->        
    
    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile " >
        <div class="kt-header-mobile__logo">
            <a href="{{ url('') }}">
                <img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->

    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
            <!-- header -->
            @include('cod.header')
            <!-- header --> 


            <div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
                <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                    <div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">        
                        <!-- begin:: Subheader -->
                        <div class="kt-subheader   kt-grid__item" id="kt_subheader">
                            <div class="kt-container ">
                                <div class="kt-subheader__main">
                                    <h3 class="kt-subheader__title">Rejected (စာငြင်းပယ်) </h3>
                                    <span class="kt-subheader__separator kt-hidden"></span>

                                </div>
                            </div>
                        </div>
                        <!-- end:: Subheader -->

                        <!-- begin:: Content -->
                        <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                            

                            <!--begin::Portlet-->
                            <div class="kt-portlet">
                                <div class="kt-portlet__head">
                                    <div class="kt-portlet__head-label">
                                        <h3 class="kt-portlet__head-title">
                                            Rejected - Scan Accepted Wabyills (စာငြင်းပယ် - စာလက်ခံရရှိပြီးစာရင်းသွင်းရန်) 
                                        </h3>
                                    </div>
                                </div>
                                <!--begin::Form-->
                                <form id="form" class="kt-form kt-form--fit kt-form--label-right">
                                    <div class="kt-portlet__body">
                                        <div class="row">
                                            <div class="col-lg-3 col-md-6 col-sm-12">
                                                <div class="card action-received">
                                                    <div class="card-header bg-success text-white">စာလက်ခံရရှိ လုပ်ဆောင်ရန်</div>
                                                    <div class="card-body">
                                                        <a href="{{ url('scan/inbound/accepted') }}" class="btn btn-success text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Received</a>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-lg-4 col-md-6 col-sm-12">
                                                <div class="form-group">
                                                    <label class="form-label">Waybill No (စာအမှတ်)</label>
                                                    <input type="text" id="waybill" class="form-control" placeholder="C2103477890">
                                                </div>
                                                <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                            </div>

                                            <div class="col-lg-5 col-md-6 col-sm-12">
                                                <h5 class="kt-portlet__head-title">Scanned <span id="scanned" class="text-primary">0</span>,<span class="success-lbl">Success <span id="success" class="text-success">0</span></span>,<span class="failed-lbl">Failed <span id="failed" class="text-danger">0</span></span></h5>
                                                <ul class="list-group" id="scanned-lists">
                                                    
                                                </ul>
                                                <ul class="list-group" id="failed-lists">
                                                    
                                                </ul>
                                                <div class="kt-section__content kt-section__content--border">
                                                    <button type="button" class="btn btn-primary btn-wide scan-btn">စာရင်းသွင်းရန်နှိပ်ပါ</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <!--end::Form-->
                            </div>
                            <!--end::Portlet-->



    </div>
<!-- end:: Content -->                          </div>
                                            </div>
                </div>

                <!-- begin:: Footer -->
                @include('layouts.footer')
    <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
    <input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
    <input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
    <!-- end:: Footer -->           
</div>
        </div>
    </div>
    
<!-- end:: Page -->

    @include('operator.quick-panel')

     
    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->

    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var scanned = 0;
        var success = 0;
        var failed  = 0;
        var raw     = '';

        $('#from_city').select2({
            placeholder: "Select From City"
        });

        $("#form").submit(function(event){
            event.preventDefault();  
        });

        $("#waybill").on("keydown",function search(e) {
            if(e.keyCode == 13) {
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(success);
                $("#failed").text(failed);
                
                $("#failed-lists").empty();
                $("#scanned-lists").show(); 

                if(scanned < 11){
                    waybill   = $("#waybill").val().toUpperCase();
                    $(".scan-btn").show();
                    $("#scanned-lists").prepend('<li class="list-group-item"><i class="fa fa-qrcode"></i>  '+waybill+'</li>');
                    if(scanned > 1){
                        raw = waybill+',';
                    }else{
                        raw = waybill;
                    }
                    $(this).val('');
                    $('#multi_scanned_waybills').prepend(raw);
                    console.log(raw);
                }else{
                    alert('limit');
                }
            }
        });

        $(".scan-btn").on("click",function search(e) {
            waybills        = $('#multi_scanned_waybills').val();
            user_id         = $("#user_id").val();
            package_id      = $("#package_id").val();
            city_id         = $("#city_id").val();
            branch_id       = $("#branch_id").val();
            from_city       = $("#from_city").val();
            //check           = waybill.length;
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                
            $.ajax({
                type: 'post',
                url: "{{ url('api/action/inbound') }}",
                dataType:'json',
                data: {
                    'waybills'      :waybills,
                    'user_id'       :user_id,
                    'city_id'       :city_id,
                    'branch_id'     :branch_id,
                    'from_city'     :from_city,
                    'package_id'    :package_id,
                    'action'        :'accepted'
                },
                success: function(data) { 
                    $("#scanned-lists").empty(); 
                    $("#failed-lists").empty();


                    success = $("#scanned").text() - data.failed.length;
                    failed  = data.failed.length;

                    $("#success").text(success);
                    $("#failed").text(failed);

                    if(data.failed.length > 0 ){
                        for (i = 0; i < data.failed.length; i++) {
                            $("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-times text-danger"></i> '+data.failed[i]+'</li>');
                        }
                    }else{
                        $("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-check text-success"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                    }
                },
            });
            $(this).val(''); 

            $('#multi_scanned_waybills').empty();
            $('.scan-btn').hide();
            scanned = 0;
        });

        
    </script>
</body>
</html>