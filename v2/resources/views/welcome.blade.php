<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Royal Express Cargo</title>
        <meta name="description" content="Royal Express Cargo"> 
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

        <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-weight: 200;
                margin: 0;
                background: #FF5722;
                overflow:hidden;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
                background: #FF5722;
                width: 100%;
                color: #fff;
                padding: 50px 0px;
                margin-top: 80px;
            }

            .title {
                font-size: 32px;
            }

            .links{
                display: block;
                text-align: center;
                width: 100%;
                background: #FF9800;
                height: 54px;
                padding-top:10px;
            }
            .links > a {
                margin:0 auto;
                text-align: center;
                display: block;
                color: #636b6f;
                padding: 0 10px;
                font-size: 20px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
                color:#fff;
                height: 40px;
                width:200px;
                line-height: 40px;
                background:#f95723;
                border:1px solid #FF9800;
            }
            .links > a:hover{
                opacity: 0.8;
            }
            .m-b-md {
                margin-bottom: 30px;
            }
            h3 {
                font-size: 30px;
            }
        </style>
    </head>
    <body>
        <div class="">
           
            <div class="content">
                <div class="title m-b-md">
                    <h3>28/09/2020 မှစ၍ Cargo App Version အသစ်အား စတင်အသုံးပြုနိုင်ပြီဖြစ်သည်။</h3>
                    <h2>version 3.0</h2>
                </div>
            </div>
            <div class="links">
                @if (Route::has('login'))
                    @auth
                        <a href="{{ url('/home') }}" class="pulse">Home</a>
                    @else
                        <a href="{{ url('/login') }}" class="pulse">Login</a>
                    @endif
                @endauth
            </div>
        </div>
    </body>
</html>
