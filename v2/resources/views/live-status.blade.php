<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<style type="text/css">
		.count{
			font-size: 20px;
			float:right;
		}
		ul li{
			width: 100%;
    		display: inline-block;
    		border-bottom: 1px dashed #ccc7c7;
    		padding-top: 4px;
		}
		.card .card-content{
			padding: 6px 16px;
		}
		h5{
			margin: 4px 0px 0px 0px;
		}
	</style>
</head>
<body>
	<div>
		<nav>
		  	<div class="nav-wrapper">
		    	<a href="" class="brand-logo">Logo</a>
		    	Date: {{ date('Y-m-d H:i:s') }}
		  	</div>
		</nav>
		<nav>
		    <div class="nav-wrapper">
		      	<div class="col s12">
		        	<a href="#!" class="breadcrumb">Dashboard</a>
		        	<a href="#!" class="breadcrumb">Inbound</a>
		      	</div>
		    </div>
	  	</nav>
		<div class="row">
			@foreach(App\Branch::where('city_id',Auth::user()->city_id)->where('is_sorting',0)->where('is_transit_area',0)->get() as $branch)
		    <div class="col s6 m4">
		      	<div class="card">
			        <div class="card-content">
			        	<h5 class="red-text">{{ $branch->name }}</h5>
			        	<ul>
				          	<li><span>စုစုပေါင်း</span> <span class="count red-text">{{ total_handover($branch->id) }}</span></li>
				          	<li><span>စာလက်ခံရန်ကျန်</span> <span class="count orange-text">{{ total_handover($branch->id)-total_received($branch->id) }}</span></li>
				          	<li><span>စာလက်ခံပြီး</span> <span class="count green-text">{{ total_received($branch->id) }}</span></li>
				        </ul>
			        </div>
		      	</div>
		    </div>
		    @endforeach
	  	</div>
	</div>

	<!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
</body>
</html>