<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link rel="stylesheet" href="{{ asset('_assets/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="{{ asset('_assets/css/custom.css') }}">

    <title>Outbound Scanned Received Form</title>
    <style type="text/css">
      	body{
        	background:#ececec;
        	font-family: "Poppins-Regular","Pyidaungsu";
      	}
      	.jumbotron{
        	background: #F44336;
        	color: #fff;
        	margin-bottom: 1rem;
      	}
      	.times{
            height:80px;
            width:80px;
            background:url('{{ asset("_assets/5sec.gif") }}');
            background-size: cover;
            background-position: left;
            background-repeat: no-repeat;
        }
      	.label-lists,strong{
         	padding:2px;
         	font-size: 18px;
      	}
      	.list-group-item{
        	font-size:20px;
      	}
      	.badge{
        	padding: .5em .4em;
      	}
      	.badge-sm{
        	width:40px;
        	font-size: 18px;
        	padding: 2px;
      	}
      	.continue-action,.continue-action-btn{
          	display: none;
        }
      	.continue-action-btn{
          	margin-top:10px;
        }
      	.list-group-item{
            padding: .5rem 1.25rem;
      	}
      	.jumbotron{
        	padding:1rem;
      	}
      	.lead{
      		font-size: 16px;
      	}
      	.display-6{
      		font-size: 30px;
      	}
    </style>
</head>
<body>
	<div class="container-fluid">
        <div class="jumbotron jumbotron">
		  	<div class="container-fluid">
		    	<h1 class="display-6">Received For Digital Waybills</h1>
		    	<p class="lead">Outbound တွင် digital waybill ကို received လုပ်ပါက branch out ပြန်လုပ်ရန်မလိုပဲ branch out အဆင့်အထိ ကျော်သွားမည်ဖြစ်သည်။
		  		Same city waybill ဖြစ်ပါက နောက်နေ့ရက်စွဲဖြင့် branch in ဝင်ပြီးသာ ဖြစ်သွားပါမည်။ Main Sorting အတွက်သာ အသုံးပြုရန် ရည်ရွယ်ထားခြင်းဖြစ်သည်။</p>
		  	</div>
		</div>
        <div class="row">
            <div class="col-md-5 col-sm-5">
               	<div class="card">
                  	<div class="card-body">
                        <div class="form-group">
                           <label for="scanned_waybill"><strong>Scanned Waybill</strong><small class="text-danger check-number hide">(digital waybill မဟုတ်ပါ)</small></label>
                           <input type="text" class="form-control" id="scanned_waybill" aria-describedby="emailHelp" autofocus="">
                        </div>
                        <div class="form-group">
                          	<div class="row">
	                            <div class="col-md-6">
	                              	<button type="button" class="btn btn-primary continue-action-btn" >အသစ်ပြန်ဖတ်မည်</button>
	                            </div>
	                            <div class="col-md-6">
	                              	<input type="text" class="form-control continue-action" id="continue-action" >
	                            </div>
                          	</div>
                        </div>
                     
                        <input type="hidden" id="url" value="{{ url('') }}">
                        <input type="hidden" id="_token" value="{{ csrf_token() }}">
                  	</div>
               	</div>
            </div>
            <div class="col-md-7 col-sm-7">
               	<div class="label-lists">
                  	<strong class="text-primary">Scanned <span class="badge badge-pill badge-primary badge-sm" id="scanned">0</span></strong>,
                  	<strong class="text-success">Success <span class="badge badge-pill badge-success badge-sm" id="success">0</span></strong>, 
                  	<strong class="text-danger">Failed <span class="badge badge-pill badge-danger badge-sm" id="failed">0</span></strong>
               	</div>
               	<ul class="list-group" id="scanned-lists">
                  
               	</ul>
            </div>
        </div>
    </div>

    <input type="hidden" id="user_id" value="{{ Auth::id() }}">
    <input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
    <input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  	<div class="modal-dialog" role="document">
		    <div class="modal-content">
		      	<div class="modal-header">
			        <h5 class="modal-title text-danger" id="exampleModalLabel">သတ်မှတ်ထားသော အရေအတွက်ပြည့်သွားပါပြီ</h5>
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
			          	<span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
		      	<div class="modal-body">
		      		<div class="row">
		      			<div class="col-md-4">
		      				<div class="times"></div>
		      			</div>
		      			<div class="col-md-8 fs-18">
		      				Scanned ဖတ်ရန်သတ်မှတ်ထားသော အရေအတွက် ၃၀ ပြည့်သွားပါပြီ။
		      			</div>
		      		</div>
		      	</div>
		    </div>
	  	</div>
	</div>
	<!-- Modal -->

    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script type="text/javascript" src="{{ asset('_assets/js/jquery-3.5.1.js') }}"></script>
    <script type="text/javascript" src="{{ asset('_assets/js/popper.min.js') }}" ></script>
    <script type="text/javascript" src="{{ asset('_assets/js/bootstrap.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('_assets/js/O1a1b575a/ai_received_scanned.js') }}"></script>
</body>
</html>