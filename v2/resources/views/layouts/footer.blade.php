<div class="kt-footer kt-grid__item" id="kt_footer">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-footer__bottom">
			<div class="kt-footer__copyright">
				2020&nbsp;&copy;&nbsp;<a href="http://royalx.net" target="_blank" class="kt-link">Royal Express</a>
				, Powered By Royal Express IT.
			</div>
			<div class="kt-footer__menu">
				<a href="#" target="_blank" class="kt-link">About</a>
				<a href="#" target="_blank" class="kt-link">Team</a>
				<a href="#" target="_blank" class="kt-link">Contact</a>
			</div>
		</div>
	</div>
</div>
<input type="hidden" id="url" value="{{ url('') }}">
<input type="hidden" id="_token" value="{{ csrf_token() }}">
<input type="hidden" id="no_secure" value="{{ password_checker(Auth::id()) }}">