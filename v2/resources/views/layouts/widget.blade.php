<!-- begin:: Demo Toolbar -->
		<ul class="kt-sticky-toolbar" style="margin-top: 30px;">
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--demo-toggle" id="kt_demo_panel_toggle" data-toggle="kt-tooltip" title="Check out more demos" data-placement="right">
				<a href="#" class="">DOCS</a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--builder" data-toggle="kt-tooltip" title="API Collections" data-placement="left">
		        <a href="{{ url('documentation/api') }}" ><i class="flaticon2-open-text-book"></i></a>
			</li>
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--docs" data-toggle="kt-tooltip" title="Scanned Outbound Received" data-placement="left">
				<a href="{{ url('ai-outbound-received') }}" target="_blank"><i class="flaticon-businesswoman"></i></a>
			</li>
			@if(App\SetupTime::where('city_id',Auth::user()->city_id)->where('action_label','branch-in')->where('active',1)->first())
			<li class="kt-sticky-toolbar__item kt-sticky-toolbar__item--docs active-time-updated" data-toggle="kt-tooltip" title="Active Times" data-placement="left">
				<a href="{{ url('setting/setting-times') }}" target="_blank"><i class="flaticon-time-1 text-success faa-ring animated"></i></a>
			</li>
			@endif
		</ul>
	<!-- end:: Demo Toolbar -->  

<!-- begin::Demo Panel -->

<div id="kt_demo_panel" class="kt-demo-panel">
	<div class="kt-demo-panel__head">
		<h3 class="kt-demo-panel__title">
			Download App & Docs
			<!--<small>5</small>-->
		</h3>
		<a href="#" class="kt-demo-panel__close" id="kt_demo_panel_close"><i class="flaticon2-delete"></i></a>
	</div>
	<div class="kt-demo-panel__body">
        <div class="kt-demo-panel__item ">
            <div class="kt-demo-panel__item-title">
                Demo 20
            </div>
            <div class="kt-demo-panel__item-preview">
                <img src="{{ asset('_assets/images/logo.png') }}" alt=""/>
                <div class="kt-demo-panel__item-preview-overlay">
                    <a href="#" class="btn btn-brand btn-elevate disabled" >Coming soon</a>    
                </div>
            </div>                    
        </div>
		<a href="#" target="_blank" class="kt-demo-panel__purchase btn btn-brand btn-elevate btn-bold btn-upper">
			Download App
		</a>
		<a href="#" target="_blank" class="kt-demo-panel__purchase btn btn-success btn-elevate btn-bold btn-upper">
			Download Docs
		</a>
	</div>
</div>
<!-- end::Demo Panel -->