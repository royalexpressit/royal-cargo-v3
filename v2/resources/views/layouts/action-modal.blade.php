<!-- Limit Alert Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">အရေအတွက်ပြည့်သွားပါပြီ </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="times"></div>
                        </div>
                        <div class="col-md-9">
                            <h5>သတ်မှတ်ထားသော အရေအတွက်ပြည့်သွားပါပြီ</h5>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Limit Alert Modal --> 

    <!-- Limit Alert Modal -->
    <div class="modal fade" id="voiceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">အသံပြောင်းရန် </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <ul class="list-group unstyled-list"> 
                        <li>
                            <audio controls>
                                <source src="{{ asset('_assets/alert-1.mp3') }}" type="audio/mpeg">
                            </audio>
                            <button class="choice-voice btn btn-success btn-bold btn-sm text-upper pull-right" value="alert-1.mp3">Choice</button>
                        </li>
                        <li>
                            <audio controls>
                                <source src="{{ asset('_assets/alert-2.mp3') }}" type="audio/mpeg">
                            </audio>
                            <button class="choice-voice btn btn-primary btn-bold btn-sm text-upper pull-right" value="alert-2.mp3">Choice</button>
                        </li>
                        <li>
                            <audio controls>
                                <source src="{{ asset('_assets/alert-3.mp3') }}" type="audio/mpeg">
                            </audio>
                            <button class="choice-voice btn btn-primary btn-bold btn-sm text-upper pull-right" value="alert-3.mp3">Choice</button>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- Limit Alert Modal --> 