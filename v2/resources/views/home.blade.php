<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Royal Cargo | Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  id="blockui_content_1">
	<!-- begin::Page loader -->
	
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="/keen/preview/demo4/index.html">
				<img alt="Logo" src="/keen/themes/keen/theme/demo4/dist/assets/media/logos/logo-5.png"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Dashboard</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
            <div class="kt-subheader__wrapper">
                <a href="{{ url('coming-waybills') }}" class="btn btn-primary btn-bold" data-toggle="kt-tooltip" title="" data-placement="top" data-original-title="ဝင်လာမည့်စာများ"><i class="flaticon2-hourglass-1"></i> Coming Waybills</a>
        
                            </div>
        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"> Outbound Status </h3>
									
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--primary">
														<label>
														<input type="checkbox" id="outbound-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>
										   
										    <div class="kt-none">
										        <div class="kt-widget-1 outbound-switch">
										        	<a href="{{ url('outbounds/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Outbound Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ outbound_dashboard_status(Auth::user()->city_id)['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbounds/collected') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Collected Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာကောက်ထားသောအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['collected'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbounds/handover') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
											                    Today Handover Waybills
											                </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာလွှဲပြောင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['handover'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbounds/received') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                    	Today Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာလက်ခံအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['received'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbounds/branch-out') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Branch Out Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့မှစာထွက်</span>- စာထွက်ပြီးသောအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['branch_out'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 outbound-transit-switch">
										        	<a href="{{ url('inbounds/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Outbound <span class="text-danger">Transit</span> Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['outbound'] }}</div>
										                </div>
										            </a>
										        	<a href="{{ url('inbounds/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today <span class="text-danger">Transit</span> Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာထွက်</span>- လက်ခံပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['received'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today <span class="text-danger">Transit</span> Branch Out Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာထွက်</span>- စာထွက်ပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['branch_out'] }}</div>
										                </div>
										            </a>
										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] > 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													
													<div class="alert alert-outline-warning mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text">
														  	နယ်စာထွက်ရန်အတွက် အရေအတွက် <strong>{{ transit_dashboard_status(Auth::user()->city_id)['balance'] }}</strong> စောင် လိုနေပါသည်။!
														  	<a href="#">စာရင်းကြည့်ရန်</a>
														  </div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@else
													<div class="alert alert-outline-success mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon2-check-mark"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် ကိုက်ညီမှုရှိသည်။</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@endif
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->

										
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">Today Inbound Status</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="inbound-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>
										    <div class="kt-none">
										        <div class="kt-widget-1 inbound-switch">
										        	<a href="{{ url('inbounds/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ inbound_dashboard_status()['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/branch-in') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Branch-In Waybills 
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['branch_in'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/handover') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Handover Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['handover'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/received') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Received Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['received'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/postponed') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">Today Postponed/Rejected Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့မိမိမြို့သို့စာဝင်</span>- စာဆိုင်းငံ့/ငြင်းပယ် အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['postponed'] + inbound_dashboard_status()['rejected'] + inbound_dashboard_status()['transferred'] + inbound_dashboard_status()['accepted'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 inbound-transit-switch">
										        	<a href="{{ url('inbounds/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Inbound Transit Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['inbound'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Transit Branch In Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာဝင်</span>- စာဝင်ထားဆဲအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['branch_in'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbounds/all') }}"  class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        Today Transit Handover Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">ယနေ့နယ်စာဝင်</span>- စာလွှဲပြောင်းပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ transit_dashboard_status(Auth::user()->city_id)['handover'] }}</div>
										                </div>
										            </a>
										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] > 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
										            @endif	
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

 														
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာပို့သမား (သို့) ကောင်တာမှ ကောက်ထားသောစာများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
										            	<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md removed-outbound-filtered hide" >
															<i class="flaticon-close"></i>
														</button>
														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Filtered By Branch </span>
																	</li>
																	@foreach(collected_outbound_by_branches() as $branch)
																	<li class="kt-nav__item">
																		<span class="kt-nav__link filtered-outbound-by-branch" id="{{ $branch->branch_id }}">
																			<i class="kt-nav__link-icon flaticon-home-2"></i>
																			<span class="kt-nav__link-text" >{{ branch($branch->branch_id)['name'] }} <i class="flaticon2-check-mark text-success checked-item checked-item-{{ $branch->branch_id }} hide"></i></span>
																		</span>
																	</li>
																	@endforeach
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body fixed-height">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>စာပို့သမား</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြီး</th>
														        <th>စာလက်ခံပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@if(!group_by_delivery(Auth::user()->city_id)->isEmpty())
															    	@foreach(group_by_delivery(Auth::user()->city_id) as $key => $delivery)
															      	<tr class="branch branch-{{ user($delivery->user_id)['branch_id'] }}">
																        <th scope="row"><a href="{{ url('collected-by/'.user($delivery->user_id)['id']) }}" class="text-danger"><i class="flaticon2-user"></i> {{ user($delivery->user_id)['name'] }} <span class="text-primary">({{ branch(user($delivery->user_id)['branch_id'])['name'] }})</span></a></th>
																        <td><span class="badge badge-pill badge-primary badge-sm">{{ delivery_man_collected($delivery->user_id)['total'] }}</span></td>
																        <td><span class="badge badge-pill badge-danger badge-sm">{{ delivery_man_collected($delivery->user_id)['collected'] }}</span></td>
																        <td><span class="badge badge-pill badge-warning badge-sm">{{ delivery_man_collected($delivery->user_id)['handover'] }}</span></td>
																        <td><span class="badge badge-pill badge-success badge-sm">{{ delivery_man_collected($delivery->user_id)['received'] }}</span> {!! (delivery_man_collected($delivery->user_id)['total'] == delivery_man_collected($delivery->user_id)['received']? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															      	</tr>
															      	@endforeach
															      	<input type="text" id="check-tb-height" value="{{ $key }}">
														    	@endif
														    </tbody>
													  	</table>

													</div>
												</div>
										    </div>
										    <div class="kt-portlet__foot kt-portlet__foot--md">
												<div class="">
													<a href="{{ url('to-receive-lists/outbounds') }}" class="btn btn-danger btn-bold">စာထွက်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
													<a href="{{ url('filtered-outbound/by-offices') }}" class="btn btn-warning">ရုံးအလိုက်ကြည့်ရန်</a>
												</div>
											</div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာထွက်လုပ်ထားသော မြို့များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">

														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Export Tools</span>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-print"></i>
																			<span class="kt-nav__link-text">Print</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-copy"></i>
																			<span class="kt-nav__link-text">Copy</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-excel-o"></i>
																			<span class="kt-nav__link-text">Excel</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-text-o"></i>
																			<span class="kt-nav__link-text">CSV</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link">
																			<i class="kt-nav__link-icon la la-file-pdf-o"></i>
																			<span class="kt-nav__link-text">PDF</span>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>မြို့သို့ (စာထွက်)</th>
														        <th>စုစုပေါင်း စာထွက်</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@foreach(group_by_to_city(Auth::user()->city_id) as $city)
														      	<tr>
														        	<th scope="row"><a href="{{ url('branch-out-by-city/'.$city->destination) }}" class="text-danger">{{ city($city->destination)['shortcode'].' <'.city($city->destination)['mm_name'].'>' }}</a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ to_city_status_count($city->destination)['total'] }}</span></td>
														      	</tr>
														      	@endforeach
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">မိမိမြို့သို့ မြို့အလိုက် စာဝင်စာရင်များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="city-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>

										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr>
														        <th>စာဝင်လာသည့်မြို့ </th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြောင်းပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    @foreach(group_by_from_city(Auth::user()->city_id) as $city)
														    	<tr>
														        	<th scope="row"><a href="{{ url('inbound-from-city/'.$city->origin) }}" class="text-danger">{{ city($city->origin)['shortcode'] }} <span class="text-primary"><{{ city($city->origin)['mm_name'] }}></span></a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ from_city_status_count($city->origin)['total'] }}</span></td>
														        	<td><span class="badge badge-pill badge-danger badge-sm">{{ from_city_status_count($city->origin)['branch_in'] }}</span></td>
														        	<td><span class="badge badge-pill badge-warning badge-sm">{{ from_city_status_count($city->origin)['handover'] }}</span> {!! (from_city_status_count(102)['branch_in'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
														      	</tr>
														    @endforeach	
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">ရုံးအလိုက် လွှဲပြောင်းထားသော စာရင်းများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="city-transit">
														<span></span>
														<strong class="switch-label">Transit</strong>
														</label>
													</span>
										        </div>
										    </div>
<?php 
	$receivers = App\Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
				->join('action_logs','action_logs.waybill_id','=','branch_action_logs.waybill_id')
                ->select('action_logs.action_by')
                ->where('waybills.created_at','like',$date.'%')
                ->where('branch_action_logs.to_branch',2)
                ->groupBy('action_logs.action_by')
                ->get();

               foreach ($receivers as $key => $receiver) {
               		//echo $receiver->action_by;
               }
?>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr class="text-warning bg-whitesmoke">
														        <th>ရုံးသို့စာလွှဲ</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလက်ခံကျန်</th>
														        <th>စာလက်ခံပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@foreach(inbound_handover_branches(Auth::user()->city_id) as $branch)
														    	<tr>
														    		<th scope="row"><a href="{{ url('handover-by-branch/'.$branch->to_branch) }}">{{ branch($branch->to_branch)['name'] }}</a></th>
														    		<td><span class="badge badge-pill badge-primary badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['total'] }}</span></td>
														    		<td><span class="badge badge-pill badge-warning badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['remain'] }}</span></td>
														    		<td><span class="badge badge-pill badge-success badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['received'] }}</span> {!! (inbound_handover_count_by_branch($branch->to_branch)['remain'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
														    	</tr>
														    	@endforeach
														    </tbody>
														    <tbody class="hide">
														    	@foreach(transit_handover_branches(Auth::user()->city_id) as $branch)
														    	<tr>
														    		<th scope="row"><a href="{{ url('handover-by-branch/'.$branch->to_branch) }}">{{ branch($branch->to_branch)['name'] }}</a></th>
														    		<td></td>
														    		<td></td>
														    		<td></td>
														    	</tr>
														    	@endforeach
														    </tbody>
													  	</table>
													  	<a href="{{ url('to-receive-lists/inbound') }}" class="btn btn-danger btn-bold">စာဝင်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->

	<!-- begin:: Topbar Offcanvas Panels -->
	
	
			<!-- begin::Offcanvas Toolbar Quick Actions -->
			<div id="kt_offcanvas_toolbar_quick_actions" class="kt-offcanvas-panel">
				<div class="kt-offcanvas-panel__head">
					<h3 class="kt-offcanvas-panel__title">
						Quick Actions
					</h3>
					<a href="#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_quick_actions_close"><i class="flaticon2-delete"></i></a>
				</div>
				<div class="kt-offcanvas-panel__body">
					<div class="kt-grid-nav-v2">
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-box"></i></div>
							<div class="kt-grid-nav-v2__item-title">Orders</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon-download-1"></i></div>
							<div class="kt-grid-nav-v2__item-title">Uploades</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-supermarket"></i></div>
							<div class="kt-grid-nav-v2__item-title">Products</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-avatar"></i></div>
							<div class="kt-grid-nav-v2__item-title">Customers</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-list"></i></div>
							<div class="kt-grid-nav-v2__item-title">Blog Posts</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-settings"></i></div>
							<div class="kt-grid-nav-v2__item-title">Settings</div>				
						</a>			
					</div>
				</div>
			</div>
			<!-- end::Offcanvas Toolbar Quick Actions -->	
		<!-- end:: Topbar Offcanvas Panels -->

        <!-- begin:: Quick Panel -->
		<div id="kt_quick_panel" class="kt-offcanvas-panel">
			<div class="kt-offcanvas-panel__nav">
				<ul class="nav nav-pills" role="tablist">
			        <li class="nav-item active">
			            <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
			        </li>
			        <li class="nav-item">
			            <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_actions" role="tab">Actions</a>
			        </li>
			        <li class="nav-item">
			            <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
			        </li>
			    </ul>

			    <button class="kt-offcanvas-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></button>
			</div>

			<div class="kt-offcanvas-panel__body">
			    <div class="tab-content">
			        <div class="tab-pane fade show kt-offcanvas-panel__content kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
		 				<!--Begin::Timeline -->
		                <div class="kt-timeline">
		                    <!--Begin::Item -->                      
		                    <div class="kt-timeline__item kt-timeline__item--success">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-feed kt-font-success"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">02:30 PM</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                             
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            HTML,CSS,VueJS                                                                                            
		                        </div>
		                    </div>
		                    <!--End::Item -->  

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--danger">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-safe-shield-protection kt-font-danger"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">01:20 AM</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            Security, Fieewall                                                                                         
		                        </div>
		                    </div>  
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--brand">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon2-box kt-font-brand"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">Yestardey</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard                                                                                            
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            PSD, Sketch, AJ                                                                                       
		                        </div>
		                    </div>  
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--warning">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-pie-chart-1 kt-font-warning"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">Aug 13,2018</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br> England, BA12FJ                                                                                           
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            Meeting, Customer                                                                                          
		                        </div>
		                    </div> 
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--info">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-notepad kt-font-info"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">May 09, 2018</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                                
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            HTML,CSS,VueJS                                                                                            
		                        </div>
		                    </div> 
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--accent">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon"                                        >
		                                    <i class="flaticon-bell kt-font-success"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">01:20 AM</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            Security, Fieewall                                                                                         
		                        </div>
		                    </div>   
		                    <!--End::Item -->                    

		                </div> 
		                <!--End::Timeline --> 
			        </div>
			        <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_actions" role="tabpanel">
			        	<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-success">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
									<h3 class="kt-portlet__head-title">Recent Bills</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->


						<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-info">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Latest Invoices</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->

						<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-warning">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">New Comments</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->

						<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-brand">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Recent Posts</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->
			        </div>
			        <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
			        	<form class="kt-form">
							<div class="kt-heading kt-heading--space-sm">Notifications</div>

							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable notifications:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_1">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable audit log:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
											<input type="checkbox"  name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Notify on new orders:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							
							<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

							<div class="kt-heading kt-heading--space-sm">Orders</div>

							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable order tracking:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable orders reports:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox"  name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Allow order status auto update:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_4">
											<span></span>
										</label>
									</span>
								</div>
							</div>

							<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

							<div class="kt-heading kt-heading--space-sm">Customers</div>

							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable customer singup:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable customers reporting:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
											<input type="checkbox"  name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Notifiy on new customer registration:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_6">
											<span></span>
										</label>
									</span>
								</div>
							</div>

						</form>
			        </div>
			    </div>
			</div>
		</div>
		<!-- end:: Quick Panel -->

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
        

        
    

        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
    "colors": {
        "state": {
            "brand": "#385aeb",
            "metal": "#c4c5d6",
            "light": "#ffffff",
            "accent": "#00c5dc",
            "primary": "#5867dd",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995",
            "focus": "#9816f4"
        },
        "base": {
            "label": [
                "#c5cbe3",
                "#a1a8c3",
                "#3d4465",
                "#3e4466"
            ],
            "shape": [
                "#f0f3ff",
                "#d9dffa",
                "#afb4d4",
                "#646c9a"
            ]
        }
    }
};
        </script>
        <!-- end::Global Config -->

    <script src="{{ asset('assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

   	<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		var fixed_height = $("#check-tb-height").val();
   		if(fixed_height > 10){
   			 $(".fixed-height").addClass('add-scroll');
   		}
   		/*
   		$("#inbound-transit").change(function(){
		 	if($(this).prop("checked") == true){
		       //run code
		       $(".inbound-item").hide();
		       $(".inbound-transit").show();
		    }else{
		       $(".inbound-transit").hide();
		       $(".inbound-item").show();
		    }
   		});
   		*/

   		
   		$("#inbound-transit").change(function(){
		 	if($(this).prop("checked") == true){
		       //run code
		       $(".inbound-switch").hide();
		       $(".inbound-transit-switch").show();
		    }else{
		       $(".inbound-transit-switch").hide();
		       $(".inbound-switch").show();
		    }
   		});

   		$("#outbound-transit").change(function(){
		 	if($(this).prop("checked") == true){
		       //run code
		       $(".outbound-switch").hide();
		       $(".outbound-transit-switch").show();
		    }else{
		       $(".outbound-transit-switch").hide();
		       $(".outbound-switch").show();
		    }
   		});

   		$(".filtered-outbound-by-branch").click(function(){
   			$('.removed-outbound-filtered').removeClass('hide');
		 	var branch = $(this).attr('id');
		 	$('.branch').hide();
		 	$('.branch-'+branch).show();
		 	$('.checked-item').addClass('hide');
		 	$('.checked-item-'+branch).removeClass('hide');
   		});

   		$(".removed-outbound-filtered").click(function(){
   			$('.removed-outbound-filtered').addClass('hide');
   			$('.branch').show();
   		});

   	</script>
</body>
</html>