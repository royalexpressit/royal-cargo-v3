<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Royal Cargo | Transit Routes</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	


				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Transit - Transit Routes (စာပို့လမ်းကြောင်းစာရင်းများ)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        <div class="typeahead">
										<input class="form-control" id="kt_typeahead_1" type="text" dir="ltr" placeholder="Search Routes">
									</div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">							
								<!--begin::Portlet-->
								<div class="routes-listing">
									<div class="kt-portlet">
										<div class="list-group" id="fetched-data">
										</div>
									</div>
									<div class="btn-group" role="group" aria-label="Basic example">
										<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
										<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
										<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
									</div>
								</div>
								<!--end::Portlet-->

								<!--begin::Portlet-->
								<div class="search-listing">
									<div class="kt-portlet">
										<div class="list-group" id="search-data">
										</div>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->


	@include('operator.quick-panel')

    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 
	
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/typeahead.js') }}" type="text/javascript"></script>

    <script type="text/javascript">
    	$(document).ready(function(){
    		var url 		= $("#url").val();

			//declared first loaded json data
			var load_url = url+'/api/transit-routes';

			$.ajax({
			    url: load_url,
			    type: 'GET',
			    data: {},
			    success: function(data){
			        console.log(data);
			        $.each( data.data, function( key, value ) {
						$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
		    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><span class="badge badge-pill badge-danger badge-xs">'+value.origin_shortcode+'-'+value.destination_shortcode
		    			+ '</span> (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin_shortcode+' - <i class="flaticon-placeholder-3 text-primary"></i>'+value.transit_shortcode+' - <i class="flaticon-placeholder-3 text-success"></i>'+value.destination_shortcode+')</h5>'
		    			+ '<span>ကြာချိန် '+value.duration+'</span></div>'
		    			+ '<span>Origin:'+value.origin+' ,Destination:'+value.destination+' ,Transit:'+value.transit+'</span></a>');
					});

			        $("#to-records").text(data.to);
					$("#total-records").text(data.total);
						
					if(data.prev_page_url === null){
						$("#prev-btn").attr('disabled',true);
					}else{
						$("#prev-btn").attr('disabled',false);
					}
					if(data.next_page_url === null){
						$("#next-btn").attr('disabled',true);
					}else{
						$("#next-btn").attr('disabled',false);
					}
					$("#prev-btn").val(data.prev_page_url);
					$("#next-btn").val(data.next_page_url);
			    }
			});
		});

		$('.pagination-btn').click(function(){
			//clicked url json data
			var clicked_url = $(this).val();
			$(this).siblings().removeClass('active')
			$(this).addClass('active');
			$.ajax({
			    url: clicked_url,
			    type: 'GET',
			    data: {},
			    success: function(data){
			        console.log(data);
			        $("#fetched-data").empty();
			        $.each( data.data, function( key, value ) {
						$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
		    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><span class="badge badge-pill badge-danger badge-xs">'+value.origin_shortcode+'-'+value.destination_shortcode
		    			+ '</span> (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin_shortcode+' - <i class="flaticon-placeholder-3 text-primary"></i>'+value.transit_shortcode+' - <i class="flaticon-placeholder-3 text-success"></i>'+value.destination_shortcode+')</h5>'
		    			+ '<span>ကြာချိန် '+value.duration+'</span></div>'
		    			+ '<span>Origin:'+value.origin+' ,Destination:'+value.destination+' ,Transit:'+value.transit+'</span></a>');
					});
			        $("#to-records").text(data.to);
					if(data.prev_page_url === null){
						$("#prev-btn").attr('disabled',true);
					}else{
						$("#prev-btn").attr('disabled',false);
					}
					if(data.next_page_url === null){
						$("#next-btn").attr('disabled',true);
					}else{
						$("#next-btn").attr('disabled',false);
					}
					$("#prev-btn").val(data.prev_page_url);
					$("#next-btn").val(data.next_page_url);
			    }
			});
		});

		/*
		$("#kt_typeahead_1").on("keyup",function search(e) {
			check           = $("#kt_typeahead_1").val().length;
			console.log(check);
			if(check > 0){
				$('.routes-listing').hide();
			}else{
				$('.routes-listing').show();
			}
			
		});
		*/

		$('#kt_typeahead_1').on('typeahead:selected', function (e, datum) {
		    if(datum){
		    	$('.routes-listing').hide();
		    	$.ajax({
				    url: '{{ url("api/search-transit") }}',
				    type: 'POST',
				    data: {
				    	'term':datum
				    },
				    success: function(data){
				        console.log(data);
				        $("#search-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
			    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><span class="badge badge-pill badge-danger badge-xs">'+data.origin_shortcode+'-'+data.destination_shortcode
			    			+ '</span> (<i class="flaticon-placeholder-3 text-danger"></i>'+data.origin_shortcode+' - <i class="flaticon-placeholder-3 text-primary"></i>'+data.transit_shortcode+' - <i class="flaticon-placeholder-3 text-success"></i>'+data.destination_shortcode+')</h5>'
			    			+ '<span>ကြာချိန် '+data.duration+'</span></div>'
			    			+ '<span>Origin:'+data.origin+' ,Destination:'+data.destination+' ,Transit:'+data.transit+'</span></a>');
					}
				});
		    }else{
		    	$('.routes-listing').show();
		    }
		});
		
    </script>
</body>
</html>