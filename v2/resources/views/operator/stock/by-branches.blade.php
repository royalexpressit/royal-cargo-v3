<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Branch Out By Branch</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    	.coming{
    		margin:0;
    		padding:4px !important;
    	}
    	.coming-city-list{
    		background: #f5f5f5;
		    padding: 4px 10px;
		    border: 1px dashed #e6e6f5;
		    margin-bottom: 4px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		$total 		= 0;
		$handover 	= 0;
		$postponed 	= 0;
	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title"><strong class="badge badge-warning text-uppercase">Stock Statistics</strong> - Remain Waybills By Branches <span class="text-danger">(ရုံးအလိုက်စာဝေကျန်စာရင်းများ)</span></h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
            							<div class="kt-subheader__wrapper">
                
                            			</div>
        							</div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-9 col-xl-9 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">ရုံးအလိုက် စာဝေရန် ကျန်ရှိစာရင်းများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
										            	<span class="removed-outbound-filtered hide">
											            	<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md">
																<i class="flaticon-close"></i>
															</button>
														</span>
														<div class="dropdown dropdown-inline">
															
														</div>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body fixed-height">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmok ">
														        <th>မိမိမြို့ရှိရုံးမှ</th>
														        <th>စာဝေလက်ကျန်</th>
														        <th>လက်ခံရန်ကျန်</th>
														        <th>ဆိုင်းငံ့စာ</th>
														      </tr>
														    </thead>
														    <tbody>	
														    	@foreach(App\Branch::where('city_id',Auth::user()->city_id)->where('is_sorting',0)->get() as $branch)
														    	@if(!stock_by_branch($branch->id)['total'] == 0)
														    	<tr class="branch ">
																	<th scope="row"><a href="{{ url('stock/check-stock/'.$branch->id )}}" class="text-danger fs-16"><i class="flaticon-placeholder-3"></i> {{ $branch->name }}</a></th>
																	<td><span class="badge badge-pill badge-primary badge-sm handover-total">{{ stock_by_branch($branch->id)['total'] }}</span></td>
																	<td><span class="badge badge-pill badge-success badge-sm remain-total">{{ stock_by_branch($branch->id)['handover'] }}</span></td>
																	<td><span class="badge badge-pill badge-purple badge-sm postponed-total">{{ stock_by_branch($branch->id)['postponed'] }}</span></td>
															    </tr>
															    @endif
															    @endforeach
															    <tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm" id="handover-total">0</span></td>
																    <td class="border-bold"><span class="badge badge-success badge-sm" id="remain-total">0</span></td>
																    <td class="border-bold"><span class="badge badge-purple badge-sm" id="postponed-total">0</span></td>
															    </tr>
																<input type="hidden" id="check-tb-height" value="0">													    
															</tbody>
													  	</table>
													</div>

												</div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

								</div>
								<!--end::Row--> 

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->

	<!-- begin:: Topbar Offcanvas Panels -->
	


     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->
    
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		var handover = remain = postponed = 0;
   		$(".handover-total").each(function(){
	        handover += +$(this).text();
	    });
	    $(".remain-total").each(function(){
	        remain += +$(this).text();
	    });
	    $(".postponed-total").each(function(){
	        postponed += +$(this).text();
	    });
	    $("#handover-total").text(handover);
	    $("#remain-total").text(remain);
	    $("#postponed-total").text(postponed);
   	</script>
</body>
</html>