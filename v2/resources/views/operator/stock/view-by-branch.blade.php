<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Inbound</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.alert-box{
    		display: none;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
											<a href="{{ url('stock/by-branches') }}" class="badge badge-warning text-uppercase">Stock Statistics</a>  
											<strong class="badge badge-warning text-uppercase">{{ branch($branch_id)['name'] }}</strong>(စာဝေရန်ကျန်ရှိသော စာရင်းများ)
										</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        <div class="typeahead">
										<input class="form-control" id="kt_typeahead_1" type="text" dir="ltr" placeholder="Search Waybill">
									</div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-solid-warning alert-bold alert-box" role="alert">
		                            <div class="alert-text">စာရင်းများ မရှိသေးပါ</div>
		                        </div>
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="list-group" id="fetched-data">
									</div>
								</div>
								<div class="pagination">
									<div class="btn-group" role="group" aria-label="Basic example">
										<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
										<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
										<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="json" value="{{ $json }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->


	@include('operator.quick-panel')
	@include('operator.inbound.view-modal')

	<!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->

    @include('layouts.widget')   
	
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/O1a1b575a/stock-by-branch.js') }}" type="text/javascript"></script>
</body>
</html>