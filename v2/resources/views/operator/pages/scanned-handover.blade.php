<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z" crossorigin="anonymous">
    <title>[Inbound] Scanned Handover</title>
    <link rel="stylesheet" type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
  	<link rel="stylesheet" type="text/css" href="{{ url('_assets/css/custom.css') }}">
  	<style type="text/css">
  		body{
  			font-family: "Pyidaungsu";
  		}
  		.scroll{
  			height: 200px;
  			overflow-y: scroll;
  		}
  		.show-branch,
  		.add-branch,
  		.scanned-waybill,
  		.error{
  			display: none;
  		}
  		.fs-20{
  			font-size:20px;
  		}
  		.list-group-item{
  			padding: .5rem 1.25rem;
  		}
  		.mt-10{
  			margin-top:10px;
  		}
  		.card-body{
  			padding: .5rem 1rem;
  		}
  		.lbl-block{
  			display: inline-block;
    		padding: 4px;
  		}
  		.fs-14{
  			font-size:14px;
  		}
  		.hide{
  			display: none;
  		}
  		label{
  			font-size:20px;
  		}
  		#selected-branch, label{
  			text-shadow: 1px 1px 2px #b9b7b7;
    		color: #F44336;
  		}
  		.scanned-waybill{
  			margin-top:4px;
  		}
  	</style>
</head>
<body>
    <div class="container-fluid">
	  	<div class="row">
    		<div class="col-12 col-sm-12">
    			<div class="card">
				  	<div class="card-body">
				  		<div class="row scan-branch">
				    		<div class="col-12 col-sm-12">
				    			<label>Scan Branch QR Code <span class="error fs-14 text-danger">(Invalid Branch Code)</span></label>
				    			<input type="email" class="form-control" id="branch_qr" autofocus="">
				    		</div>
				    	</div>
				    	<div class="show-branch">
				    		<div class="row"> 
					    		<div class="col-8">
					    			<h3 class="text-center" id="selected-branch"></h3>
					    		</div>
					    		<div class="col-4">
					    			<button type="button" class="btn btn-primary add-branch">Change Branch</button>
					    		</div>
					    	</div>
					    	<div class="row"> 
					    		<div class="col-4">
					    			<div class="fs-20 text-primary">
					    				<span class="lbl-block">Scanned: </span>
					    				<span class="scanned-count">0</span>
					    			</div>
					    		</div>
					    		<div class="col-4">
					    			<div class="fs-20 text-success ">
					    				<span class="lbl-block">Success: </span>
					    				<span class="success-count">0</span>
					    			</div>
					    		</div>
					    		<div class="col-4">
					    			<div class="fs-20 text-danger">
					    				<span class="lbl-block">Failed: </span>
					    				<span class="failed-count">0</span>
					    			</div>
					    		</div>
					    	</div>
				    	</div>
				  	</div>
				</div>
				
				<div class="form-group scanned-waybill">
					<label for="scanned_waybill">Scanned For Handover</label>
					<input type="text" class="form-control" id="scanned_waybill" aria-describedby="emailHelp">
				</div>
				<ul class="list-group" id="scanned-lists">
				</ul>
    		</div>
    		<div class="col-12 col-sm-12 hide">
    			<input type="" id="url" value="{{ url('http://05c41792a58b.ngrok.io/') }}">
    			<input type="" id="from_branch" value="1">
    			<input type="" id="to_branch" value="1">
    			<input type="" id="user_id" value="12">
    			<input type="" id="city_id" value="102">
    			<input type="" id="_token" value="{{ csrf_token() }}">
    		</div>
    	</div>
	</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.js" integrity="sha256-QWo7LDvxbWT2tbbQ97B53yJnYU3WhH/C8ycbRAkjPDc=" crossorigin="anonymous"></script>
 	<script type="text/javascript">
 		$(document).ready(function(){
		    var url     = $("#url").val();
		    var _token  = $("#_token").val();
		    var scanned = 0;
		    var success = 0;
		    var failed  = 0;

		    $("#scanned_waybill").on("keydown",function search(e) {
		        if(e.keyCode == 13) {
		            var scanned_waybill   = $("#scanned_waybill").val().toUpperCase();
		            if(scanned_waybill.length > 0){
		                //valid length && continue
		                ++scanned;
		                $(".scanned-count").text(scanned);
		                $(".check-number").addClass('hide');

		                data_send();
		                 
		                /*             
		                if(scanned == 10){
		                    $("#scanned_waybill").attr("disabled", true);
		                    $('#exampleModal').modal({show:true});
		                    $(".continue-action").show();
		                    $(".continue-action-btn").show();
		                    setTimeout(function(){
		                    	$('#exampleModal').modal('hide');
		                    	$("#continue-action").trigger('focus');
		                    },5000);
		                }
		                */
		                if(scanned > 10){
		                	$("#scanned-lists").addClass('scroll');
		                }
		            }else{
		                //invalid length && try again
		                $(".check-number").removeClass('hide');
		                $("#scanned_waybill").val('');
		            }
		        }
		    });


		    var data_send = function(){
		        //prepare input for api request
		        waybill         = $('#scanned_waybill').val();
		        user_id         = $("#user_id").val();
		        from_branch     = $("#from_branch").val();
		        to_branch       = $("#to_branch").val();
		        city_id       	= $("#city_id").val();
		        
		        $.ajaxSetup({
		            headers: {
		               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		            }
		        });
		                
		        $.ajax({
		            type: 'post',
		            url: url+'/scanned-handover',
		            dataType:'json',
		            data: {
		               	'waybill'     :waybill,
		               	'user_id'     :user_id,
		               	'from_branch' :from_branch,
		               	'to_branch'   :to_branch,
		               	'city_id'     :city_id,
		               	'_token'      : _token,
		            },
		            success: function(data) { 
		               	if(data.success == 1){
		                    //added old and new count for tmp 
		                    ++success;
		               	}else{
		                    ++failed;
		                }
		                $(".success-count").text(success);
		                $(".failed-count").text(failed);
		                $("#scanned-lists").prepend(data.message);
		            },
		        });
		        

		        $('.scan-btn').hide();
		        $('.continue-action').hide();
		        $('#scanned_waybill').val('');
		        $('#scanned-waybill').trigger('focus');
		    }

		    $("#branch_qr").on("keydown",function search(e) {
		    	if(e.keyCode == 13) {
		    		branch = $("#branch_qr").val();

		    		if(
		    			branch == 'YGN-48' || 
		    			branch == 'YGN-NOKA' ||
		    			branch == 'YGN-SOKA' ||
		    			branch == 'YGN-LMD' ||
		    			branch == 'YGN-TKA' ||
		    			branch == 'YGN-TME' ||
		    			branch == 'YGN-HLG' ||
		    			branch == 'YGN-MYG' ||
		    			branch == 'YGN-SCG'
		    		){
		    			if(branch == 'YGN-NOKA' ){
		    				$("#to_branch").val(1);
			    		}else if(branch == 'YGN-48'){
			    			$("#to_branch").val(2);
			    		}else if(branch == 'YGN-SOKA'){
			    			$("#to_branch").val(10);
			    		}else if(branch == 'YGN-LMD'){
			    			$("#to_branch").val(6);
			    		}else if(branch == 'YGN-TKA'){
			    			$("#to_branch").val(7);
			    		}else if(branch == 'YGN-TME'){
			    			$("#to_branch").val(5);
			    		}else if(branch == 'YGN-HLG'){
			    			$("#to_branch").val(9);
			    		}else if(branch == 'YGN-MYG'){
			    			$("#to_branch").val(3);
			    		}else if(branch == 'YGN-SCG'){
			    			$("#to_branch").val(8);
			    		}else{
			    			$("#to_branch").val(0);
		    			}
		    			$('#selected-branch').text(branch);
			    		$('.add-branch').show();
			    		$('.show-branch').show();
			    		$('.scan-branch').hide();
			    		$('.error').hide();
			    		$('.scanned-waybill').show();
			    		$("#scanned_waybill").trigger('focus');
			    		$("#branch_qr").val('');
		    		
		    		}else{
		    			$("#branch_qr").val('');
		    			$("#branch_qr").trigger('focus');
		    			$('.error').show();
	    			}

	    			
		    	}
		    });
		    
		    $(".add-branch").on("click",function search(e) {
		    	scanned = 0;
		    	success = 0;
		    	failed  = 0;

		    	$(".scanned-count").text(success);
		    	$(".success-count").text(success);
		        $(".failed-count").text(failed);

		        $("#scanned-lists").empty();
		    	$('.add-branch').hide();
		    	$('.show-branch').hide();
		    	$('.scan-branch').show();
		    	$('.scanned-waybill').hide();
		    	$("#branch_qr").trigger('focus');
		    });
		    
		});
 	</script> 
</body>
</html>