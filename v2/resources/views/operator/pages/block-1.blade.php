<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.mb-0{
    		margin-bottom: 0px;
    	}
    	.kt-widget-12__title{
    		font-size: 16px !important;
    	}
    	.kt-portlet{
    		margin-bottom: 10px !important;
    	}
    	.kt-portlet .kt-portlet__body{
    		padding: 10px 16px;
    	}
    	.fs-20{
    		font-size: 20px;
    	}
    	.active-success{
    		background: #e8faf8;
    		border: 2px solid #33cab7;
    	}
    	.active-primary{
    		background: #eeeffc;
    		border: 2px solid #5867dd;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
								
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
										</h3>		
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	<div class="input-group date">
												<input type="text" class="form-control set-datetime" readonly  placeholder="Select date" id="kt_datepicker_2" value="{{ get_date() }}"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
												</div>
											</div>
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
							
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										@include('operator.pages.sidebar')
									</div>

									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->	
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"><strong>{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }}</strong> Inbound Status 
										            	<span class="pull-right">({{ inbound_dashboard_status()['total'] + transit_dashboard_status(Auth::user()->city_id)['inbound'] }})</span>
										            </h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="inbound-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>
										    <div class="kt-none">
										        <div class="kt-widget-1 inbound-switch">
										        	<a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-success">{{ inbound_dashboard_status()['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/branch-in') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Branch-In Waybills 
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['branch_in'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/handover') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Handover Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['handover'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/received') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Received Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['received'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/postponed') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Postponed/Rejected Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- စာဆိုင်းငံ့/ငြင်းပယ် အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['postponed'] + inbound_dashboard_status()['rejected'] + inbound_dashboard_status()['transferred'] + inbound_dashboard_status()['accepted'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 inbound-transit-switch">
										        	<a href="{{ url('transit/inbound') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Inbound Transit Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-danger">{{ transit_dashboard_status(Auth::user()->city_id)['inbound'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('transit/branch-in') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Transit Branch In Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာဝင်</span>- စာဝင်ထားဆဲအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['branch_in'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['inbound'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('transit/handover') }}"  class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Transit Handover Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာဝင်</span>- စာလွှဲပြောင်းပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['handover'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['inbound'] !!}</div>
										                </div>
										            </a>
										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] != 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
										            @endif
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->

										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"><strong>{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }}</strong> Outbound Status 
										            	<span class="pull-right">({{ outbound_dashboard_status(Auth::user()->city_id)['total'] + transit_dashboard_status(Auth::user()->city_id)['outbound'] }})</span>
										            </h3>									
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--primary">
														<label>
														<input type="checkbox" id="outbound-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>
										   
										    <div class="kt-none">
										        <div class="kt-widget-1 outbound-switch">
										        	<a href="{{ url('outbound/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Outbound Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-primary">{{ outbound_dashboard_status(Auth::user()->city_id)['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/collected') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Collected Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာကောက်ထားပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['collected'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/handover') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
											                    {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Handover Waybills
											                </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာလွှဲပြောင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['handover'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/received') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                    	{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာလက်ခံအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['received'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/branch-out') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Branch Out Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာထွက်ပြီးသောအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['branch_out'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 outbound-transit-switch">
										        	<a href="{{ url('transit/outbound') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Outbound <span class="text-danger">Transit</span> Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-primary">{{ transit_dashboard_status(Auth::user()->city_id)['outbound'] }}</div>
										                </div>
										            </a>
										        	<a href="{{ url('transit/received') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} <span class="text-danger">Transit</span> Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာထွက်</span>- လက်ခံပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['received'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['outbound'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('transit/branch-out') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} <span class="text-danger">Transit</span> Branch Out Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာထွက်</span>- စာထွက်ပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['branch_out'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['outbound'] !!}</div>
										                </div>
										            </a>

										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] != 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													
													<div class="alert alert-outline-warning mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text">
														  	နယ်စာထွက်ရန်အတွက် အရေအတွက် <strong>{{ transit_dashboard_status(Auth::user()->city_id)['balance'] }}</strong> စောင် လိုနေပါသည်။!
														  	<a href="#">စာရင်းကြည့်ရန်</a>
														  </div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@else
													<div class="alert alert-outline-success mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon2-check-mark"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် ကိုက်ညီမှုရှိသည်။</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@endif
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
								
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="hours" value="{{ date('H') }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Change Date For Dashboard</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Cargo Dashboard အား <span id="set_date" class="text-danger"></span> ရက်စွဲပြောင်းမည်။ </h5>
					<p>Dashboard တွင်မြင်ရသော waybills များသည် ၎င်းရက်စွဲ အတိုင်းမြင်ရမည်ဖြစ်သည်။</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-brand" data-dismiss="modal">ပိတ်မည်</button>
					<button type="button" class="btn btn-brand set-date" data-dismiss="modal">ပြောင်းမည်</button>
					<input type="hidden" id="config_date" value="">
				</div>
			</div>
		</div>
	</div>

	<!-- Password no secure alert -->
	<div class="modal fade" id="check-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white"><i class="fa fa-calendar"></i> မနေ့က မိမိရုံး စာဝင်/စာထွက် အခြေအနေစစ်ပါ</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="fs-16">မနေ့က မိမိရုံး စာဝင်(inbound)/စာထွက်(outbound) စာရင်းများကို စစ်ပေးပါ။
					စာလက်ခံကျန်စာရင်းများ စစ်ခြင်းဖြင့် စာပျောက်ခြင်းများအား လျော့ချနိုင်ပါသည်။</p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('daily-check-lists/outbound') }}" class="btn btn-brand">စာထွက်စာရင်းစစ်ရန်</a>
					<a href="{{ url('daily-check-lists/inbound') }}" class="btn btn-success">စာဝင်စာရင်းစစ်ရန်</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Password no secure alert -->

	<!-- Password no secure alert -->
	<div class="modal fade" id="no-secure-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white"><i class="flaticon-lock"></i> စကားဝှက်(password) လုံခြုံမှုဆိုင်ရာ အသိပေးခြင်း</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="fs-16">လူကြီးမင်း အသုံးပြုထားသော <strong class="text-danger">စကားဝှက်(password)</strong> မှာ လုံခြုံမှု မရှိကြောင်းတွေ့ရပါသည်။ တခြားသူမှ အလွယ်တကူ အသုံးမပြုနိုင်အောင် <strong class="text-danger">စကားဝှက်(password)</strong>အား အသစ်ပြောင်းလဲရန် အကြံပြုပါသည်။ </p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('my-profile') }}" class="btn btn-brand">စကားဝှက်ပြောင်းမည်</a>
					<button type="button" class="btn btn-danger" data-dismiss="modal">ပိတ်မည်</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Password no secure alert -->

    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$("#kt_datepicker_2").datepicker({
			format: 'yyyy-mm-dd'
		})
   	</script>
</body>
</html>