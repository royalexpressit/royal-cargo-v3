<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.mb-0{
    		margin-bottom: 0px;
    	}
    	.kt-widget-12__title{
    		font-size: 16px !important;
    	}
    	.kt-portlet{
    		margin-bottom: 10px !important;
    	}
    	.kt-portlet .kt-portlet__body{
    		padding: 10px 16px;
    	}
    	.fs-20{
    		font-size: 20px;
    	}
    	.active-success{
    		background: #e8faf8;
    		border: 2px solid #33cab7;
    	}
    	.active-primary{
    		background: #eeeffc;
    		border: 2px solid #5867dd;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		//total(to_city)
		$tc_total 		= 0;

		//total(to_transit_city)
		$ttc_total 		= 0;
	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
								
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
										</h3>		
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	<div class="input-group date">
												<input type="text" class="form-control set-datetime" readonly  placeholder="Select date" id="kt_datepicker_2" value="{{ get_date() }}"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
												</div>
											</div>
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
							
								
								
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										@include('operator.pages.sidebar')
									</div>

									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာထွက်ထားသော မြို့များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">

														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Filtered</span>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link filtered-outbound-city">
																			<i class="kt-nav__link-icon fa fa-home"></i>
																			<span class="kt-nav__link-text">မိမိမြို့မှထွက်</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link filtered-outbound-city">
																			<i class="kt-nav__link-icon fa fa-share"></i>
																			<span class="kt-nav__link-text">နယ်စာဝင်/ထွက်</span>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    <div class="kt-portlet__body ">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-primary">
														        <th>မြို့သို့ (စာထွက်)</th>
														        <th>စုစုပေါင်း စာထွက်</th>
														      </tr>
														    </thead>
														    <tbody>
														    @if(!group_by_to_city(Auth::user()->city_id)->isEmpty())
														    	@foreach(group_by_to_city(Auth::user()->city_id) as $city)
														    	@php
														    		$tc_total += to_city_status_count($city->destination)['total'];
														    	@endphp
														      	<tr>
														        	<th scope="row"><a href="{{ url('branch-out-by-city/'.$city->destination) }}" class="text-danger">{{ city($city->destination)['shortcode'].' <'.city($city->destination)['mm_name'].'>' }}</a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ to_city_status_count($city->destination)['total'] }}</span></td>
														      	</tr>
														      	@endforeach
														      	<tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><a href="{{ url('outbound/branch-out') }}" class="badge badge-primary badge-sm">{{ $tc_total }}</a></td>
															    </tr>
														    @else
														    	<tr>
														    		<td colspan="5">
														    			<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                        <div class="alert-text">မြို့အလိုက်စာထွက် စာရင်းများ မရှိသေးပါ</div>
													                    </div>
														    		</td>
														    	</tr>
														    @endif
														    </tbody>
													  	</table>
													</div>
													<div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-danger">
														        <th>မြို့သို့ (နယ်စာထွက်)</th>
														        <th>စုစုပေါင်း စာထွက်</th>
														      </tr>
														    </thead>
														    <tbody>
														    @if(!group_by_to_transit_city(Auth::user()->city_id)->isEmpty())
														    	@foreach(group_by_to_transit_city(Auth::user()->city_id) as $city)
														    	@php
														    		$ttc_total += to_transit_city_status_count($city->destination)['total'];
														    	@endphp
														      	<tr>
														        	<th scope="row"><a href="{{ url('transit-out-by-city/'.$city->destination) }}" class="text-danger">{{ city($city->destination)['shortcode'].' <'.city($city->destination)['mm_name'].'>' }}</a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ to_transit_city_status_count($city->destination)['total'] }}</span></td>
														      	</tr>
														      	@endforeach
														      	<tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $ttc_total }}</span></td>
															    </tr>
														    @else
														    	<tr>
														    		<td colspan="5">
														    			<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                        <div class="alert-text">မြို့အလိုက်စာထွက် စာရင်းများ မရှိသေးပါ</div>
													                    </div>
														    		</td>
														    	</tr>
														    @endif
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										    @if(!group_by_to_city(Auth::user()->city_id)->isEmpty() || !group_by_to_transit_city(Auth::user()->city_id)->isEmpty())
											<div class="kt-portlet__foot kt-portlet__foot--md">
											    <div class="">
													<a href="{{ url('/branch-out-summary') }}" class="btn btn-primary btn-bold">ရုံးအလိုက် စာထွက်ကြည့်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="hours" value="{{ date('H') }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Change Date For Dashboard</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Cargo Dashboard အား <span id="set_date" class="text-danger"></span> ရက်စွဲပြောင်းမည်။ </h5>
					<p>Dashboard တွင်မြင်ရသော waybills များသည် ၎င်းရက်စွဲ အတိုင်းမြင်ရမည်ဖြစ်သည်။</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-brand" data-dismiss="modal">ပိတ်မည်</button>
					<button type="button" class="btn btn-brand set-date" data-dismiss="modal">ပြောင်းမည်</button>
					<input type="hidden" id="config_date" value="">
				</div>
			</div>
		</div>
	</div>

    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$("#kt_datepicker_2").datepicker({
			format: 'yyyy-mm-dd'
		})
   	</script>
</body>
</html>