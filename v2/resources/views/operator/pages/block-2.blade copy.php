<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.mb-0{
    		margin-bottom: 0px;
    	}
    	.kt-widget-12__title{
    		font-size: 16px !important;
    	}
    	.kt-portlet{
    		margin-bottom: 10px !important;
    	}
    	.kt-portlet .kt-portlet__body{
    		padding: 10px 16px;
    	}
    	.fs-20{
    		font-size: 20px;
    	}
    	.active-success{
    		background: #e8faf8;
    		border: 2px solid #33cab7;
    	}
    	.active-primary{
    		background: #eeeffc;
    		border: 2px solid #5867dd;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		//total(delivery)
		$total 		= 0;
		$collected 	= 0;
		$handover 	= 0;
		$received 	= 0;
	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
								
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
										</h3>		
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	<div class="input-group date">
												<input type="text" class="form-control set-datetime" readonly  placeholder="Select date" id="kt_datepicker_2" value="{{ get_date() }}"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
												</div>
											</div>
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
							
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										@include('operator.pages.sidebar')
									</div>

									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာပို့သမား (သို့) ကောင်တာမှ ကောက်ထားသောစာများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
										            	<span class="removed-outbound-filtered hide">
											            	<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" >
																<i class="flaticon-close"></i>
															</button>
														</span>
														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Filtered By Branch </span>
																	</li>
																	@if(!collected_outbound_by_branches()->isEmpty())
																		@foreach(collected_outbound_by_branches() as $branch)
																		<li class="kt-nav__item">
																			<span class="kt-nav__link filtered-outbound-by-branch" id="{{ $branch->branch_id }}">
																				<i class="kt-nav__link-icon flaticon-home-2"></i>
																				<span class="kt-nav__link-text" >{{ branch($branch->branch_id)['name'] }} <i class="flaticon2-check-mark text-success checked-item checked-item-{{ $branch->branch_id }} hide"></i></span>
																			</span>
																		</li>
																		@endforeach
																	@else
																		<li class="kt-nav__item">
																			<span class="kt-nav__link">
																				<i class="kt-nav__link-icon flaticon-close"></i>
																				<span class="kt-nav__link-text" >မရှိသေးပါ</span>
																			</span>
																		</li>
																	@endif
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body fixed-height">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-primary">
														        <th>စာပို့သမား</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြီး</th>
														        <th>စာလက်ခံပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@if(!group_by_delivery(Auth::user()->city_id)->isEmpty())
															    	@foreach(group_by_delivery(Auth::user()->city_id) as $key => $delivery)
															      	@php 
															      		$total 		+= delivery_man_collected($delivery->user_id)['total'];
															      		$collected 	+= delivery_man_collected($delivery->user_id)['collected'];
															      		$handover 	+= delivery_man_collected($delivery->user_id)['handover'];
															      		$received 	+= delivery_man_collected($delivery->user_id)['received'];
															      	@endphp
															      	<tr class="branch branch-{{ user($delivery->user_id)['branch_id'] }}">
																        <th scope="row"><a href="{{ url('delivery-by/'.user($delivery->user_id)['id']) }}" class="text-danger"><i class="flaticon2-user"></i> {{ user($delivery->user_id)['name'] }} <span class="text-primary">({{ branch(user($delivery->user_id)['branch_id'])['name'] }})</span></a></th>
																        <td><span class="badge badge-pill badge-primary badge-sm total-{{ user($delivery->user_id)['branch_id'] }}">{{ delivery_man_collected($delivery->user_id)['total'] }}</span></td>
																        <td><span class="badge badge-pill badge-danger badge-sm {{ delivery_man_collected($delivery->user_id)['collected'] == 0? 'finished':'pulse' }} collected-{{ user($delivery->user_id)['branch_id'] }}" >{{ delivery_man_collected($delivery->user_id)['collected'] }}</span></td>
																        <td><span class="badge badge-pill badge-warning badge-sm {{ delivery_man_collected($delivery->user_id)['handover'] == 0? 'finished':'pulse' }} handover-{{ user($delivery->user_id)['branch_id'] }}">{{ delivery_man_collected($delivery->user_id)['handover'] }}</span></td>
																        <td><span class="badge badge-pill badge-success badge-sm received-{{ user($delivery->user_id)['branch_id'] }}">{{ delivery_man_collected($delivery->user_id)['received'] }}</span> {!! (delivery_man_collected($delivery->user_id)['total'] == delivery_man_collected($delivery->user_id)['received']? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															      	</tr>
															      	@endforeach
															      	<tr class="bg-whitesmoke filtered-hide-1">
																        <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																        <td class="border-bold"><a href="{{ url('outbound/all') }}" class="badge badge-primary badge-sm">{{ $total }}</a> <span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="မိမိမြို့မှစာထွက် - စုစုပေါင်းအရေအတွက် နှင့်တူသည်။"><i class="fa fa-info-circle text-info"></i></span></td>
																        <td class="border-bold"><a href="{{ url('outbound/handover') }}" class="badge badge-danger badge-sm">{{ $collected }}</a></td>
																        <td class="border-bold"><a href="{{ url('outbound/handover') }}" class="badge badge-warning badge-sm">{{ $handover }}</a></td>
																        <td class="border-bold"><a href="{{ url('outbound/finished-received') }}" class="badge badge-success badge-sm">{{ $received }}</a></td>
															      	</tr>
															      	<tr class="bg-whitesmoke filtered-show-1 hide">
																        <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																        <td class="border-bold"><span class="badge badge-primary badge-sm" id="filtered-total">0</span> </td>
																        <td class="border-bold"><span class="badge badge-danger badge-sm" id="filtered-collected">0</span></td>
																        <td class="border-bold"><span class="badge badge-warning badge-sm" id="filtered-handover">0</span></td>
																        <td class="border-bold"><span class="badge badge-success badge-sm" id="filtered-received">0</span></td>
															      	</tr>
															      	<input type="hidden" id="check-tb-height" value="{{ $key }}">
														    	@else
														    		<tr>
														    			<td colspan="5">
														    				<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                            <div class="alert-text">စာကောက် စာရင်းများ မရှိသေးပါ</div>
													                        </div>
														    			</td>
														    		</tr>
														    	@endif
														    </tbody>
													  	</table>
													</div>
												</div>
										    </div>
										    @if(!group_by_delivery(Auth::user()->city_id)->isEmpty())
										    <div class="kt-portlet__foot kt-portlet__foot--md">
										    	<div class="">
													<a href="{{ url('to-receive-lists/outbound') }}" class="btn btn-primary btn-bold">စာထွက်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
													<a href="{{ url('filtered-outbound/by-branches') }}" class="btn btn-primary">ရုံးအလိုက်စာကောက်စာရင်းကြည့်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
								
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="hours" value="{{ date('H') }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Change Date For Dashboard</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Cargo Dashboard အား <span id="set_date" class="text-danger"></span> ရက်စွဲပြောင်းမည်။ </h5>
					<p>Dashboard တွင်မြင်ရသော waybills များသည် ၎င်းရက်စွဲ အတိုင်းမြင်ရမည်ဖြစ်သည်။</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-brand" data-dismiss="modal">ပိတ်မည်</button>
					<button type="button" class="btn btn-brand set-date" data-dismiss="modal">ပြောင်းမည်</button>
					<input type="hidden" id="config_date" value="">
				</div>
			</div>
		</div>
	</div>

	<!-- Password no secure alert -->
	<div class="modal fade" id="check-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white"><i class="fa fa-calendar"></i> မနေ့က မိမိရုံး စာဝင်/စာထွက် အခြေအနေစစ်ပါ</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="fs-16">မနေ့က မိမိရုံး စာဝင်(inbound)/စာထွက်(outbound) စာရင်းများကို စစ်ပေးပါ။
					စာလက်ခံကျန်စာရင်းများ စစ်ခြင်းဖြင့် စာပျောက်ခြင်းများအား လျော့ချနိုင်ပါသည်။</p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('daily-check-lists/outbound') }}" class="btn btn-brand">စာထွက်စာရင်းစစ်ရန်</a>
					<a href="{{ url('daily-check-lists/inbound') }}" class="btn btn-success">စာဝင်စာရင်းစစ်ရန်</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Password no secure alert -->

	<!-- Password no secure alert -->
	<div class="modal fade" id="no-secure-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white"><i class="flaticon-lock"></i> စကားဝှက်(password) လုံခြုံမှုဆိုင်ရာ အသိပေးခြင်း</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="fs-16">လူကြီးမင်း အသုံးပြုထားသော <strong class="text-danger">စကားဝှက်(password)</strong> မှာ လုံခြုံမှု မရှိကြောင်းတွေ့ရပါသည်။ တခြားသူမှ အလွယ်တကူ အသုံးမပြုနိုင်အောင် <strong class="text-danger">စကားဝှက်(password)</strong>အား အသစ်ပြောင်းလဲရန် အကြံပြုပါသည်။ </p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('my-profile') }}" class="btn btn-brand">စကားဝှက်ပြောင်းမည်</a>
					<button type="button" class="btn btn-danger" data-dismiss="modal">ပိတ်မည်</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Password no secure alert -->

    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$("#kt_datepicker_2").datepicker({
			format: 'yyyy-mm-dd'
		})
   	</script>
</body>
</html>