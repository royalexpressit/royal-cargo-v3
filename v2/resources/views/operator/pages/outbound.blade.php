<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
	<style type="text/css">
		.count{
			font-size: 24px;
			float:right;
		}
		li span{
			font-size: 20px;
		}
		ul li{
			width: 100%;
    		display: inline-block;
    		border-bottom: 1px dashed #ccc7c7;
    		padding-top: 4px;
		}
		.card .card-content{
			padding: 6px 16px;
		}
		h5{
			margin: 4px 0px 0px 0px;
		}
		.btn-wide{
			width:100%;
		}
		.nav{
			position: fixed;
			top:0;
		}
		.fixme { 
			background: green; 
			color: white; 
			width: 100%; 
			z-index: 999;
		}
		.mt-120{
			margin-top: 130px;
		}
		.pull-right{
			display: inline-block;
			float: right;
		}
	</style>
</head>
<body>
	<div>
		<div class="fixme">
			<nav>
			  	<div class="nav-wrapper">
			    	Date: {{ date('Y-m-d H:i') }} (Next updated - <span class="countdown">0:00</span>)
			  		<span class="pull-right"><i class="material-icons" onclick="toggleFullScreen()">airplay</i></span>
			  		<a href="" >
			      		<span class="pull-right"><i class="material-icons">autorenew</i></span>
			      	</a>
			  	</div>
			</nav>
			<nav class="blue-grey">
			    <div class="row">
			      	<div class="col s6">
			        	<a class="btn waves-effect waves-light green lighten-2 btn-wide" href="{{ url('board/inbound') }}">Inbound (စာဝင်)</a>
			      	</div>
			      	<div class="col s6">
			        	<a class="btn waves-effect waves-light blue lighten-2 btn-wide" href="{{ url('board/outbound') }}">Outbound (စာထွက်)</a>
			      	</div>
			    </div>
		  	</nav>
		</div>
		<div class="row mt-120">
			@foreach(App\Branch::where('city_id',102)->where('is_sorting',0)->where('is_transit_area',0)->get() as $branch)
		    <div class="col s6 m6">
		      	<div class="card">
			        <div class="card-content">
			        	<h5 class="blue-text">
			        		{{ $branch->name }}
			        	</h5>
			        	<ul>
				          	<li><span>စာကောက်စုစုပေါင်း</span> <span class="count blue-text">{{ total_collected($branch->id) }}</span></li>
				          	<li><span>စာလွှဲရန်ကျန်</span> <span class="count red-text">{{ total_collected($branch->id)-total_outbound_handover($branch->id) }}</span></li>
				          	<li><span>စာလွှဲပြောင်းပြီး</span> <span class="count orange-text">{{ total_outbound_handover($branch->id) }}</span></li>
				          	<li><span>စာလွှဲလက်ခံပြီး</span> <span class="count green-text">{{ total_outbound_received($branch->id) }}</span><span class="green-danger count">({{ total_outbound_handover($branch->id) - total_outbound_received($branch->id) }})</span> </li>
				        </ul>
			        </div>
		      	</div>
		    </div>
		    @endforeach
	  	</div>
	</div>

	<!-- Compiled and minified JavaScript -->
	<script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/js/materialize.min.js"></script>
    <script type="text/javascript">
    	var fixmeTop = $('.fixme').offset().top;
		$(window).scroll(function() {
		    var currentScroll = $(window).scrollTop();
		    if (currentScroll >= fixmeTop) {
		        $('.fixme').css({
		            position: 'fixed',
		            top: '0',
		            left: '0'
		        });
		    } else {
		        $('.fixme').css({
		            position: 'static'
		        });
		    }
		});

		var timer2 = "9:59";
		var interval = setInterval(function() {


	  		var timer = timer2.split(':');
		  	//by parsing integer, I avoid all extra string processing
		  	var minutes = parseInt(timer[0], 10);
		  	var seconds = parseInt(timer[1], 10);
		  	--seconds;
		  	minutes = (seconds < 0) ? --minutes : minutes;
		  	seconds = (seconds < 0) ? 59 : seconds;
		  	seconds = (seconds < 10) ? '0' + seconds : seconds;
		  	//minutes = (minutes < 10) ?  minutes : minutes;
		  	$('.countdown').html(minutes + ':' + seconds);
		  	if (minutes < 0) clearInterval(interval);
		  	//check if both minutes and seconds are 0
		  	if ((seconds <= 0) && (minutes <= 0)) clearInterval(interval);
		  	timer2 = minutes + ':' + seconds;
		},1000);

    	setInterval(function() {
            window.location.reload();
        }, 600000);
    </script>
</body>
</html>