<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.mb-0{
    		margin-bottom: 0px;
    	}
    	.kt-widget-12__title{
    		font-size: 16px !important;
    	}
    	.kt-portlet{
    		margin-bottom: 10px !important;
    	}
    	.kt-portlet .kt-portlet__body{
    		padding: 10px 16px;
    	}
    	.fs-20{
    		font-size: 20px;
    	}
    	.active-success{
    		background: #e8faf8;
    		border: 1px solid #33cab7;
    	}
    	.active-primary{
    		background: #eeeffc;
    		border: 1px solid #5867dd;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		//total(handover_branch)
		$hb_total 		= 0;
		$hb_remain 		= 0;
		$hb_received 	= 0;

		//total(handover_transit)
		$ht_total 		= 0;
		$ht_remain 		= 0;
		$ht_received 	= 0;

	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
								
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
										</h3>		
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	<div class="input-group date">
												<input type="text" class="form-control set-datetime" readonly  placeholder="Select date" id="kt_datepicker_2" value="{{ get_date() }}"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
												</div>
											</div>
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										@include('operator.pages.sidebar')
									</div>

									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">ရုံးအလိုက် လွှဲပြောင်းထားသော စာရင်းများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="handover-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>

										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered handover-handover-switch">
														    <thead>
														      	<tr class="bg-whitesmoke text-success">
														        	<th>ရုံးသို့စာလွှဲ</th>
														        	<th>စုစုပေါင်း</th>
														        	<th>စာလက်ခံကျန်</th>
														        	<th>စာလက်ခံပြီး</th>
														      	</tr>
														    </thead>
														    <tbody>
														   	@if(!inbound_handover_branches(Auth::user()->city_id)->isEmpty())
															   	@foreach(inbound_handover_branches(Auth::user()->city_id) as $branch)
															    @php 
															    	$hb_total 		+= inbound_handover_count_by_branch($branch->to_branch)['total'];
																	$hb_remain 		+= inbound_handover_count_by_branch($branch->to_branch)['remain'];
																	$hb_received 	+= inbound_handover_count_by_branch($branch->to_branch)['received'];
														    	@endphp
															    <tr class="{{ ($branch->to_branch==Auth::user()->branch_id? 'highlight':'') }}">
															    	<th scope="row"><a href="{{ url('handover-by-branch/'.$branch->to_branch) }}" class="fs-16 text-primary">{{ branch($branch->to_branch)['name'] }}</a></th>
															    	<td><span class="badge badge-pill badge-primary badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['total'] }}</span></td>
															    	<td><span class="badge badge-pill badge-warning badge-sm {{ inbound_handover_count_by_branch($branch->to_branch)['remain'] == 0? 'finished':'pulse' }}">{{ inbound_handover_count_by_branch($branch->to_branch)['remain'] }}</span></td>
															    	<td><span class="badge badge-pill badge-success badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['received'] }}</span> {!! (inbound_handover_count_by_branch($branch->to_branch)['remain'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															    </tr>
															    @endforeach
															    <tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $hb_total }}</span></td>
																    <td class="border-bold"><span class="badge badge-warning badge-sm">{{ $hb_remain }}</span></td>
																    <td class="border-bold"><span class="badge badge-success badge-sm">{{ $hb_received }}</span></td>
															    </tr>
															@else
															    <tr>
															    	<td colspan="5">
															    		<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
														                    <div class="alert-text">စာလွှဲထားသောရုံး စာရင်းများ မရှိသေးပါ</div>
														                </div>
															    	</td>
															    </tr>
															@endif
														    </tbody>								    
													  	</table>
													  	<table class="table table-bordered handover-transit-switch">
														    <thead>
														      	<tr class="bg-whitesmoke text-danger">
														        	<th>နယ်စာရုံးသို့စာလွှဲ</th>
														        	<th>စုစုပေါင်း</th>
														        	<th>စာလက်ခံကျန်</th>
														        	<th>စာလက်ခံပြီး</th>
														      	</tr>
														    </thead>
														    <tbody>
														   	@if(!inbound_handover_transit(Auth::user()->city_id)->isEmpty())
															   	@foreach(inbound_handover_transit(Auth::user()->city_id) as $branch)
															    @php 
															    	$ht_total 		+= inbound_transit_count_by_branch($branch->to_branch)['total'];
																	$ht_remain 		+= inbound_transit_count_by_branch($branch->to_branch)['remain'];
																	$ht_received 	+= inbound_transit_count_by_branch($branch->to_branch)['received'];
														    	@endphp
															    <tr class="{{ ($branch->to_branch==Auth::user()->branch_id? 'highlight':'') }}">
															    	<th scope="row"><a href="{{ url('handover-by-transit/'.$branch->to_branch) }}" class="fs-16 text-primary">{{ branch($branch->to_branch)['name'] }}</a></th>
															    	<td><span class="badge badge-pill badge-primary badge-sm">{{ inbound_transit_count_by_branch($branch->to_branch)['total'] }}</span></td>
															    	<td><span class="badge badge-pill badge-warning badge-sm {{ inbound_transit_count_by_branch($branch->to_branch)['remain'] == 0? 'finished':'pulse' }}">{{ inbound_transit_count_by_branch($branch->to_branch)['remain'] }}</span></td>
															    	<td><span class="badge badge-pill badge-success badge-sm">{{ inbound_transit_count_by_branch($branch->to_branch)['received'] }}</span> {!! (inbound_transit_count_by_branch($branch->to_branch)['remain'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															    </tr>
															    @endforeach
															    <tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $ht_total }}</span></td>
																    <td class="border-bold"><span class="badge badge-warning badge-sm">{{ $ht_remain }}</span></td>
																    <td class="border-bold"><span class="badge badge-success badge-sm">{{ $ht_received }}</span></td>
															    </tr>
															@else
															    <tr>
															    	<td colspan="5">
															    		<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
														                    <div class="alert-text">စာလွှဲထားသောရုံး စာရင်းများ မရှိသေးပါ</div>
														                </div>
															    	</td>
															    </tr>
															@endif
														    </tbody>								    
													  	</table>
													</div>
										        </div>

										    </div>
										    @if(!inbound_handover_branches(Auth::user()->city_id)->isEmpty() || !inbound_handover_transit(Auth::user()->city_id)->isEmpty())
											<div class="kt-portlet__foot kt-portlet__foot--md">
										    	<div class="">
													<a href="{{ url('to-receive-lists/inbound') }}" class="btn btn-success btn-bold">စာဝင်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="hours" value="{{ date('H') }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Change Date For Dashboard</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Cargo Dashboard အား <span id="set_date" class="text-danger"></span> ရက်စွဲပြောင်းမည်။ </h5>
					<p>Dashboard တွင်မြင်ရသော waybills များသည် ၎င်းရက်စွဲ အတိုင်းမြင်ရမည်ဖြစ်သည်။</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-brand" data-dismiss="modal">ပိတ်မည်</button>
					<button type="button" class="btn btn-brand set-date" data-dismiss="modal">ပြောင်းမည်</button>
					<input type="hidden" id="config_date" value="">
				</div>
			</div>
		</div>
	</div>

    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$("#kt_datepicker_2").datepicker({
			format: 'yyyy-mm-dd'
		})
   	</script>
</body>
</html>