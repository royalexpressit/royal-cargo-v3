<a href="{{ url('home/section-1') }}" class="kt-portlet  kt-widget-12 {{ $link == 'sec-1'? 'active-success pulse':'' }} ">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head mb-0">
														<div class="kt-widget-12__date kt-widget-12__date--success">
															<span class="kt-widget-12__day"> <i class="flaticon-feed kt-font-success"></i></span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">Inbound/Outbound Status</h3>
															<span class="kt-widget-12__desc fs-20 text-success">စာဝင်/စာထွက် အခြေအနေ</span>
														</div>
													</div>
												</div>
											</div>
										</a>

										<a href="{{ url('home/section-2') }}" class="kt-portlet  kt-widget-12 {{ $link == 'sec-2'? 'active-primary pulse':'' }}">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head mb-0">
														<div class="kt-widget-12__date kt-widget-12__date--primary">
															<span class="kt-widget-12__day"><i class="flaticon-feed kt-font-primary"></i></span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">Collected By Branches</h3>
															<span class="kt-widget-12__desc fs-20 text-primary">စာကောက် အခြေအနေ</span>
														</div>
													</div>
												</div>
											</div>
										</a>

										<a href="{{ url('home/section-3') }}" class="kt-portlet  kt-widget-12 {{ $link == 'sec-3'? 'active-primary pulse':'' }}">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head mb-0">
														<div class="kt-widget-12__date kt-widget-12__date--primary">
															<span class="kt-widget-12__day"><i class="flaticon-feed kt-font-primary"></i></span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">Branch Out By City</h3>
															<span class="kt-widget-12__desc fs-20 text-primary">မြို့အလိုက်စာထွက်</span>
														</div>
													</div>
												</div>
											</div>
										</a>

										<a href="{{ url('home/section-4') }}" class="kt-portlet  kt-widget-12 {{ $link == 'sec-4'? 'active-success pulse':'' }}">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head mb-0">
														<div class="kt-widget-12__date kt-widget-12__date--success">
															<span class="kt-widget-12__day"><i class="flaticon-feed kt-font-success"></i></span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">Inbound From City</h3>
															<span class="kt-widget-12__desc fs-20 text-success">မိမိမြို့သို့ စာဝင်</span>
														</div>
													</div>
												</div>
											</div>
										</a>

										<a href="{{ url('home/section-5') }}" class="kt-portlet  kt-widget-12 {{ $link == 'sec-5'? 'active-success pulse':'' }}">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head mb-0">
														<div class="kt-widget-12__date kt-widget-12__date--success">
															<span class="kt-widget-12__day"><i class="flaticon-feed kt-font-success"></i></span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">Inbound By Branch</h3>
															<span class="kt-widget-12__desc fs-20 text-success">မိမိမြို့ရှိရုံးများသို့ စာလွှဲ</span>
														</div>
													</div>
												</div>
											</div>
										</a>

										<a href="{{ url('handover-by-branch/'.Auth::user()->branch_id) }}" class="kt-portlet kt-widget-12" style="background: #fae6ee;">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head mb-0">
														<div class="kt-widget-12__date kt-widget-12__date--danger">
															<span class="kt-widget-12__day"><i class="flaticon-feed kt-font-danger"></i></span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">Inbound Handover</h3>
															<span class="kt-widget-12__desc fs-20 text-danger">မိမိရုံးသို့ လွှဲထားသောစာများ</span>
														</div>
													</div>
												</div>
											</div>
										</a>