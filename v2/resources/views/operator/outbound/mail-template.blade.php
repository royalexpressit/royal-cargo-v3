<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <title>Cargo Waybill Report</title>
  <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@200&display=swap" rel="stylesheet">
</head>
<body style="width:80%;margin:0 auto;">
  	<h3 style="color:#ee1c24;font-size: 22px;font-family: 'Poppins', sans-serif;margin: 10px 0px;">Cargo Outbound Information</h3>
  	<p>Collected By {{ $delivery->name }},Date {{ $date }}</p>
  	<table style="font-family: 'Poppins', sans-serif;width:auto;border:1px solid #ddd;text-align: center">
    	<thead>
	      	<tr>
	        	<td style="border:1px solid #ddd;width:40px">No.</td>
	        	<td style="border:1px solid #ddd;width:200px">Waybill</td>
	        	<td style="border:1px solid #ddd;width:200px">City</td>
            <td style="border:1px solid #ddd;width:200px">Collected Date</td>
	      	</tr>
    	</thead>
	    <tbody>
	    	@foreach($waybills as $key => $waybill)
	      	<tr>
	        	<td style="border:1px solid #ddd">{{ ++$key }}</td>
	        	<td style="border:1px solid #ddd">{{ $waybill->waybill_no }}</td>
	        	<td style="border:1px solid #ddd">{{ $waybill->origin }}</td>
	      	  <td style="border:1px solid #ddd">{{ $waybill->action_date }}</td>
          </tr>
	      	@endforeach
	    </tbody>
  	</table>
</body>
</html>