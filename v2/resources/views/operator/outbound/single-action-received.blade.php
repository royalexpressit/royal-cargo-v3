<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Outbound [Single Scan Received]</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.times{
            height:80px;
            width:80px;
            background:url('{{ asset("_assets/5sec.gif") }}');
            background-size: cover;
            background-position: left;
            background-repeat: no-repeat;
        }
        .continue-action{
        	display: none;
        }
        .continue-action{
        	margin-top:10px;
        }
        .invalid-no{

        }
        .scanned-panel{
        	height: 460px;
        	overflow-y: auto;
        }
        .continue-action-btn{
        	margin-top:10px;
        	display: none;
        }
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Outbound (စာထွက်)စာထွက် <small class="text-danger">[Single Scan]</small></h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        <a href="{{ url('scan/outbound/received') }}" class="btn btn-sm btn-primary right"><i class="fa fa-qrcode"></i> Multiple Scan</a>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid" id="blockui_content_1">
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												<strong class="badge badge-primary text-uppercase">Outbound</strong> - Received Wabyill (စာထွက် - စာလက်ခံစာရင်းသွင်းရန်) 
											</h3>
										</div>
										<div class="kt-portlet__head-toolbar">
								            <div class="kt-portlet__head-wrapper kt-form">
								                <div class="kt-form__group kt-form__group--inline kt-margin-r-10">
								                    <div class="kt-form__label marked-lists">
								                    	Scanned: <span id="temp-scanned" class="text-primary">0</span>,
								                    	Received <span id="temp-received" class="text-success">0</span>:
								                    </div>
								                </div>
								                @if(Session::get('marked-branch-ed3c2622064f'))
									                <button class="btn btn-sm btn-success btn-upper marked-branch" data-toggle="modal" data-target="#countModal">
									                	<span id="choice-branch">
									                		{{ branch(marked_branch())['name'] }}
									                	</span>
									                </button>
									                <input type="hidden" id="check-marked-branch" value="{{ branch(marked_branch())['name'] }}">
								                @else
									                <button class="btn btn-sm btn-danger btn-upper marked-branch" data-toggle="modal" data-target="#countModal">
									                	<span id="choice-branch">
									                		Set Branch
									                	</span>
									                </button>
								                	<input type="hidden" id="check-marked-branch" value="">		
								                @endif
								            </div>
								        </div>
									</div>
									<!--begin::Form-->
									<form id="form" class="kt-form kt-form--fit kt-form--label-right">
										<div class="kt-portlet__body">
											<div class="row">
												<div class="col-lg-3 col-md-6 col-sm-12">
													<div class="card">
	                                                    <div class="card-header bg-info text-white fs-16">စာထွက်အတွက် လုပ်ဆောင်ရန်</div>
	                                                    <div class="card-body">
	                                                        @if(city(Auth::user()->city_id)['is_service_point'] == 1)
																<a href="{{ url('scan/outbound/collected') }}" class="btn btn-danger text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Collected</a>
																<span class="btn btn-warning text-left btn-block mb-2 disabled"><i class="fa fa-qrcode"></i> Scan Handover <span class="badge badge-danger badge-pill badge-sm pull-right">skip</span></span>
																<span class="btn btn-success text-left btn-block mb-2 disabled"><i class="fa fa-qrcode"></i> Scan Received <span class="badge badge-danger badge-pill badge-sm pull-right">skip</span></span>
																<a href="{{ url('scan/outbound/branch-out') }}" class="btn btn-primary text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Branch Out</a>
															@else
																<a href="{{ url('scan/outbound/collected') }}" class="btn btn-danger text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Collected</a>
																<a href="{{ url('scan/outbound/handover') }}" class="btn btn-warning text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Handover</a>
																@if(branch(Auth::user()->branch_id)['is_main_office'] == 1)
																<a href="{{ url('scan/outbound/received') }}" class="btn btn-success text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Received</a>
																<a href="{{ url('scan/outbound/branch-out') }}" class="btn btn-primary text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Branch Out</a>
																@else
																<span class="btn btn-success text-left btn-block mb-2 disabled"><i class="fa fa-qrcode"></i> Scan Received</span>
																<span class="btn btn-primary text-left btn-block mb-2 disabled"><i class="fa fa-qrcode"></i> Scan Branch Out</span>
																@endif
															@endif
	                                                    </div>
	                                                </div>
												</div>
												<div class="col-lg-4 col-md-6 col-sm-12">
													<div class="form-group">
														<label class="form-label">Waybill No (စာအမှတ်) <strong class="text-danger check-number hide">(နံပါတ်မှားနေသည်)</strong></label>
														<input type="text" id="scanned_waybill" class="form-control" placeholder="C2103477890" autofocus="">
													</div>
													<div class="form-group">
														<div class="row">
															<div class="col-md-6">
																<button type="button" class="btn btn-primary btn-wide continue-action-btn">ပြန်ဖတ်မည်</button>
															</div>
															<div class="col-md-6">
																<input type="text" class="inline-input form-control continue-action" id="continue-action" placeholder="Scan Submit QR Code">
															</div>
														</div>
													</div>
												</div>
												<div class="col-lg-5 col-md-6 col-sm-12">
													<h5 class="kt-portlet__head-title">Scanned <span id="scanned" class="text-primary">0</span>,<span class="success-lbl">Success <span id="success" class="text-success">0</span></span>,<span class="failed-lbl">Failed <span id="failed" class="text-danger">0</span></span></h5>
													<ul class="list-group" id="scanned-lists">
														
													</ul>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
				<input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
				<input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')

    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 
	@include('layouts.action-modal') 

	<!-- Modal - scanned/success count for branch -->
	<div class="modal fade" id="countModal" tabindex="-1" role="dialog" aria-labelledby="countModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="countModalLabel">စာလက်ခံ Branch၏ အရေအတွက်မှတ်သားထားရန်</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<div class="form-group">
						<label class="form-label">Branch အားရွေးပေးပါ</label>
						<select class="form-control kt-select2" id="marked-branch">
							@foreach(App\Branch::where('city_id',Auth::user()->city_id)->orderBy('name','asc')->get() as $branch)
							<option value="{{ $branch->id }}">{{ $branch->name }}</option>
							@endforeach
						</select>
					</div>
						<div class="card">
							<div class="card-body bg-warning text-white text-left">
								<strong class="text-danger text-uppercase">*Warning</strong> Clear လုပ်လိုက်ပါက လက်ရှိရွေးချယ်ထားသော branch နှင့် လက်ရှိမှတ်ထားသော အရေအတွက်များ အသစ်ဖြစ်သွားပါမည်။
							</div>
						</div>
					
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-danger clear-branch" data-dismiss="modal">Clear</button>
					<button type="button" class="btn btn-brand selected-branch" data-dismiss="modal">Save Branch</button>
				</div>
			</div>
		</div>
	</div> 
	<!-- Modal - scanned/success count for branch -->

	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>     
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/O1a1b575a/single-outbound-received.js') }}" type="text/javascript"></script>
</body>
</html>