<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Branch Out Summary</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		$branch_out 	= 0;
		$same_city 		= 0;
		$other_city 	= 0;

		$transit_branch_out = 0;
		$transit_other_city = 0;
	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
								
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
											<strong class="badge badge-primary text-uppercase">Outbound</strong> 
											<span class="fs-16">- Branch Out Summary By Branches</span>
										</h3>
										
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
							
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-9 col-xl-9 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">ရုံးအလိုက်စာထွက် အခြေအနေများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
										            	<span class="removed-outbound-filtered hide">
											            	<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" >
																<i class="flaticon-close"></i>
															</button>
														</span>
														<div class="dropdown dropdown-inline">
															
														</div>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body fixed-height">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmok ">
														        <th>မိမိမြို့ရှိရုံးမှ</th>
														        <th>စာထွက်</th>
														        <th>မိမိမြို့သို့({{ city(Auth::user()->city_id)['shortcode'] }})</th>
														        <th>အခြားမြို့သို့(Other)</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@if(!outbound_branch_summary()->isEmpty())
														    		@foreach(outbound_branch_summary() as $key => $branch)
															      	@php 
															      		$branch_out 	+= branch_out_summary($branch->branch_id)['branch_out'];
															      		$same_city 		+= branch_out_summary($branch->branch_id)['same_city'];
															      		$other_city 	+= branch_out_summary($branch->branch_id)['other_city'];
															      	@endphp
															      	<tr class="branch ">
																        <th scope="row"><a href="{{ url('branch-out-summary/'.$branch->branch_id) }}" class="text-danger fs-16"><i class="flaticon-placeholder-3"></i> {{ branch($branch->branch_id)['name'] }}</a></th>
																        <td><span class="badge badge-pill badge-primary badge-sm">{{ branch_out_summary($branch->branch_id)['branch_out'] }}</span></td>
																        <td><span class="badge badge-pill badge-success badge-sm">{{ branch_out_summary($branch->branch_id)['same_city'] }}</span></td>
																        <td><span class="badge badge-pill badge-purple badge-sm">{{ branch_out_summary($branch->branch_id)['other_city'] }}</span></td>
															      	</tr>
															      	@endforeach
															      	<tr class="bg-whitesmoke">
																        <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																        <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $branch_out }}</span></td>
																        <td class="border-bold"><span class="badge badge-success badge-sm">{{ $same_city }}</span></td>
																        <td class="border-bold"><span class="badge badge-purple badge-sm">{{ $other_city }}</span></td>
															      	</tr>
															      	<input type="hidden" id="check-tb-height" value="{{ $key }}">
														    	@else
														    		<tr>
														    			<td colspan="4">
														    				<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                            <div class="alert-text">စာထွက်လုပ်ထားသော စာရင်းများ မရှိသေးပါ</div>
													                        </div>
														    			</td>
														    		</tr>
														    	@endif
														    </tbody>
													  	</table>
													</div>

													<div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmok ">
														        <th>မိမိမြို့ရှိနယ်စာရုံးမှ</th>
														        <th>စာထွက်လုပ်ပြီး</th>
														     </tr>
														    </thead>
														    <tbody>
														    	@if(!outbound_transit_summary()->isEmpty())
														    		@foreach(outbound_transit_summary() as $key => $branch)
															      	<tr class="branch ">
																        <th scope="row"><a href="{{ url('branch-out-summary/'.$branch->to_branch) }}" class="text-danger fs-16"><i class="flaticon-placeholder-3"></i> {{ branch($branch->to_branch)['name'] }}</a></th>
																        <td><span class="badge badge-pill badge-primary badge-sm">{{ transit_branch_out_summary($branch->to_branch)['branch_out'] }}</span></td>
															      	</tr>
															      	@endforeach
															      	<tr class="bg-whitesmoke">
																        <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																        <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $branch_out }}</span></td>
															      	</tr>
															      	<input type="hidden" id="check-tb-height" value="{{ $key }}">
														    	@else
														    		<tr>
														    			<td colspan="2">
														    				<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                            <div class="alert-text">စာထွက်လုပ်ထားသော စာရင်းများ မရှိသေးပါ</div>
													                        </div>
														    			</td>
														    		</tr>
														    	@endif
														    </tbody>
													  	</table>
													</div>
												</div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
									<div class="col-lg-3 col-xl-3 order-lg-1 order-xl-1">
										<div class="kt-portlet pa-10">
											<div>
												<strong class="fs-16 text-primary">Total</strong>
												<p>action-date, status = branch-in</p>
											</div>
											<div>
												<strong class="fs-16 text-primary">Branch In</strong>
												<p>action-date, status = branch-in</p>
											</div>
											<div>
												<strong class="fs-16 text-primary">Handover(no-transit)</strong>
												<p>action-date, status = handover</p>
											</div>
											<div>
												<strong class="fs-16 text-primary">Handover(transit)</strong>
												<p>action-date, status = handover,<br> type = transit</p>
											</div>
										</div>
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->


    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
</body>
</html>