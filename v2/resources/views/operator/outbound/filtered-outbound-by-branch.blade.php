<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Royal Cargo | Filtered Outbound By Branch</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    	.p-4{
    		padding:10px 4px !important;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader --> 
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">	
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
											<strong class="badge badge-primary text-uppercase">Outbound</strong> 
											<span class="fs-16">- Status By Branches</span>
										</h3>
				                    </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head badge-danger">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">Today Total Outbound </h3>
									
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <h4><span class="badge badge-primary badge-pill badge-sm">{{ collected_count_outbound(Auth::user()->city_id) }}</span></h4>
										        </div>
										    </div>
										   
										    <div class="kt-none">
										        <div class="kt-widget-1 outbound-switch">
										        	@foreach(collected_outbound_by_branches() as $branch)
										        	<a href="{{ url('collected-by-branch/'.$branch->branch_id) }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ branch($branch->branch_id)['name'] }}
										                    </span>
										                    <div class="kt-widget-1__item-desc">စာကောက် - စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{{ collected_count_outbound_by_branches($branch->branch_id) }}</div>
										                </div>
										            </a>
										            @endforeach
										            
										        </div>
										    </div>
										    <div class="kt-portlet__foot kt-portlet__foot--md p-4">
										    	<a href="{{ url('to-receive-lists/outbound') }}" class="btn btn-danger btn-bold ">စာထွက်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
											</div>
										</div>
										<!--end::Portlet-->
										
									</div>

								</div>
								<!--end::Row--> 

 										
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
      

    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>   
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
</body>
</html>