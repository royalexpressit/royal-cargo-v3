<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; To Receive Outbound</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
											<strong class="badge badge-primary text-uppercase">Outbound</strong> 
											<span class="fs-16">- To Receive Waybills (စာထွက်လက်ခံပေးရန် ကျန်ရှိစာရင်းများ)</span>
										</h3>
				                    </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										@if(!$branches->isEmpty())
										<!--begin::Portlet-->
										<div class="accordion accordion-outline" id="accordionExample3">
											@foreach($branches as $key => $branch)
											<div class="card">
												<div class="card-header" id="headingOne3">
													<div class="card-title {{ ($key == 0 ? '':'collapsed')}}" data-toggle="collapse" data-target="#collapseOne{{ $branch->branch_id }}" aria-expanded="true" aria-controls="collapseOne3">
														{{ branch($branch->branch_id)['name'] }} ({{ $branch->total }})
													</div>
												</div>
												<div id="collapseOne{{ $branch->branch_id }}" class="card-body-wrapper collapse {{ ($key == 0 ? 'show':'')}}" aria-labelledby="headingOne3" data-parent="#accordionExample3">
													<div class="card-body">
														<ul class="list-group">
															@foreach(to_receive_outbound_waybills($branch->branch_id) as $waybill)
															<li class="list-group-item"><i class="fa fa-qrcode"></i> <span class="text-danger fs-16">{{ $waybill->waybill_no }}</span> <span class="fs-14">(Handover By: {{ user($waybill->action_by)['name'] }})</span> <span class="pull-right">{{ $waybill->action_date }}</span></li>
															@endforeach
														</ul>
													</div>
												</div>
											</div>
											@endforeach
										</div>    
										@else
											<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
												<div class="alert-text">စာလွှဲပြောင်း စာရင်းများ မရှိသေးပါ</div>
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
 				</div>						
			
				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
        
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>   
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
</body>
</html>