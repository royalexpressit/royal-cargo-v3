<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Outbound View Logs</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
			<!-- header -->
			@include('operator.header')
			<!-- header -->	

			<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
				<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
					<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
						<!-- begin:: Subheader -->
						<div class="kt-subheader   kt-grid__item" id="kt_subheader">
						    <div class="kt-container ">
						        <div class="kt-subheader__main">
									<h3 class="kt-subheader__title">Outbound - Waybill Logs (စာထွက် လုပ်ဆောင်မှုများ)</h3>
						            <span class="kt-subheader__separator kt-hidden"></span>
						        </div>
						    </div>
						</div>
						<!-- end:: Subheader -->

						<!-- begin:: Content -->
						<div class="kt-container  kt-grid__item kt-grid__item--fluid">
							<div class="row">
							    <div class="col-lg-6">
							        <div class="kt-portlet ">
									    <div class="kt-widget kt-widget--general-2">
										    <div class="kt-portlet__body kt-portlet__body--fit">
									            <div class="kt-widget__top">
									                
									                <div class="kt-widget__wrapper">
									                    <div class="kt-widget__label">
									                        <a href="#" class="kt-widget__title">
									                            {{ $waybill->waybill_no }}
									                        </a>
									                        <p>
									                        	<span class="badge badge-md badge-primary badge-pill">{{ city($waybill->origin)['shortcode'] }}</span>
									                        	-
									                        	<span class="badge badge-sm badge-danger badge-pill">{{ city($waybill->transit)['shortcode'] }}</span>
									                        	-
									                        	<span class="badge badge-sm badge-success badge-pill">{{ city($waybill->destination)['shortcode'] }}</span>
									                        </p>
									                        <p>
									                        	Delivery/Counter : {{ user($waybill->user_id)['name'] }}
									                        </p>
									                    </div>
									                </div>                
									            </div>
									            <div class="kt-widget__bottom">
									                <div class="kt-widget__actions">
									                    <a href="#" class="btn btn-primary btn-sm btn-bold btn-upper">Marked</a>&nbsp;
									                </div>
									            </div>
									        </div>
									    </div>
									</div>
							    </div>

							    <div class="col-lg-6">
							        <!--begin::Portlet-->
							        <div class="kt-portlet kt-portlet--height-fluid">
							            <div class="kt-portlet__head">
							                <div class="kt-portlet__head-label">
							                    <h3 class="kt-portlet__head-title">Logs (လုပ်ဆောင်မှု မှတ်တမ်းများ)</h3>
							                </div>
							            </div>
							            <div class="kt-portlet__body">
							                <div class="kt-scroll" data-scroll="true">
							                    <!--Begin::Timeline -->
							                    <div class="kt-timeline">
							                    	@foreach($logs as $key => $log)
								                    	@if($key < 5)
							                        	<!--Begin::Item -->                       
								                        <div class="kt-timeline__item kt-timeline__item--accent">
								                            <div class="kt-timeline__item-section">
								                                <div class="kt-timeline__item-section-border">
								                                    <div class="kt-timeline__item-section-icon">
								                                        <i class="flaticon-calendar-3 kt-font-success"></i>
								                                    </div>
								                                </div>
								                                <span class="kt-timeline__item-datetime fs-16 text-capitalize">{{ $log->action }} Log</span>, [<span class="fs-16">{{ branch($log->branch_id)['name'] }}]</span>
								                            </div>	
								                            <div class="kt-timeline__item-text">
								                                <span class="text-primary fs-16">{{ $log->action_log}}</span><br>      
								                            	<span class=""><i class="flaticon-clock-1"></i> {{ $log->action_date }}</span>                  
								                            </div>   
								                        </div>  
								                        <!--End::Item -->  
								                        @endif 
							                        @endforeach        
							                    </div> 
							                    <!--End::Timeline 1 -->  
							                <div class="ps__rail-x" style="left: 0px; bottom: 0px;"><div class="ps__thumb-x" tabindex="0" style="left: 0px; width: 0px;"></div></div><div class="ps__rail-y" style="top: 0px; height: 700px; right: 0px;"><div class="ps__thumb-y" tabindex="0" style="top: 0px; height: 300px;"></div></div></div>
							            </div>
							        </div>
							        <!--end::Portlet-->        
							    </div> 
							</div>



	</div>
<!-- end:: Content -->							</div>
											</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->


	@include('operator.quick-panel')
   
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 

	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
</body>
</html>