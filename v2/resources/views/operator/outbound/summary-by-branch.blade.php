<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Branch Out By Branch</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    	.coming{
    		margin:0;
    		padding:4px !important;
    	}
    	.coming-city-list{
    		background: #f5f5f5;
		    padding: 4px 10px;
		    border: 1px dashed #e6e6f5;
		    margin-bottom: 4px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		$branch_out 	= 0;
		$same_city 		= 0;
		$other_city 	= 0;

		foreach(outbound_branch_summary() as $key => $branch){
			$branch_out 	+= branch_out_summary($branch->branch_id)['branch_out'];
			$same_city 		+= branch_out_summary($branch->branch_id)['same_city'];
			$other_city 	+= branch_out_summary($branch->branch_id)['other_city'];
		}
	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
											<a href="{{ url('branch-out-summary') }}" class="badge badge-primary text-uppercase">Outbound</a> 
											<span class="fs-16">- Branch Out By <span class="text-danger">{{ branch($branch_id)['name'] }}</span></span>
										</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
            							<div class="kt-subheader__wrapper">
                
                            			</div>
        							</div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head badge-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">Other Cities(အခြားမြို့သို့စာထွက်)</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar text-white">
										            <div class="kt-portlet__head-actions fs-18">
										                Total - <strong class="text-upper">{{ $other_city }}</strong>
										            </div>

										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-4">
										        	<div class="alert alert-solid-warning alert-bold alert-box1" role="alert" >
													    <div class="alert-text">စာထွက်လုပ်ထားသော စာရင်းများ မရှိသေးပါ</div>
													</div>
										        	<div class="list-group" id="fetched-data">
													
													</div>
										            <div class="pagination">
														<div class="btn-group" role="group" aria-label="Basic example">
															<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
															<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
															<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
														</div>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">Same City(မိမိမြို့သို့စာထွက်)</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar text-white">
										            <div class="kt-portlet__head-actions fs-18">
										                Total - <strong class="text-upper">{{ $same_city }}</strong>
										            </div>
										            
										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-4">
										        	<div class="">
										        		<div class="alert alert-solid-warning alert-bold alert-box2 " role="alert" >
														    <div class="alert-text">စာထွက်လုပ်ထားသော စာရင်းများ မရှိသေးပါ</div>
														</div>
										        	</div>
										        	
										        	<div class="list-group" id="fetched-data2">
													
													</div>
										            <div class="pagination">
														<div class="btn-group" role="group" aria-label="Basic example">
															<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
															<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
															<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
														</div>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->

	<!-- begin:: Topbar Offcanvas Panels -->
	

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->
    
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$(document).ready(function(){
   			var url 		= $("#url").val();
   			var branch_id   = {{ $branch_id }};

			//declared first loaded json data
			var load_url = url+'/{{ $other_cities }}';
			var waybills = [];
			var item     = 0;


			$.ajax({
			    url: load_url,
			    type: 'GET',
			    data: {},
			    success: function(data){
			    	if(data.total > 0){
			    		$(".alert-box1").hide();
			    		$.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.city_id);
							$("#fetched-data").append('<a href="'+branch_id+'/to/'+value.city_id+'/"><div class="kt-widget-4__item coming-city-list">'
                			+ '<div class="kt-widget-4__item-content">'
                    		+ '<div class="kt-widget-4__item-section">'
                        	+ '<div class="kt-widget-4__item-pic">'
                            + '<img class="" src="{{ asset('_assets/images/favicon.png')}}" alt="">'
				            + '</div>'
				            + '<div class="kt-widget-4__item-info">'
				            + '<span class="kt-widget-4__item-username text-primary">'+value.shortcode+' ('+value.name+')</span>'
				            + '<div class="kt-widget-4__item-desc text-danger">စာထွက်သွားသည့် အရေအတွက်</div>'
				            + '</div>'
				            + '</div>'
				            + '</div>'
				            + '<div class="kt-widget-4__item-content">'
				            + '<div class="kt-widget-4__item-price">'
				            + '<span class="kt-widget-4__item-number"><span class="badge badge-primary badge-pill badge-sm">'+value.total+'</span></span>'
				            + '</div>'
				            + '</div>'
				            + '</div></a>');
						});

						//console.log(waybills);
						//$(".pagination").show();
				        $("#to-records").text(data.to);
						$("#total-records").text(data.total);
							
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
			    	}else{
			    		$(".pagination").hide();
			    		$(".alert-box").show();
			    	}
			    }
			});

			$('.pagination-btn').click(function(){
				//clicked url json data
				var clicked_url = $(this).val();
				

				$(this).siblings().removeClass('active')
				$(this).addClass('active');
				$.ajax({
				    url: clicked_url,
				    type: 'GET',
				    data: {},
				    success: function(data){
				        //console.log(data);
				        $("#fetched-data").empty();
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
			    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<span class="text-danger"><i class="flaticon-placeholder-3 "></i>'+value.branch+'</span>)</h5>'
			    			+ '<small>'+value.action_date+'</small></div>'
			    			+ '<small> စာဝင်လုပ်ဆောင်သူ - '+value.branch_in_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 5? 'danger':(value.current_status == 6? 'warning':(value.current_status == 7? 'success':'primary')))+' pull-right">'+(value.current_status == 5? 'Branch In':(value.current_status == 6? 'Handover':(value.current_status == 7? 'Received':'Postponed')))+'</span></a>');
						});

						

				        $("#to-records").text(data.to);
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
				    }
				});
			});

			//transit loaded data
			var load_transit = url+'/{{ $same_cities }}';
			var waybills = [];
			var item     = 0;
			$.ajax({
			    url: load_transit,
			    type: 'GET',
			    data: {},
			    success: function(data){
			    	if(data.total > 0){
			    		$(".alert-box2").hide();
			    		//console.log(data);
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.city_id);
							$("#fetched-data2").append('<a href="transit-coming-waybills/'+value.city_id+'"><div class="kt-widget-4__item coming-city-list">'
                			+ '<div class="kt-widget-4__item-content">'
                    		+ '<div class="kt-widget-4__item-section">'
                        	+ '<div class="kt-widget-4__item-pic">'
                            + '<img class="" src="{{ asset('_assets/images/favicon.png')}}" alt="">'
				            + '</div>'
				            + '<div class="kt-widget-4__item-info">'
				            + '<span class="kt-widget-4__item-username text-primary">'+value.shortcode+' ('+value.name+')</span>'
				            + '<div class="kt-widget-4__item-desc text-danger">ဝင်လာမည့်စာအရေအတွက်</div>'
				            + '</div>'
				            + '</div>'
				            + '</div>'
				            + '<div class="kt-widget-4__item-content">'
				            + '<div class="kt-widget-4__item-price">'
				            + '<span class="kt-widget-4__item-number"><span class="badge badge-warning badge-pill badge-sm">'+value.total+'</span></span>'
				            + '</div>'
				            + '</div>'
				            + '</div></a>');
						});

						console.log(data);
						//$(".pagination").show();
				        $("#to-records").text(data.to);
						$("#total-records").text(data.total);
							
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
			    	}else{
			    		$(".pagination").hide();
			    		$(".alert-box").show();
			    	}
			    }
			});
		})

   	</script>
</body>
</html>