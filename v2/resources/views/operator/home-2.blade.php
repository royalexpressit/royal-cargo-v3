<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Dashboard</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	@php
		//total(delivery)
		$total 		= 0;
		$collected 	= 0;
		$handover 	= 0;
		$received 	= 0;

		//total(to_city)
		$tc_total 		= 0;

		//total(to_transit_city)
		$ttc_total 		= 0;

		//total(from_city)
		$fc_total 		= 0;
		$fc_branch_in 	= 0;
		$fc_handover 	= 0;

		//total(from_city)
		$it_total 		= 0;
		$it_branch_in 	= 0;
		$it_handover 	= 0;

		//total(handover_branch)
		$hb_total 		= 0;
		$hb_remain 		= 0;
		$hb_received 	= 0;

		//total(handover_transit)
		$ht_total 		= 0;
		$ht_remain 		= 0;
		$ht_received 	= 0;

	@endphp
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
								
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
										</h3>		
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
						                	<div class="input-group date">
												<input type="text" class="form-control set-datetime" readonly  placeholder="Select date" id="kt_datepicker_2" value="{{ get_date() }}"/>
												<div class="input-group-append">
													<span class="input-group-text">
														<i class="la la-calendar-check-o"></i>
													</span>
												</div>
											</div>
                            			</div>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
							
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"><strong>{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }}</strong> Outbound Status 
										            	<span class="pull-right">({{ outbound_dashboard_status(Auth::user()->city_id)['total'] + transit_dashboard_status(Auth::user()->city_id)['outbound'] }})</span>
										            </h3>									
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--primary">
														<label>
														<input type="checkbox" id="outbound-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>
										   
										    <div class="kt-none">
										        <div class="kt-widget-1 outbound-switch">
										        	<a href="{{ url('outbound/all') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Outbound Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-primary">{{ outbound_dashboard_status(Auth::user()->city_id)['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/collected') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Collected Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာကောက်ထားပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['collected'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/handover') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
											                    {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Handover Waybills
											                </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာလွှဲပြောင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['handover'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/received') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                    	{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာလက်ခံအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['received'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('outbound/branch-out') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Branch Out Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့မှစာထွက်</span>- စာထွက်ပြီးသောအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! outbound_dashboard_status(Auth::user()->city_id)['branch_out'].' <small>of</small> '.outbound_dashboard_status(Auth::user()->city_id)['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 outbound-transit-switch">
										        	<a href="{{ url('transit/outbound') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Outbound <span class="text-danger">Transit</span> Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာထွက်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-primary">{{ transit_dashboard_status(Auth::user()->city_id)['outbound'] }}</div>
										                </div>
										            </a>
										        	<a href="{{ url('transit/received') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} <span class="text-danger">Transit</span> Received Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာထွက်</span>- လက်ခံပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['received'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['outbound'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('transit/branch-out') }}" class="kt-widget-1__item widget-item border-1">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} <span class="text-danger">Transit</span> Branch Out Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာထွက်</span>- စာထွက်ပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['branch_out'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['outbound'] !!}</div>
										                </div>
										            </a>

										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] != 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													
													<div class="alert alert-outline-warning mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text">
														  	နယ်စာထွက်ရန်အတွက် အရေအတွက် <strong>{{ transit_dashboard_status(Auth::user()->city_id)['balance'] }}</strong> စောင် လိုနေပါသည်။!
														  	<a href="#">စာရင်းကြည့်ရန်</a>
														  </div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@else
													<div class="alert alert-outline-success mx-1 custom-alert mb-0" role="alert">
														<span class="alert-icon"><i class="flaticon2-check-mark"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် ကိုက်ညီမှုရှိသည်။</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
													@endif
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->

										
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white"><strong>{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }}</strong> Inbound Status 
										            	<span class="pull-right">({{ inbound_dashboard_status()['total'] + transit_dashboard_status(Auth::user()->city_id)['inbound'] }})</span>
										            </h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="inbound-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>
										    <div class="kt-none">
										        <div class="kt-widget-1 inbound-switch">
										        	<a href="{{ url('inbound/all') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Total Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-success">{{ inbound_dashboard_status()['total'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/branch-in') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Branch-In Waybills 
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['branch_in'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/handover') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Handover Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['handover'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/received') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Received Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['received'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('inbound/postponed') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">{{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Postponed/Rejected Waybills</span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">မိမိမြို့သို့စာဝင်</span>- စာဆိုင်းငံ့/ငြင်းပယ် အရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! inbound_dashboard_status()['postponed'] + inbound_dashboard_status()['rejected'] + inbound_dashboard_status()['transferred'] + inbound_dashboard_status()['accepted'].' <small>of</small> '.inbound_dashboard_status()['total'] !!}</div>
										                </div>
										            </a>
										        </div>
										        <div class="kt-widget-1 inbound-transit-switch">
										        	<a href="{{ url('transit/inbound') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Inbound Transit Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာဝင်</span>- စုစုပေါင်းအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value fs-24 text-danger">{{ transit_dashboard_status(Auth::user()->city_id)['inbound'] }}</div>
										                </div>
										            </a>
										            <a href="{{ url('transit/branch-in') }}" class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Transit Branch In Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာဝင်</span>- စာဝင်ထားဆဲအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['branch_in'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['inbound'] !!}</div>
										                </div>
										            </a>
										            <a href="{{ url('transit/handover') }}"  class="kt-widget-1__item widget-item border-2">
										                <div class="kt-widget-1__item-info">
										                    <span class="kt-widget-1__item-title text-primary">
										                        {{ (get_date() == date('Y-m-d')? 'Today':get_date_shortformat()) }} Transit Handover Waybills
										                    </span>
										                    <div class="kt-widget-1__item-desc"><span class="text-danger">နယ်စာဝင်</span>- စာလွှဲပြောင်းပြီးအရေအတွက်</div>
										                </div>
										                <div class="kt-widget-1__item-stats">
										                    <div class="kt-widget-1__item-value">{!! transit_dashboard_status(Auth::user()->city_id)['handover'] .' <small>of</small> '. transit_dashboard_status(Auth::user()->city_id)['inbound'] !!}</div>
										                </div>
										            </a>
										            @if(transit_dashboard_status(Auth::user()->city_id)['balance'] != 0)
										            <div class="alert alert-outline-danger mx-1 custom-alert" role="alert">
														<span class="alert-icon"><i class="flaticon-exclamation-1"></i></span>
														  <div class="alert-text"><u>နယ်စာဝင်</u>အရေအတွက်နှင့် <u>နယ်စာထွက်</u>အရေအတွက် တူရမည်!</div>
														  <div class="alert-close">
															<button type="button" class="close" data-dismiss="alert" aria-label="Close">
															    <span aria-hidden="true"><i class="la la-close"></i></span>
															</button>
														</div>
													</div>
										            @endif
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
								
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာပို့သမား (သို့) ကောင်တာမှ ကောက်ထားသောစာများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">
										            	<span class="removed-outbound-filtered hide">
											            	<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" >
																<i class="flaticon-close"></i>
															</button>
														</span>
														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Filtered By Branch </span>
																	</li>
																	@if(!collected_outbound_by_branches()->isEmpty())
																		@foreach(collected_outbound_by_branches() as $branch)
																		<li class="kt-nav__item">
																			<span class="kt-nav__link filtered-outbound-by-branch" id="{{ $branch->branch_id }}">
																				<i class="kt-nav__link-icon flaticon-home-2"></i>
																				<span class="kt-nav__link-text" >{{ branch($branch->branch_id)['name'] }} <i class="flaticon2-check-mark text-success checked-item checked-item-{{ $branch->branch_id }} hide"></i></span>
																			</span>
																		</li>
																		@endforeach
																	@else
																		<li class="kt-nav__item">
																			<span class="kt-nav__link">
																				<i class="kt-nav__link-icon flaticon-close"></i>
																				<span class="kt-nav__link-text" >မရှိသေးပါ</span>
																			</span>
																		</li>
																	@endif
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    
										    <div class="kt-portlet__body fixed-height">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-primary">
														        <th>စာပို့သမား</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြီး</th>
														        <th>စာလက်ခံပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@if(!group_by_delivery(Auth::user()->city_id)->isEmpty())
															    	@foreach(group_by_delivery(Auth::user()->city_id) as $key => $delivery)
															      	@php 
															      		$total 		+= delivery_man_collected($delivery->user_id)['total'];
															      		$collected 	+= delivery_man_collected($delivery->user_id)['collected'];
															      		$handover 	+= delivery_man_collected($delivery->user_id)['handover'];
															      		$received 	+= delivery_man_collected($delivery->user_id)['received'];
															      	@endphp
															      	<tr class="branch branch-{{ user($delivery->user_id)['branch_id'] }}">
																        <th scope="row"><a href="{{ url('delivery-by/'.user($delivery->user_id)['id']) }}" class="text-danger"><i class="flaticon2-user"></i> {{ user($delivery->user_id)['name'] }} <span class="text-primary">({{ branch(user($delivery->user_id)['branch_id'])['name'] }})</span></a></th>
																        <td><span class="badge badge-pill badge-primary badge-sm total-{{ user($delivery->user_id)['branch_id'] }}">{{ delivery_man_collected($delivery->user_id)['total'] }}</span></td>
																        <td><span class="badge badge-pill badge-danger badge-sm {{ delivery_man_collected($delivery->user_id)['collected'] == 0? 'finished':'pulse' }} collected-{{ user($delivery->user_id)['branch_id'] }}" >{{ delivery_man_collected($delivery->user_id)['collected'] }}</span></td>
																        <td><span class="badge badge-pill badge-warning badge-sm {{ delivery_man_collected($delivery->user_id)['handover'] == 0? 'finished':'pulse' }} handover-{{ user($delivery->user_id)['branch_id'] }}">{{ delivery_man_collected($delivery->user_id)['handover'] }}</span></td>
																        <td><span class="badge badge-pill badge-success badge-sm received-{{ user($delivery->user_id)['branch_id'] }}">{{ delivery_man_collected($delivery->user_id)['received'] }}</span> {!! (delivery_man_collected($delivery->user_id)['total'] == delivery_man_collected($delivery->user_id)['received']? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															      	</tr>
															      	@endforeach
															      	<tr class="bg-whitesmoke filtered-hide-1">
																        <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																        <td class="border-bold"><a href="{{ url('outbound/all') }}" class="badge badge-primary badge-sm">{{ $total }}</a> <span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="မိမိမြို့မှစာထွက် - စုစုပေါင်းအရေအတွက် နှင့်တူသည်။"><i class="fa fa-info-circle text-info"></i></span></td>
																        <td class="border-bold"><a href="{{ url('outbound/handover') }}" class="badge badge-danger badge-sm">{{ $collected }}</a></td>
																        <td class="border-bold"><a href="{{ url('outbound/handover') }}" class="badge badge-warning badge-sm">{{ $handover }}</a></td>
																        <td class="border-bold"><a href="{{ url('outbound/finished-received') }}" class="badge badge-success badge-sm">{{ $received }}</a></td>
															      	</tr>
															      	<tr class="bg-whitesmoke filtered-show-1 hide">
																        <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																        <td class="border-bold"><span class="badge badge-primary badge-sm" id="filtered-total">0</span> </td>
																        <td class="border-bold"><span class="badge badge-danger badge-sm" id="filtered-collected">0</span></td>
																        <td class="border-bold"><span class="badge badge-warning badge-sm" id="filtered-handover">0</span></td>
																        <td class="border-bold"><span class="badge badge-success badge-sm" id="filtered-received">0</span></td>
															      	</tr>
															      	<input type="hidden" id="check-tb-height" value="{{ $key }}">
														    	@else
														    		<tr>
														    			<td colspan="5">
														    				<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                            <div class="alert-text">စာကောက် စာရင်းများ မရှိသေးပါ</div>
													                        </div>
														    			</td>
														    		</tr>
														    	@endif
														    </tbody>
													  	</table>
													</div>
												</div>
										    </div>
										    @if(!group_by_delivery(Auth::user()->city_id)->isEmpty())
										    <div class="kt-portlet__foot kt-portlet__foot--md">
										    	<div class="">
													<a href="{{ url('to-receive-lists/outbound') }}" class="btn btn-primary btn-bold">စာထွက်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
													<a href="{{ url('filtered-outbound/by-branches') }}" class="btn btn-primary">ရုံးအလိုက်စာကောက်စာရင်းကြည့်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">စာထွက်ထားသော မြို့များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-toolbar-wrapper">

														<div class="dropdown dropdown-inline">
															<button type="button" class="btn btn-clean btn-sm btn-icon btn-icon-md" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
																<i class="flaticon-more-1"></i>
															</button>
															<div class="dropdown-menu dropdown-menu-right">
																<ul class="kt-nav">
																	<li class="kt-nav__section kt-nav__section--first">
																		<span class="kt-nav__section-text">Filtered</span>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link filtered-outbound-city">
																			<i class="kt-nav__link-icon fa fa-home"></i>
																			<span class="kt-nav__link-text">မိမိမြို့မှထွက်</span>
																		</a>
																	</li>
																	<li class="kt-nav__item">
																		<a href="#" class="kt-nav__link filtered-outbound-city">
																			<i class="kt-nav__link-icon fa fa-share"></i>
																			<span class="kt-nav__link-text">နယ်စာဝင်/ထွက်</span>
																		</a>
																	</li>
																</ul>
															</div>
														</div>
													</div>
										        </div>
										    </div>
										    <div class="kt-portlet__body ">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-primary">
														        <th>မြို့သို့ (စာထွက်)</th>
														        <th>စုစုပေါင်း စာထွက်</th>
														      </tr>
														    </thead>
														    <tbody>
														    @if(!group_by_to_city(Auth::user()->city_id)->isEmpty())
														    	@foreach(group_by_to_city(Auth::user()->city_id) as $city)
														    	@php
														    		$tc_total += to_city_status_count($city->destination)['total'];
														    	@endphp
														      	<tr>
														        	<th scope="row"><a href="{{ url('branch-out-by-city/'.$city->destination) }}" class="text-danger">{{ city($city->destination)['shortcode'].' <'.city($city->destination)['mm_name'].'>' }}</a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ to_city_status_count($city->destination)['total'] }}</span></td>
														      	</tr>
														      	@endforeach
														      	<tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><a href="{{ url('outbound/branch-out') }}" class="badge badge-primary badge-sm">{{ $tc_total }}</a></td>
															    </tr>
														    @else
														    	<tr>
														    		<td colspan="5">
														    			<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                        <div class="alert-text">မြို့အလိုက်စာထွက် စာရင်းများ မရှိသေးပါ</div>
													                    </div>
														    		</td>
														    	</tr>
														    @endif
														    </tbody>
													  	</table>
													</div>
													<div class="table-responsive">
													  	<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-danger">
														        <th>မြို့သို့ (နယ်စာထွက်)</th>
														        <th>စုစုပေါင်း စာထွက်</th>
														      </tr>
														    </thead>
														    <tbody>
														    @if(!group_by_to_transit_city(Auth::user()->city_id)->isEmpty())
														    	@foreach(group_by_to_transit_city(Auth::user()->city_id) as $city)
														    	@php
														    		$ttc_total += to_transit_city_status_count($city->destination)['total'];
														    	@endphp
														      	<tr>
														        	<th scope="row"><a href="{{ url('transit-out-by-city/'.$city->destination) }}" class="text-danger">{{ city($city->destination)['shortcode'].' <'.city($city->destination)['mm_name'].'>' }}</a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ to_transit_city_status_count($city->destination)['total'] }}</span></td>
														      	</tr>
														      	@endforeach
														      	<tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $ttc_total }}</span></td>
															    </tr>
														    @else
														    	<tr>
														    		<td colspan="5">
														    			<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                        <div class="alert-text">မြို့အလိုက်စာထွက် စာရင်းများ မရှိသေးပါ</div>
													                    </div>
														    		</td>
														    	</tr>
														    @endif
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										    @if(!group_by_to_city(Auth::user()->city_id)->isEmpty() || !group_by_to_transit_city(Auth::user()->city_id)->isEmpty())
											<div class="kt-portlet__foot kt-portlet__foot--md">
											    <div class="">
													<a href="{{ url('/branch-out-summary') }}" class="btn btn-primary btn-bold">ရုံးအလိုက် စာထွက်ကြည့်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">မိမိမြို့သို့ မြို့အလိုက် စာဝင်စာရင်များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="inbound-city-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>

										    <div class="kt-portlet__body fixed-height2">
										        <div class="kt-widget-1">
										            <div class="table-responsive inbound-city-branch-switch">
														<table class="table table-bordered inbound-city-branch-switch">
														    <thead>
														      <tr class="bg-whitesmoke text-success">
														        <th>စာဝင်လာသည့်မြို့</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြောင်းပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    @if(!group_by_from_city(Auth::user()->city_id)->isEmpty())
														    	@foreach(group_by_from_city(Auth::user()->city_id) as $key => $city)
														    	@php 
														    		$fc_total 		+= from_city_status_count($city->origin)['total'];
														    		$fc_branch_in 	+= from_city_status_count($city->origin)['branch_in'];
														    		$fc_handover 	+= from_city_status_count($city->origin)['handover'];
														    	@endphp
														    	<tr>
														        	<th scope="row"><a href="{{ url('inbound-from-city/'.$city->origin) }}" class="text-danger fs-16"><i class="flaticon-placeholder-3"></i>{{ city($city->origin)['shortcode'] }} <span class="text-primary"><{{ city($city->origin)['mm_name'] }}></span></a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ from_city_status_count($city->origin)['total'] }}</span></td>
														        	<td><span class="badge badge-pill badge-danger badge-sm {{ from_city_status_count($city->origin)['branch_in'] == 0? 'finished':'pulse' }}">{{ from_city_status_count($city->origin)['branch_in'] }}</span></td>
														        	<td><span class="badge badge-pill badge-warning {{ (from_city_status_count($city->origin)['branch_in'] == 0? 'finished':'pulse') }} badge-sm">{{ from_city_status_count($city->origin)['handover'] }}</span> {!! (from_city_status_count($city->origin)['branch_in'] == 0? '<i class="fa fa-check text-warning"></i>':'') !!}</td>
														      	</tr>
														    	@endforeach	
														    	<tr class="bg-whitesmoke">
																    <th class="border-bold"> <span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $fc_total }}</span></td>
																    <td class="border-bold"><span class="badge badge-danger badge-sm">{{ $fc_branch_in }}</span></td>
																    <td class="border-bold"><span class="badge badge-warning badge-sm">{{ $fc_handover }}</span> <span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="မိမိမြို့တွင်စာဝေရန် လွှဲပြောင်းထားသော စာအရေအတွက်ဖြစ်သည်။နယ်စာများ မပါဝင်ပါ။မိမိမြို့ရှိရုံးများအား လွှဲပြောင်းထားသော စာအရေအတွက်နှင့်တူရမည်။"><i class="fa fa-info-circle text-info"></i></span></td>
															    </tr>
														    	<input type="hidden" id="check-tb-height2" value="{{ $key }}">
														    @else
														    	<tr>
														    		<td colspan="5">
														    			<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                        <div class="alert-text">စာဝင်ထားသောမြို့ စာရင်းများ မရှိသေးပါ</div>
													                    </div>
														    		</td>
														    	</tr>
														    @endif
														    </tbody>						    
													  	</table>
													</div>

													<div class="table-responsive inbound-city-transit-switch">
														<table class="table table-bordered">
														    <thead>
														      <tr class="bg-whitesmoke text-danger">
														        <th>နယ်စာဝင်လာသည့်မြို့</th>
														        <th>စုစုပေါင်း</th>
														        <th>စာလွှဲရန်ကျန်</th>
														        <th>စာလွှဲပြောင်းပြီး</th>
														      </tr>
														    </thead>
														    <tbody>
														    	@if(!group_by_transit_from_city(Auth::user()->city_id)->isEmpty())
														    	@foreach(group_by_transit_from_city(Auth::user()->city_id) as $key => $city)
														    	@php 
														    		$it_total 		+= from_city_transit_status_count($city->origin)['total'];
														    		$it_branch_in 	+= from_city_transit_status_count($city->origin)['branch_in'];
														    		$it_handover 	+= from_city_transit_status_count($city->origin)['handover'];
														    	@endphp
														    	<tr>
														        	<th scope="row"><a href="{{ url('transit-from-city/'.$city->origin) }}" class="text-danger fs-16"><i class="flaticon-placeholder-3"></i>{{ city($city->origin)['shortcode'] }} <span class="text-primary"><{{ city($city->origin)['mm_name'] }}></span></a></th>
														        	<td><span class="badge badge-pill badge-primary badge-sm">{{ from_city_transit_status_count($city->origin)['total'] }}</span></td>
														        	<td><span class="badge badge-pill badge-danger badge-sm {{ from_city_transit_status_count($city->origin)['branch_in'] == 0? 'finished':'pulse' }}">{{ from_city_transit_status_count($city->origin)['branch_in'] }}</span></td>
														        	<td><span class="badge badge-pill badge-warning badge-sm {{ (from_city_transit_status_count($city->origin)['branch_in'] == 0? 'finished':'pulse') }}" >{{ from_city_transit_status_count($city->origin)['handover'] }}</span> {!! (from_city_transit_status_count($city->origin)['branch_in'] == 0? '<i class="fa fa-check text-warning"></i>':'') !!}</td>
														      	</tr>
														    	@endforeach	
														    	<tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $it_total }}</span></td>
																    <td class="border-bold"><span class="badge badge-danger badge-sm">{{ $it_branch_in }}</span></td>
																    <td class="border-bold"><span class="badge badge-warning badge-sm">{{ $it_handover }}</span></td>
															    </tr>
														    	<input type="hidden" id="check-tb-height2" value="{{ $key }}">
														    @else
														    	<tr>
														    		<td colspan="5">
														    			<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													                        <div class="alert-text">စာဝင်ထားသောမြို့ စာရင်းများ မရှိသေးပါ</div>
													                    </div>
														    		</td>
														    	</tr>
														    @endif
														    </tbody>
													  	</table>
													</div>
										        </div>
										    </div>
										    @if(!group_by_from_city(Auth::user()->city_id)->isEmpty() || !group_by_transit_from_city(Auth::user()->city_id)->isEmpty())
										    <div class="kt-portlet__foot kt-portlet__foot--md">
										    	<div class="">
													<a href="{{ url('inbound-summary') }}" class="btn btn-success">အသေးစိတ်ကြည့်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet">
										    <div class="kt-portlet__head bg-success">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">ရုံးအလိုက် လွှဲပြောင်းထားသော စာရင်းများ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--success">
														<label>
														<input type="checkbox" id="handover-transit">
														<span></span>
														<strong class="switch-label">နယ်စာ</strong>
														</label>
													</span>
										        </div>
										    </div>

										    <div class="kt-portlet__body">
										        <div class="kt-widget-1">
										            <div class="table-responsive">
														<table class="table table-bordered handover-handover-switch">
														    <thead>
														      	<tr class="bg-whitesmoke text-success">
														        	<th>ရုံးသို့စာလွှဲ</th>
														        	<th>စုစုပေါင်း</th>
														        	<th>စာလက်ခံကျန်</th>
														        	<th>စာလက်ခံပြီး</th>
														      	</tr>
														    </thead>
														    <tbody>
														   	@if(!inbound_handover_branches(Auth::user()->city_id)->isEmpty())
															   	@foreach(inbound_handover_branches(Auth::user()->city_id) as $branch)
															    @php 
															    	$hb_total 		+= inbound_handover_count_by_branch($branch->to_branch)['total'];
																	$hb_remain 		+= inbound_handover_count_by_branch($branch->to_branch)['remain'];
																	$hb_received 	+= inbound_handover_count_by_branch($branch->to_branch)['received'];
														    	@endphp
															    <tr class="{{ ($branch->to_branch==Auth::user()->branch_id? 'highlight':'') }}">
															    	<th scope="row"><a href="{{ url('handover-by-branch/'.$branch->to_branch) }}" class="fs-16 text-primary">{{ branch($branch->to_branch)['name'] }}</a></th>
															    	<td><span class="badge badge-pill badge-primary badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['total'] }}</span></td>
															    	<td><span class="badge badge-pill badge-warning badge-sm {{ inbound_handover_count_by_branch($branch->to_branch)['remain'] == 0? 'finished':'pulse' }}">{{ inbound_handover_count_by_branch($branch->to_branch)['remain'] }}</span></td>
															    	<td><span class="badge badge-pill badge-success badge-sm">{{ inbound_handover_count_by_branch($branch->to_branch)['received'] }}</span> {!! (inbound_handover_count_by_branch($branch->to_branch)['remain'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															    </tr>
															    @endforeach
															    <tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $hb_total }}</span></td>
																    <td class="border-bold"><span class="badge badge-warning badge-sm">{{ $hb_remain }}</span></td>
																    <td class="border-bold"><span class="badge badge-success badge-sm">{{ $hb_received }}</span></td>
															    </tr>
															@else
															    <tr>
															    	<td colspan="5">
															    		<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
														                    <div class="alert-text">စာလွှဲထားသောရုံး စာရင်းများ မရှိသေးပါ</div>
														                </div>
															    	</td>
															    </tr>
															@endif
														    </tbody>								    
													  	</table>
													  	<table class="table table-bordered handover-transit-switch">
														    <thead>
														      	<tr class="bg-whitesmoke text-danger">
														        	<th>နယ်စာရုံးသို့စာလွှဲ</th>
														        	<th>စုစုပေါင်း</th>
														        	<th>စာလက်ခံကျန်</th>
														        	<th>စာလက်ခံပြီး</th>
														      	</tr>
														    </thead>
														    <tbody>
														   	@if(!inbound_handover_transit(Auth::user()->city_id)->isEmpty())
															   	@foreach(inbound_handover_transit(Auth::user()->city_id) as $branch)
															    @php 
															    	$ht_total 		+= inbound_transit_count_by_branch($branch->to_branch)['total'];
																	$ht_remain 		+= inbound_transit_count_by_branch($branch->to_branch)['remain'];
																	$ht_received 	+= inbound_transit_count_by_branch($branch->to_branch)['received'];
														    	@endphp
															    <tr class="{{ ($branch->to_branch==Auth::user()->branch_id? 'highlight':'') }}">
															    	<th scope="row"><a href="{{ url('handover-by-transit/'.$branch->to_branch) }}" class="fs-16 text-primary">{{ branch($branch->to_branch)['name'] }}</a></th>
															    	<td><span class="badge badge-pill badge-primary badge-sm">{{ inbound_transit_count_by_branch($branch->to_branch)['total'] }}</span></td>
															    	<td><span class="badge badge-pill badge-warning badge-sm {{ inbound_transit_count_by_branch($branch->to_branch)['remain'] == 0? 'finished':'pulse' }}">{{ inbound_transit_count_by_branch($branch->to_branch)['remain'] }}</span></td>
															    	<td><span class="badge badge-pill badge-success badge-sm">{{ inbound_transit_count_by_branch($branch->to_branch)['received'] }}</span> {!! (inbound_transit_count_by_branch($branch->to_branch)['remain'] == 0? '<i class="fa fa-check text-success"></i>':'') !!}</td>
															    </tr>
															    @endforeach
															    <tr class="bg-whitesmoke">
																    <th class="border-bold"><span class="text-primary fs-18 text-center">စုစုပေါင်း အရေအတွက်</span></th>
																    <td class="border-bold"><span class="badge badge-primary badge-sm">{{ $ht_total }}</span></td>
																    <td class="border-bold"><span class="badge badge-warning badge-sm">{{ $ht_remain }}</span></td>
																    <td class="border-bold"><span class="badge badge-success badge-sm">{{ $ht_received }}</span></td>
															    </tr>
															@else
															    <tr>
															    	<td colspan="5">
															    		<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
														                    <div class="alert-text">စာလွှဲထားသောရုံး စာရင်းများ မရှိသေးပါ</div>
														                </div>
															    	</td>
															    </tr>
															@endif
														    </tbody>								    
													  	</table>
													</div>
										        </div>

										    </div>
										    @if(!inbound_handover_branches(Auth::user()->city_id)->isEmpty() || !inbound_handover_transit(Auth::user()->city_id)->isEmpty())
											<div class="kt-portlet__foot kt-portlet__foot--md">
										    	<div class="">
													<a href="{{ url('to-receive-lists/inbound') }}" class="btn btn-success btn-bold">စာဝင်လက်ခံရန်ကျန် စာရင်းများစစ်ရန်</a>
												</div>
											</div>
											@endif
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="hours" value="{{ date('H') }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title" id="exampleModalLabel">Change Date For Dashboard</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<h5>Cargo Dashboard အား <span id="set_date" class="text-danger"></span> ရက်စွဲပြောင်းမည်။ </h5>
					<p>Dashboard တွင်မြင်ရသော waybills များသည် ၎င်းရက်စွဲ အတိုင်းမြင်ရမည်ဖြစ်သည်။</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-outline-brand" data-dismiss="modal">ပိတ်မည်</button>
					<button type="button" class="btn btn-brand set-date" data-dismiss="modal">ပြောင်းမည်</button>
					<input type="hidden" id="config_date" value="">
				</div>
			</div>
		</div>
	</div>

	<!-- Password no secure alert -->
	<div class="modal fade" id="check-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white"><i class="fa fa-calendar"></i> မနေ့က မိမိရုံး စာဝင်/စာထွက် အခြေအနေစစ်ပါ</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="fs-16">မနေ့က မိမိရုံး စာဝင်(inbound)/စာထွက်(outbound) စာရင်းများကို စစ်ပေးပါ။
					စာလက်ခံကျန်စာရင်းများ စစ်ခြင်းဖြင့် စာပျောက်ခြင်းများအား လျော့ချနိုင်ပါသည်။</p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('daily-check-lists/outbound') }}" class="btn btn-brand">စာထွက်စာရင်းစစ်ရန်</a>
					<a href="{{ url('daily-check-lists/inbound') }}" class="btn btn-success">စာဝင်စာရင်းစစ်ရန်</a>
				</div>
			</div>
		</div>
	</div>
	<!-- Password no secure alert -->

	<!-- Password no secure alert -->
	<div class="modal fade" id="no-secure-alert" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
		<div class="modal-dialog modal-dialog-centered" role="document">
			<div class="modal-content">
				<div class="modal-header bg-danger">
					<h5 class="modal-title text-white"><i class="flaticon-lock"></i> စကားဝှက်(password) လုံခြုံမှုဆိုင်ရာ အသိပေးခြင်း</h5>
					<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<p class="fs-16">လူကြီးမင်း အသုံးပြုထားသော <strong class="text-danger">စကားဝှက်(password)</strong> မှာ လုံခြုံမှု မရှိကြောင်းတွေ့ရပါသည်။ တခြားသူမှ အလွယ်တကူ အသုံးမပြုနိုင်အောင် <strong class="text-danger">စကားဝှက်(password)</strong>အား အသစ်ပြောင်းလဲရန် အကြံပြုပါသည်။ </p>
				</div>
				<div class="modal-footer">
					<a href="{{ url('my-profile') }}" class="btn btn-brand">စကားဝှက်ပြောင်းမည်</a>
					<button type="button" class="btn btn-danger" data-dismiss="modal">ပိတ်မည်</button>
				</div>
			</div>
		</div>
	</div>
	<!-- Password no secure alert -->

    @include('layouts.widget')      
       
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/home.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$("#kt_datepicker_2").datepicker({
			format: 'yyyy-mm-dd'
		})
   	</script>
</body>
</html>