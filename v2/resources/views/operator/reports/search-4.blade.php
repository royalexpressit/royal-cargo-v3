<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; [Search 4] Report </title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
    
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('reports') }}" class="badge badge-primary text-uppercase">Reports</a> - Search 4, 
											<span class="fs-16">From City: {{ city($from_city)['name'] }} , Inbound Date: {{ $date }}, Filtered: {{ $filtered==1? 'မိမိမြို့တွင်ဝေစာ':'နယ်စာဝင်' }}</span>
										</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
							            	<input type="hidden" id="params" value="{{ $from_city.'&'.$date.'&'.$filtered }}">
							            	<button type="button" class="btn btn-primary btn-bold btn-sm text-upper export-csv"><i class="fa fa-file-excel"></i> Export</button>
                            			</div>
							        </div>
							        
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-solid-warning alert-bold alert-box" role="alert">
		                            <div class="alert-text"> စာရင်းများ မရှိသေးပါ</div>
		                        </div>
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="list-group" id="fetched-data">
									</div>
									<div class="list-group" id="search-data">
									</div>
								</div>
								<div class="pagination">
									<div class="btn-group" role="group" aria-label="Basic example">
										<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
										<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
										<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="json" value="{{ $json }}">
				<input type="hidden" id="from_city" value="{{ $from_city }}">
				<input type="hidden" id="date" value="{{ $date }}">
				<input type="hidden" id="filtered" value="{{ $filtered }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')
	@include('operator.outbound.view-modal')
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 
	@include('operator.reports.modal')
        
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/export.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/O1a1b575a/report-search-4.js') }}" type="text/javascript"></script>
</body>
</html>