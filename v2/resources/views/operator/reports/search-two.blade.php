<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Royal Cargo | Filtered Outbound By Branch</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">	
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Search Outbound, Date: {{ $date }}, 
											<span class="badge badge-sm badge-pill badge-success">{{ $count }}</span>
											<span class="badge badge-pill badge-{{ ($filtered == 0? 'primary':($filtered == 1? 'success':'warning')) }}">{{ ($filtered == 0? 'All Records':($filtered == 1? 'Without Transit':'Only Transit')) }}</span>
										</h3>
				                    </div>
				                    <a href="#" class="btn btn-success btn-bold btn-sm" data-toggle="kt-tooltip" title="" data-placement="top" data-original-title="Excel ထုတ်ရန်"><i class="flaticon2-checking"></i> Export</a>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-4 col-md-6 order-lg-1 order-xl-1">
										<ul class="list-group">
											@foreach($waybills as $key => $waybill)
											<li class="list-group-item">
												<span class="w-200">{{ ++$key }}.<strong>{{ $waybill->waybill_no }}</strong></span>
												<span>
													{{ $waybill->origin }} - {{ $waybill->destination }} 
													@if($filtered == 2)
													<span class="badge badge-sm badge-pill badge-danger">{{ $waybill->transit }}</span>
													@endif
												</span>
												<span class="pull-right">{{ cargo_status($waybill->current_status) }}</span>
											</li>
											@endforeach
										</ul>
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
      

    <script src="{{ asset('assets/js/config.js') }}" type="text/javascript"></script>   
    <script src="{{ asset('assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
</body>
</html>