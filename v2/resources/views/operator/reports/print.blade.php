<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<style type="text/css">
		body{
			font-size: 1.08em;
		}
		
		.box-1{
			padding:2px 10px;
			font-size: 18px;
		}
		.box-2{
			padding:2px 10px;
			font-size: 18px;
			margin-bottom: 6px;
		}
		.box{
			border-top:1px solid #ccc;
			border-bottom:1px solid #ccc;
			border-right:1px solid #ccc;
			background:#f5f5f5;
		}
		.border-2{
			border-bottom:1px solid #ccc;
			border-right:1px solid #ccc;
			padding:4px 2px;
		}
		.border-left{
			border-left:1px solid #ccc;
			padding:4px 2px;
		}
		.border-top{
			border-top:1px solid #ccc;
		}
		.col-2-5{
			width:19.9%;
			background:#ffffff;
		}
		.align-center{
			margin:0 auto;
		}
		.print-container{
			width:100%;
			padding-left: 2px;
		}
		.break-1{
			clear: both;
			width: 100%;
			height: 12px;
		}
		.break-2{
			clear: both;
			width: 100%;
			height: 40px;
		}
		.block-1{
			background:red;
			height:100px;
		}
		.block-2{
			background:blue;
			height:100px;
		}
		.column{
			background: #ddd;
			width:100%;
		}
		.column-1{
			background: red;
			width:20%;
		}
		.column-2{
			background: pink;
			float: left;
			width:20%;
		}
		.bg{
			background: #fefefe;
			width:19.9%;
			float: left;
		}

		li{
			display: block;
			padding:2px 0px;
			border:1px solid #ddd;
			margin:1px;
		}
		.mt-60{
			margin-top: 60px;
		}
		.serial{
			display: inline-block;
			width:40px;
			border-right: 1px solid #ddd;
			padding-left: 1px;
			color:#989494;
		}
		.font-sm{
			font-size: 13px;
		}
	</style>
</head>
<body>
<div class="">

	<div class="row box-1">
	    <div class="col-2 box">
	      <small>Outbound Date</small> <br>
	      {{ $date }}
	    </div>
	    <div class="col-2 box">
	      <small>From - To</small> <br>
	      {{ city($from_city)['shortcode'].' - '.city($to_city)['shortcode'] }}
	    </div>
	    <div class="col-2 box">
	      <small>Total</small> <br>
	      {{ $count }}
	    </div>
	    <div class="col-6 box">
	      <small>Pack</small> <br>
	      {{ $pack }} 
	    </div>
	  </div>
	  <div class="print-container">
		<div class="row align-center">
			
		</div>
	</div>
	<div class="row box-2">
	    <div class="col box">
	      <small>Printed At : {{ date('Y-m-d H:i:s') }}</small> <br>
	      <small>Branch Out By :</small>
	      @foreach($users as $user)
	      	<small> {{ $user->name }} </small>
	      @endforeach
	    </div>
	  </div>
	  <div class="print-container">
		<div class="column">
			@foreach($waybills as $key => $waybill)
				@if($key == 0)
				<div class="bg"> 
			 	@endif

			 	@if($key == 45)
				<div class="bg"> 
			 	@endif

			 	@if($key == 90)
				<div class="bg"> 
			 	@endif

			 	@if($key == 135)
				<div class="bg"> 
			 	@endif

			 	@if($key == 180)
				<div class="bg"> 
			 	@endif

			 	@if($key == 225)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 270)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 315)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 360)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 405)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 450)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 495)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 540)
				<div class="bg mt-60"> 
			 	@endif
			 	
			 	@if($key == 585)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 630)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 675)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 720)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 765)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 810)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 855)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 900)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 945)
				<div class="bg mt-60"> 
			 	@endif

			 	@if($key == 990)
				<div class="bg mt-60"> 
			 	@endif

			 		<li>
			 			<span class="serial">{{ ++$key }}</span>&nbsp;
			 			<span class="{{ strlen($waybill->waybill_no)>11? 'font-sm':'' }}">{{ $waybill->waybill_no }}</span>
			 		</li>
			 	
			 	@if($key == 45)
			 	</div>
			 	@endif

			 	@if($key == 90)
			 	</div>
			 	@endif

			 	@if($key == 135)
			 	</div>
			 	@endif

			 	@if($key == 180)
			 	</div>
			 	@endif

			 	@if($key == 225)
			 	</div>
			 	@endif

			 	@if($key == 270)
			 	</div>
			 	@endif

			 	@if($key == 315)
			 	</div>
			 	@endif

			 	@if($key == 360)
			 	</div>
			 	@endif

			 	@if($key == 405)
			 	</div>
			 	@endif

			 	@if($key == 450)
			 	</div>
			 	@endif

			 	@if($key == 495)
			 	</div>
			 	@endif

			 	@if($key == 540)
			 	</div>
			 	@endif

			 	@if($key == 585)
			 	</div>
			 	@endif

			 	@if($key == 630)
			 	</div>
			 	@endif
			 		
			 	@if($key == 675)
			 	</div>
			 	@endif

			 	@if($key == 720)
			 	</div>
			 	@endif

			 	@if($key == 765)
			 	</div>
			 	@endif

			 	@if($key == 810)
			 	</div>
			 	@endif

			 	@if($key == 855)
			 	</div>
			 	@endif

			 	@if($key == 900)
			 	</div>
			 	@endif

			 	@if($key == 945)
			 	</div>
			 	@endif

			 	@if($key == 990)
			 	</div>
			 	@endif

		  	@endforeach
		</div>
	</div>
</div>
</body>
</html>