<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; [Search 3] Report </title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
    
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('reports') }}" class="badge badge-primary text-uppercase">Reports</a> - Search 3
										</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
							            	<input type="hidden" id="params" value="{{ $to_city.'&'.$date.'&'.$filtered }}">
							            	<button type="button" class="btn btn-primary btn-bold btn-sm text-upper export-csv"><i class="flaticon2-hourglass-1"></i> Export</button>
                            			</div>
							        </div>
							        
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="alert alert-solid-warning alert-bold alert-box" role="alert">
		                            <div class="alert-text"> စာရင်းများ မရှိသေးပါ</div>
		                        </div>
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="list-group" id="fetched-data">
									</div>
									<div class="list-group" id="search-data">
									</div>
								</div>
								<div class="pagination">
									<div class="btn-group" role="group" aria-label="Basic example">
										<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
										<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
										<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
									</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="json" value="{{ $json }}">
				<input type="hidden" id="to_city" value="{{ $to_city }}">
				<input type="hidden" id="date" value="{{ $date }}">
				<input type="hidden" id="filtered" value="{{ $filtered }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')
	@include('operator.outbound.view-modal')
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 
	@include('operator.reports.modal')
        
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/export.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	$(document).ready(function(){
			var url 		= $("#url").val();
			var json    	= $("#json").val();
			var to_city     = $("#to_city").val();
			var date        = $("#date").val();
			var filtered    = $("#filtered").val();

			var params   	= to_city+'&'+date+'&'+filtered;

		   	//declared first loaded json data
			var load_json 	= url+'/'+json+'/'+params;
			var waybills 	= [];
			var item     	= 0;
			var _token   	= $("#_token").val();

			/** first data load function  **/
		    var load_data = function() { 
			    $.ajax({
					url: load_json,
					type: 'get',
					data: {},
					success: function(data){
						if(data.total > 0){
							$(".alert-box").hide();
							$('#fetched-data').empty();
							$.each( data.data, function( key, value ) {
								++item;
								waybills.push(value.waybill_no);
								$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
							    	+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
							    	+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
							    	+ '<small class="fs-15">'+value.action_date+'</small></div>'
							    	+ '<small class="fs-15"> စာပို့သမား/ကောင်တာ - '+value.delivery+' , စာရင်းသွင်းသူ - '+value.collected_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>'
							    );
							});

							//console.log(waybills);
							//$(".pagination").show();
							$("#to-records").text(data.to);
							$("#total-records").text(data.total);
											
							if(data.prev_page_url === null){
								$("#prev-btn").attr('disabled',true);
							}else{
								$("#prev-btn").attr('disabled',false);
							}
							if(data.next_page_url === null){
								$("#next-btn").attr('disabled',true);
							}else{
								$("#next-btn").attr('disabled',false);
							}

							$("#prev-btn").val(data.prev_page_url);
							$("#next-btn").val(data.next_page_url);
						}else{
							$(".pagination").hide();
							$(".alert-box").show();
						}
					}
				});
		    };

		    //called function when page loading
    		load_data();	

		    /** pagination button click event **/
			$('.pagination-btn').click(function(){
				//clicked url json data
				var clicked_url = $(this).val();
				$(this).siblings().removeClass('active')
				$(this).addClass('active');

				$.ajax({
					url: clicked_url,
					type: 'get',
					data: {},
					success: function(data){
						if(data.total > 0){
							$(".alert-box").hide();
							$('#fetched-data').empty();
							$.each( data.data, function( key, value ) {
								++item;
								waybills.push(value.waybill_no);
								$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
							    	+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
							    	+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
							    	+ '<small class="fs-15">'+value.action_date+'</small></div>'
							    	+ '<small class="fs-15"> စာပို့သမား/ကောင်တာ - '+value.delivery+' , စာရင်းသွင်းသူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
							});

							//console.log(waybills);
							//$(".pagination").show();
							$("#to-records").text(data.to);
							$("#total-records").text(data.total);
											
							if(data.prev_page_url === null){
								$("#prev-btn").attr('disabled',true);
							}else{
								$("#prev-btn").attr('disabled',false);
							}
							if(data.next_page_url === null){
								$("#next-btn").attr('disabled',true);
							}else{
								$("#next-btn").attr('disabled',false);
							}

							$("#prev-btn").val(data.prev_page_url);
							$("#next-btn").val(data.next_page_url);
						}else{
							$(".pagination").hide();
							$(".alert-box").show();
						}
					}
				});		
			});

		    /** view waybill detail with modal **/
			$('body').delegate(".load-modal","click",function () {
				var id = $(this).attr('id');
				$.ajax({
					url: url+'/outbound/'+id+'/view',
					type: 'POST',
					data: {
						'id':id,
						'_token': _token
					},
					success: function(data){
						console.log(data);
						$("#waybill_label").text(data.waybill_no);
						$("#outbound_date").text(data.outbound_date);
						$("#from_city").text(data.origin);
						$("#transit_city").text(data.transit);
						$("#to_city").text(data.destination);
						$("#delivery").text('_ '+data.delivery);
						$("#current_status").html(data.current_status);
						$(".view-logs").attr("href",url+'/outbound/view/'+data.id+'/logs')
					}
				});
			});
			

			$('.export-csv').click(function(){
				$('#export-modal').modal({show:true});
				var params = $('#params').val();
				$.ajax({
					url: url+'/export/reports/action-3/',
					type: 'post',
					data: {
						params:params,
						_token:_token
					},
					success: function(data){
						if(data != ''){
							json = JSON.stringify(data);
							$("#raw").val(json);
							$('.confirmed').show();
						}else{
							$('.confirmed').hide();
						}
						
					}

				});
			});

		});
    </script>
</body>
</html>