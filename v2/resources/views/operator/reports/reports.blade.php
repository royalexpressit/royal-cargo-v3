<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Reports</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Reports (စာထွက်)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container kt-grid__item kt-grid__item--fluid">
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">1.Total Outbound By Date</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-1') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာထွက်လုပ်သည့်ရက်စွဲ</label>
															<input type="text" class="form-control" id="kt_datepicker_1" readonly value="{{ date('Y-m-d') }}" name="branch_out_date_1"/>
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_1"  value="1" checked=""> Without Transit
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_1"  value="2"> Transit
																	<span></span>
																</label>
															</div>
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide">ရှာမည်</button>
															<span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="နစ်ခုပေါင်းခြင်း အရေအတွက်သည် စုစုပေါင်းစာထွက်(Outbound) အရေအတွက် နှင့်တူသည်။"><i class="fa fa-info-circle text-info fs-18"></i></span>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
					 				<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-success">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">2.Total Inbound By Date</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-2') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာဝင်လုပ်သည့်ရက်စွဲ</label>
															<input type="text" class="form-control" id="kt_datepicker_2" readonly value="{{ date('Y-m-d') }}" name="branch_in_date_2" />
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--success">
																	<input type="radio" name="filtered_2"  value="1" checked=""> Without Transit
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--success">
																	<input type="radio" name="filtered_2"  value="2"> Transit
																	<span></span>
																</label>
															</div>
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-success btn-wide">ရှာမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
										<!--end::Portlet-->
					 				</div>
								</div>

								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">3.Total Outbound To City & Outbound Date</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-3') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာလက်ခံမည့်မြို့ (Destination)</label>
															<select class="form-control kt-select2" id="to_city3" name="to_city3">
																@foreach(App\City::orderBy('name','asc')->get() as $city)
																<option value="{{ $city->id }}">{{ $city->shortcode.' ('.$city->name.' - '.$city->mm_name.')' }}</option>
																@endforeach
															</select>
														</div>
														<div class="form-group">
															<label class="form-label">စာထွက်သည့်ရက်စွဲ (Outbound Date)</label>
															<input type="text" class="form-control" name="outbound_date3" id="kt_datepicker_3" readonly value="{{ date('Y-m-d') }}"/>
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_3"  value="1" checked=""> မိမိမြို့မှထွက်
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_3"  value="2"> နယ်စာပြန်ထွက်
																	<span></span>
																</label>
															</div>
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide">ရှာမည်</button>
															<span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="မိမိမြို့မှထွက်သောစာ(origin - destination) နှင့် မိမိမြို့သို့တဆင့်ဝင်ပြီး(transit - destination) ပြန်ထွက်သောစာများဖြစ်သည်။" class="fs-18"><i class="fa fa-info-circle text-info"></i></span>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
					 				<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-success">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">4.Total Inbound From City & Inbound Date</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-4') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာဝင်လာသည့်မြို့(origin/transit)</label>
															<select class="form-control kt-select2" id="from_city4" name="from_city4">
																@foreach(App\City::orderBy('name','asc')->get() as $city)
																<option value="{{ $city->id }}">{{ $city->shortcode.' ('.$city->name.' - '.$city->mm_name.')' }}</option>
																@endforeach
															</select>
														</div>
														<div class="form-group">
															<label class="form-label">စာဝင်လုပ်သည့်ရက်စွဲ</label>
															<input type="text" class="form-control" id="kt_datepicker_4" name="inbound_date4" readonly value="{{ date('Y-m-d') }}" />
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_4" value="1" checked=""> မိမိမြို့တွင်ဝေစာ
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_4" value="2"> နယ်စာဝင်
																	<span></span>
																</label>
															</div>
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-success btn-wide">ရှာမည်</button>
															<span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="မိမိမြို့တွင်စာဝေ(origin - destination) နှင့် နယ်စာ(origin - transit) ပြန်ထွက်သောစာများဖြစ်သည်။" class="fs-18"><i class="fa fa-info-circle text-info"></i></span>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
								</div>

								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">5.Outbound By Branch</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-5') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာကောက်ထားသောရုံး</label>
															<select class="form-control kt-select2" id="from_branch5" name="from_branch5">
																@foreach(App\Branch::where('city_id',Auth::user()->city_id)->orderBy('name','asc')->get() as $branch)
																@if(branch($branch->id)['is_sorting'] == 0)
																<option value="{{ $branch->id }}">{{ $branch->name }}</option>
																@endif
																@endforeach
															</select>
														</div>
														<div class="form-group">
															<label class="form-label">စာဝင်လုပ်သည့်ရက်စွဲ</label>
															<input type="text" class="form-control" id="kt_datepicker_5" name="outbound_date5" readonly value="{{ date('Y-m-d') }}"/>
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide">ရှာမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
					 				<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-success">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">6.Inbound Handover</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-6') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">မိမိရုံးမှ စာလွှဲပြောင်းသည့်ရုံး</label>
															<select class="form-control kt-select2" id="to_branch6" name="to_branch6">
																@foreach(App\Branch::where('city_id',Auth::user()->city_id)->orderBy('name','asc')->get() as $branch)
																<option value="{{ $branch->id }}">{{ $branch->name }}</option>
																@endforeach
															</select>
														</div>
														<div class="form-group">
															<label class="form-label">မိမိရုံးမှ စာလွှဲပြောင်းသည့်ရက်စွဲ</label>
															<input type="text" class="form-control" id="kt_datepicker_6" name="handover_date5" readonly value="{{ date('Y-m-d') }}" />
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-success btn-wide">ရှာမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
								</div>
								
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">7.Outbound Handover With Time Slot</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-7')}}" class="kt-form kt-form--fit kt-form--label-right" method="POST">
														<div class="form-group">
															<div class="row">
																<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
																	<label class="form-label">စာလွှဲသည့်ရုံး</label>
																	<select class="form-control kt-select2" id="from_branch_7" name="from_branch_7">
																		@foreach(App\Branch::where('city_id',Auth::user()->city_id)->orderBy('name','asc')->get() as $branch)
																		@if(branch($branch->id)['is_sorting'] != 1)
																		<option value="{{ $branch->id }}">{{ $branch->name }}</option>
																		@endif
																		@endforeach
																	</select>
																</div>
																<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
																	<label class="form-label">စာလွှဲသည့်ရက်စွဲ</label>
																	<input type="text" class="form-control" name="date_7" id="kt_datepicker_7" readonly value="{{ date('Y-m-d') }}"/>
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
																	<label class="form-label">စာလွှဲသည့်အချိန်(အစ)</label>
																	<input class="form-control" name="start_time_7" id="kt_timepicker_71" readonly="readonly" placeholder="Select time" type="text" />
																</div>
																<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
																	<label class="form-label">စာလွှဲသည့်အချိန်(အဆုံး)</label>
																	<input class="form-control" name="end_time_7" id="kt_timepicker_72" readonly="readonly" placeholder="Select time" type="text" />
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--success">
																	<input type="radio" name="filtered_7" checked="" value="1"> ရုံးစာ
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--success">
																	<input type="radio" name="filtered_7"  value="2"> နယ်စာ
																	<span></span>
																</label>
															</div>
															@csrf
															<button type="submit" class="btn btn-primary btn-wide pull-right">ရှာမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
										<!--end::Portlet-->
					 				</div> 				
					 				<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-warning">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">Download Failed Logs(txt files)</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<div class="form-group">
														<a href="{{ url('assets/logs/inbound-branch-in-failed.txt') }}" class="btn btn-success btn-wide pull-right btn-block" download="inbound_branch_in_failed_logs">(Inbound) Branch In Failed Logs</a>
														<a href="{{ url('assets/logs/inbound-handover-failed.txt') }}" class="btn btn-success btn-wide pull-right btn-block" download="inbound_handover_failed_logs">(Inbound) Handover Failed Logs</a>
														<a href="{{ url('assets/logs/inbound-handover-failed.txt') }}" class="btn btn-success btn-wide pull-right btn-block" download="inbound_handover_failed_logs">(Inbound) Received Failed Logs</a>
														<a href="{{ url('assets/logs/outbound-received-failed.txt') }}" class="btn btn-primary btn-wide pull-right btn-block" download="outbound_received_failed_logs">(Outbound) Received Failed Logs</a>
														<a href="{{ url('assets/logs/outbound-branch-out-failed.txt') }}" class="btn btn-primary btn-wide pull-right btn-block" download="outbound_branch_out_failed_logs">(Outbound) Branch Out Failed Logs</a>
													</div>
												</div>
											</div>	 
										</div>
										<!--end::Portlet-->
					 				</div>

					 				<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
					 					<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">8.Print Outbound By City</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('search-8') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာလက်ခံမည့်မြို့ (Destination)</label>
															<select class="form-control kt-select2" id="to_city3" name="to_city8">
																@foreach(App\City::orderBy('name','asc')->get() as $city)
																<option value="{{ $city->id }}">{{ $city->shortcode.' ('.$city->name.' - '.$city->mm_name.')' }}</option>
																@endforeach
															</select>
														</div>
														<div class="form-group">
															<label class="form-label">စာထွက်သည့်ရက်စွဲ (Outbound Date)</label>
															<input type="text" class="form-control" name="outbound_date8" id="kt_datepicker_3" readonly value="{{ date('Y-m-d') }}"/>
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_8"  value="1" checked=""> မိမိမြို့မှထွက်
																	<span></span>
																</label>
																<label class="kt-radio kt-radio--primary">
																	<input type="radio" name="filtered_8"  value="2"> နယ်စာပြန်ထွက်
																	<span></span>
																</label>
															</div>
														</div>
														<div class="form-group">
															<label class="form-label">မှတ်ချက်</label>
															<textarea class="form-control" name="note"></textarea>
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide">ရှာမည်</button>
															<span data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="မိမိမြို့မှထွက်သောစာ(origin - destination) နှင့် မိမိမြို့သို့တဆင့်ဝင်ပြီး(transit - destination) ပြန်ထွက်သောစာများဖြစ်သည်။" class="fs-18"><i class="fa fa-info-circle text-info"></i></span>
														</div>
													</form>
												</div>
											</div>	 
										</div>
									</div>
								</div>
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
				<input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
				<input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->


	@include('operator.quick-panel')

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

                              
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	

    	$('#to_city3,#from_city4').select2({
            placeholder: "Select City"
        });
        $('#from_branch5,#to_branch6,#from_branch_7').select2({
            placeholder: "Select Branch"
        });
        

		$('#kt_datepicker_1,#kt_datepicker_2,#kt_datepicker_3,#kt_datepicker_4,#kt_datepicker_5,#kt_datepicker_6,#kt_datepicker_7,#kt_datepicker_1_validate').datepicker({
			format: 'yyyy-mm-dd'
		});

    	$("#form").submit(function(event){
		    event.preventDefault();  
		});
    </script>
    <script type="text/javascript" src="{{ asset('_assets/js/bootstrap-timepicker.js') }}"></script>
    <script type="text/javascript">
    	 $('#kt_timepicker_71,#kt_timepicker_72, #kt_timepicker_1_modal').timepicker();
    </script>
</body>
</html>