<!-- Start: Export Excel Modal -->
<div class="modal fade" id="export-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header bg-danger">
				<h5 class="modal-title text-white"><i class="fa fa-file-excel"></i> Export Excel File</h5>
				<button type="button" class="close text-white" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<textarea id="raw" class="form-control"></textarea>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-danger confirmed" data-dismiss="modal">Export Excel</button>
			</div>
		</div>
	</div>
</div>
<!-- End: Export Excel Modal -->