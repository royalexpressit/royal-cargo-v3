<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Setting</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		border:1px solid #f5f5f5;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Setting Times</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            

							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
									<!--begin::Portlet-->
									<div class="kt-portlet kt-portlet--height-fluid kt-widget-12 " id="blockui_content_1">
										<div class="kt-portlet__body">
											<div class="kt-widget-12__body">
												<div class="kt-widget-18">
													<form class="kt-form kt-form--fit kt-form--label-right" >
														<div class="form-group">
															<div class="row">
																<div class="col-8">
																	<label class="form-label">စာဝင်သည့်အချိန် သတ်မှတ်မည် (Branch In Times)</label>
																	<input class="form-control fs-18 text-primary h-40" id="branch_in_time" name="branch_in_time"  placeholder="Select time" type="text" value="{{ setup_times(Auth::user()->city_id) }}"/>
																</div>
																<div class="col-4">
																	@if(App\SetupTime::where('city_id',Auth::user()->city_id)->where('action_label','branch-in')->where('active',1)->first())
																	<div class="bg-success time-block times-on " role="alert" style="display: block;">
													                    <div class="alert-text">ဖွင့်ထားသည်</div>
													                </div>
													                <div class="bg-danger time-block times-off" role="alert" style="display: none;">
													                    <div class="alert-text">ပိတ်ထားသည်</div>
													                </div>
																	@else
																	<div class="bg-success time-block times-on" role="alert" style="display: none;">
													                    <div class="alert-text">ဖွင့်ထားသည်</div>
													                </div>
																	<div class="bg-danger time-block times-off" role="alert" style="display: block;">
													                    <div class="alert-text">ပိတ်ထားသည်</div>
													                </div>
																	@endif
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
													            <div class="alert-text fs-16">အထက်ပါ သတ်မှတ်ထားသော အချိန်နောက်ပိုင်း branch-in(စာဝင်) ပါက နောက်တစ်နေ့ နေ့စွဲဖြင့် ဝင်သွားမည်ဖြစ်သည်။</div>
													        </div>
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-3">
																	<span class="kt-switch kt-switch--md kt-switch--outline kt-switch--primary">
																		<label>
																		@if(App\SetupTime::where('city_id',Auth::user()->city_id)->where('action_label','branch-in')->where('active',1)->first())
																		<input type="checkbox" name="active_times" id="active_times" checked="">
																		@else
																		<input type="checkbox" name="active_times" id="active_times" >
																		@endif
																		<span></span>
																		<strong class="switch-label text-success fs-16">ဖွင့်သည်</strong>
																		</label>
																	</span>
																</div>
																<div class="col-9">
																	@csrf
																	<button type="button" class="btn btn-primary btn-wide btn-sm setup-1">ပြင်ဆင်မည်</button>
																</div>
															</div>
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<!--end::Portlet-->

								</div>
								</div>

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	 @include('layouts.widget')   

	 
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>    
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript" src="https://preview.keenthemes.com//keen/themes/keen/theme/demo4/dist/assets/js/pages/components/forms/widgets/bootstrap-timepicker.js"></script>
   	<script type="text/javascript">

	   	"use strict";
		// Class definition

		var KTBootstrapTimepicker = function () {
		    
		    // Private functions
		    var demos = function () {
		        // minimum setup
		        $('#branch_in_time, #kt_timepicker_1_modal').timepicker();

		        // minimum setup
		        $('#kt_timepicker_2, #kt_timepicker_2_modal').timepicker({
		            minuteStep: 1,
		            defaultTime: '',
		            showSeconds: true,
		            showMeridian: false,
		            snapToStep: true
		        });

		        // default time
		        $('#kt_timepicker_3, #kt_timepicker_3_modal').timepicker({
		            defaultTime: '11:45:20 AM',
		            minuteStep: 1,
		            showSeconds: true,
		            showMeridian: true
		        });

		        // default time
		        $('#kt_timepicker_4, #kt_timepicker_4_modal').timepicker({
		            defaultTime: '10:30:20 AM',           
		            minuteStep: 1,
		            showSeconds: true,
		            showMeridian: true
		        });

		        // validation state demos
		        // minimum setup
		        $('#kt_timepicker_1_validate, #kt_timepicker_2_validate, #kt_timepicker_3_validate').timepicker({
		            minuteStep: 1,
		            showSeconds: true,
		            showMeridian: false,
		            snapToStep: true
		        });
		    }

		    return {
		        // public functions
		        init: function() {
		            demos(); 
		        }
		    };
		}();

		jQuery(document).ready(function() {    
		    KTBootstrapTimepicker.init();
		});

   		$(document).ready(function(){
			var url 	= $("#url").val();
			var _token  = $("#_token").val();
			var active 	= '';


			/** first data load function  **/
		    var load_data = function() { 
		    	var branch_in_time 	= $("#branch_in_time").val();
		    	if($("#active_times").prop("checked") == true){
			       active = 1;
			    }else{
			       active = 0;
			    }

			    $.ajax({
					url: url+'/setting/setup-times',
					type: 'POST',
					data: {
						time:branch_in_time,
						active:active,
						_token:_token
					},
					success: function(data){
						KTApp.block('#blockui_content_1', {
                            overlayColor: '#000000',
                            type: 'v2',
                            state: 'danger',
                            message: 'လုပ်ဆောင်နေပါသည် ...'
                        });

						if(data.success == 1){
							console.log(data.time);
							$("#branch_in_time").val(data.time);
							if(data.active == 1){
								$('.times-on').show();
								$('.times-off').hide();
								$('.active-time-updated').show();
							}else{
								$('.times-on').hide();
								$('.times-off').show();
								$('.active-time-updated').hide();
							}
							setTimeout(function() {
                                KTApp.unblock('#blockui_content_1');
                            }, 1000);
						}
		   			}
		   		});
		   	};


		   	/** pagination button click event **/
			$('.setup-1').click(function(){
				load_data();
			});
		});
   		
   	</script>
</body>
</html>