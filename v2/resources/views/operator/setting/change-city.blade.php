<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; [Change Branch] Setting</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		border:1px solid #f5f5f5;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Change To City & Branch</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            

							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
									<!--begin::Portlet-->
									<div class="kt-portlet kt-portlet--height-fluid kt-widget-12 " id="blockui_content_1">
										<div class="kt-portlet__body">
											<div class="kt-widget-12__body">
												<div class="kt-widget-18">
													<form class="kt-form kt-form--fit kt-form--label-right" >
														<div class="form-group">
															<label class="form-label">Current City & Branch</label>
															<input class="form-control fs-18 text-primary h-40" type="text" value="{{ branch(Auth::user()->branch_id)['name'].' ('.city(Auth::user()->city_id)['shortcode'].')' }}" disabled="" />
														</div>
														<div class="form-group">
															<label class="form-label">Change City & Branch</label>
															<select class="form-control kt-select2" id="branch" name="branch">
																@foreach(App\Branch::orderBy('name','asc')->get() as $branch)
																<option value="{{ $branch->id }}">{{ $branch->name .' ('.city($branch->city_id)['shortcode'].' - '.city($branch->city_id)['name'].')' }}{{ $branch->is_main_office==1 ? '*':'' }}</option>
																@endforeach
															</select>
														</div>
														<div class="form-group">
															@csrf
															<input type="hidden" id="current_branch" value="{{ Auth::user()->branch_id }}">
															<button type="button" class="btn btn-primary btn-wide btn-sm setup-city">ပြင်ဆင်မည်</button>	
														</div>
													</form>
												</div>
											</div>
										</div>
									</div>
									<!--end::Portlet-->

								</div>
								</div>

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	 @include('layouts.widget') 
	 
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>    
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript" src="https://preview.keenthemes.com//keen/themes/keen/theme/demo4/dist/assets/js/pages/components/forms/widgets/bootstrap-timepicker.js"></script>
   	<script type="text/javascript">
   		var current_branch = $('#current_branch').val();
   		$("#branch").val(current_branch).change();

   		// basic
        $('#branch, #branch_validate').select2({
            placeholder: "Select a state"
        });



   		$(document).ready(function(){
			var url 	= $("#url").val();
			var _token  = $("#_token").val();


			/** first data load function  **/
		    var load_data = function() { 
				var branch = $('#branch').val();
		    	
			    $.ajax({
					url: url+'/setting/change-city',
					type: 'POST',
					data: {
						branch:branch,
						_token:_token
					},
					success: function(data){
						if(data.success == 1){
							console.log(data);
							location.reload();
						}
		   			}
		   		});
		   	};

		   	/** pagination button click event **/
			$('.setup-city').click(function(){
				load_data();
			});
		});
   		
   	</script>
</body>
</html>