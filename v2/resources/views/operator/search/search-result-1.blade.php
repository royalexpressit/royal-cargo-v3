<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; {{ $title }}</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    	.date{
    		display: block;
    	}
    	.label{
    		padding: 6px 0px 0px 4px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">	
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('advanced-search') }}" class="badge badge-primary text-uppercase">Advanced Search</a>
											- Search Waybill
										</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4 col-md-8 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">Waybill Details</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													@if(!empty($waybill))
													<div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Waybill No:</span>
										                <div class="kt-widget-1__item-desc"><span class="text-danger">{{ $waybill->waybill_no }}</span></div>
										            </div>
										            <hr>
										            <div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Origin(စာထွက်မြို့)</span>
										                <div class="kt-widget-1__item-desc">
										                	<span class="text-danger">- {{ city($waybill->origin)['name'].' ('.city($waybill->origin)['shortcode'].')' }} </span>
										                	<span class="text-danger">- {{ $waybill->outbound_date }}</span>
										                </div>
										            </div>
										            <hr>
										            <div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Transit(နယ်စာဝင်မြို့)</span>
										                <div class="kt-widget-1__item-desc">
										                	<span class="text-danger">- {{ city($waybill->transit)['name'].' ('.city($waybill->transit)['shortcode'].')' }} </span>
										                	<span class="text-danger date">- {{ $waybill->transit_date }}</span>
										                </div>
										            </div>
										            <hr>
										            <div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Destination(စာလက်ခံမြို့)</span>
										                <div class="kt-widget-1__item-desc">
										                	<span class="text-danger">- {{ city($waybill->destination)['name'].' ('.city($waybill->destination)['shortcode'].')' }}</span>
										                	<span class="text-danger date">-{{ $waybill->inbound_date }}</span>
										                </div>
										            </div>
										            <hr>
										            <div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Delivery/Counter()</span>
										                <div class="kt-widget-1__item-desc">
										                	<span class="text-danger date">- {{ user($waybill->user_id)['name'] }}</span>
										                </div>
										            </div>
										            <hr>
										            <div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Current Status(လက်ရှိအခြေအနေ)</span>
										                <div class="kt-widget-1__item-desc">
										                	<span class="text-danger">- {{ $waybill->current_status }}</span>
										                </div>
										            </div>
										            <hr>
										            <div class="kt-widget-1__item-info">
										                <span class="kt-widget-1__item-title text-primary">Transit Status(နယ်စာဝင်အခြေအနေ)</span>
										                <div class="kt-widget-1__item-desc">
										                	<span class="text-danger">- {{ $waybill->transit_status }}</span>
										                </div>
										            </div>
										            @else
										            	No waybill found
										            @endif
												</div>
											</div>	 
										</div>
										<!--end::Portlet-->
									</div>
									<div class="col-lg-8 col-xl-8 col-md-8 order-lg-1 order-xl-1">
										
											<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">Branch Action & Logs 
										            	<span class="pull-right"></span>
										            </h3>									
										        </div>
										    </div>

										    <div class="kt-none">
										    	@if(!empty($branch_logs))
											    	<h5 class="label">Branch Logs:</h5>
											    	@foreach($branch_logs as $branch_log)
											        <div class="kt-widget-1 outbound-switch">
											        	<div class="kt-widget-1__item widget-item border-1">
											                <div class="kt-widget-1__item-info">
											                    <span class="kt-widget-1__item-title text-primary">
											                        {{ $branch_log->action_type }}, {{ $branch_log->created_at }}
											                    </span>
											                    <div class="kt-widget-1__item-desc">
											                    	<span class="text-danger">
											                    		{{ branch($branch_log->from_branch)['name'] }}
											                    		-
											                    		{{ branch($branch_log->to_branch)['name'] }}
											                    	</span>
											                    </div>
											                </div>
											            </div>
											        </div>
											        @endforeach
											    @else
													<span class="text-danger">Not found branch logs.</span>
												@endif
												<div>&nbsp;</div>
												@if(!empty($logs))
											    	<h5 class="label">Action Logs:</h5>
											    	@foreach($logs as $log)
											        <div class="kt-widget-1 outbound-switch">
											        	<div class="kt-widget-1__item widget-item border-1">
											                <div class="kt-widget-1__item-info">
											                    <span class="kt-widget-1__item-title text-primary">
											                        {{ $log->action_type }}, {{ $log->action_date }}
											                    </span>
											                    <div class="kt-widget-1__item-desc">
											                    	<span class="text-danger">
											                    		{{ city($log->city_id)['name'] }}: {{ $log->action_log }}
											                    	</span>
											                    </div>
											                </div>
											            </div>
											        </div>
											        @endforeach
											    @else
													<span class="text-danger">Not found waybill logs.</span>
												@endif
										    </div>
										</div>
										
									</div>
								</div>
								<!--end::Row--> 
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
      

    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>   
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
</body>
</html>