<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Advanced Search</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.scan-btn{
    		display: none;
    		margin-top:10px;
    	}
    	.hide{
    		display: none;
    	}
    	.fs-20{
    		font-size: 20px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	


				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Reports (စာထွက်)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-danger">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">1.[Waybill] Search with single waybill</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('advanced-search-1') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာအမှတ် (eg. C2345670123)</label>
															<input type="text" class="form-control" name="waybill_no" placeholder="C1234567890" />
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-danger btn-wide">ရှာမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
					 				<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-danger">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">2.[Waybill] Search with serial number </h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('advanced-search-2') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">စာအမှတ် အုပ်စုနဲ့ရှာမယ်(eg. C23456)</label>
															<input type="text" class="form-control" name="serial_no" placeholder="C12345" />
														</div>
														<div class="form-group">
															@csrf
															<button type="submit" class="btn btn-danger btn-wide">ရှာမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
										<!--end::Portlet-->
					 				</div>
								</div>
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
				<input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
				<input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->


	@include('operator.quick-panel')

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

                              
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	

    	$('#to_city,#from_city').select2({
            placeholder: "Select City"
        });
        $('#to_branch,#from_branch').select2({
            placeholder: "Select Branch"
        });
        

		$('#kt_datepicker_1, #kt_datepicker_1_validate').datepicker({
			format: 'yyyy-mm-dd'
		});

    	$("#form").submit(function(event){
		    event.preventDefault();  
		});

    </script>
    <script type="text/javascript" src="https://preview.keenthemes.com//keen/themes/keen/theme/demo4/dist/assets/js/pages/components/forms/widgets/bootstrap-datepicker.js"></script>
</body>
</html>