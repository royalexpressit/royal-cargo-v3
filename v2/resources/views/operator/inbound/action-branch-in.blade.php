<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Inbound [Scan Branch In]</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
        .scan-btn{
            display: none;
            margin-top:10px;
        }
        marquee{
            width:400px;
            font-size: 16px;
            color: #ee1c24;
        }
        .times{
            height:80px;
            width:80px;
            background:url('{{ asset("_assets/5sec.gif") }}');
            background-size: cover;
            background-position: left;
            background-repeat: no-repeat;
        }
        .continue-action{
            display: none;
        }
        .continue-action{
            margin-top:10px;
        }
        .invalid-no{

        }
        .scanned-panel{
            height: 460px;
            overflow-y: auto;
        }
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading" id="blockui_content_1">
    <!-- begin::Page loader -->
    @include('layouts.loading')
    <!-- end::Page Loader -->  

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile " >
        <div class="kt-header-mobile__logo">
            <a href="{{ url('') }}">
                <img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->

    <!-- begin:: Page -->
    <div class="kt-grid kt-grid--hor kt-grid--root">
        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
            <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
                <!-- header -->
                @include('operator.header')
                <!-- header --> 

                <!-- begin:: Container -->
                <div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
                    <div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
                        <div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">        
                            <!-- begin:: Subheader -->
                            <div class="kt-subheader kt-grid__item" >
                                <div class="kt-container ">
                                    <div class="kt-subheader__main">
                                        @if(check_active_time(Auth::user()->city_id) == 1)
                                        <div class="row">
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <h3 class="kt-subheader__title">Inbound (စာဝင်) </h3>
                                                <span class="kt-subheader__separator kt-hidden"></span>
                                            </div>
                                            <div class="col-lg-6 col-md-6 col-sm-12">
                                                <marquee>
                                                    {{ time_to_string(setup_times(Auth::user()->city_id)) }} နောက်ပိုင်းစာဝင်လုပ်ဆောင်ပါက မနက်ဖြန်ရက်စွဲဖြင့် စာဝင်သွားမည်ဖြစ်သည်။ 
                                                    ပြန်လည်ပြင်ဆင်လိုပါက <code class="text-primary fs-16">setting > setting times</code> ထဲတွင် ပြန်လည်ပြင်ဆင်နိုင်ပါသည်။ 
                                                </marquee>
                                            </div>
                                        </div>
                                        @else
                                        <h3 class="kt-subheader__title">Inbound (စာဝင်) </h3>
                                        <span class="kt-subheader__separator kt-hidden"></span>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <!-- end:: Subheader -->

                            <!-- begin:: Content -->
                            <div class="kt-container  kt-grid__item kt-grid__item--fluid">
                                <!--begin::Portlet-->
                                <div class="kt-portlet">
                                    <div class="kt-portlet__head">
                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title">
                                                <strong class="badge badge-success text-uppercase">Inbound</strong> - Scan Branch In Wabyill (စာဝင် - စာဝင်စာရင်းသွင်းရန်) 
                                            </h3>
                                        </div>
                                    </div>
                                    <!--begin::Form-->
                                    <form id="form" class="kt-form kt-form--fit kt-form--label-right">
                                        <div class="kt-portlet__body">
                                            <div class="row">
                                                <div class="col-lg-3 col-md-6 col-sm-12">
                                                    <div class="card action-received show">
                                                        <div class="card-header bg-success text-white fs-16">စာလက်ခံ လုပ်ဆောင်ရန်</div>
                                                        <div class="card-body">
                                                            @if(branch(Auth::user()->branch_id)['is_main_office'] == 1)
                                                            <a href="{{ url('scan/inbound/branch-in') }}" class="btn btn-danger text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Branch In 
                                                                @if(check_active_time(Auth::user()->city_id) == 1)
                                                                <i class="flaticon-time-1 faa-ring animated right text-white mt--4 fs-22"></i>
                                                                @endif
                                                            </a>
                                                            @else
                                                            <span class="btn btn-danger text-left btn-block mb-2 disabled"><i class="fa fa-qrcode"></i> Scan Branch In</span>
                                                            @endif
                                                            <a href="{{ url('scan/inbound/handover') }}" class="btn btn-warning text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Handover</a>
                                                            <a href="{{ url('scan/inbound/received') }}" class="btn btn-success text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Received</a>
                                                        </div>
                                                    </div>
                                                    <div class="card action-postponed hide">
                                                        <div class="card-header bg-danger text-white fs-16">စာဆိုင်းငံ့/ငြင်းပယ် လုပ်ဆောင်ရန်</div>
                                                        <div class="card-body">
                                                            <a href="{{ url('scan/inbound/postponed') }}" class="btn btn-warning text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Postponed</a>
                                                            <a href="{{ url('scan/inbound/rejected') }}" class="btn btn-danger text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Rejected</a>
                                                            <a href="{{ url('scan/inbound/transferred') }}" class="btn btn-primary text-left btn-block mb-2"><i class="fa fa-qrcode"></i> Scan Transferred</a>
                                                        </div>
                                                    </div>
                                                    <span class="kt-switch kt-switch--sm kt-switch--outline kt-switch--primary mt-10">
                                                        <label>
                                                            <input type="checkbox" id="switch-postponed">
                                                            <span></span>
                                                            <strong class="switch-label switch-lbl text-primary">Switch Postponed</strong>
                                                        </label>
                                                    </span>
                                                </div>
                                                <div class="col-lg-4 col-md-6 col-sm-12">
                                                    <div class="form-group">
                                                        <label class="form-label">From City (စာဝင်လာသည့်မြို့)</label>
                                                        <select class="form-control kt-select2" id="from_city" name="from_city">
                                                            @foreach(App\City::orderBy('name','asc')->get() as $city)
                                                            <option value="{{ $city->id }}">{{ $city->shortcode.' - '.$city->name.' ('.$city->mm_name.')' }}</option>
                                                            @endforeach
                                                        </select>
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="form-label">Waybill No (စာအမှတ်)<strong class="text-danger check-number hide">(နံပါတ်မှားနေသည်)</strong></label>
                                                        <input type="text" id="waybill" class="form-control" placeholder="C2103477890" autofocus="">
                                                    </div>
                                                    <div class="form-group">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <button type="button" class="btn btn-primary btn-wide scan-btn">စာရင်းသွင်းမည်</button>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <input type="text" class="inline-input form-control continue-action" id="continue-action" placeholder="Scan Submit QR Code">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <textarea class="form-control hide" id="multi_scanned_waybills"></textarea>
                                                </div>
                                                <div class="col-lg-5 col-md-6 col-sm-12">
                                                    <h5 class="kt-portlet__head-title">Scanned <span id="scanned" class="text-primary">0</span>,<span class="success-lbl">Success <span id="success" class="text-success">0</span></span>,<span class="failed-lbl">Failed <span id="failed" class="text-danger">0</span></span> <span class="btn-alert pull-right text-warning set-voice"><i class="flaticon2-notification"></i></span></h5>
                                                    <ul class="list-group" id="scanned-lists">
                                                        
                                                    </ul>
                                                    <ul class="list-group" id="failed-lists">
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                    <!--end::Form-->
                                </div>
                                <!--end::Portlet-->
                            </div> 
                            <!-- begin:: Content -->                        
                        </div>
                    </div>
                </div>
                <!-- end:: Container -->

                <!-- begin:: Footer -->
                @include('layouts.footer')
                <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
                <input type="hidden" id="city" value="{{ city(Auth::user()->city_id)['shortcode'] }}">
                <input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
                @if(check_active_time(Auth::user()->city_id) == 1)
                <input type="hidden" id="active_time" value="1">
                @else
                <input type="hidden" id="active_time" value="0">
                @endif
                <!-- end:: Footer -->           
            </div>
        </div>
    </div>
    <!-- end:: Page -->

    @include('operator.quick-panel')
     
    <!-- begin::Scrolltop -->
    <div id="kt_scrolltop" class="kt-scrolltop">
        <i class="fa fa-arrow-up"></i>
    </div>
    <!-- end::Scrolltop -->

    @include('layouts.widget')                            
    @include('layouts.action-modal')     
    
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/O1a1b575a/inbound-branch-in.js') }}" type="text/javascript"></script>
</body>
</html>