<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Coming Waybills</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.coming{
    		margin:0;
    		padding:4px !important;
    	}
    	.coming-city-list{
    		background: #f5f5f5;
		    padding: 4px 10px;
		    border: 1px dashed #e6e6f5;
		    margin-bottom: 4px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Inbound - Coming Waybills</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
            							<div class="kt-subheader__wrapper">
                
                            			</div>
        							</div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head badge-primary">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">မိမိမြို့သို့ တိုက်ရိုက်စာဝင်လာမည့်မြို့များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar text-white">
										            <div class="kt-portlet__head-actions">
										                စာဝင်မည့်မြို့ - <strong class="text-upper">{{ city($city_id)['name'] }}</strong>
										            </div>

										        </div>
										    </div>
										    <div class="kt-portlet__body coming">
										        <div class="kt-widget-4">
										        	<div class="list-group" id="fetched-data">
													
													</div>
										        </div>
										        &nbsp;
										        <div class="pagination">
										            <div class="btn-group" role="group" aria-label="Basic example">
														<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
														<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
														<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
													</div>
												</div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>

									<div class="col-lg-6 col-xl-6 order-lg-1 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid">
										    <div class="kt-portlet__head bg-danger">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title text-white">မိမိမြို့သို့ Transit စာဝင်လာမည့်မြို့များ</h3>
										        </div>
										        <div class="kt-portlet__head-toolbar text-white">
										            <div class="kt-portlet__head-actions">
										                စာဝင်မည့်မြို့ - <strong class="text-upper">{{ city($city_id)['name'] }}</strong>
										            </div>
										        </div>
										    </div>
										    <div class="kt-portlet__body coming">
										        <div class="kt-widget-4">
										        	<div class="list-group" id="fetched-data2">
													
													</div>
										            <br>
										            <div class="pagination2">
														<div class="btn-group" role="group" aria-label="Basic example">
															<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
															<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
															<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
														</div>
													</div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->
									</div>
								</div>
								<!--end::Row--> 

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="json" value="{{ $json }}">
				<input type="hidden" id="transit" value="{{ $transit }}">
			
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->



     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->
    
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/O1a1b575a/coming-waybills.js') }}" type="text/javascript"></script>
</body>
</html>