<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; To Receive Inbound</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											<a href="{{ url('home') }}" class="badge badge-info text-uppercase">Dashboard</a>
											<strong class="badge badge-success text-uppercase">Inbound</strong> - To Receive Waybills By Branches (စာဝင်လက်ခံရန် ကျန်စာရင်းများ)
										</h3>
				                    </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										@if(!$branches->isEmpty())
										<!--begin::Portlet-->
										<div class="accordion accordion-outline" id="accordionExample3">
											@foreach($branches as $key => $branch)
											<div class="card">
												<div class="card-header" id="headingOne3">
													<div class="card-title {{ ($key == 0 ? '':'collapsed')}}" data-toggle="collapse" data-target="#collapseOne{{ $branch->to_branch }}" aria-expanded="true" aria-controls="collapseOne3">
														{{ branch($branch->to_branch)['name'] }} ({{ $branch->total }})
													</div>
												</div>
												<div id="collapseOne{{ $branch->to_branch }}" class="card-body-wrapper collapse {{ ($key == 0 ? 'show':'')}}" aria-labelledby="headingOne3" data-parent="#accordionExample3">
													<div class="card-body">
														<ul class="list-group">
															@foreach(to_receive_inbound_waybills($branch->to_branch) as $waybill)
															<li class="list-group-item fs-14"><i class="fa fa-qrcode"></i> <span class="text-danger fs-16">{{ $waybill->waybill_no }}</span> 
																<em class="fs-14">(Handover By: {{ user($waybill->action_by)['name'] }})</em> 
																<span class="pull-right">{{ $waybill->action_date }}</span>
															</li>
															@endforeach
														</ul>
													</div>
												</div>
											</div>
											@endforeach
										</div>
										            
										@else
											<div class="alert alert-solid-warning alert-bold alert-box" role="alert" style="display: block;">
												<div class="alert-text">စာလွှဲပြောင်း စာရင်းများ မရှိသေးပါ</div>
											</div>
										@endif
									</div>
								</div>
							</div>
						</div>
					</div>
 				</div>						
			
				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
        

    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>   
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		/*
   		$("#inbound-transit").change(function(){
		 	if($(this).prop("checked") == true){
		       //run code
		       $(".inbound-item").hide();
		       $(".inbound-transit").show();
		    }else{
		       $(".inbound-transit").hide();
		       $(".inbound-item").show();
		    }
   		});
   		*/

   		
   		$("#inbound-transit").change(function(){
		 	if($(this).prop("checked") == true){
		       //run code
		       $(".inbound-switch").hide();
		       $(".inbound-transit-switch").show();
		    }else{
		       $(".inbound-transit-switch").hide();
		       $(".inbound-switch").show();
		    }
   		});

   		$("#outbound-transit").change(function(){
		 	if($(this).prop("checked") == true){
		       //run code
		       $(".outbound-switch").hide();
		       $(".outbound-transit-switch").show();
		    }else{
		       $(".outbound-transit-switch").hide();
		       $(".outbound-switch").show();
		    }
   		});

   		$(".filtered-outbound-by-branch").click(function(){
   			$('.removed-outbound-filtered').removeClass('hide');
		 	var branch = $(this).attr('id');
		 	$('.branch').hide();
		 	$('.branch-'+branch).show();
		 	$('.checked-item').addClass('hide');
		 	$('.checked-item-'+branch).removeClass('hide');
   		});

   		$(".removed-outbound-filtered").click(function(){
   			$('.removed-outbound-filtered').addClass('hide');
   			$('.branch').show();
   		});
   		
   	</script>
</body>
</html>