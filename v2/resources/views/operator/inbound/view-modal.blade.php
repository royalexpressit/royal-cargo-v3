<!-- View Cargo Modal -->
<div class="modal fade" id="exampleModalLong" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
	<div class="modal-dialog modal-dialog-scrollable" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLongTitle">View Waybill #<span class="text-danger fs-18" id="waybill_label"></span></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<div class="kt-list kt-list--badge">
					<div class="kt-list__item">
						<span class="kt-list__badge"></span>
						<span class="kt-list__icon"><i class="flaticon2-browser-2 kt-font-focus"></i></span>
						<span class="kt-list__text"><span id="action_date" class="text-primary fs-16"></span></span>
						<span class="kt-list__time"><span id="current_status"></span></span>
					</div>
					<div class="kt-list__item">
						<span class="kt-list__badge kt-font-danger"></span>
						<span class="kt-list__icon"><i class="flaticon-placeholder-3 kt-font-primary"></i></span>
						<span class="kt-list__text">From City(မြို့မှ) <br> <strong id="from_city" class="text-primary fs-18"></strong></span>
						<span class="kt-list__time"></span>
					</div>
					<div class="kt-list__item">
						<span class="kt-list__badge"></span>
						<span class="kt-list__icon"><i class="flaticon-placeholder-3 kt-font-danger"></i></span>
						<span class="kt-list__text">Transit City(မြို့မှ	တဆင့်) <br> <strong id="transit_city" class="text-danger fs-18"></strong></span>
						<span class="kt-list__time"><span id="transit_status"></span></span>
					</div>
					<div class="kt-list__item">
						<span class="kt-list__badge"></span>
						<span class="kt-list__icon"><i class="flaticon-placeholder-3 kt-font-success"></i></span>
						<span class="kt-list__text">To City(မြို့သို့) <br> <strong id="to_city" class="text-success fs-18"></strong></span>
						<span class="kt-list__time"></span>
					</div>
					<div class="kt-list__item kt-font-info">
						<span class="kt-list__badge"></span>
						<span class="kt-list__icon"><i class="flaticon2-user kt-font-focus"></i></span>
						<span class="kt-list__text">Branch In By <br> <span id="branch_in_by"></span></span>
						<span class="kt-list__time"></span>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<a href="" class="btn btn-primary btn-sm view-logs">View Logs</a>
				<button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Close</button>
			</div>
		</div>
	</div>
</div>
<!-- View Cargo Modal -->