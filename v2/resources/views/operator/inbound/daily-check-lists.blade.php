<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Inbound [Daily Check Lists]</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.times{
            height:80px;
            width:80px;
            background:url('{{ asset("_assets/5sec.gif") }}');
            background-size: cover;
            background-position: left;
            background-repeat: no-repeat;
        }
        .continue-action{
        	display: none;
        }
        .continue-action{
        	margin-top:10px;
        }
        .invalid-no{

        }
        .scanned-panel{
        	height: 460px;
        	overflow-y: auto;
        }
        .awesome img{
        	//height:200px;
        }
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading" id="blockui_content_1">
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  
	
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Inbound (စာဝင်)</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<!--begin::Portlet-->
								<div class="kt-portlet">
									<div class="kt-portlet__head">
										<div class="kt-portlet__head-label">
											<h3 class="kt-portlet__head-title">
												<strong class="badge badge-success text-uppercase">Inbound</strong> - Inbound Wabyill (စာဝင် - စာရင်းများ) 
											</h3>
										</div>
									</div>
									<!--begin::Form-->
									<form id="form" class="kt-form kt-form--fit kt-form--label-right">
										<div class="kt-portlet__body">
											<div class="row">
												<div class="col-lg-3 col-md-8 col-sm-12">
													<div class="card">
	                                                    <div class="card-header bg-info text-white fs-16">မနေ့ကစာဝင်အခြေအနေ ({{ $response['total'] }})</div>
	                                                    <div class="card-body">
	                                                    	<button class="btn btn-warning text-left btn-block mb-2 check" value="daily-check-lists/inbound/handover" disabled=""><i class="fa fa-bars"></i> Handover <span class="pull-right">{{ $response['handover'] }}</span></button>
															<button class="btn btn-success text-left btn-block mb-2 check" value="daily-check-lists/inbound/received"><i class="fa fa-bars"></i> Received <span class="pull-right">{{ $response['received'] }}</span></button>
														</div>
	                                                </div>
												</div>
												<div class="col-lg-9 col-md-8 col-sm-12">
													<h3>မနေ့က စာဝင်စာရင်းများ</h3>
													<div class="alert alert-solid-warning alert-bold alert-box" role="alert">
							                            <div class="alert-text">စာရင်းများ မရှိပါ</div>
							                        </div>
													<div class="kt-portlet">
														<div class="list-group" id="fetched-data">
														</div>
													</div>
													<div class="pagination">
														<div class="btn-group" role="group" aria-label="Basic example">
															<button type="button" id="prev-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-left"></i></button>
															<button type="button" id="next-btn" class="btn btn-secondary pagination-btn"><i class="la la-caret-right"></i></button>
															<button type="button" class="btn btn-secondary">Records: <span id="to-records"></span> of <span id="total-records"></span> </button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
									<!--end::Form-->
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
				<input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
				<input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
				<input type="hidden" id="handover_branch_id" value="{{ main_office(Auth::user()->city_id) }}">
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')

    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')   
	@include('layouts.action-modal')
	@include('operator.inbound.view-modal')                   	     
        
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	var url 		= $("#url").val();
    	var load_json 	= url+'/daily-check-lists/inbound/handover';
    	var waybills 	= [];
		var item     	= 0;
		var _token   	= $("#_token").val();

    	/** pagination button click event **/
		$('.check').click(function(){
			var load_json = $(this).val();
			$('.check').attr('disabled',false);
			$(this).attr('disabled',true);
			var json = url+'/'+load_json;
			load_data(json);
		});

    	/** first data load function  **/
    	var load_data = function(json) { 
		    $.ajax({
				url: json,
				type: 'GET',
				data: {},
				success: function(data){
					if(data.total > 0){
						$(".alert-box").hide();
						$('#fetched-data').empty();
						$.each( data.data, function( key, value ) {
							++item;
							waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
						    	+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
						    	+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
						    	+ '<small class="fs-15">'+value.inbound_date+'</small></div>'
						    	+ '<small class="fs-15">လုပ်ဆောင်သူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+value.current_status+' pull-right">'+cargo_status(value.current_status)+'</span></a>'
						    );
						});

						//console.log(waybills);
						//$(".pagination").show();
						$("#to-records").text(data.to);
						$("#total-records").text(data.total);
										
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}

						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
					}else{
						$('#fetched-data').empty();
						$(".pagination").hide();
						$(".alert-box").show();
					}
				}
			});
	    };

    	/** load pagination function **/
    	$('.pagination-btn').click(function(){
	    	//clicked url json data
			var clicked_url = $(this).val();
					

			$(this).siblings().removeClass('active')
			$(this).addClass('active');
			
			$.ajax({
				url: clicked_url,
				type: 'GET',
				data: {},
				success: function(data){
					//console.log(data);
					$("#fetched-data").empty();
					$.each( data.data, function( key, value ) {
						++item;
						waybills.push(value.waybill_no);
						$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
							+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
							+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
							+ '<small class="fs-15">'+value.inbound_date+'</small></div>'
							+ '<small class="fs-15">လုပ်ဆောင်သူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+value.current_status+' pull-right">'+cargo_status(value.current_status)+'</span></a>'
						);
					});	

							
					$("#to-records").text(data.to);
					if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
					}
			});
    	});


    	//called function when page loading
    	load_data(load_json);	
			
    	

		/** view waybill detail with modal **/
		$('body').delegate(".load-modal","click",function () {
		var id = $(this).attr('id');
				
		$.ajax({
			url: url+'/inbound/'+id+'/view',
			type: 'POST',
			data: {
				'id':id,
				'_token': _token
			},
			success: function(data){
				$("#waybill_label").text(data.waybill_no);
				$("#action_date").text(data.action_date);
				$("#from_city").text('_ '+data.origin);
				$("#transit_city").text('_ '+data.transit);
				$("#to_city").text('_ '+data.destination);
				$("#branch_in_by").text('_ '+data.branch_in_by);
				$("#current_status").html(data.current_status);
				$(".view-logs").attr("href",url+'/inbound/view/'+data.id+'/logs')	
			}
		});		
	});
    </script>
</body>
</html>