<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Inbound [Group by receivers]</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.alert-box{
    		display: none;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
    @include('layouts.loading')
    <!-- end::Page Loader --> 

    <!-- begin:: Header Mobile -->
    <div id="kt_header_mobile" class="kt-header-mobile " >
        <div class="kt-header-mobile__logo">
            <a href="{{ url('') }}">
                <img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
            </a>
        </div>
        <div class="kt-header-mobile__toolbar">
            <button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
            <button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
        </div>
    </div>
    <!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							
							<!-- begin:: Subheader -->
                            <div class="kt-subheader kt-grid__item" id="kt_subheader">
                                <div class="kt-container ">
                                    <div class="kt-subheader__main">
                                        <h3 class="kt-subheader__title"><strong class="badge badge-success text-uppercase">Inbound</strong> - Grouped by receivers (စာဝင် - စာလက်ခံ စာရင်းများ) </h3>
                                        <span class="kt-subheader__separator kt-hidden"></span>

                                    </div>
                                    <div>
							        	စုစုပေါင်း <span class="badge badge-primary badge-pill">{{ inbound_handover_count_by_branch($branch_id)['total'] }}</span>,
										လက်ခံရန်ကျန် <span class="badge badge-warning badge-pill">{{ inbound_handover_count_by_branch($branch_id)['remain'] }}</span>,
										လက်ခံပြီး <span class="badge badge-success badge-pill">{{ inbound_handover_count_by_branch($branch_id)['received'] }}</span>
							        </div>
                                </div>
                            </div>
                            <!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-grid__item kt-grid__item--fluid">
								<div class="row">
									<div class="col-lg-7 col-xl-7 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title">Receiver Lists of <span class="text-danger">{{ branch($branch_id)['name'] }}</span></h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            <div class="kt-portlet__head-actions">
										                
										            </div>
										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="kt-widget-4">
										        	@foreach($receivers as $receiver)
										            <div class="kt-widget-4__item">
										                <div class="kt-widget-4__item-content">
										                    <div class="kt-widget-4__item-section">
										                        <div class="kt-widget-4__item-pic">
										                            <img class="" src="{{ asset('assets/images/users/'.user($receiver->action_by)['image']) }}" alt="">
										                        </div>
										                        <div class="kt-widget-4__item-info">
										                            <a href="{{ url('inbound/received-by/'.$receiver->action_by) }}" class="kt-widget-4__item-username fs-18">{{ user($receiver->action_by)['name'] }}</a>
										                            <div class="kt-widget-4__item-desc">{{ user_role(user($receiver->action_by)['role']) }}</div>
										                        </div>
										                    </div>
										                </div>
										                <div class="kt-widget-4__item-content">
										                    <div class="kt-widget-4__item-price">
										                        <span class="kt-widget-4__item-badge text-success"><i class="fa flaticon-open-box fs-24"></i></span>
										                        <span class="kt-widget-4__item-number fs-24 text-success">{{ inbound_received_count($receiver->action_by) }}</span>
										                    </div>
										                </div>
										            </div>
										            @endforeach
										        </div>
										    </div>
										</div>
									</div>
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">

									</div>
								</div>
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
                @include('layouts.footer')
                <input type="hidden" id="user_id" value="{{ Auth::user()->id }}">
                <input type="hidden" id="city_id" value="{{ Auth::user()->city_id }}">
                <input type="hidden" id="branch_id" value="{{ Auth::user()->branch_id }}">
                <!-- end:: Footer -->   		
			</div>
		</div>
	</div>
	<!-- end:: Page -->

	@include('operator.quick-panel')

    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->                         
        
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>   
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/O1a1b575a/group-by-inbound-receiver.js') }}" type="text/javascript"></script>
</body>
</html>