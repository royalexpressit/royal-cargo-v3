<!DOCTYPE html>
<html lang="en" >
<head>
    <meta charset="utf-8"/>
    <title>Cargo &#8703; Users</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">
       
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->  

	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@if(role() == 1)
					@include('admin.header')
				@elseif(role() == 2)
					@include('operator.header')
				@else
					@include('cod.header')
				@endif
				<!-- header -->	

				<div class="kt-container  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">		
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
							    <div class="kt-container ">
							        <div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Users - My Profile</h3>
							            <span class="kt-subheader__separator kt-hidden"></span>
							        </div>
							        
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container kt-grid__item kt-grid__item--fluid">
								<!--begin::Portlet-->
								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">({{ $user->name }}) My Profile</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('users.update',$user->id) }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">Username (အမည်)</label>
															<input type="text" class="form-control" value="{{ $user->name }}" disabled=""/>
														</div>
														<div class="form-group">
															<label class="form-label">Email (အီးမေလ်း)</label>
															<input type="email" class="form-control" value="{{ $user->email }}" disabled="" />
														</div>
														<div class="form-group">
															<div class="row">
																<div class="col-lg-6">
																	<label class="form-label">City (မြို့)</label>
																	<input type="email" class="form-control" value="{{ city(Auth::user()->city_id)['name'] }}" disabled="" />
																</div>
																<div class="col-lg-6">
																	<label class="form-label">Branch (ရုံး)</label>
																	<input type="email" class="form-control" value="{{ branch(Auth::user()->branch_id)['name'] }}" disabled="" />
																</div>
															</div>
														</div>
														<div class="form-group">
															<div class="kt-radio-inline">
																<label class="kt-radio kt-radio--primary">
																	{!! ($user->role==2? '<i class="flaticon2-checkmark text-success"></i>':'') !!} Operator
																</label>
																<label class="kt-radio kt-radio--primary">
																	{!! ($user->role==3? '<i class="flaticon2-checkmark text-success"></i>':'') !!} Delivery/Counter
																</label>
																<label class="kt-radio kt-radio--primary">
																	{!! ($user->role==4? '<i class="flaticon2-checkmark text-success"></i>':'') !!} Cash On Delivery
																</label>
															</div>
														</div>
														<div class="form-group">
															
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>

					 				<div class="col-lg-6 col-xl-4 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head bg-primary">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title text-white">({{ $user->name }}) Change Password</h3>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<form action="{{ route('changed.password') }}" class="kt-form kt-form--fit kt-form--label-right" method="post">
														<div class="form-group">
															<label class="form-label">Old Password (လက်ရှိ စကားဝှက်)<span class="text-danger fs-18">*</span></label>
															<input type="password" class="form-control" name="old_password" placeholder="Old Password" required="" />
														</div>
														<div class="form-group">
															<label class="form-label">New Password (စကားဝှက် အသစ်)<span class="text-danger fs-18">*</span></label>
															<input type="password" class="form-control" name="new_password" placeholder="New Password" required=""/>
														</div>
														<div class="form-group">
															<input type="hidden" name="user_id" value="{{ Auth::id() }}">
															@csrf
															<button type="submit" class="btn btn-primary btn-wide">ပြောင်းမည်</button>
														</div>
													</form>
												</div>
											</div>	 
										</div>
					 				</div>
								</div>
								<!--end::Portlet-->
							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->
	

	@include('operator.quick-panel')


    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

	@include('layouts.widget') 

	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/select2.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
    	$('#city').select2({
            placeholder: "Select From City"
        });
        $('#branch').select2({
            placeholder: "Select From Branch"
        });
    </script>
</body>
</html>