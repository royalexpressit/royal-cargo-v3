<div class="quick-search-result">
    @if($waybills->isEmpty())
        <!--begin::Message-->
        <div class="text-muted d-none">
            No record found
        </div>
        <!--end::Message-->
    @else
        <!--begin::Section-->
        <div class="font-size-sm text-primary font-weight-bolder text-uppercase mb-2">
            Results found:
        </div>
        <div class="mb-10">
            @foreach($waybills as $waybill)
            <div class="d-flex align-items-center flex-grow-1 mb-2" id="{{ $waybill->id }}">
                <div class="d-flex flex-column ml-3 mt-2 mb-2 wd-100">
                    <div class="font-weight-bold text-dark text-hover-primary">
                        {{ $waybill->waybill_no }} 
                        <span class="right"><span class="badge badge-pill badge-{{ $waybill->current_status }}">{!! current_status($waybill->current_status) !!}</span></span>
                    </div>
                    <div class="font-size-sm font-weight-bold text-muted">
                        <span>{{ $waybill->origin }} - {{ $waybill->destination }}</span>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
        <!--end::Section-->
    @endif
    
</div>
