<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Royal Cargo | Help</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('assets/images/favicon.png') }}">

    <link href="{{ asset('assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.switch-label{
    		display: inline-block;
		    margin-top: 4px;
		    color: #fff;
    	}
    	.outbound-transit-switch,
    	.inbound-transit-switch{
    		display: none;
    	}
    	.widget-item{
    		background:#f7f7fb;
    		padding:6px 12px;
    		margin:4px;
    		margin-top: 6px !important;
    	}
    	.kt-none{
    		margin-bottom: 10px;
    	}
    	.border-1{
    		border:1px dashed #dfdfff;
    	}
    	.border-1:hover{
    		background:#f0f1ff;
    	}
    	.border-2{
    		border:1px dashed #bae4df;
    	}
    	.border-2:hover{
    		background:#eaf7f5;
    	}
    	#label{
    		font-size:20px;
    	}
    	.scroll{
    		height:600px;
    		overflow-y: auto;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="/keen/preview/demo4/index.html">
				<img alt="Logo" src="/keen/themes/keen/theme/demo4/dist/assets/media/logos/logo-5.png"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Help Center</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
            
        							</div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4 order-lg-1 order-xl-1">
										<div class="q-default">
											<img src="{{ asset('assets/images/stickers/sticker01.gif') }}" height="300px">
										</div>
										<div class="q-result hide">
											<div class="row">
												<div class="col-lg-4 col-xl-4">
													<img src="{{ asset('assets/images/stickers/sticker01.gif') }}" style="width:100%">
												</div>
												<div class="col-lg-8 col-xl-8">
													<p class="q-msg fs-18">dd</p>
												</div>
											</div>
										</div>
									</div>

									<div class="col-lg-8 col-xl-8 order-lg-1 order-xl-1">
										<div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
										    <div class="kt-portlet__head">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title">
										                Request help for outbound & inbound                       
										            </h3>
										        </div>
										        <div class="kt-portlet__head-toolbar">
										            
										        </div>
										    </div>
										    <div class="kt-portlet__body scroll">
										        <div class="tab-content ">
										            <div class="tab-pane fade active show" id="kt_portlet_tabs_1_1_content" role="tabpanel">
										                <div class="kt-widget-5">
										                    <div class="kt-widget-5__item kt-widget-5__item--info">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                (Outbound) collected လုပ်တယ်ဆိုတာဘာလဲ?
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Outbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
																		<input type="radio" name="complaint" class="complaint-item" value="2">
																		<span></span>
																	</label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--danger">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                (Outbound) handover လုပ်တယ်ဆိုတာဘာလဲ?
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Outbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="2">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--brand">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                (Outbound) received လုပ်တယ်ဆိုတာဘာလဲ?
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Outbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="3">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--success">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                (Outbound) branch out လုပ်တယ်ဆိုတာဘာလဲ?
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Outbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="4">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--danger">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                (Inbound) Branch-In လုပ်တယ်ဆိုတာဘာလဲ?
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Inbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="5">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--success">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                (Inbound) Handover လုပ်တယ်ဆိုတာဘာလဲ?
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Inbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="6">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--success">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                Inbound မှာ received လုပ်လို့မရဘူး။
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Inbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="7">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--success">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                Inbound မှာ received လုပ်လို့မရဘူး။
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Inbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="7">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--success">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                Inbound မှာ received လုပ်လို့မရဘူး။
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Inbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="7">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item kt-widget-5__item--success">
										                        <div class="kt-widget-5__item-info">
										                            <a href="#" class="kt-widget-5__item-title">
										                                Inbound မှာ received လုပ်လို့မရဘူး။
										                            </a>
										                            <div class="kt-widget-5__item-datetime">
										                                Inbound
										                            </div>
										                        </div>
										                        <div class="kt-widget-5__item-check">
										                            <label class="kt-radio kt-radio--bold kt-radio--success">
										                                <input type="radio" name="complaint" class="complaint-item" value="7">
										                                <span></span>
										                            </label>
										                        </div>
										                    </div>
										                </div>
										            </div>
										        </div>
										    </div>
										</div>
									</div>
								</div>
								<!--end::Row--> 

							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	
<!-- end:: Page -->

	<!-- begin:: Topbar Offcanvas Panels -->
	
	
			<!-- begin::Offcanvas Toolbar Quick Actions -->
			<div id="kt_offcanvas_toolbar_quick_actions" class="kt-offcanvas-panel">
				<div class="kt-offcanvas-panel__head">
					<h3 class="kt-offcanvas-panel__title">
						Quick Actions
					</h3>
					<a href="#" class="kt-offcanvas-panel__close" id="kt_offcanvas_toolbar_quick_actions_close"><i class="flaticon2-delete"></i></a>
				</div>
				<div class="kt-offcanvas-panel__body">
					<div class="kt-grid-nav-v2">
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-box"></i></div>
							<div class="kt-grid-nav-v2__item-title">Orders</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon-download-1"></i></div>
							<div class="kt-grid-nav-v2__item-title">Uploades</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-supermarket"></i></div>
							<div class="kt-grid-nav-v2__item-title">Products</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-avatar"></i></div>
							<div class="kt-grid-nav-v2__item-title">Customers</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-list"></i></div>
							<div class="kt-grid-nav-v2__item-title">Blog Posts</div>				
						</a>
						<a href="#" class="kt-grid-nav-v2__item">
							<div class="kt-grid-nav-v2__item-icon"><i class="flaticon2-settings"></i></div>
							<div class="kt-grid-nav-v2__item-title">Settings</div>				
						</a>			
					</div>
				</div>
			</div>
			<!-- end::Offcanvas Toolbar Quick Actions -->	
		<!-- end:: Topbar Offcanvas Panels -->

        <!-- begin:: Quick Panel -->
		<div id="kt_quick_panel" class="kt-offcanvas-panel">
			<div class="kt-offcanvas-panel__nav">
				<ul class="nav nav-pills" role="tablist">
			        <li class="nav-item active">
			            <a class="nav-link active" data-toggle="tab" href="#kt_quick_panel_tab_notifications" role="tab">Notifications</a>
			        </li>
			        <li class="nav-item">
			            <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_actions" role="tab">Actions</a>
			        </li>
			        <li class="nav-item">
			            <a class="nav-link" data-toggle="tab" href="#kt_quick_panel_tab_settings" role="tab">Settings</a>
			        </li>
			    </ul>

			    <button class="kt-offcanvas-panel__close" id="kt_quick_panel_close_btn"><i class="flaticon2-delete"></i></button>
			</div>

			<div class="kt-offcanvas-panel__body">
			    <div class="tab-content">
			        <div class="tab-pane fade show kt-offcanvas-panel__content kt-scroll active" id="kt_quick_panel_tab_notifications" role="tabpanel">
		 				<!--Begin::Timeline -->
		                <div class="kt-timeline">
		                    <!--Begin::Item -->                      
		                    <div class="kt-timeline__item kt-timeline__item--success">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-feed kt-font-success"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">02:30 PM</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                             
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            HTML,CSS,VueJS                                                                                            
		                        </div>
		                    </div>
		                    <!--End::Item -->  

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--danger">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-safe-shield-protection kt-font-danger"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">01:20 AM</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            Security, Fieewall                                                                                         
		                        </div>
		                    </div>  
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--brand">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon2-box kt-font-brand"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">Yestardey</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            FlyMore design mock-ups been uploadet by designers Bob, Naomi, Richard                                                                                            
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            PSD, Sketch, AJ                                                                                       
		                        </div>
		                    </div>  
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--warning">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-pie-chart-1 kt-font-warning"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">Aug 13,2018</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            Meeting with Ken Digital Corp ot Unit14, 3 Edigor Buildings, George Street, Loondon<br> England, BA12FJ                                                                                           
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            Meeting, Customer                                                                                          
		                        </div>
		                    </div> 
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--info">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon">
		                                    <i class="flaticon-notepad kt-font-info"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">May 09, 2018</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            KeenThemes created new layout whith tens of new options for Keen Admin panel                                                                                                
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            HTML,CSS,VueJS                                                                                            
		                        </div>
		                    </div> 
		                    <!--End::Item --> 

		                    <!--Begin::Item --> 
		                    <div class="kt-timeline__item kt-timeline__item--accent">
		                        <div class="kt-timeline__item-section">
		                            <div class="kt-timeline__item-section-border">
		                                <div class="kt-timeline__item-section-icon"                                        >
		                                    <i class="flaticon-bell kt-font-success"></i>
		                                </div>
		                            </div>
		                            <span class="kt-timeline__item-datetime">01:20 AM</span>
		                        </div>

		                        <a href="" class="kt-timeline__item-text">
		                            New secyrity alert by Firewall & order to take aktion on User Preferences                                                                                             
		                        </a>
		                        <div class="kt-timeline__item-info">
		                            Security, Fieewall                                                                                         
		                        </div>
		                    </div>   
		                    <!--End::Item -->                    

		                </div> 
		                <!--End::Timeline --> 
			        </div>
			        <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_actions" role="tabpanel">
			        	<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-success">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<span class="kt-portlet__head-icon kt-hide"><i class="flaticon-stopwatch"></i></span>
									<h3 class="kt-portlet__head-title">Recent Bills</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->


						<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-info">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Latest Invoices</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->

						<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-warning">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">New Comments</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->

						<!--begin::Portlet-->
						<div class="kt-portlet kt-portlet--solid-brand">
							<div class="kt-portlet__head">
								<div class="kt-portlet__head-label">
									<h3 class="kt-portlet__head-title">Recent Posts</h3>
								</div>
								<div class="kt-portlet__head-toolbar">
									<div class="kt-portlet__head-group">
										<div class="dropdown dropdown-inline">
											<button type="button" class="btn btn-sm btn-font-light btn-outline-hover-light btn-circle btn-icon" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
												<i class="flaticon-more"></i>
											</button>
											<div class="dropdown-menu dropdown-menu-right">
												<a class="dropdown-item" href="#">Action</a>
												<a class="dropdown-item" href="#">Another action</a>
												<a class="dropdown-item" href="#">Something else here</a>
												<div class="dropdown-divider"></div>
												<a class="dropdown-item" href="#">Separated link</a>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="kt-portlet__body">
								<div class="kt-portlet__content">
									Lorem Ipsum is simply dummy text of the printing and typesetting simply dummy text of the printing industry. 
								</div>
							</div>	
							<div class="kt-portlet__foot kt-portlet__foot--sm kt-align-right">
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">Dismiss</a>
								<a href="#" class="btn btn-bold btn-upper btn-sm btn-font-light btn-outline-hover-light">View</a>
							</div>
						</div>
						<!--end::Portlet-->
			        </div>
			        <div class="tab-pane fade kt-offcanvas-panel__content kt-scroll" id="kt_quick_panel_tab_settings" role="tabpanel">
			        	<form class="kt-form">
							<div class="kt-heading kt-heading--space-sm">Notifications</div>

							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable notifications:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_1">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable audit log:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
											<input type="checkbox"  name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Notify on new orders:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_2">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							
							<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

							<div class="kt-heading kt-heading--space-sm">Orders</div>

							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable order tracking:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable orders reports:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox"  name="quick_panel_notifications_3">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Allow order status auto update:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--danger">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_4">
											<span></span>
										</label>
									</span>
								</div>
							</div>

							<div class="kt-separator kt-separator--space-md kt-separator--border-dashed"></div>

							<div class="kt-heading kt-heading--space-sm">Customers</div>

							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable customer singup:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-xs row">
								<label class="col-8 col-form-label">Enable customers reporting:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
											<input type="checkbox"  name="quick_panel_notifications_5">
											<span></span>
										</label>
									</span>
								</div>
							</div>
							<div class="form-group form-group-last form-group-xs row">
								<label class="col-8 col-form-label">Notifiy on new customer registration:</label>
								<div class="col-4 kt-align-right">
									<span class="kt-switch kt-switch--sm kt-switch--success">
										<label>
											<input type="checkbox" checked="checked" name="quick_panel_notifications_6">
											<span></span>
										</label>
									</span>
								</div>
							</div>

						</form>
			        </div>
			    </div>
			</div>
		</div>
		<!-- end:: Quick Panel -->

     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget') 
        <input type="hidden" id="csrf" value="{{ csrf_token() }}">


<!-- Large Modal -->
						<div class="modal fade bd-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
							<div class="modal-dialog modal-lg">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title">Modal title</h5>
										<button type="button" class="close" data-dismiss="modal" aria-label="Close">
											<span aria-hidden="true">&times;</span>
										</button>
									</div>
									<div class="modal-body">
										<div class="row">
											<div class="col-lg-4 col-md-6 col-sm-12">
												<img src="{{ asset('assets/images/stickers/002.png') }}">
											</div>
											<div class="col-lg-8 col-md-6 col-sm-12">
												<div class="form-group">
													<label id="label" class="kt-radio kt-radio--bold kt-radio--success"></label>
													<input type="text" class="form-control w-300 mt-10 fs-22" id="waybill_no" aria-describedby="emailHelp" placeholder="A341066789">
												</div>
											</div>
										</div>
									</div>
									<div class="modal-footer">
										<input type="hidden" id="complaint_id" value="">
										<button type="button" class="btn btn-outline-brand" data-dismiss="modal">မမေးတော့ဘူး</button>
										<button type="button" class="btn btn-outline-brand ask" data-dismiss="modal">မေးမယ်</button>
									</div>
								</div>
							</div>
						</div>
        
    

        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
    "colors": {
        "state": {
            "brand": "#385aeb",
            "metal": "#c4c5d6",
            "light": "#ffffff",
            "accent": "#00c5dc",
            "primary": "#5867dd",
            "success": "#34bfa3",
            "info": "#36a3f7",
            "warning": "#ffb822",
            "danger": "#fd3995",
            "focus": "#9816f4"
        },
        "base": {
            "label": [
                "#c5cbe3",
                "#a1a8c3",
                "#3d4465",
                "#3e4466"
            ],
            "shape": [
                "#f0f3ff",
                "#d9dffa",
                "#afb4d4",
                "#646c9a"
            ]
        }
    }
};
        </script>
        <!-- end::Global Config -->

    <script src="{{ asset('assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('assets/js/scripts.bundle.js') }}" type="text/javascript"></script>

   	<script src="{{ asset('assets/js/bootstrap-datepicker.js') }}" type="text/javascript"></script>
   	<script type="text/javascript">
   		$(".complaint-item").change(function(){
   			$('.bd-example-modal-lg').modal({show:true});
   			var complaint = $("input[name='complaint']:checked").val();
   			$('#complaint_id').val(complaint);
   			if(complaint == 1){
   				$("#label").text('စာထွက်(outbound)မှာ စာကောက်(collected) အတွက် စာရင်းသွင်းလို့မရဘူးလား? အောက်က box လေးထဲမှာ နံပါတ်ရိုက်ထည့်ပြီး မေးပေးပါ။ ဖြေပေးပါမယ် ရှင်။');
   			}else if(complaint == 2){
   				$("#label").text('စာထွက်(outbound)မှာ စာလွှဲပြောင်း(handover) အတွက် စာရင်းသွင်းလို့မရဘူးလား? အောက်က box လေးထဲမှာ နံပါတ်ရိုက်ထည့်ပြီး မေးပေးပါ။ ဖြေပေးပါမယ် ရှင်။');
   			}else if(complaint == 3){
   				$("#label").text('စာထွက်(outbound)မှာ စာလက်ခံ(received) အတွက် စာရင်းသွင်းလို့မရဘူးလား? အောက်က box လေးထဲမှာ နံပါတ်ရိုက်ထည့်ပြီး မေးပေးပါ။ ဖြေပေးပါမယ် ရှင်။');
   			}else if(complaint == 4){
   				$("#label").text('စာထွက်(outbound)မှာ စာထွက်(branch out) အတွက် စာရင်းသွင်းလို့မရဘူးလား? အောက်က box လေးထဲမှာ နံပါတ်ရိုက်ထည့်ပြီး မေးပေးပါ။ ဖြေပေးပါမယ် ရှင်။');
   			}else{

   			}
   		});

   		$(".ask").on("click",function search(e) {
   			var csrf 			= $("#csrf").val();
   			var waybill 		= $("#waybill_no").val();
   			var complaint_id 	= $('#complaint_id').val();

   			$.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
                
            $.ajax({
                type: 'post',
                url: "{{ url('complaints/ask') }}",
                dataType:'json',
                data: {
                	'_token'		: csrf,
                    'complaint_id'	: complaint_id,
                    'waybill_no'	: waybill,
                    'user_id'		: 1
            	},
                success: function(data) { 
                	console.log(data);
                	if(data.success == 1){
                		$(".q-result").removeClass('hide');
                		$(".q-default").hide(); 
                		$(".q-result").show();
                		$(".q-msg").text(data.msg); 
                	}
                },
            });
   		});
   		
   		
   	</script>
</body>
</html>