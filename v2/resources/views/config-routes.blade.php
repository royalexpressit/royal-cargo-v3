<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Config Routes</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">


	<link href="{{ asset('_assets/css/jquery-ui.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Page -->
	<!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('_assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">Config Routes For <strong class="text-danger">{{ city($city_id)['name'] }}</strong></h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            
							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-6 col-xl-4 order-lg-2 order-xl-1">
										<!--begin::Portlet-->
										<div class="kt-portlet kt-portlet--height-fluid kt-portlet--tabs">
										    <div class="kt-portlet__head">
										        <div class="kt-portlet__head-label">
										            <h3 class="kt-portlet__head-title">
										                Transit Routes                         
										            </h3>
										        </div>
										    </div>
										    <div class="kt-portlet__body">
										        <div class="tab-pane fade active show" id="kt_portlet_tabs_1_1_content" role="tabpanel">
										            <div class="kt-widget-5">
										            	@foreach($routes as $transit)
										                <div class="kt-widget-5__item kt-widget-5__item--info">
										                    <div class="kt-widget-5__item-info">
										                        <a href="#" class="kt-widget-5__item-title">
										                            <h5><span class="text-primary">{{ city($transit->origin)['shortcode'] }}</span> - <span class="text-success">{{ city($transit->destination)['shortcode'] }}</span></h5>
										                        </a>
										                        <div class="kt-widget-5__item-datetime">
										                            ({{ city($transit->origin)['name'] }} - {{ city($transit->destination)['name'] }})
										                        </div>
										                    </div>
										                    <div class="kt-widget-5__item-check">
										                        <a href="{{ url('set-routes?origin='.$transit->origin.'&destination='.$transit->destination) }}" class="btn btn-primary btn-sm btn-icon btn-circle"><i class="fa fa-cogs"></i></a>
										                    </div>
										                </div>
										                @endforeach
										            </div>
										        </div>
										    </div>
										</div>
										<!--end::Portlet-->	
									</div>

									<!--end::Row--> 	
								</div>
								<!-- end:: Content -->							
							</div>
						</div>
					</div>							
				</div>
			</div>
		</div>
		<!-- begin:: Footer -->
		@include('layouts.footer')
		<!-- end:: Footer -->	
		<!-- end:: Page -->
	</div>
     
    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->

    @include('layouts.widget')      
        
    <script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/jquery-ui.bundle.js') }}" type="text/javascript"></script>
   	<script src="{{ asset('_assets/js/draggable.js') }}" type="text/javascript"></script>
</body>
</html>