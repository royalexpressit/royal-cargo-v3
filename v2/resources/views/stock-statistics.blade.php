<!DOCTYPE html>
<html lang="en" >
<head>
	<meta charset="utf-8"/>
	<title>Cargo &#8703; Stock Statistics</title>
    <meta name="description" content="Royal Express Cargo"> 
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <link rel="shortcut icon" href="{{ asset('_assets/images/favicon.png') }}">

    <link href="{{ asset('_assets/css/plugins.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('_assets/css/custom.css') }}" rel="stylesheet" type="text/css" />
    <style type="text/css">
    	.bg-whitesmoke{
    		background:#f5f5f5;
    	}
    	.border-opening{
    		border: 2px solid #ee1c24;
    		border-radius: 6px;
    	}
    	.border-branch-in{
    		border: 2px solid #5867dd;
    		border-radius: 6px;
    	}
    	.border-delivered{
    		border: 2px solid #1dc9b7;
    		border-radius: 6px;
    	}
    	.border-postponed{
    		border: 2px solid #ffb822;
    		border-radius: 6px;
    	}
    	.border-remaining{
    		border: 2px solid #9816f4;
    		border-radius: 6px;
    	}
    	.text-left{
    		text-align: left;
    	}
    	.block{
    		display: block;
    	}
    	.w-250{
    		width:280px;
    	}
    	.fs-22{
    		font-size:22px;
    	}
    </style>
</head>
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header--minimize-menu kt-subheader--enabled kt-subheader--transparent kt-page--loading"  >
	<!-- begin::Page loader -->
	@include('layouts.loading')
	<!-- end::Page Loader -->        
    
    <!-- begin:: Header Mobile -->
	<div id="kt_header_mobile" class="kt-header-mobile " >
		<div class="kt-header-mobile__logo">
			<a href="{{ url('') }}">
				<img class="logo-sm" alt="Logo" src="{{ asset('assets/images/logo.png') }}"/>
			</a>
		</div>
		<div class="kt-header-mobile__toolbar">
			<button class="kt-header-mobile__toolbar-toggler" id="kt_header_mobile_toggler"><span></span></button>
			<button class="kt-header-mobile__toolbar-topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
		</div>
	</div>
	<!-- end:: Header Mobile -->

	<div class="kt-grid kt-grid--hor kt-grid--root">
		<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">
				<!-- header -->
				@include('operator.header')
				<!-- header -->	

				<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch">
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
									
							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
				    			<div class="kt-container ">
				        			<div class="kt-subheader__main">
										<h3 class="kt-subheader__title"><strong class="badge badge-success text-uppercase">Inbound</strong> - Stock Statistics</h3>
				                    </div>
				                    
							        <div class="kt-subheader__toolbar">
							            <div class="kt-subheader__wrapper">
							            	
							            </div>

							        </div>
							    </div>
							</div>
							<!-- end:: Subheader -->


							<!-- begin:: Content -->
							<div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
							<!--begin::Dashboard 6-->

								<!--begin::Row--> 
								<div class="row">
									<div class="col-lg-4 col-xl-4">
										<!--begin::Portlet-->
										<div class="kt-portlet  kt-widget-12">
											<div class="kt-portlet__body">
												<div class="kt-widget-12__body">
													<div class="kt-widget-12__head">
														<div class="kt-widget-12__date kt-widget-12__date--danger">
															<span class="kt-widget-12__day">{{ date('d') }}</span>
															<span class="kt-widget-12__month">{{ date('M') }}</span>
														</div>
														<div class="kt-widget-12__label">
															<h3 class="kt-widget-12__title">ယနေ့စာဝေရန် စုစုပေါင်း</h3>
															<span class="kt-widget-12__desc fs-24 text-danger">{{ stock_statistics(Auth::user()->city_id)['opening']+stock_statistics(Auth::user()->city_id)['total'] }}</span>
														</div>
													</div>
													<div class="kt-widget-12__info">
														(အရင်ရက်လက်ကျန် <strong class="text-warning">{{ stock_statistics(Auth::user()->city_id)['opening'] }}</strong> နှင့် ယနေ့စာဝင် <strong class="text-danger">{{ stock_statistics(Auth::user()->city_id)['total'] }}</strong> အရေအတွက်အား ပေါင်းထားသည်) 
													</div>
												</div>
											</div>
										</div>
										<div class="kt-portlet kt-widget-13">
											<div class="kt-portlet__body">
												<!--
												<div id="kt-widget-slider-13-2" class="kt-slider carousel slide" data-ride="carousel" data-interval="4000">
													<div class="kt-slider__head">
														<div class="kt-slider__label">မှတ်ချက်များ</div>
														<div class="kt-slider__nav">
															<a class="kt-slider__nav-prev carousel-control-prev" href="#kt-widget-slider-13-2" role="button" data-slide="prev">
																<i class="fa fa-angle-left"></i>
															</a>
															<a class="kt-slider__nav-next carousel-control-next" href="#kt-widget-slider-13-2" role="button" data-slide="next">
																<i class="fa fa-angle-right"></i>
															</a>
														</div>
													</div>
													<div class="carousel-inner">
														<div class="carousel-item kt-slider__body">
															<div class="kt-widget-13">
																<div class="kt-widget-13__body">
																	<a class="kt-widget-13__title" href="#">Today Opening</a>
																	<div class="kt-widget-13__desc">
																		Received (သို့) Rejected အဆင့်ထိ မလုပ်ထားသောစာများသည် Today Opening စာရင်ထဲတွင်ရှိနေမည်ဖြစ်သည်။
																	</div>
																</div>
															</div>
														</div>
														<div class="carousel-item kt-slider__body active">
															<div class="kt-widget-13">
																<div class="kt-widget-13__body">
																	<a class="kt-widget-13__title" href="#">Rejected လုပ်ခြင်း</a>
																	<div class="kt-widget-13__desc">
																		To start a blog, think of a topic about and first brainstorm party is ways to write details
																	</div>
																</div>
															</div>
														</div>
														<div class="carousel-item kt-slider__body">
															<div class="kt-widget-13">
																<div class="kt-widget-13__body">
																	<a class="kt-widget-13__title" href="#">Reached 50,000 sales</a>
																	<div class="kt-widget-13__desc">
																		To start a blog, think of a topic about and first brainstorm party is ways to write details
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												-->
												<?php 
													echo "<pre>";
													print_r(stock_statistics(Auth::user()->city_id));
													echo "</pre>"; 
												?>
											</div>
										</div>
										<!--end::Portlet-->	

									</div>
									<div class="col-lg-8 col-xl-8">
										<div class="kt-portlet kt-portlet--height-fluid">
											<div class="kt-portlet__head">
												<div class="kt-portlet__head-label">			
													<h3 class="kt-portlet__head-title">
														ယနေ့စာဝေရန် အခြေအနေ
														<a href="#">By Branches</a>
													</h3>
												</div>
												<div class="kt-portlet__head-toolbar">
													<div class="kt-portlet__head-wrapper">
												
													</div>
												</div>
											</div>
											<div class="kt-portlet__body">
												<div class="kt-widget-18">
													<div class="kt-widget-18__item border-opening">
														<div class="kt-widget-18__desc " data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="Received (သို့) Rejected အဆင့်ထိမလုပ်ထားသော<br> အရင်ရက်က စာလက်ကျန်များ အရေအတွက်">
															<a href="{{ url('stock-statistics/opening') }}" class="btn btn-danger w-250"><div class="kt-widget-18__title" >
																<i class="flaticon-tool"></i> <span class="text-left block">Today Opening</span>
															</div>
															<div class="kt-widget-18__desc text-left">
																ယမန်နေ့ စာဝေလက်ကျန် အရေအတွက်
															</div>
															</a>
														</div>
														<div class="kt-widget-18__orders">
															<span class="fs-24">{{ stock_statistics(Auth::user()->city_id)['opening'] }}</span> Waybills
														</div>
													</div>
													<div class="kt-widget-18__item border-branch-in">
														<div class="kt-widget-18__desc" data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="ယနေ့ မိမိမြို့သို့ ဝင်လာသောစာဝေများ အရေအတွက်">
															<a href="{{ url('stock-statistics/branch-in') }}" class="btn btn-primary w-250"><div class="kt-widget-18__title ">
																<i class="flaticon-truck"></i> <span class="text-left block">Today Branch In</span>
															</div>
															<div class="kt-widget-18__desc text-left">
																ယနေ့ စုစုပေါင်းစာဝင် အရေအတွက်
															</div>
															</a>
														</div>
														<div class="kt-widget-18__orders">
															<span class="fs-24">{{ stock_statistics(Auth::user()->city_id)['total'] }} </span> Waybills
														</div>
													</div>
													<div class="text-center mt--8">
														<i class="flaticon2-sort fs-24 text-primary"></i>
														<span class="space-between"></span>
														<i class="flaticon2-sort fs-24 text-primary"></i>
													</div>
													
													<div class="kt-widget-18__item border-delivered">
														<div class="kt-widget-18__desc" data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="အရင်ရက်လက်ကျန် နှင့် ယနေ့စာများ ဝေပြီးသော အရေအတွက်">
															<a href="{{ url('stock-statistics/delivered') }}" class="btn btn-success w-250"><div class="kt-widget-18__title">
																<i class="flaticon2-calendar-5"></i> <span class="text-left block">Today Delivered</span>
															</div>
															<div class="kt-widget-18__desc text-left">
																ယနေ့ စာဝေပြီး အရေအတွက်
															</div>
															</a>
														</div>
														<div class="kt-widget-18__orders">
															<span class="fs-24">{{ stock_statistics(Auth::user()->city_id)['delivered'] }}</span> Waybills
														</div>
													</div>
													<div class="kt-widget-18__item border-remaining">
														<div class="kt-widget-18__desc" data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="အရင်ရက်လက်ကျန် နှင့် ယနေ့စာများထဲမှ စာဝေရန် ကျန်ရှိနေသော စုစုပေါင်း စာအရေအတွက် (စုစုပေါင်း စာဝေရန်အရေအတွက်ထဲမှ delivered ဖြစ်သွားသော အရေအတွက်ကို နှုတ်ထားခြင်းဖြစ်သည်)">
															<a href="{{ url('stock-statistics/remaining') }}" class="btn btn-focus w-250"><div class="kt-widget-18__title">
																<i class="flaticon2-bell-2"></i> <span class="text-left block">Today Remaining</span>
															</div>
															<div class="kt-widget-18__desc text-left">
																ယနေ့ စာဝေရန်လက်ကျန် အရေအတွက်
															</div>
															</a>
														</div>
														<div class="kt-widget-18__orders">
															<span class="fs-24">{{ stock_statistics(Auth::user()->city_id)['remaining'] }}</span> Waybills
														</div>
													</div>
													<div class=" text-center mt--8">
														<i class="flaticon-calendar-with-a-clock-time-tools fs-24 text-warning"></i>
													</div>
													<div class="kt-widget-18__item border-postponed">
														<div class="kt-widget-18__desc" data-toggle="kt-tooltip" data-skin="dark" data-html="true" data-placement="right" data-original-title="အရင်ရက်လက်ကျန် နှင့် ယနေ့စာများထဲမှ စာဝေရန် ဆိုင်းငံ့ထားသော စုစုပေါင်း စာအရေအတွက် (ယနေ့ စာဝေရန် ကျန်နေသော အရက်အတွက်ထဲမှာ postponed ဖြစ်နေသော )">
															<a href="{{ url('stock-statistics/postponed') }}" class="btn btn-warning w-250"><div class="kt-widget-18__title">
																<i class="flaticon-clock-2"></i> <span class="text-left block">Today Postponed</span>
															</div>
															<div class="kt-widget-18__desc text-left">
																ယနေ့ ဆိုင်းငံ့စာ အရေအတွက်
															</div>
															</a>
														</div>
														<div class="kt-widget-18__orders">
															<span class="fs-24">{{ stock_statistics(Auth::user()->city_id)['postponed'] }}</span> Waybills
														</div>
													</div>
												</div>
											</div>	 
										</div>
									</div>
								</div>
								<!--end::Row--> 


							</div>
							<!-- end:: Content -->							
						</div>
					</div>
				</div>

				<!-- begin:: Footer -->
				@include('layouts.footer')
				<!-- end:: Footer -->			
			</div>
		</div>
	</div>
	<!-- end:: Page -->


    <!-- begin::Scrolltop -->
	<div id="kt_scrolltop" class="kt-scrolltop">
		<i class="fa fa-arrow-up"></i>
	</div>
	<!-- end::Scrolltop -->
    
	<script src="{{ asset('_assets/js/config.js') }}" type="text/javascript"></script>
    <script src="{{ asset('_assets/js/plugins.bundle.js') }}" type="text/javascript"></script>
	<script src="{{ asset('_assets/js/scripts.bundle.js') }}" type="text/javascript"></script>
</body>
</html>