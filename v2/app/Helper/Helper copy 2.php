<?php 
use App\User;
use App\City;
use App\Branch;
use App\Waybill;
use App\ActionLog;
use App\SetupTime;
use Illuminate\Support\Facades\DB;

	function base_url(){
		return url('').'/';
	}

	function batch_id(){
		return time();
	}

	function server_address(){
		return 'http://cargo.royalx.biz';
	}

	function setup_date($date){
		Session::put('set-dashboard-date',$date);
	}

	function get_date(){
		if(Session::get('set-dashboard-date')){
			return Session::get('set-dashboard-date');
		}else{
			return date('Y-m-d');
		}
	}

	function get_date_shortformat(){
		$shortformat = substr(get_date(),5);
		return '('.str_replace("-","/",$shortformat).')';
	}

	function setup_branch($branch){
		Session::put('marked-branch-ed3c2622064f',$branch);
	}

	function clear_branch(){
		Session::forget('marked-branch-ed3c2622064f');
	}

	function marked_branch(){
		if(Session::get('marked-branch-ed3c2622064f')){
			return Session::get('marked-branch-ed3c2622064f');
		}else{
			return 'not marked';
		}
	}

	function current_date(){
		return date('Y-m-d');
	}

	function api_key(){
		return 'd53c832d-67c1-44bc-96fc-187a0f1c6b90';
	}

	function role(){
		return Auth::user()->role;
	}

	function check_inbound_access($role,$action){
		if($action == 'branch-in'){
			if($role == 2){
				//return view('operator.inbound.action-branch-in');
			}
		}else{
			return '404';
		}
		
	}

	/*
	function user($user_id,$key){
		$user = User::find($user_id);
		if($user){
			if($key == 'name'){
				return $user->name;
			}else{
				return '';
			}
		}else{
			return '';
		}
	}
	*/

	function city($city_id){
		$city = City::find($city_id);
		if($city){
			$response['id'] 				= $city->id;
			$response['name'] 				= $city->name;
			$response['mm_name'] 			= $city->mm_name;
			$response['shortcode'] 			= $city->shortcode;
			$response['is_service_point'] 	= $city->is_service_point;
		}else{
			$response['id'] 				= '';
			$response['name'] 				= '';
			$response['mm_name'] 			= '';
			$response['shortcode'] 			= '';
			$response['is_service_point'] 	= '';
		}

		return $response;
	}

	function branch($branch_id){
		$branch = Branch::find($branch_id);
		if($branch){
			$response['id'] 		= $branch->id;
			$response['name'] 		= $branch->name;
			$response['city_id'] 	= $branch->city_id;
			$response['is_main_office'] 	= $branch->is_main_office;
			$response['is_transit_area'] = $branch->is_transit_area;
			$response['is_sorting'] = $branch->is_sorting;
		}else{
			$response['id'] 		= '';
			$response['name'] 		= '';
			$response['city_id'] 	= '';
			$response['is_main_office'] 	= '';
			$response['is_transit_area'] = '';
			$response['is_sorting'] = '';
		}

		return $response;
	}


	/** Outbound status for dashboard **/
	function outbound_dashboard_status($city_id){
		$date 		= get_date();
		$response 	= array();

		if(Auth::user()->role == 1){
			//admin view for all cities
			$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('action_logs.action_id',1)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}else{
			//operator view for related cities
			$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.origin',$city_id)
				->where('action_logs.action_id',1)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}
		
		if(Auth::user()->role == 1){
			//admin view for all cities
			$collected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status',1)
				->where('action_logs.action_id',1)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}else{
			//operator view for related cities
			$collected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status',1)
				->where('action_logs.action_id',1)
				->where('waybills.origin',$city_id)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}

		if(Auth::user()->role == 1){
			//admin view for all cities
			$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status',2)
				->where('action_logs.action_id',2)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}else{
			//operator view for related cities
			$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status',2)
				->where('action_logs.action_id',2)
				->where('waybills.origin',$city_id)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();	
		}

		if(Auth::user()->role == 1){
			//admin view for all cities
			$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status',3)
				->where('action_logs.action_id',3)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}else{
			//operator view for related cities
			$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status',3)
				->where('action_logs.action_id',3)
				->where('waybills.origin',$city_id)
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}
			
		if(Auth::user()->role == 1){
			//admin view for all cities
			$branch_out 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status','>=',4)
				->where('action_logs.action_id','>=',4)
				->where('action_logs.action_type','outbound')
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}else{
			//operator view for related cities
			$branch_out 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('action_logs.waybill_id')
				->where('waybills.outbound_date','like',$date.'%')
				->where('waybills.current_status','>=',4)
				->where('action_logs.action_id','>=',4)
				->where('waybills.origin',$city_id)
				->where('action_logs.action_type','outbound')
				->groupBy('action_logs.waybill_id')
				->get()
				->count();
		}
		
		$response['total'] 		= $total;
		$response['collected'] 	= $collected;
		$response['handover'] 	= $handover;
		$response['received'] 	= $received;
		$response['branch_out'] = $branch_out;

		return $response;
	}

	/** Inbound status for dashboard **/
	function inbound_dashboard_status(){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();
		$response 	= array();

		if(role() == 1){
			$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}
		
		if(role() == 1){
			$branch_in 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',5)
				->where('action_logs.action_id',5)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$branch_in 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',5)
				->where('action_logs.action_id',5)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		if(role() == 1){
			$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',6)
				->where('action_logs.action_id',6)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',6)
				->where('action_logs.action_id',6)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		if(role() == 1){
			$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',7)
				->where('action_logs.action_id',7)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',7)
				->where('action_logs.action_id',7)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		if(role() == 1){
			$postponed 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',8)
				->where('action_logs.action_id',8)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$postponed 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',8)
				->where('action_logs.action_id',8)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		if(role() == 1){
			$rejected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',9)
				->where('action_logs.action_id',9)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$rejected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',9)
				->where('action_logs.action_id',9)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		if(role() == 1){
			$transferred = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',10)
				->where('action_logs.action_id',10)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$transferred = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',10)
				->where('action_logs.action_id',10)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		if(role() == 1){
			$accepted = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('waybills.created_at','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.current_status',11)
				->where('action_logs.action_id',11)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$accepted = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_type','inbound')
				->where('waybills.destination',$city_id)
				->where('waybills.current_status',11)
				->where('action_logs.action_id',11)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}

		$response['total'] 			= $total;
		$response['branch_in'] 		= $branch_in;
		$response['handover'] 		= $handover;
		$response['received'] 		= $received;
		$response['postponed'] 		= $postponed;
		$response['rejected'] 		= $rejected;
		$response['transferred'] 	= $transferred;
		$response['accepted'] 		= $accepted;

		return $response;
	}

	function transit_dashboard_status($city_id){
		$date 		= get_date();
		$response 	= array();

		$pending 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('action_logs.waybill_id')
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->where('waybills.transit',$city_id)
			->where('waybills.transit_status',0)
			->groupBy('action_logs.waybill_id')
			->get()
			->count();

		$branch_in 	= Waybill::select('waybill_no')
			->where('transit_date','like',$date.'%')
			->where('transit_status',1)
			->where('transit',$city_id)
			->get()
			->count();

		$handover 	= Waybill::select('waybill_no')
			->where('transit_date','like',$date.'%')
			->where('transit_status','>=',2)
			->where('transit',$city_id)
			->get()
			->count();

		$received 	= Waybill::select('waybill_no')
			->where('transit_date','like',$date.'%')
			->where('transit_status',3)
			->where('transit',$city_id)
			->get()
			->count();

		$branch_out = Waybill::select('waybill_no')
			->where('transit_date','like',$date.'%')
			->where('transit_status',4)
			->where('transit',$city_id)
			->get()
			->count();

		$in_total 		= $branch_in + $handover;
		$out_total 		= $branch_out + $received;
		
		$response['balance'] 	= $in_total - $out_total;
		$response['inbound'] 	= $in_total;
		$response['outbound'] 	= $out_total;
		$response['pending'] 	= $pending;
		$response['branch_in'] 	= $branch_in;
		$response['handover'] 	= $handover;
		$response['received'] 	= $received;
		$response['branch_out'] = $branch_out;

		return $response;
	}

	function delivery_man_collected($delivery_id){
		$date 		= get_date();
		$response 	= array();

		$total = DB::table('waybills')
			->join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.user_id',$delivery_id)
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$collected = DB::table('waybills')
			->join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.user_id',$delivery_id)
			->where('waybills.current_status',1)
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$handover = DB::table('waybills')
			->join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.user_id',$delivery_id)
			->where('waybills.current_status',2)
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$received = DB::table('waybills')
			->join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.user_id',$delivery_id)
			->where('waybills.current_status','>=',3)
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$response['total'] 		= $total;
		$response['collected'] 	= $collected;
		$response['handover'] 	= $handover;
		$response['received'] 	= $received;

		return $response;
	}

	function inbound_summary($city_id){
		$destination= Auth::user()->city_id;
		$transit 	= Auth::user()->city_id;
		$date 		= get_date();
		$response 	= array();

		$branch_total = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.origin',$city_id)
			->where('waybills.destination',$destination)
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.action_id',5)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$transit_total = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.origin',$city_id)
			->where('waybills.transit',$transit)
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.action_id',5)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$branch_in = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.origin',$city_id)
			->where('waybills.destination',$destination)
			->where('waybills.current_status',5)
			->where('action_logs.action_date','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$transit_branch_in = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.origin',$city_id)
			->where('waybills.transit',$transit)
			->where('waybills.transit_status',1)
			->where('action_logs.action_date','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$branch_handover = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.origin',$city_id)
			->where('waybills.destination',$destination)
			->where('waybills.current_status','>=',6)
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.action_type','inbound')
			->where('action_logs.active',1)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		// transit_handover ( transit_status >= 2)
		$transit_handover = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.origin',$city_id)
			->where('waybills.transit',$transit)
			->where('waybills.transit_status','>=',2)
			->where('waybills.transit_date','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$response['total'] 				= $branch_total + $transit_total;
		$response['branch_in'] 			= $branch_in + $transit_branch_in;
		$response['handover_branch'] 	= $branch_handover ;
		$response['handover_transit'] 	= $transit_handover;

		return $response;
	}

	function outbound_branch_summary(){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();

        $branches = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
            ->select('action_logs.branch_id')
            ->where('waybills.origin',$city_id)
            ->where('waybills.current_status','>=',4)
            ->where('action_logs.action_id',1)
            ->where('action_logs.action_type','outbound')
            ->where('action_logs.action_date','like',$date.'%')
            ->groupBy('action_logs.branch_id')
            ->get();

        return $branches;
	}

	function outbound_transit_summary(){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();

        $branches = Waybill::join('branch_action_logs','branch_action_logs.waybill_id', '=', 'waybills.id')
            ->select('branch_action_logs.to_branch')
            ->where('waybills.transit',$city_id)
            ->where('branch_action_logs.action_type','transit')
            ->where('waybills.transit_date','like',$date.'%')
            ->groupBy('branch_action_logs.to_branch')
            ->get();

        return $branches;
	}

	function branch_out_summary($branch_id){
		$date 		= get_date();
		$response 	= array();
		$city_id 	= Auth::user()->city_id;

		$branch_out = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
            ->select('waybills.waybill_no')
            ->where('action_logs.branch_id',$branch_id)
            ->where('waybills.current_status','>=',4)
            ->where('action_logs.action_id',1)
            ->where('action_logs.action_type','outbound')
            ->where('action_logs.action_date','like',$date.'%')
            ->get()
            ->count();

        $same_city = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
            ->select('waybills.waybill_no')
            ->where('action_logs.branch_id',$branch_id)
            ->where('waybills.destination',$city_id)
            ->where('waybills.current_status','>=',4)
            ->where('action_logs.action_id',1)
            ->where('action_logs.action_type','outbound')
            ->where('action_logs.action_date','like',$date.'%')
            ->get()
            ->count();

        $other_city = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
            ->select('waybills.waybill_no')
            ->where('action_logs.branch_id',$branch_id)
            ->where('waybills.destination','!=',$city_id)
            ->where('waybills.current_status','>=',4)
            ->where('action_logs.action_id',1)
            ->where('action_logs.action_type','outbound')
            ->where('action_logs.action_date','like',$date.'%')
            ->get()
            ->count();

		$response['branch_out'] = $branch_out;
		$response['same_city'] 	= $same_city;
		$response['other_city'] = $other_city;

		return $response;
	}

	function transit_branch_out_summary($branch_id){
		$date 		= get_date();
		$response 	= array();
		$city_id 	= Auth::user()->city_id;

		$branch_out = Waybill::join('branch_action_logs','branch_action_logs.waybill_id', '=', 'waybills.id')
            ->select('waybills.waybill_no')
            ->where('branch_action_logs.to_branch',$branch_id)
            ->where('branch_action_logs.action_type','transit')
            ->where('waybills.transit_status',4)
            ->where('waybills.transit_date','like',$date.'%')
            ->get()
            ->count();

        $other_city = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
            ->select('waybills.waybill_no')
            ->where('action_logs.branch_id',$branch_id)
            ->where('waybills.destination','!=',$city_id)
            ->where('waybills.current_status','>=',4)
            ->where('action_logs.action_id',1)
            ->where('action_logs.action_type','outbound')
            ->where('action_logs.action_date','like',$date.'%')
            ->get()
            ->count();

		$response['branch_out'] = $branch_out;
		$response['other_city'] = $other_city;

		return $response;
	}

	function user($user_id){
		$response = array();

		$user = User::find($user_id);
		if($user){
			$response['id'] 		= $user->id;
			$response['name'] 		= $user->name;
			$response['city_id'] 	= $user->city_id;
			$response['branch_id'] 	= $user->branch_id;
			$response['role'] 		= $user->role;
			$response['image'] 		= $user->image;
		}else{
			$response['id'] 		= '';
			$response['name'] 		= '';
			$response['city_id'] 	= '';
			$response['branch_id'] 	= '';
			$response['role'] 		= '';
			$response['image'] 		= '';
		}

		return $response;
	}

	function group_by_delivery($city_id){
		$date 		= get_date();

		if(role() == 1){
			//admin view for all counters and delivery men
			$delivery 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.user_id')
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.outbound_date','like',$date.'%')
			->where('action_logs.action_id',1)
			->groupBy('waybills.user_id')
			->get();
		}else{
			//operator view for related counters and delivery men
			$delivery 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.user_id')
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.outbound_date','like',$date.'%')
			->where('waybills.origin',$city_id)
			->where('action_logs.action_id',1)
			->groupBy('waybills.user_id')
			->get();
		}
		
		return $delivery;
	}

	function group_by_inbound_from_city($city_id){
		$date 		= get_date();
		$cities 	= array();

		if(role() == 1){
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.origin')
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.created_at','like',$date.'%')
				->where('action_logs.action_id',5)
				->where('action_logs.action_type','inbound')
				->groupBy('waybills.origin')
				->get();
		}else{
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.origin')
				->where('waybills.inbound_date','like',$date.'%')	
				->where('waybills.destination',$city_id)				
				->where('action_logs.action_id',5)
				->where('action_logs.action_type','inbound')
				->groupBy('waybills.origin')
				->get();
		}

		return $cities;
	}

	function group_by_transit_inbound_from_city($city_id){
		$date 		= get_date();
		$cities 	= array();

		if(role() == 1){
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.origin')
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.created_at','like',$date.'%')
				->where('action_logs.action_id',5)
				->where('action_logs.action_type','inbound')
				->groupBy('waybills.origin')
				->get();
		}else{
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.origin')
				->where('waybills.transit_date','like',$date.'%')	
				->where('waybills.transit',$city_id)				
				->where('action_logs.action_id',5)
				->where('action_logs.action_type','transit')
				->groupBy('waybills.origin')
				->get();

		}

		return $cities;
	}

	function merged_cities_for_inbound($city_id){
		$tmp 		= array();

		foreach(group_by_inbound_from_city($city_id) as $list_1){
			array_push($tmp,$list_1->origin);
		}

		foreach(group_by_transit_inbound_from_city($city_id) as $list_2){
			array_push($tmp,$list_2->origin);
		}

		return array_unique($tmp);
	}

	function group_by_from_city($city_id){
		$date 	= get_date();

		if(role() == 1){
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.origin')
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_logs.action_id',5)
				->where('action_logs.action_type','inbound')
				->groupBy('waybills.origin')
				->get();
		}else{
			$cities = Waybill::select('origin')
				->where('inbound_date','like',$date.'%')
				->where('destination',$city_id)
				->groupBy('origin')
				->get();
		}
		

		return $cities;
	}

	function group_by_transit_from_city($city_id){
		$date 	= get_date();

		if(role() == 1){
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.origin')
				->where('waybills.transit_date','like',$date.'%')
				->where('action_logs.action_id',5)
				->where('action_logs.action_type','transit')
				->groupBy('waybills.origin')
				->get();
		}else{
			$cities = Waybill::select('origin')
				->where('transit_date','like',$date.'%')
				->where('transit',$city_id)
				->groupBy('waybills.origin')
				->get();
		}
		
		return $cities;
	}


	function group_by_to_city($city_id){
		$date 	= get_date();

		if(role() == 1){
			//admin view for all cities of total outbound
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.destination')
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.created_at','like',$date.'%')
				->where('waybills.destination','!=','')
				->where('action_logs.action_type','outbound')
				->groupBy('waybills.destination')
				->get();
		}else{
			//operator view for all cities of related outbound
			$cities = Waybill::select('destination')
				->where('outbound_date','like',$date.'%')
				->where('waybills.origin',$city_id)
				->where('waybills.destination','!=','')
				->groupBy('waybills.destination')
				->get();
		}

		return $cities;
	}

	function group_by_to_transit_city($city_id){
		$date 	= get_date();

		if(role() == 1){
			//admin view for all cities of total outbound
			$cities = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.destination')
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.transit_date','like',$date.'%')
				->where('waybills.destination','!=','')
				->where('action_logs.action_type','outbound')
				->groupBy('waybills.destination')
				->get();
		}else{
			//operator view for all cities of related outbound
			$cities = Waybill::select('destination')
				->where('waybills.transit_date','like',$date.'%')
				->where('waybills.transit',$city_id)
				->where('waybills.destination','!=','')
				->groupBy('waybills.destination')
				->get();
		}

		return $cities;
	}

	function from_city_status_count($origin){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();
		$response 	= array();

		if(role() == 1){
			$total = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.origin',$origin)
				->where('waybills.inbound_date','like',$date.'%')
				->where('action_logs.action_id',5)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$total = Waybill::select('waybill_no')
				->where('waybills.origin',$origin)
				->where('waybills.destination',$city_id)
				->where('waybills.inbound_date','like',$date.'%')
				->get()
				->count();
		}
		
		if(role() == 1){
			$branch_in =  Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.origin',$origin)
				->where('waybills.current_status',5)
				->where('action_logs.action_id',5)
				->where('action_logs.action_date','like',$date.'%')
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$branch_in =  Waybill::select('waybill_no')
				->where('origin',$origin)
				->where('destination',$city_id)
				->where('current_status',5)
				->where('inbound_date','like',$date.'%')
				->get()
				->count();
		}

		$response['total'] 		= $total;
		$response['branch_in'] 	= $branch_in;
		$response['handover'] 	= $total - $branch_in;

		return $response;
	}

	function from_city_transit_status_count($origin){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();
		$response 	= array();

		if(role() == 1){
			$total = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.transit',$origin)
				->where('action_logs.action_date','like',$date.'%')
				->where('action_logs.action_id',5)
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$total = Waybill::select('waybill_no')
				->where('waybills.origin',$origin)
				->where('waybills.transit',$city_id)
				->where('transit_date','like',$date.'%')
				->get()
				->count();
		}
		
		if(role() == 1){
			$branch_in =  Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.origin',$origin)
				->where('waybills.current_status',5)
				->where('action_logs.action_id',5)
				->where('action_logs.action_date','like',$date.'%')
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$branch_in =  Waybill::select('waybill_no')
				->where('origin',$origin)
				->where('transit',$city_id)
				->where('transit_status',1)
				->where('transit_date','like',$date.'%')
				->get()
				->count();
		}

		$response['total'] 		= $total;
		$response['branch_in'] 	= $branch_in;
		$response['handover'] 	= $total - $branch_in;

		return $response;
	}

	function to_city_status_count($city_id){
		$origin 	= Auth::user()->city_id;
		$date 		= get_date();
		$response 	= array();

		if(role() == 1){
			$total = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.destination',$city_id)
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.outbound_date','like',$date.'%')
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$total = Waybill::select('waybill_no')
				->where('waybills.origin',$origin)
				->where('waybills.destination',$city_id)
				->where('outbound_date','like',$date.'%')
				->get()
				->count();
		}
		


		$response['total'] 		= $total;

		return $response;
	}

	function to_transit_city_status_count($city_id){
		$transit_id = Auth::user()->city_id;
		$date 		= get_date();
		$response 	= array();

		if(role() == 1){
			$total = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
				->select('waybills.waybill_no')
				->where('waybills.destination',$city_id)
				->where('action_logs.action_date','like',$date.'%')
				->where('waybills.transit_date','like',$date.'%')
				->groupBy('waybills.waybill_no')
				->get()
				->count();
		}else{
			$total = Waybill::select('waybill_no')
				->where('waybills.transit',$transit_id)
				->where('waybills.destination',$city_id)
				->where('transit_date','like',$date.'%')
				->get()
				->count();
		}
		
		$response['total'] 		= $total;

		return $response;
	}

	function group_by_handover_branch($city_id){
		$date 		= get_date();
		
		$branches = DB::table('waybills')
			->join('action_logs','waybills.waybill_no','=','action_logs.waybill_id')
			->select('action_logs.handover_branch_id')
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->where('action_logs.action_id','>=',6)
			->where('waybills.destination',$city_id)
			->groupBy('action_logs.handover_branch_id')
			->get();

		return $branches;
	}

	function handover_branch_status_count($branch_id){
		$date 		= get_date();
		$response 	= array();
		
		
		$total = Waybill::join('action_logs','waybills.waybill_no','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('waybills.created_at','like',$date.'%')
			->groupBy('waybills.waybill_no')
			->where('action_logs.branch_id',$branch_id)
			->where('action_logs.action_id',6)
			->get()
			->count();

		
		$handover = DB::table('waybills')
			->join('action_logs','waybills.waybill_no','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.branch_id',$branch_id)
			->where('action_logs.action_id',6)
			->where('waybills.current_status',6)
			->groupBy('waybills.waybill_no')
			->get()
			->count();
		
		$received = Waybill::join('action_logs','waybills.waybill_no','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.branch_id',$branch_id)
			->where('action_logs.action_id',7)
			->groupBy('waybills.waybill_no')
			->get()
			->count();
		
		
		//$total 		= 0;
		//$handover 	= 0;
		//$received 	= 0;

		$response['total'] 		= $total;
		$response['handover'] 	= $handover;
		$response['received'] 	= $received;
		

		return $response;
	}

	function stock_statistics($city_id){
		$date 		= current_date();
		$response 	= array();
		$destination= Auth::user()->city_id;

		//remaining
		$opening_in = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.destination',$destination)
			->where('waybills.current_status',5)
			->where('action_logs.action_id',5)
            ->where('action_logs.action_date','not like',$date.'%')
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$opening_handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.destination',$destination)
			->where('action_logs.action_date','not like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',6)
			->where('action_logs.action_id',6)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$opening_postponed 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('waybills.destination',$destination)
			->where('action_logs.action_date','not like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',8)
			->where('action_logs.action_id',8)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$opening 	= $opening_in + $opening_handover + $opening_postponed;


		$branch_in 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',5)
			->where('action_logs.action_id',5)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',6)
			->where('action_logs.action_id',6)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$delivered 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',7)
			->where('action_logs.action_id',7)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$postponed 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',8)
			->where('action_logs.action_id',8)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$rejected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',9)
			->where('action_logs.action_id',9)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$transferred 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',10)
			->where('action_logs.action_id',10)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$accepted 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.waybill_no')
			->where('action_logs.action_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.current_status',11)
			->where('action_logs.action_id',11)
			->groupBy('waybills.waybill_no')
			->get()
			->count();

		$today_total 	= $branch_in + $delivered + $handover + $postponed + $rejected + $transferred + $accepted;
		$remaining  	= $branch_in + $handover + $postponed + $rejected + $transferred + $accepted + $opening_in + $opening_handover + $opening_postponed;

		$response['opening_total']  	= $opening;
		$response['opening_in']  		= $opening_in;
		$response['opening_handover']  	= $opening_handover;
		$response['opening_postponed']  = $opening_postponed;

		$response['today_total']  		= $today_total;
		$response['branch_in']  		= $branch_in;
		$response['handover']  			= $handover;
		$response['delivered']  		= $delivered;
		$response['postponed']  		= $postponed + $opening_postponed;
		$response['rejected']  			= $rejected;
		$response['transferred']  		= $transferred;
		$response['accepted']  			= $accepted;
		$response['remaining']  		= $remaining;

		return $response;
	}

	function inbound_handover_branches($city_id){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();

		$branches 	= Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
			->join('action_logs','action_logs.waybill_id','=','waybills.id')
			->select('branch_action_logs.to_branch')
			->where('waybills.inbound_date','like',$date.'%')
			->where('waybills.destination',$city_id)
			->where('branch_action_logs.action_id',6)
			->where('branch_action_logs.action_type','inbound')
			->where('branch_action_logs.active',1)
			->groupBy('branch_action_logs.to_branch')
			->get();

		return $branches;
	}

	function inbound_handover_transit($city_id){
		$city_id 	= Auth::user()->city_id;
		$date 		= get_date();

		$branches 	= Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
			->join('action_logs','action_logs.waybill_id','=','waybills.id')
			->select('branch_action_logs.to_branch')
			->where('waybills.transit_date','like',$date.'%')
			->where('waybills.transit',$city_id)
			->where('branch_action_logs.action_id',6)
			->where('branch_action_logs.action_type','transit')
			->where('branch_action_logs.active',1)
			->groupBy('branch_action_logs.to_branch')
			->get();

		return $branches;
	}

	function transit_handover_branches($city_id){
		$date 		= get_date();
		$branches 	= Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
			->join('action_logs','action_logs.waybill_id','=','waybills.id')
			->select('branch_action_logs.to_branch')
			->where('waybills.inbound_date','like',$date.'%')
			->where('action_logs.city_id',$city_id)
			->where('waybills.transit_status',2)
			->groupBy('branch_action_logs.to_branch')
			->get();

		return $branches;
	}

	function inbound_handover_count_by_branch($branch_id){
		$date 		= get_date();
		$city_id 	= Auth::user()->city_id;
		$response 	= array();

		$total = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('branch_action_logs.waybill_id')
				->where('waybills.inbound_date','like',$date.'%')
				->where('branch_action_logs.to_branch',$branch_id)
				->where('waybills.current_status','>=',6)
				->where('waybills.destination',$city_id)
				->where('action_logs.active',1)
                ->where('branch_action_logs.active',1)
				->groupBy('branch_action_logs.waybill_id')
				->get()
				->count();

		$received = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('branch_action_logs.waybill_id')
				->where('waybills.inbound_date','like',$date.'%')
				->where('branch_action_logs.to_branch',$branch_id)
				->where('waybills.current_status','>=',7)
				->where('waybills.destination',$city_id)
				->where('action_logs.active',1)
                ->where('branch_action_logs.active',1)
				->groupBy('branch_action_logs.waybill_id')
				->get()
				->count();

		$response['total'] 		= $total;
		$response['received'] 	= $received;
		$response['remain'] 	= $total-$received;

		return $response;
	}

	function inbound_transit_count_by_branch($branch_id){
		$date 		= get_date();
		$city_id 	= Auth::user()->city_id;
		$response 	= array();

		$total = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('branch_action_logs.waybill_id')
				->where('waybills.transit_date','like',$date.'%')
				->where('branch_action_logs.to_branch',$branch_id)
				->where('waybills.transit_status','>=',1)
				->where('waybills.transit',$city_id)
				->where('action_logs.active',1)
                ->where('branch_action_logs.active',1)
				->groupBy('branch_action_logs.waybill_id')
				->get()
				->count();

		$received = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('branch_action_logs.waybill_id')
				->where('waybills.transit_date','like',$date.'%')
				->where('branch_action_logs.to_branch',$branch_id)
				->where('waybills.transit_status','>=',3)
				->where('waybills.transit',$city_id)
				->where('action_logs.active',1)
                ->where('branch_action_logs.active',1)
				->groupBy('branch_action_logs.waybill_id')
				->get()
				->count();

		$response['total'] 		= $total;
		$response['received'] 	= $received;
		$response['remain'] 	= $total-$received;

		return $response;
	}

	function rejected_status_count($type){
		$date 		= get_date();
		$response 	= array();

		$pending = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',9)
				->where('action_logs.action_id',9)
				->groupBy('waybills.waybill_no')
				->get()
				->count();

		$transferred = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',10)
				->where('action_logs.action_id',10)
				->groupBy('waybills.waybill_no')
				->get()
				->count();

		$accepted = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',11)
				->where('action_logs.action_id',11)
				->groupBy('waybills.waybill_no')
				->get()
				->count();

		$total 	= $pending + $transferred + $accepted;

		if($type == 'percent'){
			
			
			

			if($pending == 0){
				$p 		= 0;
				$response['pending'] 		= 0;
			}else{
				$p 		= ($pending/($pending+$transferred+$accepted))*100;
				$response['pending'] 		= $p;
			}

			if($transferred == 0){
				$t 		= 0;
				$response['transferred'] 	= 0;
			}else{
				$t 		= ($transferred/($pending+$transferred+$accepted))*100;
				$response['transferred'] 	= $t;
			}

			if($accepted == 0){
				$a 		= 0;
				$response['accepted'] 		= 0;
			}else{
				$a 		= ($accepted/($pending+$transferred+$accepted))*100;
				$response['accepted'] 		= $a;
			}

			if($p+$t+$a > 0){
				$response['total'] 		= 100;
			}else{
				$response['total'] 		= 0;
			}
				
			//$response['total'] 			= 0;
			//$response['pending'] 			= 0;//($pending/($pending+$transferred+$accepted))*100;
			//$response['transferred'] 		= 0;//($transferred/($pending+$transferred+$accepted))*100;
			//$response['accepted'] 		= 0;//($accepted/($pending+$transferred+$accepted))*100;
		}else{
			$response['total'] 			= $total;
			$response['pending'] 		= $pending;
			$response['transferred'] 	= $transferred;
			$response['accepted'] 		= $accepted;
		}
		

		return $response;
	}

	function cod_users(){
		return User::where('role',4)->orderBy('name','asc')->get();
	}

	function accepted_count_by_user($user_id){
		$waybills = Waybill::join('branch_action_logs','branch_action_logs.waybill_id','=','waybills.id')
				->join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',11)
				->where('action_logs.action_id',11)
				->where('action_logs.action_by',$user_id)
				->groupBy('waybills.waybill_no')
				->get()
				->count();

		return $waybills;
	}

	function transferred_notifications($limit){
		$waybills = Waybill::join('action_logs','action_logs.waybill_id','=','waybills.id')
				->join('users as u', 'u.id', '=', 'action_logs.action_by')
				->select('waybills.waybill_no','action_logs.action_date','u.name AS action_by')
				->where('waybills.current_status',11)
				->where('action_logs.action_id',11)
				->limit($limit)
				->orderBy('action_logs.id','desc')
				->get();

		return $waybills;
	}

	function rejected_waybills_by_city(){
		$response = array();

		$waybills = Waybill::join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.destination')
				->where('action_logs.action_id',9)
				->groupBy('waybills.destination')
				->get();

		return $waybills;
	}

	function rejected_waybills_status_by_city($city_id){
		$response = array();

		$pending 	= Waybill::join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',9)
				->where('action_logs.action_id',9)
				->where('waybills.destination',$city_id)
				->get()
				->count();
		$transferred = Waybill::join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',10)
				->where('action_logs.action_id',10)
				->where('waybills.destination',$city_id)
				->get()
				->count();
		$accepted = Waybill::join('action_logs','action_logs.waybill_id','=','waybills.id')
				->select('waybills.waybill_no')
				->where('waybills.current_status',11)
				->where('action_logs.action_id',11)
				->where('waybills.destination',$city_id)
				->get()
				->count();

		$response['pending'] 		= $pending;
		$response['transferred'] 	= $transferred;
		$response['accepted'] 		= $accepted;
		$response['total'] 			= $pending + $transferred + $accepted;

		return $response;
	}

	function user_role($role){
		if($role == 1){
			return 'Admin';
		}elseif($role == 2){
			return 'Operator';
		}elseif($role == 3){
			return 'Delivery';
		}else{
			return 'Cash On Delivery';
		}
	}

	/** -- inbound waybills count by person -- **/
	function inbound_received_count($user_id){
		$city_id    = Auth::user()->city_id;
		$date 		= get_date();

		$count 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->select('action_logs.action_by')
            ->where('action_logs.action_id',7)
            ->where('action_logs.active',1)
            ->where('action_logs.action_by',$user_id)
            ->where('action_logs.city_id',$city_id)
            ->get()
            ->count();

        return $count;
	}

	function collected_outbound_by_branches(){
		$date 		= get_date();
		$city_id 	= Auth::user()->city_id;

		if(role() == 1){
			//admin view for all cities' branches
			$branches = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches as b', 'b.id', '=', 'action_logs.branch_id')
                ->select('action_logs.branch_id','b.name')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('action_logs.action_id',1)
                ->groupBy('action_logs.branch_id')
                ->orderBy('b.name')
                ->get();
		}else{
			//operator view for related branches
			$branches = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches as b', 'b.id', '=', 'action_logs.branch_id')
                ->select('action_logs.branch_id')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('action_logs.action_id',1)
                ->where('action_logs.city_id',$city_id)
                ->groupBy('action_logs.branch_id')
                ->orderBy('b.name','ASC')
                ->get();
		}
        
        return $branches;
	}

	function collected_count_outbound_by_branches($branch_id){
		$date 	= get_date();

        $count 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->select('waybills.waybill_no')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('action_logs.action_id',1)
                ->where('action_logs.branch_id',$branch_id)
                ->get()
                ->count();

        return $count;
	}

	function collected_count_outbound($city_id){
		$date 	= get_date();

        $count 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->select('waybills.waybill_no')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('action_logs.action_id',1)
                ->where('action_logs.city_id',$city_id)
                ->get()
                ->count();


        return $count;
	}

	function to_receive_outbound_waybills($branch_id){
		$date 	= get_date();

		$waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->select(['waybills.waybill_no','action_logs.action_by','action_logs.action_date'])
                ->where('action_logs.action_date','like',$date.'%')
                ->where('action_logs.action_id',2)
                ->where('action_logs.branch_id',$branch_id)
                ->where('waybills.current_status',2)
                ->get();

        return $waybills;
	}

	function to_receive_inbound_waybills($branch_id){
		$date 	= get_date();

		$waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->select(['waybills.waybill_no','branch_action_logs.action_by','branch_action_logs.action_date'])
                ->where('waybills.inbound_date','like',$date.'%')
                ->where('branch_action_logs.action_date','like',$date.'%')
                ->where('branch_action_logs.active',1)
                ->where('branch_action_logs.action_id',6)
                ->where('waybills.current_status',6)
                ->where('branch_action_logs.to_branch',$branch_id)
                ->where('branch_action_logs.action_type','inbound')
                ->where('waybills.destination',$city_id)
                ->get();

        return $waybills;
	}

	function cargo_status($status){
		if($status == 1){
			return 'outbound - collected';
		}elseif($status == 2){
			return 'outbound - handover';
		}elseif($status == 3){
			return 'outbound - received';
		}elseif($status == 4){
			return 'outbound - received';
		}elseif($status == 5){
			return 'inbound - branch in';
		}elseif($status == 6){
			return 'inbound - handover';
		}elseif($status == 7){
			return 'inbound - received';
		}elseif($status == 8){
			return 'inbound - postponed';
		}elseif($status == 9){
			return 'rejected - rejected';
		}elseif($status == 10){
			return 'rejected - transferred';
		}elseif($status == 11){
			return 'rejected - accepted';
		}else{
			return 'undefined';
		}
	}

	function cargo_transit_status($status){
		if($status == 1){
			return 'transit - branch in';
		}elseif($status == 2){
			return 'transit - handover';
		}elseif($status == 2){
			return 'transit - received';
		}elseif($status == 2){
			return 'transit - branch out';
		}else{
			return 'undefined';
		}
	}

	function password_checker($user_id){
		$user = User::find($user_id);
		if($user){
			$name = strstr($user->email, '@', true);
			if($name == $user->mobile_password){
				return 1;
			}elseif($user->mobile_password == '12345678'){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	function current_status($status){
		if($status == 1){
			return 'collected';
		}elseif($status == 2){
			return 'handover';
		}elseif($status == 3){
			return 'received';
		}elseif($status == 4){
			return 'branch out';
		}elseif($status == 5){
			return 'branch in';
		}elseif($status == 6){
			return 'handover';
		}elseif($status == 7){
			return 'received';
		}else{
			return 'postponed';
		}
	}

	function transit_status($status){
		if($status == 1){
			return 'branch in';
		}elseif($status == 2){
			return 'handover';
		}elseif($status == 3){
			return 'received';
		}elseif($status == 4){
			return 'branch out';
		}
	}

	function am_pm($date){
		return date("A", strtotime($date));
	}

	function time_to_string($string){
		$time = strtotime($string);

		return date("h:i A",$time);
	}

	function string_to_time($string){
		$time = strtotime($string);

		return date("H:i:s",$time);
	}

	function setup_times($city_id){
		$times = SetupTime::where('city_id',$city_id)->where('action_label','branch-in')->first();
		if($times){
			return $times->setting_time;
		}else{
			return '';
		}
	}

	function check_active_time($city_id){
		$times = SetupTime::where('city_id',$city_id)->where('action_label','branch-in')->first();
		if($times){
			if($times->active == 1){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	function main_office($city_id){
		$main = Branch::where('is_main_office',1)->where('city_id',$city_id)->first();
		if($main){
			$sorting = Branch::where('is_sorting',1)->where('city_id',$city_id)->first();
			if($sorting){
				return $sorting->id;
			}else{
				return $main->id;
			}
		}else{
			return '';
		}
		
	}

	/** saved inbound branch-in failed logs **/
	function saved_inbound_branch_in_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_assets/logs/inbound-branch-in-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound handover failed logs **/
	function saved_inbound_handover_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_assets/logs/inbound-handover-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved inbound received failed logs **/
	function saved_inbound_received_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_assets/logs/inbound-received-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved outbound received failed logs **/
	function saved_outbound_received_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_assets/logs/outbound-received-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	/** saved outbound branch-out failed logs **/
	function saved_outbound_branch_out_failed($data){
		$data = $data.PHP_EOL;
		$fp = fopen('_assets/logs/outbound-branch-out-failed.txt','a');
		fwrite($fp, $data);
		fclose($fp);
	}

	function stock_by_branch($branch_id){
		$response = array();

		$handover 	= Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
			->where('branch_action_logs.to_branch',$branch_id)
			->where('waybills.current_status',6)
			->get()
			->count();

		$postponed 	= Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
			->where('branch_action_logs.to_branch',$branch_id)
			->where('waybills.current_status',8)
			->get()
			->count();

		$response['total'] 		= $handover + $postponed;
		$response['handover'] 	= $handover;
		$response['postponed'] 	= $postponed;

		return $response;
	}

	function is_sorting($city_id){
		$sorting = Branch::where('city_id',$city_id)->where('is_sorting',1)->first();
		if($sorting){
			return $sorting->id;
		}else{
			$branch = Branch::where('city_id',$city_id)->where('is_main_office',1)->first();
			return $branch->id;
		}
	}

	function check_sorting(){
		$sorting = Branch::where('city_id',Auth::user()->city_id)->where('is_sorting',1)->first();
		if($sorting){
			return 1;
		}else{
			return 0;
		}
	}

	function sent_to_odoo($cargos,$city,$status){
		$data = array(
			"cargo" 	=> $cargos,
			"city" 		=> $city, 
			"status" 	=> $status
		);       

		$data_string = json_encode($data);  
	                                                                                                                  
		$ch = curl_init('http://uat.royalx.biz/royalx/cargo/status');                                                                   
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");                                                                     
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);                                                                      
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(                                                                          
			'Content-Type: application/json',                                                                                
			'Content-Length: ' . strlen($data_string))                                                                       
		);                                                                                                                   
			                                                                                                                     
		$result = curl_exec($ch);

		//$obj = json_decode($result);
	}


	function access_branch($branch_id){
		$branch = Branch::find($branch_id);
		if($branch){
			if($branch->is_main_office == 1){
				return 1;
			}else{
				return 0;
			}
		}else{
			return 0;
		}
	}

	function total_handover($branch_id){
		$date = get_date();

		$handover = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.inbound_date','like',$date.'%')
			->where('branch_action_logs.to_branch',$branch_id)
			->where('branch_action_logs.active',1)
			->where('branch_action_logs.action_type','inbound')
			->get()
			->count();

		return $handover;
	}

	function total_received($branch_id){
		$date = get_date();

		$handover = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.inbound_date','like',$date.'%')
			->where('branch_action_logs.to_branch',$branch_id)
			->where('branch_action_logs.active',1)
			->where('waybills.current_status','>=',7)
			->where('branch_action_logs.action_type','inbound')
			->get()
			->count();

		return $handover;
	}

	function total_collected($branch_id){
		$date = get_date();

		$collected = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.outbound_date','like',$date.'%')
			->where('action_logs.branch_id',$branch_id)
			->where('action_logs.action_id',1)
			->where('action_logs.action_type','outbound')
			->get()
			->count();

		return $collected;
	}

	function total_outbound_handover($branch_id){
		$date = get_date();

		$handover = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.outbound_date','like',$date.'%')
			->where('action_logs.branch_id',$branch_id)
			->where('action_logs.action_id',2)
			->where('action_logs.action_type','outbound')
			->get()
			->count();

		return $handover;
	}

	function total_outbound_received($branch_id){
		$date = get_date();

		$received = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.outbound_date','like',$date.'%')
			->where('branch_action_logs.from_branch',$branch_id)
			->where('waybills.current_status','>=','3')
			->where('branch_action_logs.action_type','outbound')
			->get()
			->count();

		return $received;
	}

	function collected_by_delivery($branch_id){
        $date = get_date();

        $delivery = User::where('branch_id',$branch_id)->where('role',3)->get();

        return $delivery;
    }

    function collected_count($delivery_id){
        $date = get_date();

		$collected = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.outbound_date','like',$date.'%')
			->where('action_logs.action_id',1)
			->where('action_logs.action_type','outbound')
			->where('waybills.user_id',$delivery_id)
			->get()
			->count();

		return $collected;
    }
    function handover_count($delivery_id){
        $date = get_date();

		$collected = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.outbound_date','like',$date.'%')
			->where('action_logs.action_id',2)
			->where('action_logs.action_type','outbound')
			->where('waybills.user_id',$delivery_id)
			->get()
			->count();

		return $collected;
    }
	function total_transit_handover($city_id){
		$date = get_date();

		$handover = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
			->select('waybills.id')
			->where('waybills.transit_date','like',$date.'%')
			->where('waybills.transit',$city_id)
			->where('branch_action_logs.action_type','transit')
			->get()
			->count();

		return $handover;
	}

	

?>
