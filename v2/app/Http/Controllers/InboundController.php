<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\Action;
use App\ActionLog;
use Illuminate\Support\Facades\DB;

class InboundController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function inbound(){
    	$response = array();

    	$waybills = Inbound::get();

    	return response()->json($waybills);
    }

    public function inbound_by_status($status){
    	if($status == 'branch-in'){
            $keyword    = 'Branch In';
            $mm_keyword = 'ရုံးရှိ';
            $json       = 'json/inbound/branch-in';
        }elseif($status == 'handover'){
            $keyword    = 'Handover';
            $mm_keyword = 'စာလွှဲပြောင်း';
            $json       = 'json/inbound/handover';
        }elseif($status == 'received'){
            $keyword    = 'Received';
            $mm_keyword = 'စာလက်ခံ';
            $json       = 'json/inbound/received';
        }elseif($status == 'postponed'){
            $keyword    = 'Postponed';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/inbound/postponed';
        }else{
            $keyword    = 'All';
            $mm_keyword = '';
            $json       = 'json/inbound/all';
        }
        if(role() == 1){
            return view('admin.inbound.lists',compact('json','keyword','mm_keyword'));
        }elseif(role() == 2){
            return view('operator.inbound.lists',compact('json','keyword','mm_keyword'));
        }else{
            return view('layouts.errors.404');
        }
        
    }

    public function json_inbound_by_status($status){
        $date       = get_date();
        $city_id    = Auth::user()->city_id;
        $response   = array();

        if($status == 'branch-in'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',5)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',5)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.destination',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }
        }elseif($status == 'handover'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',6)
                    ->where('action_logs.action_id',6)
                    ->where('action_logs.active',1)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);  
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',6)
                    ->where('action_logs.action_id',6)
                    ->where('action_logs.active',1)
                    ->where('waybills.destination',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100); 
            }
            
        }elseif($status == 'received'){
            if(role() == 1){
                $waybills  = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',7)
                    ->where('action_logs.action_id',7)
                    ->where('action_logs.active',1)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills  = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',7)
                    ->where('action_logs.action_id',7)
                    ->where('action_logs.active',1)
                    ->where('waybills.destination',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }
        }elseif($status == 'postponed'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status','>=',8)
                    ->where('action_logs.action_id',8)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status','>=',8)
                    ->where('action_logs.action_id',8)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.destination',$city_id)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }
        }else{
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',5)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',5)
                    ->where('waybills.destination',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }
        }
        
        return response()->json($waybills);
    }


    public function inbound_scan(Request $request){

        if($request->action == 'branch-in'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-branch-in');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'handover'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-handover');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'received'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-received');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'postponed'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-postponed');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'rejected'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-rejected');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'transferred'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-transferred');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'accepted'){
            if(Auth::user()->role == 4){
                return view('cod.action-accepted');
            }else{
                return view('layouts.errors.404');
            }
        }else{
            return view('layouts.errors.404');
        }

    }

    public function inbound_action(Request $request){

        if($request->action == 'branch-in'){
            if(Auth::user()->role == 2){
                return view('operator.inbound.action-branch-in');
            }else{

            }
        }elseif($request->action == 'handover'){
            
        }elseif($request->action == 'received'){
            
        }else{

        }
    }


    public function waybill_logs($waybill_no){
        $response = array();

        $logs = ActionLog::where('waybill_no',$waybill_no)->get();

        return response()->json($logs);
    }

    public function inbound_handover_branch($id){
        $json = 'json/inbound-handover-branch/'.$id;

        if(role() == 1){
            return view('admin.inbound.inbound-handover-branch',compact('json','id'));
        }else{
            return view('operator.inbound.inbound-handover-branch',compact('json','id'));
        }
    }

    public function inbound_from_city($id){
        $city = Auth::user()->city_id;
        $date = get_date();
        $json = 'json/inbound-from-city/'.$id;

        if(role() == 1){
            return view('admin.inbound.inbound-from-city',compact('json','id'));
        }else{
            return view('operator.inbound.inbound-from-city',compact('json','id'));
        }
        
    }

    public function json_inbound_from_city($origin){
        $response   = array();
        $date       = get_date();
        $city_id    = Auth::user()->city_id;

        if(role() == 1){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('waybills.origin',$origin)
                ->where('action_logs.action_id',5)
                ->where('action_logs.action_type','inbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);
        }else{
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('waybills.origin',$origin)
                ->where('waybills.destination',$city_id)
                ->where('action_logs.action_id',5)
                ->where('action_logs.action_type','inbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);
        }
        
        
        return response()->json($waybills);
    }

    

    public function json_inbound_handover_branch($id){
        $response   = array();
        $date       = get_date();

        $waybills = Waybill::join('action_logs','waybills.waybill_no','=','action_logs.waybill_no')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',6)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->paginate(100);
        
        return response()->json($waybills);
    }

    public function transit_from_city($id){
        $city = Auth::user()->city_id;
        $date = get_date();
        $json = 'json/transit-from-city/'.$id;

        if(role() == 1){
            return view('admin.inbound.transit-from-city',compact('json','id'));
        }else{
            return view('operator.inbound.transit-from-city',compact('json','id'));
        }
        
    }

    public function json_transit_from_city($origin){
        $response   = array();
        $date       = get_date();
        $city_id    = Auth::user()->city_id;

        if(role() == 1){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('waybills.origin',$origin)
                ->where('action_logs.action_id',5)
                ->where('action_logs.action_type','inbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);
        }else{
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c1', 'c1.id', '=', 'waybills.transit')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit_status','c.shortcode as origin','c1.shortcode as transit','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('waybills.origin',$origin)
                ->where('waybills.transit',$city_id)
                ->where('action_logs.action_id',5)
                ->where('action_logs.action_type','transit')
                ->where('waybills.transit_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);
        }
        
        
        return response()->json($waybills);
    }


    public function handover_by_branch($id){
        $json = '/json/handover-by-branch/'.$id;
        return view('operator.inbound.handover-by-branch',compact('json','id'));
    }

    public function handover_by_transit($id){
        $json = 'json/handover-by-transit/'.$id;
        return view('operator.inbound.handover-by-transit',compact('json','id'));
    }

    public function json_handover_by_branch($id){
        $city_id    = Auth::user()->city_id;
        $response   = array();
        $date       = get_date();

        $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'branch_action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->leftjoin('branches as b', 'b.id', '=', 'branch_action_logs.from_branch')
            ->leftjoin('branches as b2', 'b2.id', '=', 'branch_action_logs.to_branch')
            ->select(['waybills.id','branch_action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS from_branch','b2.name AS to_branch'])
            ->where('branch_action_logs.to_branch',$id)
            ->where('branch_action_logs.action_id',6)
            ->where('branch_action_logs.active',1)
            ->where('branch_action_logs.action_type','inbound')
            ->where('waybills.inbound_date','like',$date.'%')
            ->paginate(100);

        
        return response()->json($waybills);
    }

    public function json_handover_by_transit($id){
        $city_id    = Auth::user()->city_id;
        $response   = array();
        $date       = get_date();

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->join('branches as b2', 'b2.id', '=', 'branch_action_logs.to_branch')
            ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS from_branch','b2.name AS to_branch'])
            ->where('waybills.transit',$city_id)
            ->where('action_logs.action_id',6)
            ->where('action_logs.active',1)
            ->where('branch_action_logs.active',1)
            ->where('action_logs.action_type','transit')
            ->where('action_logs.action_date','like',$date.'%')
            ->paginate(100);

        
        return response()->json($waybills);
    }

    public function rejected_status($status){
        if($status == 'pending'){
            $keyword    = 'Pending';
            $mm_keyword = 'စာပြန်ပို့ရန်';
            $json       = 'json/rejected/pending';
        }elseif($status == 'transferred'){
            $keyword    = 'Transferred';
            $mm_keyword = 'စာပြန်ပို့ထား';
            $json       = 'json/rejected/transferred';
        }else{
            $keyword    = 'Accepted';
            $mm_keyword = 'စာလက်ခံရရှိပြီး';
            $json       = 'json/rejected/accepted';
        }

        if(role() == 1 || role() == 4){
            return view('cod.rejected-lists',compact('keyword','mm_keyword','json'));
        }elseif(role() == 2){
            return view('operator.inbound.rejected-lists',compact('keyword','mm_keyword','json'));
        }else{
            return view('layouts.errors.404');
        }
        
    }

    public function json_rejected_status($status){
        $response = array();
        $city     = Auth::user()->city_id;

        if($status == 'pending'){
            if(Auth::user()->role == 2){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',9)
                    ->where('action_logs.action_id',9)
                    ->where('waybills.destination',$city)
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',9)
                    ->where('action_logs.action_id',9)
                    ->paginate(100);  
            }
        }elseif($status == 'transferred'){
            if(Auth::user()->role == 2){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',10)
                    ->where('action_logs.action_id',10)
                    ->where('waybills.destination',$city)
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',10)
                    ->where('action_logs.action_id',10)
                    ->paginate(100);  
            }
        }else{
            if(Auth::user()->role == 2){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',11)
                    ->where('action_logs.action_id',11)
                    ->where('waybills.destination',$city)
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',11)
                    ->where('action_logs.action_id',11)
                    ->paginate(100);  
            }
        }
        
        return response()->json($waybills);
        
    }

    public function group_by_inbound_receiver($branch_id){
        $date   = get_date();

        if($branch_id){
            $receivers = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->select('action_logs.action_by')
                ->where('waybills.inbound_date','like',$date.'%')
                ->where('action_logs.action_id',7)
                ->where('action_logs.active',1)
                ->where('branch_action_logs.to_branch',$branch_id)
                ->groupBy('action_logs.action_by')
                ->get();
        }   

        return view('operator.inbound.group-by-inbound-receiver',compact('receivers','branch_id'));
    }

    public function rejected_waybills_by_city($city_id){
        $json = 'json/rejected-waybills-by-city/'.$city_id;


        return view('cod.rejected-waybills-by-city',compact('json','city_id'));
    }

    public function json_rejected_waybills_by_city($city_id){
        $date   = get_date();

        if($city_id){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',9)
                    ->where('waybills.destination',$city_id)
                    ->paginate(100); 
        }

        return $waybills;
    }

    public function accepted_by($user_id){
        $json = 'json/accepted-by/'.$user_id;
        
        return view('cod.accepted-by',compact('json','user_id'));
    }

    public function json_accepted_by($user_id){
        $date   = get_date();

        if($user_id){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as accepted_by','b.name AS branch'])
                    ->where('action_logs.action_id',11)
                    ->paginate(100); 
        }

        return $waybills;
    }

    public function coming_waybills(){
        $json       = 'json/coming-waybills';
        $transit    = 'json/transit-coming-waybills';

        $city_id    = Auth::user()->city_id;

        return view('operator.inbound.coming-waybills',compact('json','transit','city_id'));
    }

    public function json_coming_waybills(){
        $city_id = Auth::user()->city_id;

        $waybills = Waybill::join('cities as c2', 'c2.id', '=', 'waybills.origin')
            ->select('origin AS city_id', DB::raw('count(*) as total'),'c2.name AS city_name','c2.mm_name AS city_mm_name')
            ->where('current_status',4)
            ->where('destination',$city_id)
            ->groupBy('origin','c2.name','c2.mm_name')
            ->paginate(10);

        return response()->json($waybills);
    }

    public function coming_waybills_from_city($city_id){
        $json = 'json/coming-waybills/'.$city_id;

        return view('operator.inbound.coming-waybills-from-city',compact('json','city_id'));
    }

    public function transit_coming_waybills_from_city($city_id){
        $json = 'json/transit-coming-waybills/'.$city_id;

        return view('operator.inbound.transit-coming-waybills-from-city',compact('json','city_id'));
    }

    public function json_coming_waybills_from_city($city_id){
        $to_city = Auth::user()->city_id;

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c.mm_name as origin_city','c2.shortcode as destination','c2.mm_name as destination_city','u.name as branch_in_by','b.name AS branch'])
            ->where('waybills.current_status',4)
            ->where('action_logs.action_id',4)
            ->where('action_logs.action_type','outbound')
            ->where('origin',$city_id)
            ->where('destination',$to_city)
            ->orderBy('waybills.waybill_no')
            ->paginate(100);
        

        return response()->json($waybills);
    }

    public function json_transit_coming_waybills_from_city($city_id){
        $date    = get_date();
        $to_city = Auth::user()->city_id;

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
            ->where('waybills.transit_status',0)
            ->where('action_logs.action_id',4)
            ->where('action_logs.action_date','like',$date.'%')
            ->where('waybills.origin',$city_id)
            ->where('transit',$to_city)
            ->paginate(100);

        return response()->json($waybills);
    }



    public function json_transit_coming_waybills(){
        $date    = get_date();
        $city_id = Auth::user()->city_id;
        
        $waybills = Waybill::join('cities as c2', 'c2.id', '=', 'waybills.origin')
            ->select('origin AS city_id', DB::raw('count(*) as total'),'c2.name AS city_name','c2.mm_name AS city_mm_name')
            ->where('transit_status',0)
            ->where('transit',$city_id)
            ->where('waybills.outbound_date','like',$date.'%')
            ->groupBy('origin','c2.name','c2.mm_name')
            ->paginate(10); 

        return response()->json($waybills);
    }
    
    public function view_waybill($id){
        $response   = array();
        $waybill    = Waybill::find($id);
        $action     = ActionLog::where('waybill_id',$id)->where('action_id',5)->first();
        if($waybill){
            $response['action_date']    = $action->action_date.' '.am_pm($action->action_date);
            $response['id']             = $waybill->id;
            $response['waybill_no']     = $waybill->waybill_no;
            $response['origin']         = city($waybill->origin)['name'];
            $response['transit']        = city($waybill->transit)['name'];
            $response['destination']    = city($waybill->destination)['name'];
            $response['current_status'] = '<span class="badge badge-pill badge-'.$waybill->current_status.'">'.current_status($waybill->current_status).'</span>';
            $response['transit_status'] = '<span class="badge badge-pill badge-'.$waybill->transit_status.'">'.transit_status($waybill->transit_status).'</span>';
            $response['branch_in_by']   = user($action->action_by)['name'];
        }

        return response()->json($response);

    }

    public function view_logs($id){
        $waybill = Waybill::find($id);
        $logs = Waybill::join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
            ->join('users as u', 'u.id', '=', 'action_logs.action_by')
            ->join('branches as b', 'b.id', '=','action_logs.branch_id')
            ->join('actions as a', 'a.id', '=','action_logs.action_id')
            ->select(['action_logs.waybill_id','action_logs.action_log','action_logs.action_date','action_logs.action_type','action_logs.branch_id','a.action as action','u.name as action_by','b.name as branch'])
            ->where('action_logs.action_type','!=','outbound')
            ->where('action_logs.waybill_id',$id)
            ->get();
        
        return view('operator.inbound.view-logs',compact('id','logs','waybill'));
    }

    public function to_receive_lists(){
        $city_id = Auth::user()->city_id;
        $date   = get_date();

        /*
        $branches = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->select('branch_action_logs.to_branch',DB::raw('count(*) as total'))
                ->where('waybills.inbound_date','like',$date.'%')
                ->where('branch_action_logs.action_date','like',$date.'%')
                ->where('branch_action_logs.action_id',6)
                ->where('waybills.current_status',6)
                ->where('waybills.destination',$city_id)
                ->where('branch_action_logs.action_type','inbound')
                ->where('waybills.destination',$city_id)
                ->groupBy('branch_action_logs.to_branch')
                ->get();
        */
        $branches = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->select('branch_action_logs.to_branch',DB::raw('count(*) as total'))
                ->where('waybills.inbound_date','like',$date.'%')
                ->where('branch_action_logs.action_date','like',$date.'%') 
                ->where('branch_action_logs.action_id',6)
                ->where('waybills.current_status',6)
                ->where('branch_action_logs.active',1)
                ->where('waybills.destination',$city_id)
                ->where('branch_action_logs.action_type','inbound')
                ->groupBy('branch_action_logs.to_branch')
                ->get();
        
        return view('operator.inbound.to-receive-lists',compact('branches'));
    }

    public function received_by($user_id){
        $json = 'json/inbound/received-by/'.$user_id;

        return view('operator.inbound.received-by',compact('user_id','json'));
    }

    public function json_received_by($user_id){
        $city_id    = Auth::user()->city_id;
        $date       = get_date();

        $waybills  = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->select('waybills.id','action_logs.action_date','c.shortcode AS origin','c2.shortcode AS destination','waybills.current_status','waybills.waybill_no','u.name AS received_by','action_logs.branch_id','b.name AS branch')
            ->where('action_logs.action_id',7)
            ->where('action_logs.active',1)
            ->where('action_logs.city_id',$city_id)
            ->paginate(100);

        return $waybills;
    }

    public function inbound_summary(){
        return view('operator.inbound.summary');
    }


    public function transit_status($status){
        if($status == 'inbound'){
            $keyword    = 'Inbound';
            $mm_keyword = 'စာကောက်';
            $json       = 'json/transit/inbound';
        }elseif($status == 'branch-in'){
            $keyword    = 'Branch In';
            $mm_keyword = 'စာလွှဲပြောင်း';
            $json       = 'json/transit/branch-in';
        }elseif($status == 'handover'){
            $keyword    = 'Handover';
            $mm_keyword = 'စာလက်ခံ';
            $json       = 'json/transit/handover';
        }elseif($status == 'received'){
            $keyword    = 'Received';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/transit/received';
        }elseif($status == 'branch-out'){
            $keyword    = 'Branch Out';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/transit/branch-out';
        }elseif($status == 'outbound'){
            $keyword    = 'Outbound';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/transit/outbound';
        }else{
            $keyword    = '';
            $mm_keyword = '';
            $json       = '';
        }

        return view('operator.inbound.transit',compact('json','keyword','mm_keyword'));
    }

    public function json_transit_status($status){
        $date       = get_date();
        $city_id    = Auth::user()->city_id;
        $response   = array();

        if($status == 'inbound'){
            if(role() == 1){
                $waybills = array();
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',5)
                    ->where('waybills.transit',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.transit_date','like',$date.'%')
                    ->paginate(100);
            }
        }elseif($status == 'branch-in'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.transit_status',1)
                    ->where('action_logs.action_id',5)
                    ->where('action_logs.active',1)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);  
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.transit_status',1)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.transit',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.transit_date','like',$date.'%')
                    ->paginate(100);
            }
            
        }elseif($status == 'handover'){
            if(role() == 1){
                $waybills  = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status',7)
                    ->where('action_logs.action_id',7)
                    ->where('action_logs.active',1)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.transit_status','>=',2)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.transit',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.transit_date','like',$date.'%')
                    ->paginate(100);
            }
        }elseif($status == 'outbound'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status','>=',8)
                    ->where('action_logs.action_id',8)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.transit_status','>=',3)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.transit',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.transit_date','like',$date.'%')
                    ->paginate(100);
            }
        }elseif($status == 'received'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status','>=',8)
                    ->where('action_logs.action_id',8)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.transit_status',3)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.transit',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.transit_date','like',$date.'%')
                    ->paginate(100);
            }
        }elseif($status == 'branch-out'){
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.current_status','>=',8)
                    ->where('action_logs.action_id',8)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('waybills.transit_status',4)
                    ->where('action_logs.action_id',5)
                    ->where('waybills.transit',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.transit_date','like',$date.'%')
                    ->paginate(100);
            }
        }else{
            if(role() == 1){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',5)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                    ->where('action_logs.action_id',5)
                    ->where('waybills.destination',$city_id)
                    ->where('action_logs.city_id',$city_id)
                    ->where('waybills.inbound_date','like',$date.'%')
                    ->paginate(100);
            }
        }
        
        return response()->json($waybills);
    }

    public function inbound_summary_filtered($filtered){
        $city_id    = 44;
        $destination= Auth::user()->city_id;
        $transit    = Auth::user()->city_id;
        $date       = get_date();
        $response   = array();

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->select('waybills.waybill_no')
            ->where('waybills.origin',$city_id)
            ->where('waybills.destination',$destination)
            ->orwhere('waybills.transit',$transit)
            ->where('waybills.current_status',5)
            ->orwhere('waybills.transit_status',1)
            ->where('action_logs.action_date','like',$date.'%')
            ->groupBy('waybills.waybill_no')
            ->get();

        return $waybills;
    }

    
}
