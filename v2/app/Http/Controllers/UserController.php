<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use Hash;

class UserController extends Controller{

    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        if(role() == 1){
            return view('admin.users.index');
        }elseif(role() == 2){
            return view('operator.users');
        }else{
            return view('layouts.errors.404');
        }
        
    }


    public function create()
    {
        if(role() == 1){
            return view('admin.users.create');
        }else{
            return view('layouts.errors.404');
        }
    }


    public function store(Request $request)
    {
        $email = User::where('email',$request->email)->first();
        if(!$email){
            $user                   = new User;
            $user->name             = $request->name;
            $user->email            = $request->email;
            $user->phone            = $request->phone;
            $user->password         = bcrypt($request->password);
            $user->mobile_password  = $request->password;
            $user->role             = $request->role_id;
            $user->city_id          = $request->city;
            $user->branch_id        = $request->branch;
            $user->image            = 'user.png';
            if($user->save()){
                return redirect(base_url().'/users')->with('success','New user has been created.');
            }else{
                return redirect()->back()->with('danger','Operation failed.');
            }
            
        }else{
            return redirect()->back()->with('danger','Operation failed.');
        }
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        $user = User::find($id);

        if(role() == 1){
            return view('admin.users.edit',compact('user'));
        }else{
            return view('layouts.errors.404');
        }
    }


    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if($user){
            $user->name         = $request->name;
            $user->phone        = $request->phone;
            $user->role         = $request->role_id;
            $user->city_id      = $request->city;
            $user->branch_id    = $request->branch;
            $user->image        = 'user.png';
            if($user->save()){
                return redirect(base_url().'/users')->with('success','New user has been created.');
            }else{
                return redirect()->back()->with('danger','Operation failed.');
            }
            
        }else{
            return redirect()->back()->with('danger','Operation failed.');
        }
    }


    public function destroy($id)
    {
        //
    }

    public function json_users(){
        $city_id = Auth::user()->city_id;

        //$branches = Branch::orderBy('name','asc')->paginate(20);
        if(role() == 1){
            $users = User::select('users.id','users.name','users.image','users.email','users.role','users.mobile_password','branches.name AS branch_name')
                ->join('branches','users.branch_id','=','branches.id')
                ->orderBy('users.name','asc')
                ->paginate(20);
        }else{
            $users = User::select('users.id','users.name','users.image','users.email','users.role','branches.name AS branch_name')
                ->join('branches','users.branch_id','=','branches.id')
                ->where('users.city_id',$city_id)
                ->orderBy('users.name','asc')
                ->paginate(20);  
        }
        

        return response()->json($users);
    }

    public function changed_password(Request $request){
        $user = User::find($request->user_id);

        if($user){
            if(Hash::check($request->old_password,$user->password)){
                $user->password         = bcrypt($request->new_password);
                $user->mobile_password  = $request->new_password;
                if($user->save()){
                    return redirect(base_url().'/users')->with('success','New user has been created.');
                }else{
                    return redirect()->back()->with('danger','Operation failed.');
                }
            }else{
                return redirect()->back()->with('danger','Operation failed.');
            }
        }else{
            return redirect()->back()->with('danger','Operation failed.');
        }
    }

    public function my_profile(){
        $user = User::find(Auth::user()->id);
        return view('my-profile',compact('user'));
    }
}
