<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Transit;

class TransitController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index()
    {
        //
    }

    public function create()
    {
        //
    }


    public function store(Request $request)
    {
        //
    }


    public function show($id)
    {
        //
    }


    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        //
    }

    public function transit_routes(){
        return view('operator.transit-routes');
    }
}
