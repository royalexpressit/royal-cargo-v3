<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\ActionLog;
use App\BranchActionLog;
use App\Transit;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        if(Auth::user()->role == 1){
            return view('admin.home');
        }if(Auth::user()->role == 2){
            $link = 'sec-1';
            return view('operator.home',compact('link'));
        }if(Auth::user()->role == 4){
            return view('cod.home');
        }else{
            return view('delivery.home');
        }
        
    }

    public function fetch_section($section){
        if($section == 'section-1'){
            $link = 'sec-1';
            return view('operator.pages.block-1',compact('link'));
        }elseif($section == 'section-2'){
            $link = 'sec-2';
            return view('operator.pages.block-2',compact('link'));
        }elseif($section == 'section-3'){
            $link = 'sec-3';
            return view('operator.pages.block-3',compact('link'));
        }elseif($section == 'section-4'){
            $link = 'sec-4';
            return view('operator.pages.block-4',compact('link'));
        }elseif($section == 'section-5'){
            $link = 'sec-5';
            return view('operator.pages.block-5',compact('link'));
        }elseif($section == 'section-6'){
            $link = 'sec-6';
            return view('operator.pages.block-6',compact('link'));
        }else{
            $link = 'sec-1';
            return view('operator.pages.block-1',compact('link'));
        }
    }

    

    


    

    


    public function advanced_search(){
        return view('operator.search.search');
    }

    public function advanced_search_1(Request $request){
        $title      = 'Search 1';
        $waybill_no = $request->waybill_no;
        $search     = Waybill::where('waybill_no',$waybill_no)->first();
        if($search){
            $waybill        = $search;
            $branch_logs    = BranchActionLog::where('waybill_id',$waybill->id)->get();
            $logs           = ActionLog::where('waybill_id',$waybill->id)->get();
        }else{
            $waybill        = array();
            $branch_logs    = array();
            $logs           = array();
        }
        return view('operator.search.search-result-1',compact('waybill','branch_logs','logs','title'));
    }

    public function view_waybill($waybill_no){
        $title  = 'View Waybill';
        $search        = Waybill::where('waybill_no',$waybill_no)->first();
        if($search){
            $waybill        = $search;
            $branch_logs    = BranchActionLog::where('waybill_id',$waybill->id)->get();
            $logs           = ActionLog::where('waybill_id',$waybill->id)->get();
        }else{
            $waybill        = array();
            $branch_logs    = array();
            $logs           = array();
        }
        return view('operator.search.search-result-1',compact('waybill','branch_logs','logs','title'));
    }

    public function advanced_search_2(Request $request){
        $serial_no   = $request->serial_no;

        $waybills = Waybill::join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->leftjoin('cities as c1', 'c1.id', '=', 'waybills.transit')
                    ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                    ->select(['waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c1.shortcode as transit','c2.shortcode as destination'])
                    ->where('waybills.waybill_no','like','%'.$serial_no.'%')
                    ->orderBy('waybills.waybill_no')
                    ->limit(10)
                    ->get();
        $count = $waybills->count();

        return view('operator.search.search-result-2',compact('serial_no','waybills','count'));
    }

    public function advanced_search_3(Request $request){
        $city_id    = Auth::user()->city_id;
        $date       = $request->branch_out_date_3;
        $filtered   = $request->filtered_3;
        $to_city    = $request->to_city_3;
        
        
        if($filtered == 0){
            //all waybills
            $waybills = Waybill::where('origin',$city_id)->where('destination',$to_city)->where('created_at','like',$date.'%')->get();

            $count = $waybills->count();
        }elseif($filtered == 1){
            //without transit waybills
            $waybills = Waybill::where('origin',$city_id)->where('destination',$to_city)->where('created_at','like',$date.'%')->where('waybills.transit',NULL)->get();

            $count = $waybills->count();
        }elseif($filtered == 2){
            //without transit waybills
            $waybills = Waybill::where('origin',$city_id)->where('destination',$to_city)->where('created_at','like',$date.'%')->where('waybills.transit','!=',NULL)->get();

            $count = $waybills->count();
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }

        return view('operator.search.search-result-3',compact('waybills','date','to_city','filtered','count'));
    }

    public function stock_by_branches(){
        $branch_id = 'asdf';

        return view('operator.stock.by-branches',compact('json','branch_id'));
    }

    public function check_stock_by_branches($branch_id){
        $json = 'json/stock/check-stock/'.$branch_id;

        return view('operator.stock.view-by-branch',compact('branch_id','json'));
    }

    public function json_check_stock_by_branches($branch_id){
        $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
            ->join('branches as b','b.id','=','branch_action_logs.from_branch')
            ->join('branches as b2','b2.id','=','branch_action_logs.to_branch')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->select(['waybills.id','waybills.waybill_no','branch_action_logs.action_date','c.shortcode as origin','c2.shortcode as destination','waybills.current_status','b.name AS from_branch','b2.name AS to_branch'])
            ->where('branch_action_logs.to_branch',$branch_id)
            ->where('waybills.current_status',6)
            ->where('branch_action_logs.action_type','inbound')
            ->paginate(50);

        return $waybills;
    }

    public function daily_check_lists($list){
        $response = array();
        $city_id  = Auth::user()->city_id;
        $branch_id = Auth::user()->branch_id;
        $yesterday = date('Y-m-d',strtotime("-1 days"));

        if($list == 'outbound'){
            $collected = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches','branches.id','=','action_logs.branch_id')
                ->select(['waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch'])
                ->where('action_logs.action_id',1)
                ->where('waybills.current_status',1)
                ->where('waybills.origin','=',$city_id)
                ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                ->count();  

            $handover = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches','branches.id','=','action_logs.branch_id')
                ->select(['waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch'])
                ->where('action_logs.action_id',2)
                ->where('waybills.current_status',2)
                ->where('waybills.origin','=',$city_id)
                ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                ->count();  

            $received = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches','branches.id','=','action_logs.branch_id')
                ->select(['waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch'])
                ->where('action_logs.action_id',3)
                ->where('waybills.current_status',3)
                ->where('waybills.origin','=',$city_id)
                ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                ->count();  

            $branch_out = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches','branches.id','=','action_logs.branch_id')
                ->select(['waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.current_status',4)
                ->where('waybills.origin','=',$city_id)
                ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                ->count();  

            $response['total']      = $collected + $handover + $received + $branch_out;
            $response['collected']  = $collected;
            $response['handover']   = $handover;
            $response['received']   = $received;
            $response['branch_out'] = $branch_out;

            return view('operator.outbound.daily-check-lists',compact('response'));
        }else{
            $handover = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches','branches.id','=','branch_action_logs.to_branch')
                ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch'])
                ->where('action_logs.action_id',6)
                ->where('waybills.current_status',6)
                ->where('branch_action_logs.to_branch',$branch_id)
                ->where('waybills.inbound_date','like','%'.$yesterday.'%')
                ->count();  

            $received = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('branches','branches.id','=','branch_action_logs.to_branch')
                ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch'])
                ->where('action_logs.action_id','>=',7)
                ->where('waybills.current_status','>=',7)
                ->where('branch_action_logs.to_branch',$branch_id)
                ->where('waybills.inbound_date','like','%'.$yesterday.'%')
                ->count();  

            $response['total']      = $handover + $received;
            $response['handover']   = $handover;
            $response['received']   = $received;

            return view('operator.inbound.daily-check-lists',compact('response'));
        }
    }

    public function daily_check_lists_status($list,$status){
        $yesterday = date('Y-m-d',strtotime("-1 days"));
        $city_id   = Auth::user()->city_id;
        $branch_id = Auth::user()->branch_id;
        $response = array();

        if($list == 'outbound'){
            if($status == 'collected'){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users AS u','u.id','=','action_logs.action_by')
                    ->join('users AS u2','u2.id','=','waybills.user_id')
                    ->join('cities AS c','c.id','=','waybills.origin')
                    ->join('branches','branches.id','=','action_logs.branch_id')
                    ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch','c.shortcode AS origin','u.name AS action_by','u2.name AS delivery'])
                    ->where('action_logs.action_id',1)
                    ->where('waybills.current_status',1)
                    ->where('waybills.origin','=',$city_id)
                    ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                    ->paginate(50);  
            }elseif($status == 'handover'){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users AS u','u.id','=','action_logs.action_by')
                    ->join('users AS u2','u2.id','=','waybills.user_id')
                    ->join('cities AS c','c.id','=','waybills.origin')
                    ->join('branches','branches.id','=','action_logs.branch_id')
                    ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch','c.shortcode AS origin','u.name AS action_by','u2.name AS delivery'])
                    ->where('action_logs.action_id',2)
                    ->where('waybills.current_status',2)
                    ->where('waybills.origin','=',$city_id)
                    ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                    ->paginate(50);  
            }elseif($status == 'received'){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users AS u','u.id','=','action_logs.action_by')
                    ->join('users AS u2','u2.id','=','waybills.user_id')
                    ->join('cities AS c','c.id','=','waybills.origin')
                    ->join('branches','branches.id','=','action_logs.branch_id')
                    ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch','c.shortcode AS origin','u.name AS action_by','u2.name AS delivery'])
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status',3)
                    ->where('waybills.origin','=',$city_id)
                    ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                    ->paginate(50);  
            }elseif($status == 'branch-out'){
                $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users AS u','u.id','=','action_logs.action_by')
                    ->join('users AS u2','u2.id','=','waybills.user_id')
                    ->join('cities AS c','c.id','=','waybills.origin')
                    ->join('branches','branches.id','=','action_logs.branch_id')
                    ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.current_status','branches.name AS branch','c.shortcode AS origin','u.name AS action_by','u2.name AS delivery'])
                    ->where('action_logs.action_id',4)
                    ->where('waybills.current_status',4)
                    ->where('waybills.origin','=',$city_id)
                    ->where('waybills.outbound_date','like','%'.$yesterday.'%')
                    ->paginate(50);  
            }else{
                $waybills = array();  
            }
        }elseif($list == 'inbound'){
            if($status == 'handover'){
                $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                    ->join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users AS u','u.id','=','action_logs.action_by')
                    ->join('branches','branches.id','=','branch_action_logs.to_branch')
                    ->join('cities AS c','c.id','=','waybills.origin')
                    ->select(['waybills.id','waybills.waybill_no','waybills.inbound_date','waybills.current_status','branches.name AS branch','c.shortcode AS origin','u.name AS action_by'])
                    ->where('action_logs.action_id',6)
                    ->where('waybills.current_status',6)
                    ->where('branch_action_logs.to_branch',$branch_id)
                    ->paginate(50);  
            }elseif($status == 'received'){
                $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                    ->join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users AS u','u.id','=','action_logs.action_by')
                    ->join('branches','branches.id','=','branch_action_logs.to_branch')
                    ->join('cities AS c','c.id','=','waybills.origin')
                    ->select(['waybills.id','waybills.waybill_no','waybills.inbound_date','waybills.current_status','branches.name AS branch','c.shortcode AS origin','u.name AS action_by'])
                    ->where('action_logs.action_id','>=',7)
                    ->where('waybills.current_status','>=',7)
                    ->where('branch_action_logs.to_branch',$branch_id)
                    ->paginate(50);  
            }else{
                $waybills = array();
            }
        }else{
            $waybills = array();
        }

        return response()->json($waybills);
    }

    public function testing(){
        return view('testing');
    }


    public function board(){
        return view('operator.pages.board');
    }
    public function board_inbound(){
        return view('operator.pages.inbound');
    }
    public function board_outbound(){
        return view('operator.pages.outbound');
    }
}
