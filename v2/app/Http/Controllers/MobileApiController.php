<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use App\Branch;
use App\Waybill;
use App\ActionLog;
use App\Transit;
use App\BranchActionLog;
use Illuminate\Support\Facades\DB;

class MobileApiController extends Controller
{
	/* login api */
    public function login(Request $request){
    	$response = array();
    	$error    = array();

    	$username = $request->username.'@royalx.net';
    	$password = $request->password;

    	if($username || $password){
    		$temp = array();
    		$user = User::where('email',$username)->where('mobile_password',$password)->first();
    		
    		if($user){
    			$data['id'] 				= $user->id;
    			$data['name'] 				= $user->name;
	    		$data['email'] 				= $user->email;
	    		$data['role_id'] 			= $user->role;
	    		$data['branch_id'] 			= $user->branch_id;
	    		$data['city_id'] 			= $user->city_id;
	    		$data['role'] 				= user_role($user->role);
	    		$data['branch_name'] 		= branch($user->branch_id)['name'];
	    		$data['city_name'] 			= city($user->city_id)['name'];
	    		$data['city_shortcode'] 	= city($user->city_id)['shortcode'];
	    		$data['is_main_office'] 	= branch($user->branch_id)['is_main_office'];
	    		$data['is_service_point'] 	= city($user->city_id)['is_service_point'];
	    		$data['remark'] 			= $user->remark;

	    		array_push($temp, $data);
	    		$response['success'] = 1;
	    		$response['data'] 	 = $temp;
    		}else{
    			$response['success'] 	= 0;
    			$response['error']		= 'user not found.';
    		}
    		
    	}else{
    		$response['success'] 	= 0;
    		$response['error']		= 'username and password is required.';
    	}

    	return response()->json($response);
    }

    /* change password api */
    public function api_change_password(Request $request){
    	$response 	= array();
    	$error    	= array();
    	$apiKey 	= request()->header('x-api-key');

    	$user_id 	= $request->user_id;
    	$old_pass 	= $request->old_pass;
    	$new_pass 	= $request->new_pass;

    	if($apiKey ){
	    	if($apiKey == api_key()){
	    		if(!$user_id && !$old_pass && !$new_pass){
			    	$response['success'] 		= 0;
				    $response['error']			= 'user_id,old_pass and new_pass are required.';
			    }else{
			    	if(!$user_id){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'user_id is required.';
			    	}elseif(!$old_pass){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'new_pass are required.';
			    	}elseif(!$new_pass){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'new_pass id required.';
			    	}else{
			    		$user = User::where('id',$user_id)->where('mobile_password',$old_pass)->first();
			    		if($user){
			    			$user->password 		= bcrypt($new_pass);
			    			$user->mobile_password 	= $new_pass;
			    			if($user->save()){
			    				$response['success'] 	= 1;
			    				$response['error']		= 'new password is changed.';
			    			}else{
			    				$response['success'] 	= 0;
			    				$response['error']		= 'error occured.';
			    			}
			    		}else{
			    			$response['success'] 	= 0;
			    			$response['error']		= 'password is incorrect.';
			    		}
			    	}
			    }
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }
    	
    	return response()->json($response);
    }

    /* city lists api */
    public function get_cities(Request $request){
    	$response 	= array();
    	$error    	= array();
    	$apiKey 	= request()->header('x-api-key');

    	if($apiKey ){
	    	if($apiKey == api_key()){
	    		$temp 	= array();
		    	$cities = City::select('id','name','mm_name','shortcode','is_service_point')->orderBy('name','asc')->get();

		    	$response['success'] 	= 1;
		    	$response['data'] 	 	= $cities;
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }
    	
    	return response()->json($response);
    }

    /* branch lists api */
    public function get_branches(Request $request){
    	$response 	= array();
    	$error    	= array();
    	$apiKey 	= request()->header('x-api-key');
    	$city_id    = $request->city_id;

    	if($apiKey ){
	    	if($apiKey == api_key()){
	    		if($city_id){
	    			$temp 	= array();
			    	$branches = Branch::join('cities as c', 'c.id', '=', 'branches.city_id')
                	->select(['branches.id','branches.name','branches.city_id','c.name AS city_name','branches.is_main_office','branches.is_transit_area','branches.is_sorting','branches.active','branches.remark'])
                	->where('branches.active',1)
                	->where('city_id',$city_id)
                	->get();

			    	$response['success'] 	= 1;
		    		$response['data']		= $branches;
	    		}else{
	    			$response['success'] 	= 0;
		    		$response['error']		= 'city_id is required.';
		    	}	
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }
    	
    	return response()->json($response);
    }

    /* delivery/counter lists api */
    public function get_delivery_lists(Request $request){
    	$response 	= array();
    	$error    	= array();
    	$apiKey 	= request()->header('x-api-key');
    	$branch_id  = $request->branch_id;

    	if($apiKey ){
	    	if($apiKey == api_key()){
	    		if($branch_id){
	    			$temp 	= array();
			    	$branches = User::join('branches as b', 'b.id', '=', 'users.branch_id')
                	->select(['users.id','users.name','users.role'])
                	->where('users.branch_id',$branch_id)
                	->where('users.role',3)
                	->get();

			    	$response['success'] 	= 1;
		    		$response['data']		= $branches;
	    		}else{
	    			$response['success'] 	= 0;
		    		$response['error']		= 'branch_id is required.';
		    	}	
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }
    	
    	return response()->json($response);
    }

    /* transit route lists api */
    public function get_transit_routes(Request $request){
    	$apiKey 		= request()->header('x-api-key');
    	$origin 		= $request->origin;
    	$destination 	= $request->destination;
    	
    	$response 		= array();
		$error    		= array();

		if($apiKey){
	    	if($apiKey == api_key()){
	    		if(!$origin && !$destination ){
			    	$response['success'] 		= 0;
				    $response['error']			= 'origin and destination are required.';
			    }else{
			    	if(!$origin){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'origin is required.';
			    	}elseif(!$destination){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'destination are required.';
			    	}else{
						$cities = Transit::join('cities as u', 'u.id', '=', 'transits.transit')
					    ->select(['transits.route','transits.duration','u.id as transit_id','u.name as transit','u.shortcode as transit_shortcode'])
					    ->where('transits.origin',$origin)
					    ->where('transits.active',1)
					    ->where('transits.destination',$destination)
					    ->get();

					    $response['success'] 	= 1;
					    $response['transit'] 		= $cities;
					}
				}
			}else{
				$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
			}
		}else{
			$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
		}

		return response()->json($response);
    }

    /* tracking api */
    public function tracking(Request $request){
    	$response 	= array();
    	$apiKey 	= request()->header('x-api-key');
    	$waybill_no = $request->waybill_no;
    	$waybill 	= Waybill::join('cities AS c','c.id','=','waybills.origin')
    		->leftjoin('users AS u','u.id','=','waybills.user_id')
    		->leftjoin('cities AS c2','c2.id','=','waybills.transit')
    		->leftjoin('cities AS c3','c3.id','=','waybills.destination')
    		->select([
    			'waybills.id',
    			'waybills.waybill_no',
    			'waybills.batch_id',
    			'waybills.current_status',
    			'waybills.transit_status',
    			'c.name as origin',
    			'c2.name as transit',
    			'c3.name as destination',
    			'waybills.outbound_date',
    			'waybills.transit_date',
    			'waybills.inbound_date',
    			'u.name AS delivery'
    		])
    		->where('waybill_no',$waybill_no)
    		->first();

    		if($apiKey){
	    	if($apiKey == api_key()){
	    		/** inbound status **/
	    		if(!$waybill_no){
			    	$response['success'] 		= 0;
				    $response['error']			= 'waybill_no is required.';
			    }else{
			    	if(!$waybill){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'waybill not found.';
			    	}else{
			    		$logs = ActionLog::join('waybills','waybills.id','=','action_logs.waybill_id')
			    			->join('actions','actions.id','=','action_logs.action_id')
			    			->join('users','users.id','=','action_logs.action_by')
			    			->join('cities','cities.id','=','action_logs.city_id')
			    			->join('branches as b','b.id','=','action_logs.branch_id')
			    			->select('actions.action','actions.action_type','users.name as action_by','action_logs.action_date','b.name as branch','cities.name as city','action_logs.active')
			    			->where('waybill_id',$waybill->id)
			    			->get();		
			    		$response['success'] 		= 1;
				    	$response['data']			= $waybill;
				    	$response['action_logs']	= $logs;
			    	}
				}
		    	/** inbound status **/
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }

    	return response()->json($response);
    }

    /** -- Outbound API Action Collections -- **/
    public function outbound_action(Request $request){
    	$response 	= array(); 
    	$error    	= array(); //for errors response
    	$raw        = array();
    	$failed    	= array(); //for action failed waybills
    	$apiKey 	= request()->header('x-api-key');

    	//api request params
    	$waybills   = $request->waybills;
    	$user_id   	= $request->user_id;
    	$delivery_id= $request->delivery_id;
    	$to_branch  = $request->to_branch;
    	$action 	= $request->action;
    	$origin     = $request->origin;
    	$transit    = $request->transit;
    	$destination= $request->destination;

    	//for scanned count
    	$total      = 0;
    	
    	if($apiKey){
	    	if($apiKey == api_key()){
	    		/** check action request value **/
	    		if($action){
		    		if($action == 'collected'){
		    			//-- start collected action --//
			    		if(!$user_id && !$waybills && !$delivery_id){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}elseif(!$delivery_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'delivery_id id required.';
			    			}else{
			    				$raw = explode(',', $waybills);
			    				if($request->is_service_point == 1){
				    				//skipped to received
			    					foreach ($raw as $key => $cargo) {
					    				$total += 1;
					    				$exists = Waybill::where('waybill_no',trim($cargo))->first();
					    				if(!$exists && !is_numeric(trim($cargo[0]))){
					    					$waybill 				= new Waybill;
					    					$waybill->waybill_no    = $cargo;
						            		$waybill->batch_id      = time();
						            		$waybill->origin        = user($user_id)['city_id'];
						            		$waybill->current_status= 3;
						            		$waybill->user_id  		= $request->delivery_id;
						            		$waybill->outbound_date = date('Y-m-d H:i:s'); //marked start date for outbound

					    					if($waybill->save()){
					    						$action                 	= new ActionLog();
										        $action->waybill_id    		= $waybill->id;
										        $action->action_date      	= date('Y-m-d H:i:s');
										        $action->action_by      	= $request->user_id;
										        $action->action_type      	= 'outbound';
										        $action->branch_id    		= user($user_id)['branch_id'];
										        $action->city_id  			= user($user_id)['city_id'];
										        $action->action_id      	= 1;
										        $action->action_log  		= user($user_id)['name'].' collected waybill from '.user($request->delivery_id)['name'];
										        $action->save();

										        $action3                 	= new ActionLog();
										        $action3->waybill_id    	= $waybill->id;
										        $action3->action_date      	= date('Y-m-d H:i:s');
										        $action3->action_by      	= $request->user_id;
										        $action3->action_type      	= 'outbound';
										        $action3->branch_id    		= user($user_id)['branch_id'];
										        $action3->city_id  			= user($user_id)['city_id'];
										        $action3->action_id      	= 3;
										        $action3->action_log  		= user($user_id)['name'].' received waybill.';
										        $action3->save();
					    					}
					    				}else{
							            	array_push($failed, $cargo);
							            }
					    			}
			    				}else{
			    					if($request->handover == 0){
				    					//default action
					    				foreach ($raw as $key => $cargo) {
					    					$total += 1;
					    					$exists = Waybill::where('waybill_no',trim($cargo))->first();
					    					if(!$exists && !is_numeric(trim($cargo[0]))){
					    						$waybill 				= new Waybill;
					    						$waybill->waybill_no    = $cargo;
						            			$waybill->batch_id      = time();
						            			$waybill->origin        = user($user_id)['city_id'];
						            			$waybill->current_status= 1;
						            			$waybill->user_id  		= $request->delivery_id;
						            			$waybill->outbound_date = date('Y-m-d H:i:s'); //marked start date for outbound

					    						if($waybill->save()){
					    							$action                 	= new ActionLog();
										            $action->waybill_id    		= $waybill->id;
										            $action->action_date      	= date('Y-m-d H:i:s');
										            $action->action_by      	= $request->user_id;
										            $action->action_type      	= 'outbound';
										            $action->branch_id    		= user($user_id)['branch_id'];
										            $action->city_id  			= user($user_id)['city_id'];
										            $action->action_id      	= 1;
										            $action->action_log  		= user($user_id)['name'].' collected waybill from '.user($request->delivery_id)['name'];
										            $action->save();
					    						}
					    					}else{
							            		array_push($failed, $cargo);
							            	}
					    				}
				    				}else{
				    					//decleared var with onetime loading model
				    					$from_city      = user($user_id)['city_id'];
				    					$to_branch 		= is_sorting(user($user_id)['city_id']);
				    					$from_branch 	= user($user_id)['branch_id'];
				    					$action_by      = user($user_id)['name'];
				    					$delivery       = user($request->delivery_id)['name'];

				    					//skipped to handover
				    					foreach ($raw as $key => $cargo) {
					    					$total += 1;
					    					$exists = Waybill::where('waybill_no',trim($cargo))->first();
					    					if(!$exists && !is_numeric(trim($cargo[0]))){
					    						$waybill 				= new Waybill;
					    						$waybill->waybill_no    = $cargo;
						            			$waybill->batch_id      = time();
						            			$waybill->origin        = $from_city;
						            			$waybill->current_status= 2;
						            			$waybill->user_id  		= $request->delivery_id;
						            			$waybill->outbound_date = date('Y-m-d H:i:s'); //marked start date for outbound

					    						if($waybill->save()){
					    							//saved action logs for handover action
					    							$action                 	= new ActionLog();
										            $action->waybill_id    		= $waybill->id;
										            $action->action_id      	= 1;
										            $action->action_date      	= date('Y-m-d H:i:s');
										            $action->action_by      	= $request->user_id;
										            $action->action_type      	= 'outbound';
										            $action->branch_id    		= $from_branch;
										            $action->city_id  			= $from_city;
										            $action->action_log  		= $action_by.' collected waybill from '.$delivery;
										            $action->save();

										            //saved next logs for handover action
										            $action2                 	= new ActionLog();
										            $action2->waybill_id     	= $waybill->id;
										            $action2->action_id      	= 2;
										            $action2->action_date      	= date('Y-m-d H:i:s');
										            $action2->action_by      	= $request->user_id;
										            $action2->action_type      	= 'outbound';
										            $action2->branch_id    		= $from_branch;
										            $action2->city_id  			= $from_city;
										            $action2->action_log  		= $action_by.' is handover waybill.';
										            $action2->save();

										            //saved next logs for handover branch action 
										            $branch_log                 = new BranchActionLog();
										            $branch_log->waybill_id     = $waybill->id;
										            $branch_log->from_branch    = $from_branch;
										            $branch_log->to_branch     	= $to_branch;
										            $branch_log->action_id    	= 2;
										            $branch_log->action_by      = $request->user_id;
										            $branch_log->action_type    = 'outbound';
										            $branch_log->save();
					    						}
					    					}else{
							            		array_push($failed, $cargo);
							            	}
					    				}
				    				}
			    				}	

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end collected action --//
				    }elseif($action == 'handover'){
				    	//-- start handover action --//
			    		if(!$user_id && !$waybills && !$to_branch){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}elseif(!$to_branch){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'to_branch id required.';
			    			}else{
			    				$raw = explode(',', $waybills);

			    				
			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill = Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',1)->first();
			    					if($waybill){
				            			$waybill->batch_id      = time();
				            			$waybill->current_status= 2;

			    						if($waybill->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $request->user_id;
								            $action->action_type      	= 'outbound';
								            $action->branch_id    		= user($user_id)['branch_id'];
								            $action->city_id  			= user($user_id)['city_id'];
								            $action->action_id      	= 2;
								            $action->action_log  		= user($user_id)['name'].' handover waybill.';
								            $action->save();

								            $branch_log                 = new BranchActionLog();
								            $branch_log->waybill_id     = $waybill->id;
								            $branch_log->from_branch    = user($user_id)['branch_id'];
								            $branch_log->to_branch     	= $to_branch;
								            $branch_log->action_id    	= 2;
								            $branch_log->action_by      = $request->user_id;
								            $branch_log->action_type    = 'outbound';
								            $branch_log->save();
			    						}
			    					}else{
					            		array_push($failed, $cargo);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end handover action --//
				    }elseif($action == 'received'){
				    	//-- start received action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				$action_by 	= user($user_id)['name'];
			    				$branch_id 	= user($user_id)['branch_id'];
								$city_id 	= user($user_id)['city_id'];

			    				$raw = explode(',', $waybills);
			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					//branch waybill
			    					$waybill = Waybill::select('id')->where('waybill_no',trim($cargo))->where('origin',$city_id)->where('current_status',2)->first();
			    					
			    					//transit waybill
			    					$transit = Waybill::select('id')->where('waybill_no',trim($cargo))->where('transit',$city_id)->where('transit_status',2)->first();

			    					if($waybill){
			    						//waybill from branch
				            			$waybill->batch_id      = time();
				            			$waybill->current_status= 3;

			    						if($waybill->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $request->user_id;
								            $action->action_type      	= 'outbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 3;
								            $action->action_log  		= $action_by.' received waybill from branch.';
								            $action->save();

			    						}
			    					}elseif($transit){
			    						//waybill from transit
			    						$transit->batch_id      = time();
				            			$transit->transit_status= 3;

			    						if($transit->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $transit->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $request->user_id;
								            $action->action_type      	= 'transit';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 3;
								            $action->action_log  		= $action_by.' received waybill from transit.';
								            $action->save();

			    						}
			    					}else{
					            		array_push($failed, $cargo);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end received action --//
				    }elseif($action == 'branch-out'){
				    	//-- start branch-out action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}elseif(!$destination){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'destination is required.';
			    			}else{
			    				$action_by 			= user($user_id)['name'];
			    				$branch_id 			= user($user_id)['branch_id'];
								$city_id 			= user($user_id)['city_id'];
								$destination_name 	= city($destination)['name'];

			    				$raw = explode(',', $waybills);
			    				if(date('A') == 'PM'){
					        		$inbound_date = date('Y-m-d 00:00:00',strtotime("+1 days"));
					        	}else{
					        		$inbound_date = date('Y-m-d H:i:s');
					        	}

			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',3)->first();
			    					$transit 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('transit_status',3)->where('destination',$destination)->first();
			    					$transit2   = Waybill::select('id')->where('waybill_no',trim($cargo))->where('transit_status',3)->first();
			    					
			    					if($waybill){
			    						if($city_id == $destination){
					            			$waybill->batch_id      = time();
					            			$waybill->destination 	= $destination;
					            			$waybill->current_status= 5;
					            			$waybill->inbound_date  = $inbound_date;
					            			
					            			//branch out with transit
					            			if($transit != ''){
					            				$waybill->transit 		= $transit;
					            				$waybill->transit_status= 0;
					            			}

				    						if($waybill->save()){
				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $waybill->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $request->user_id;
									            $action->action_type      	= 'outbound';
									            $action->branch_id    		= $branch_id;
									            $action->city_id  			= $city_id;
									            $action->action_id      	= 4;
									            $action->action_log  		= $action_by.' branch out waybill to '.$destination_name;
									            $action->save();

									            $action2                 	= new ActionLog();
									            $action2->waybill_id    	= $waybill->id;
									            $action2->action_date      	= $inbound_date;
									            $action2->action_by      	= $request->user_id;
									            $action2->action_type      	= 'inbound';
									            $action2->branch_id    		= $branch_id;
									            $action2->city_id  			= $city_id;
									            $action2->action_id      	= 5;
									            $action2->action_log  		= $action_by.' branch in waybill';
									            $action2->save();

				    						}
				    					}else{
				    						$waybill->batch_id      = batch_id();
					            			$waybill->destination 	= $destination;
					            			$waybill->current_status= 4;
					            			
					            			//branch out with transit
					            			if($transit != ''){
					            				$waybill->transit 		= $transit;
					            				$waybill->transit_status= 0;
					            			}

				    						if($waybill->save()){
				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $waybill->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $request->user_id;
									            $action->action_type      	= 'outbound';
									            $action->branch_id    		= $branch_id;
									            $action->city_id  			= $city_id;
									            $action->action_id      	= 4;
									            $action->action_log  		= $action_by.' branch out waybill to '.$destination_name;
									            $action->save();

				    						}
				    					}
			    					}elseif($transit){
			    					    $transit->batch_id      = batch_id();
				            			$transit->destination 	= $destination;
				            			$transit->transit_status= 4;
				            			
			    						if($transit->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $transit->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $request->user_id;
								            $action->action_type      	= 'transit';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 4;
								            $action->action_log  		= $action_by.' branch out waybill to '.$destination_name;
								            $action->save();

			    						}
			    					}elseif($transit2){
			    					    $transit2->batch_id      = batch_id();
				            			$transit2->destination 	= $destination;
				            			$transit2->transit_status= 4;
				            			
			    						if($transit2->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $transit2->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $request->user_id;
								            $action->action_type      	= 'transit';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 4;
								            $action->action_log  		= $action_by.' branch out waybill to '.$destination_name;
								            $action->save();

			    						}
			    					}else{
					            		array_push($failed, $cargo);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end branch-out action --//
				    }else{
				    	$response['success'] 	= 0;
			    		$response['error']		= 'action is invalid.';
				    }
				}else{
					$response['success'] 	= 0;
			    	$response['error']		= 'action is required.';
				}
		    	/** check action request value **/
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }
    	
    	return response()->json($response);
    }

    /** -- Inbound API Action Collections -- **/
    public function inbound_action(Request $request){
    	$response 	= array(); 
    	$error    	= array(); //for errors response
    	$raw        = array();
    	$failed    	= array(); //for action failed waybills
    	$passed    	= array();
    	$apiKey 	= request()->header('x-api-key');
    	$batch_id   = time();

    	//api request params
    	$waybills   = $request->waybills;
    	$user_id   	= $request->user_id;
    	$branch_id  = user($user_id)['branch_id'];
    	$city_id   	= user($user_id)['city_id'];
    	$action 	= $request->action;
    	$handover_id= $request->handover_branch_id;
    	$from_city  = $request->from_city;

    	//for scanned count
    	$total      = 0;
    	
    	if($apiKey){
	    	if($apiKey == api_key()){
	    		/** check action request value **/
	    		if($action){
		    		if($action == 'branch-in'){
		    			//-- start collected action --//
			    		if(!$user_id && !$waybills){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				$raw 			= explode(',', $waybills);
			    				$cargos   		= array();
			    				$branch_in_by 	= user($user_id)['name'];

			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$inbound 	= Waybill::select('id','waybill_no')->where('waybill_no',trim($cargo))->where('current_status',4)->where('origin',$from_city)->where('destination',$city_id)->first();
			    					$transit 	= Waybill::select('id','waybill_no')->where('waybill_no',trim($cargo))->where('transit_status',0)->where('origin',$from_city)->where('transit',$city_id)->first();
			    					$waybill 	= Waybill::select('id','waybill_no')->where('waybill_no',trim($cargo))->first();
			    					$inbound2 	= Waybill::select('id','waybill_no')->where('waybill_no',trim($cargo))->where('current_status',4)->where('origin',$from_city)->first();
			    					
			    					if($inbound){
				            			$inbound->batch_id      = time();
				            			$inbound->current_status= 5;
				            			$inbound->inbound_date 	= date('Y-m-d H:i:s');

			    						if($inbound->save()){
			    							array_push($passed, $inbound->waybill_no);

			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $inbound->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 5;
								            $action->action_log  		= $branch_in_by.' branch in waybill.';
								            $action->save();

			    						}
			    					}elseif($inbound2){
				            			$inbound2->batch_id      = time();
				            			$inbound2->current_status= 5;
				            			$inbound2->transit       =$city_id;
				            			$inbound2->inbound_date 	= date('Y-m-d H:i:s');

			    						if($inbound2->save()){
			    							array_push($passed, $inbound2->waybill_no);

			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $inbound2->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 5;
								            $action->action_log  		= $branch_in_by.' branch in waybill.';
								            $action->save();

			    						}
			    					}elseif($transit){
			    						

				            			$transit->batch_id      = time();
				            			$transit->transit_status= 1;
				            			$transit->transit_date 	= date('Y-m-d H:i:s');

			    						if($transit->save()){
			    							array_push($passed, $transit->waybill_no);

			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $transit->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'transit';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 5;
								            $action->action_log  		= $branch_in_by.' branch in waybill.';
								            $action->save();

			    						}
			    					}elseif(!$waybill){
			    						$waybill = new Waybill();
			    						$waybill->waybill_no 	= trim($cargo);
			    						$waybill->batch_id   	= $batch_id;
			    						$waybill->origin   		= $request->from_city;
			    						$waybill->destination  	= $city_id;
			    						$waybill->current_status= 5;
			    						$waybill->inbound_date 	= date('Y-m-d H:i:s');
			    						if($waybill->save()){
			    							array_push($passed, $cargo);
			    							
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 5;
								            $action->action_log  		= $branch_in_by.' branch in waybill.';
								            $action->save();
			    						}

			    					}else{
					            		array_push($failed, $cargo);

					            		//saved failed waybill at logs
					            		/* ------- continue codes ---------- */
					            		$log_date 	= date('Y-m-d H:i:s');
					            		$log_no 	= $cargo.'(action_by:'.$request->user_id.')';
					            		$log_txt    = '['.$log_date.']['.$from_city.'] '.$log_no;
					            		saved_inbound_branch_in_failed($log_txt);
					            	}

			    				}

			    				//call odoo api
			    				//sent_to_odoo($cargos,$city_id,'in');

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
				    			$response['passed']		= $passed;
			    			}
				    	}
				    	//-- end collected action --//
				    }elseif($action == 'handover'){
				    	//-- start handover action --//
			    		if(!$user_id && !$waybills && !$handover_id){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}elseif(!$handover_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'handover_id id required.';
			    			}else{
			    				$raw = explode(',', $waybills);
			    				$handover_by = user($user_id)['name'];

			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',5)->where('destination',$city_id)->first();
			    					$transit 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('transit_status',1)->where('transit',$city_id)->first();
			    					$transit2 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',5)->where('destination',$city_id)->first();
			    					$received 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',7)->where('destination',$city_id)->first();

			    					if($request->transit == 0){
			    						//waybill
										if($waybill){
					            			$waybill->batch_id      = time();
					            			$waybill->current_status= 6;
				    						if($waybill->save()){
				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $waybill->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $user_id;
									            $action->action_type      	= 'inbound';
									            $action->branch_id    		= $branch_id;
									            $action->city_id  			= $city_id;
									            $action->action_id      	= 6;
									            $action->action_log  		= $handover_by.' handover waybill.';
									            $action->save();

									            $branch_log                 = new BranchActionLog();
									            $branch_log->waybill_id     = $waybill->id;
									            $branch_log->from_branch    = $branch_id;
									            $branch_log->to_branch     	= $handover_id;
									            $branch_log->action_date    = date('Y-m-d H:i:s');
									            $branch_log->action_by      = $user_id;
									            $branch_log->action_id    	= 6;
									            $branch_log->action_type    = 'inbound';
									            $branch_log->save();
				    						}
				    					}elseif($received){
				    						$received->batch_id      = time();
					            			$received->current_status= 6;
				    						if($received->save()){
				    						    //inactive old received logs
			    								DB::table('action_logs')->where('action_id',7)->where('waybill_id', $received->id)->update(['active' => '0']);
			    								DB::table('branch_action_logs')->where('action_id',6)->where('waybill_id', $received->id)->update(['active' => '0']);
			    								
				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $received->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $user_id;
									            $action->action_type      	= 'inbound';
									            $action->branch_id    		= $branch_id;
									            $action->city_id  			= $city_id;
									            $action->action_id      	= 6;
									            $action->action_log  		= $handover_by.' handover waybill.';
									            $action->save();

									            $branch_log                 = new BranchActionLog();
									            $branch_log->waybill_id     = $received->id;
									            $branch_log->from_branch    = $branch_id;
									            $branch_log->to_branch     	= $handover_id;
									            $branch_log->action_date    = date('Y-m-d H:i:s');
									            $branch_log->action_by      = $user_id;
									            $branch_log->action_id    	= 6;
									            $branch_log->action_type    = 'inbound';
									            $branch_log->save();
				    						}
				    					}else{
						            		array_push($failed, $cargo);

						            		//saved failed waybill at logs
						            		/* ------- continue codes ---------- */
						            		$log_date 	= date('Y-m-d H:i:s');
						            		$log_no 	= $cargo.'(action_by:'.$request->user_id.')(M)';
						            		$log_txt    = '['.$log_date.']['.$handover_id.'] '.$log_no;
						            		saved_inbound_handover_failed($log_txt);
						            	}
			    					}else{
			    						if($transit){
			    							//handover to transit branch
			    							$transit->batch_id      = batch_id();
					            			$transit->transit_status= 2;
					            			
				    						if($transit->save()){
				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $transit->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $user_id;
									            $action->action_type      	= 'transit';
									            $action->branch_id    		= $branch_id;
									            $action->city_id  			= $city_id;
									            $action->action_id      	= 6;
									            $action->action_log  		= $handover_by.' handover for transit waybill.';
									            $action->save();

									            $branch_log                 = new BranchActionLog();
									            $branch_log->waybill_id     = $transit->id;
									            $branch_log->from_branch    = $branch_id;
									            $branch_log->to_branch     	= $handover_id;
									            $branch_log->action_date    = date('Y-m-d H:i:s');
									            $branch_log->action_by      = $user_id;
									            $branch_log->action_id    	= 6;
									            $branch_log->action_type    = 'transit';
									            $branch_log->save();

				    						}
			    						}elseif($transit2){
			    							$transit2->batch_id      	 = time();
					            			$transit2->current_status    = 4;
								            $transit2->transit_status    = 2; //handover for transit
								            $transit2->transit       	 = $city_id;
								            $transit2->destination       = NULL;
								            if($transit2->transit_date == ''){
								                $transit2->transit_date = date('Y-m-d H:i:s');
								            }
					            			
				    						if($transit2->save()){
				    							DB::table('action_logs')->where('action_id',5)->where('waybill_id', $transit2->id)->update(['action_type' => 'transit']);

				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $transit2->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $user_id;
									            $action->action_type      	= 'transit';
									            $action->branch_id    		= $branch_id;
									            $action->city_id  			= $city_id;
									            $action->action_id      	= 6;
									            $action->action_log  		= $handover_by.' handover for transit waybill.';
									            $action->save();

									            $branch_log                 = new BranchActionLog();
									            $branch_log->waybill_id     = $transit2->id;
									            $branch_log->from_branch    = $branch_id;
									            $branch_log->to_branch     	= $handover_id;
									            $branch_log->action_date    = date('Y-m-d H:i:s');
									            $branch_log->action_by      = $user_id;
									            $branch_log->action_id    	= 6;
									            $branch_log->action_type    = 'transit';
									            $branch_log->save();
				    						}
			    						}else{
			    							array_push($failed, $cargo);

			    							//saved failed waybill at logs
						            		/* ------- continue codes ---------- */
						            		$log_date 	= date('Y-m-d H:i:s');
						            		$log_no 	= $cargo.'(action_by:'.$request->user_id.')(M)';
						            		$log_txt    = '['.$log_date.']['.$handover_id.'] '.$log_no;
						            		saved_inbound_handover_failed($log_txt);
			    						}
			    					}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end handover action --//
				    }elseif($action == 'received'){
				    	//-- start received action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				$received_by = user($user_id)['name'];
			    				$to_branch 	 = user($user_id)['branch_id'];

			    				$raw = explode(',', $waybills);
			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',6)->where('destination',$city_id)->first();
			    					$postponed 	= Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',8)->where('destination',$city_id)->first();
			    					
			    					if($waybill){
				            			$waybill->batch_id      = batch_id();
				            			$waybill->current_status= 7;
				            			//updated actual received branch
						            	$branch_action                 	= BranchActionLog::where('waybill_id',$waybill->id)->where('action_type','inbound')->where('active',1)->first();
						            	if($branch_action){
						            		$branch_action->to_branch   = $to_branch;
							            	$branch_action->save();
						            	}

			    						if($waybill->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 7;
								            $action->action_log  		= $received_by.' is received waybill.';
								            $action->save();

			    						}
			    					}elseif($postponed){
				            			$postponed->batch_id      = batch_id();
				            			$postponed->current_status= 7;

			    						if($postponed->save()){
			    							$branch_action                 	= BranchActionLog::where('waybill_id',$waybill->id)->first();
							            	if($branch_action){
							            		$branch_action->to_branch     	= $request->branch_id;
								            	$branch_action->save();
							            	}

			    							//inactive old received logs
			    							DB::table('action_logs')->where('action_id',7)->where('waybill_id', $postponed->id)->update(['active' => '0']);

			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $postponed->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 7;
								            $action->action_log  		= $received_by.' is received for postponed waybill.';
								            $action->save();

			    						}
			    					}else{
					            		array_push($failed, $cargo);

					            		//saved failed waybill at logs
					            		/* ------- continue codes ---------- */
					            		$log_date 	= date('Y-m-d H:i:s');
					            		$log_no 	= $cargo.'(action_by:'.$request->user_id.')(M)';
					            		$log_txt    = '['.$log_date.'] '.$log_no;
					            		saved_inbound_received_failed($log_txt);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end received action --//
				    }elseif($action == 'postponed'){
				    	//-- start postponed action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				$raw = explode(',', $waybills);
			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill = Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',7)->where('destination',$city_id)->first();
			    					if($waybill){
				            			$waybill->batch_id      = time();
				            			$waybill->current_status= 8;

			    						if($waybill->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 8;
								            $action->action_log  		= user($user_id)['name'].' is postponed waybills.';
								            $action->save();

			    						}
			    					}else{
					            		array_push($failed, $cargo);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end postponed action --//
				    }elseif($action == 'rejected'){
				    	//-- start rejected action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				$raw = explode(',', $waybills);
			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill = Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',8)->where('destination',$city_id)->first();
			    					if($waybill){
				            			$waybill->batch_id      = time();
				            			$waybill->current_status= 9;

			    						if($waybill->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 9;
								            $action->action_log  		= user($user_id)['name'].' is rejected waybill.';
								            $action->save();

			    						}
			    					}else{
					            		array_push($failed, $cargo);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end rejected action --//
				    }elseif($action == 'transferred'){
				    	//-- start branch-out action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				$raw = explode(',', $waybills);
			    				foreach ($raw as $key => $cargo) {
			    					$total += 1;
			    					$waybill = Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',9)->where('destination',$city_id)->first();
			    					if($waybill){
				            			$waybill->batch_id      = time();
				            			$waybill->current_status= 10;

			    						if($waybill->save()){
			    							$action                 	= new ActionLog();
								            $action->waybill_id    		= $waybill->id;
								            $action->action_date      	= date('Y-m-d H:i:s');
								            $action->action_by      	= $user_id;
								            $action->action_type      	= 'inbound';
								            $action->branch_id    		= $branch_id;
								            $action->city_id  			= $city_id;
								            $action->action_id      	= 10;
								            $action->action_log  		= user($user_id)['name'].' is transferred waybill.';
								            $action->save();

			    						}
			    					}else{
					            		array_push($failed, $cargo);
					            	}
			    				}

			    				$response['success'] 	= 1;
				    			$response['scanned']	= $total;
				    			$response['failed']		= $failed;
			    			}
				    	}
				    	//-- end branch-out action --//
				    }elseif($action == 'accepted'){
				    	//-- start accepted action --//
			    		if(!$user_id && !$waybills ){
			    			$response['success'] 		= 0;
				    		$response['error']			= 'user_id and waybills are required.';
			    		}else{
			    			if(!$user_id){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'user_id is required.';
			    			}elseif(!$waybills){
			    				$response['success'] 	= 0;
			    				$response['error']		= 'waybills are required.';
			    			}else{
			    				if(user($user_id)['role'] == 4){
				    				$raw = explode(',', $waybills);
				    				foreach ($raw as $key => $cargo) {
				    					$total += 1;
				    					$waybill = Waybill::select('id')->where('waybill_no',trim($cargo))->where('current_status',11)->first();
				    					if($waybill){
					            			$waybill->batch_id      = time();
					            			$waybill->transit 		= $transit;
					            			$waybill->destination 	= $destination;
					            			$waybill->current_status= 12;

				    						if($waybill->save()){
				    							$action                 	= new ActionLog();
									            $action->waybill_id    		= $waybill->id;
									            $action->action_date      	= date('Y-m-d H:i:s');
									            $action->action_by      	= $request->user_id;
									            $action->action_type      	= 'outbound';
									            $action->branch_id    		= user($user_id)['branch_id'];
									            $action->city_id  			= user($user_id)['city_id'];
									            $action->action_id      	= 12;
									            $action->action_log  		= user($user_id)['name'].' is accepted waybill.';
									            $action->save();

				    						}
				    					}else{
						            		array_push($failed, $cargo);
						            	}
				    				}

				    				$response['success'] 	= 1;
					    			$response['scanned']	= $total;
					    			$response['failed']		= $failed;
					    		}else{
					    			$response['success'] 	= 0;
			    					$response['error']		= 'user not allowed.';
					    		}
			    			}
				    	}
				    	//-- end accepted action --//
				    }else{
				    	$response['success'] 	= 0;
			    		$response['error']		= 'action is invalid.';
				    }
				}else{
					$response['success'] 	= 0;
			    	$response['error']		= 'action is required.';
				}
		    	/** check action request value **/
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }
    	
    	return response()->json($response);
    }

    /* inbound status api */
    public function inbound_status(Request $request){
    	$response 	= array();
    	$apiKey 	= request()->header('x-api-key');
    	$city_id 	= $request->city_id;
    	$branch_id  = $request->branch_id;
    	$date       = $request->date;

    	if($apiKey){
	    	if($apiKey == api_key()){
	    		/** inbound status **/
	    		if(!$city_id && !$date){
			    	$response['success'] 		= 0;
				    $response['error']			= 'city_id and date are required.';
			    }else{
			    	if(!$city_id){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'city_id is required.';
			    	}elseif(!$date){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'date is required.';
			    	}else{

			    	if($branch_id){
			    	    if(branch($branch_id)['is_main_office'] == 1){
			    	       	$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							    ->select('waybills.waybill_no')
							    ->where('action_logs.action_date','like',$date.'%')
							    ->where('destination',$city_id)
							    ->where('action_type','inbound')
							    ->where('action_logs.action_id',5)
							    ->groupBy('waybills.waybill_no')
							    ->get()
							    ->count();
			    	    }else{
			    	       	$total 	= Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('waybills.inbound_date','like',$date.'%')
								->where('action_type','inbound')
								->where('to_branch',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();  
			    	    }
					}else{
						$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('destination',$city_id)
							->where('action_type','inbound')
							->where('action_logs.action_id',5)
							->groupBy('waybills.waybill_no')
							->get()
							->count();
					}

					if($branch_id){
						if(branch($branch_id)['is_main_office'] == 1){
			    	       	$brach_in 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_type','inbound')
								->where('waybills.destination',$city_id)
								->where('waybills.current_status',5)
								->where('action_logs.action_id',5)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
			    	    }else{
			    	       	$brach_in 	= Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('waybills.inbound_date','like',$date.'%')
								->where('action_type','inbound')
								->where('to_branch',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
			    	    }
						
					}else{
						$brach_in 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('action_type','inbound')
							->where('waybills.destination',$city_id)
							->where('waybills.current_status',5)
							->where('action_logs.action_id',5)
							->groupBy('waybills.waybill_no')
							->get()
							->count();
					}

					if($branch_id){
						$handover 	= Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('waybills.inbound_date','like',$date.'%')
							->where('action_type','inbound')
							->where('to_branch',$branch_id)
							->groupBy('waybills.waybill_no')
							->get()
							->count();
					}else{
						$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('action_type','inbound')
							->where('waybills.destination',$city_id)
							->where('waybills.current_status',6)
							->where('action_logs.action_id',6)
							->groupBy('waybills.waybill_no')
							->get()
							->count();
					}

						

						$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('action_type','inbound')
							->where('waybills.destination',$city_id)
							->where('waybills.current_status',7)
							->groupBy('waybills.waybill_no')
							->get()
							->count();

						$postponed 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('action_type','inbound')
							->where('waybills.destination',$city_id)
							->where('waybills.current_status',8)
							->groupBy('waybills.waybill_no')
							->get()
							->count();

						$rejected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('action_type','inbound')
							->where('waybills.destination',$city_id)
							->where('waybills.current_status',9)
							->groupBy('waybills.waybill_no')
							->get()
							->count();

						$transferred = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
							->select('waybills.waybill_no')
							->where('action_logs.action_date','like',$date.'%')
							->where('action_type','inbound')
							->where('waybills.destination',$city_id)
							->where('waybills.current_status',10)
							->groupBy('waybills.waybill_no')
							->get()
							->count();

			    		$data['total'] 			= $total;
			    		$data['branch_in'] 		= $brach_in;
			    		$data['handover'] 		= $handover;
			    		$data['received'] 		= $received;
			    		$data['postponed'] 		= $postponed;
			    		$data['rejected'] 		= $rejected;
			    		$data['transferred'] 	= $transferred;
			    				
			    		$response['success'] 	= 1;
			    		$response['view_by'] 	= $branch_id? branch($branch_id)['name']:city($city_id)['name'];
			    		$response['date'] 		= $date;
				    	$response['data']		= $data;
			    	}
				}
		    	/** inbound status **/
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }

    	return response()->json($response);
    }

    /* outbound status api */
    public function outbound_status(Request $request){
    	$response 	= array();
    	$apiKey 	= request()->header('x-api-key');
    	$city_id 	= $request->city_id;
    	$branch_id 	= $request->branch_id;
    	$date       = $request->date;

    	if($apiKey){
	    	if($apiKey == api_key()){
	    		/** inbound status **/
	    		if(!$city_id && !$date){
			    	$response['success'] 		= 0;
				    $response['error']			= 'city_id and date are required.';
			    }else{
			    	if(!$city_id){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'city_id is required.';
			    	}elseif(!$date){
			    		$response['success'] 	= 0;
			    		$response['error']		= 'date is required.';
			    	}else{

			    		if($branch_id){
			    			$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_logs.action_id',1)
								->where('action_logs.action_type','outbound')
								->where('action_logs.branch_id',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}else{
							$total 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_type','outbound')
								->where('waybills.origin',$city_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}
			    		
						if($branch_id){
							$collected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('waybills.current_status',1)
								->where('action_logs.action_id',1)
								->where('action_logs.action_type','outbound')
								->where('action_logs.branch_id',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}else{
							$collected 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_type','outbound')
								->where('waybills.origin',$city_id)
								->where('waybills.current_status',1)
								->where('action_logs.action_id',1)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}

						if($branch_id){
							$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('waybills.current_status',2)
								->where('action_logs.action_id',1)
								->where('action_logs.action_type','outbound')
								->where('action_logs.branch_id',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}else{
							$handover 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_type','outbound')
								->where('waybills.origin',$city_id)
								->where('waybills.current_status',2)
								->where('action_logs.action_id',2)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}

						if($branch_id){
							$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('waybills.current_status',3)
								->where('action_logs.action_id',1)
								->where('action_logs.action_type','outbound')
								->where('action_logs.branch_id',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}else{
							$received 	= Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_type','outbound')
								->where('waybills.origin',$city_id)
								->where('waybills.current_status',3)
								->where('action_logs.action_id',3)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}

						if($branch_id){
							$branch_out = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('waybills.current_status',4)
								->where('action_logs.action_id',1)
								->where('action_logs.action_type','outbound')
								->where('action_logs.branch_id',$branch_id)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}else{
							$branch_out = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
								->select('waybills.waybill_no')
								->where('action_logs.action_date','like',$date.'%')
								->where('action_type','outbound')
								->where('waybills.origin',$city_id)
								->where('waybills.current_status',4)
								->where('action_logs.action_id',4)
								->groupBy('waybills.waybill_no')
								->get()
								->count();
						}
						

			    		$data['total'] 			= $total;
			    		$data['collected'] 		= $collected;
			    		$data['handover'] 		= $handover;
			    		$data['received'] 		= $received;
			    		$data['branch_out'] 	= $branch_out;
			    				
			    		$response['success'] 	= 1;
			    		$response['view_by'] 	= $branch_id? branch($branch_id)['name']:city($city_id)['name'];
			    		$response['date'] 		= $date;
				    	$response['data']		= $data;
			    	}
				}
		    	/** inbound status **/
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }

    	return response()->json($response);
    }

    /* rejected status api */
    public function rejected_status(Request $request){
    	$response 	= array();
    	$apiKey 	= request()->header('x-api-key');

    	if($apiKey){
	    	if($apiKey == api_key()){
	    		/** inbound status **/
	    		$pending 	= Waybill::where('current_status',9)
	    					->get()
	    					->count();
	    		$transferred= Waybill::where('current_status',10)
	    					->get()
	    					->count();
	    		$accepted 	= Waybill::where('current_status',11)
	    					->get()
	    					->count();

	    		$data['rejected']   	= $pending + $transferred + $accepted;
			    $data['pending'] 		= $pending;
			    $data['transferred'] 	= $transferred;
			    $data['accepted'] 		= $accepted;
			    				
			    $response['success'] 	= 1;
			    $response['data'] 		= $data;
		    	/** inbound status **/
	    	}else{
		    	$response['success'] 	= 0;
		    	$response['error']		= 'x-api-key is invalid.';
		    }
	    }else{
	    	$response['success'] 	= 0;
		    $response['error']		= 'x-api-key is required in header.';
	    }

    	return response()->json($response);
    }

    /* search api */
    public function search(Request $request){
    	$response 	= array();
    	$term  		= $request->waybill_no;
    	$waybills = Waybill::select('waybill_no')->where('waybill_no','like','%'.$term.'%')->limit(10)->get();
    	if($waybills->count() > 0){
    	    $response['success'] = 1;
    	}else{
    	    $response['success'] = 0;
    	}
    	$response['data']    = $waybills;

    	return response()->json($response);
    }

    /* coming waybill api */
    public function coming_waybill(Request $request){
    	$response 	= array();
        $city_id 	= $request->city_id;

        $destination_count = Waybill::where('current_status',4)->where('destination',$city_id)->get()->count(); 
        $transit_count     = Waybill::where('transit_status',0)->where('transit',$city_id)->get()->count();
        
        $destination = Waybill::join('cities','cities.id','=','waybills.origin')
        	->select('cities.name AS city',DB::raw('count(*) as total'))
        	->where('current_status',4)
        	->where('destination',$city_id)
        	->groupBy('cities.name')
        	->count(); 

        $transit     = Waybill::join('cities','cities.id','=','waybills.origin')
	        ->select('cities.name AS city',DB::raw('count(*) as total'))
	        ->where('transit_status',0)
	        ->where('transit',$city_id)
	        ->groupBy('cities.name')
	        ->count(); 



        $response['total'] 			= $destination_count + $transit_count;
        $response['destination'] 	= $destination;
        $response['transit'] 		= $transit;

        return response()->json($response);
    }

    public function sent_odoo_api_callback(Request $request){
    	$cargos = array();
    	$raw 	= explode(',', $request->cargos);
    	foreach ($raw as $cargo) {
    		array_push($cargos,$cargo);
    	}

		//sent_to_odoo($cargos,$request->city,'in');

		return response()->json(['status' => 'sent']);
	}
}
