<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\ActionLog;
use App\BranchActionLog;
use App\Transit;
use Illuminate\Support\Facades\DB;

class StockController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function stock_statistics(){
        if(role() == 1){
            return view('operator.stock.stock-statistics');
        }elseif(role() == 2){
            return view('operator.stock.stock-statistics');
        }else{
            return view('layouts.errors.404');
        }
    }

    public function stock_statistics_by_status($status){
        if($status == 'opening'){
            $title      = 'Opening';
            $mm_title   = 'အရင်ရက်လက်ကျန်';
            $json       = 'json/stock-statistics/opening';
        }elseif($status == 'inbound'){
            $title      = 'Inbound';
            $mm_title   = 'ယနေ့စာဝင်';
            $json       = 'json/stock-statistics/inbound';
        }elseif($status == 'delivered'){
            $title      = 'Delivered';
            $mm_title   = 'စာပို့ဆောင်ပြီး';
            $json       = 'json/stock-statistics/delivered';
        }elseif($status == 'remaining'){
            $title      = 'Remaining';
            $mm_title   = 'စာဝေလက်ကျန်';
            $json       = 'json/stock-statistics/remaining';
        }else{
            $title      = 'Postponed';
            $mm_title   = 'ဆိုင်းငံ့စာ';
            $json       = 'json/stock-statistics/postponed';
        }
        

        return view('operator.stock.stock-status',compact('json','title','mm_title'));
    }

    public function json_stock_statistics_by_status($status){
        $city_id    = Auth::user()->city_id;
        $date       = current_date();

        if($status == 'opening'){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.current_status','!=',7)
                ->where('waybills.transit_status','!=','')
                ->where('action_logs.action_type','inbound')
                ->where('waybills.destination',$city_id)
                ->where('waybills.inbound_date','not like',$date.'%')
                ->orderBy('waybill_no')
                ->paginate(100); 
            
        }elseif($status == 'inbound'){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.current_status','!=',7)
                ->where('waybills.transit_status','!=','')
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybill_no')
                ->paginate(100);     
            
        }elseif($status == 'delivered'){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('waybills.current_status',7)
                ->where('action_logs.action_id',7)
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybill_no')
                ->paginate(100);
        }elseif($status == 'remaining'){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('waybills.current_status','!=',7)
                ->where('action_logs.action_id',5)
                ->orderBy('waybill_no')
                ->paginate(100); 
        }elseif($status == 'total'){
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','waybills.current_status','c.shortcode as origin','c2.shortcode as destination'])
                ->where('waybills.current_status','!=',7)
                ->where('action_logs.action_id','>',7)
                ->orderBy('waybill_no')
                ->distinct('waybills.waybill_no')
                ->orderBy('waybill_no')
                ->paginate(100); 
        }else{
            $waybills = array();
        }

        return response()->json($waybills);
    }


}
