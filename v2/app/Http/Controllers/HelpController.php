<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\ActionLog;
use App\BranchActionLog;
use App\Transit;
use Illuminate\Support\Facades\DB;

class HelpController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function help(){
        $help = '';

        return view('help',compact('waybills'));
    }

    public function complaints_ask(Request $request){
        $waybill_no = $request->waybill_no;
        if($waybill_no){
            $waybill = Waybill::where('waybill_no',$waybill_no)->first();
            if($waybill){
                if($waybill->current_status == 1){
                    if($request->complaint_id == 1){
                        $response['msg']     = 'စာအမှတ် မှာ စာကောက်(collected)လုပ်ထားပြီးကြောင်း တွေ့ရပါတယ်ရှင်။ ဒါကြောင့် နောက်တစ်ကြိမ် ထပ်ပြီး စာကောက်(collected) လုပ်လို့ မရတော့ပါဘူးရှင်။';
                        //$response['msg']     = 'စာအမှတ် မှာ စာဝင်(Branch In)လုပ်ထားပြီးကြောင်း တွေ့ရပါတယ်ရှင်။ ဒါကြောင့် နောက်တစ်ကြိမ် ထပ်ပြီး စာဝင်(Branch In) လုပ်လို့ မရတော့ပါဘူးရှင်။';
                    }elseif($request->complaint_id == 2){
                        $response['msg']     = 'စာအမှတ် မှာ စာလွှဲပြောင်း(handover)အဆင့်ထိ လုပ်ထားပြီးကြောင့်တွေ့ရှိရပါတယ်ရှင်။ ဒါကြောင့် စာကောက်(collected) လုပ်လို့မရတော့ပါဘူးရှင်။ နောက်တစ်ဆင့် အနေဖြင့် စာလက်ခံ(received) လုပ်ရပါမယ်ရှင်။';
                    }elseif($request->complaint_id == 3){
                        $response['msg']     = 'waybill not found';
                    }else{
                        $response['msg']     = 'waybill not found';
                    }
                    
                }elseif($waybill->current_status == 2){
                    if($request->complaint_id == 1){
                        $response['msg']     = 'စာအမှတ် မှာ စာကောက်(collected)လုပ်ထားပြီးကြောင်း တွေ့ရပါတယ်ရှင်။ ဒါကြောင့် နောက်တစ်ကြိမ် ထပ်ပြီး စာကောက်(collected) လုပ်လို့ မရတော့ပါဘူးရှင်။';
                        //$response['msg']     = 'စာအမှတ် မှာ စာဝင်(Branch In)လုပ်ထားပြီးကြောင်း တွေ့ရပါတယ်ရှင်။ ဒါကြောင့် နောက်တစ်ကြိမ် ထပ်ပြီး စာဝင်(Branch In) လုပ်လို့ မရတော့ပါဘူးရှင်။';
                    }elseif($request->complaint_id == 2){
                        $response['msg']     = 'စာအမှတ် မှာ စာလွှဲပြောင်း(handover)အဆင့်ထိ လုပ်ထားပြီးကြောင့် တွေ့ရှိရပါတယ်ရှင်။ ဒါကြောင့် စာကောက်(collected) ပြန်လုပ်လို့မရတော့ပါဘူးရှင်။ နောက်တစ်ဆင့် အနေ​နှင့် စာလက်ခံ(received)အဆင့် လုပ်ရပါမယ်ရှင်။';
                    }elseif($request->complaint_id == 3){
                        $response['msg']     = 'waybill not found';
                    }else{
                        $response['msg']     = 'waybill not found';
                    }
                }elseif($waybill->current_status == 3){
                    $response['msg']     = 'waybill not found';
                }elseif($waybill->current_status == 4){
                    $response['msg']     = 'waybill not found';
                }elseif($waybill->current_status == 5){
                    $response['msg']     = 'waybill not found';
                }elseif($waybill->current_status == 6){
                    $response['msg']     = 'waybill not found';
                }elseif($waybill->current_status == 7){
                    $response['msg']     = 'waybill not found';
                }else{
                    $response['msg']     = 'waybill not found';
                }   
                $response['success'] = 1;
            }else{
                $response['success'] = 0;
                $response['msg']     = 'waybill not found';
            }
        }

        return response()->json($response);
    }
}
