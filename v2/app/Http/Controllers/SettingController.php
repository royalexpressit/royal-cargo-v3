<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Waybill;
use App\ActionLog;
use App\BranchActionLog;
use App\Transit;
use App\SetupTime;
use Illuminate\Support\Facades\DB;

class SettingController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function setting(){
        if(role() == 1){
            return view('admin.setting.index');
        }elseif(role() == 2){
            return view('operator.setting.index');
        }else{
            return view('layouts.errors.404');
        }
    	
    }

    public function setting_waybill(){
        if(role() == 1){
    	    return view('admin.setting.waybill-setting');
        }else{
            return view('layouts.errors.404');
        }
    }

    public function waybill_delete(Request $request){
    	$filtered   = $request->filtered;
    	$response  	= array();

    	if($filtered == 'date'){
    		$date 				= $request->date;

    		$waybill 			= Waybill::where('created_at','like',$date.'%');
	    	$action_logs    	= ActionLog::where('created_at','like',$date.'%');
	    	$branch_action_logs = BranchActionLog::where('created_at','like',$date.'%');
	    
	    	if($waybill || $action_logs || $branch_action_logs){
	    		$waybill->delete();
		    	$action_logs->delete();
		    	$branch_action_logs->delete();

	    		$response['success'] = 1;
	    		$response['message'] = 'Waybill records for '.$date.' have been deleted.';
	    	}else{
	    		$response['success'] = 0;
	    		$response['message'] = 'Something wrong.';
	    	}
    	}else{
    		$waybill_no 	=	$request->waybill_no;
    		$waybill 		= Waybill::where('waybill_no',$waybill_no)->first();
    		
    		if($waybill){
    			$action_logs    	= ActionLog::where('waybill_id',$waybill->id);
	    		$branch_action_logs = BranchActionLog::where('waybill_id',$waybill->id);

    			$waybill->delete();
		    	$action_logs->delete();
		    	$branch_action_logs->delete();
    		}else{
	    		$response['success'] = 0;
	    		$response['message'] = 'Waybill not found.';
	    	}
    	}
	    	
    	return response()->json($response);
    }

    public function setting_routes(Request $request){
        $origin         = $request->origin;
        $destination    = $request->destination;
        $transit        = $request->transit;
        $data = array();

        $route = Transit::where('origin',$origin)->where('destination',$destination)->where('transit',$transit)->first();
        if($route){
            if($route->active == 0){
                $route->active = 1;
            }else{
                $route->active = 0;
            }
            $route->save();

            $data['success']    = 1;
            $data['active']     = $route->active;
            $data['msg']        = 'updated';
        }else{
            $route              = new Transit;
            $route->route       = city($origin)['shortcode'].'-'.city($destination)['shortcode'];
            $route->origin      = $origin;
            $route->transit     = $transit;
            $route->destination = $destination;
            $route->duration    = '3 Days';
            $route->save();

            $data['success']    = 1;
            $data['active']     = $route->active;
            $data['msg']        = 'created';
        }
        return response()->json($data);
    }

    public function setting_times(){
        return view('operator.setting.setting-times');
    }

    public function setup_times(Request $request){
        $city_id = Auth::user()->city_id;
        $response = array();

        $setup = SetupTime::where('city_id',$city_id)->where('action_label','branch-in')->first();
        if($setup){
            $setup->setting_time = string_to_time($request->time);
            if(isset($request->active) && $request->active == 1){
                $setup->active       = 1;
            }else{
                $setup->active       = 0;
            }

            if($setup->save()){
                $response['success']    = 1;
                $response['active']     = $request->active;
                $response['time']       = $request->time;
            }else{
                $response['success']    = 0;
                $response['msg']        = 'error';
            }
        }

        return response()->json($response);
    }

    public function run_raw_query(){

        return view('admin.setting.run-raw-query');
    }

    public function run_query1(Request $request){
        $table  = $request->table;
        $limit  = $request->limit;
        $order  = $request->order;
        $field  = $request->field;
        $option = $request->option;

        $result = DB::table($table)->limit($limit)->orderBy($field,$order)->get();
        
        return view('admin.setting.query-result',compact('result'));
    }

    public function run_query2(Request $request){
        $table  = $request->table2;
        $search = $request->search2;
        $order  = $request->order2;
        $field  = $request->field2;
        $limit  = $request->limit2;
        $option = $request->option2;

        $result = DB::table($table)->where($field,'like','%'.$search.'%')->orderBy($field,$order)->limit($limit)->get();
      
        return view('admin.setting.query-result',compact('result'));
    }

    public function edit_waybill(){
        $waybill_no = $_GET['waybill'];
        $waybill = Waybill::where('waybill_no',$waybill_no)->first();

        return view('admin.setting.edit-waybill',compact('waybill'));
    }

    public function update_waybill(Request $request){
        $waybill_no     = $request->waybill_no;
        $destination    = $request->destination;

        $waybill = Waybill::where('waybill_no',$waybill_no)->first();
        if($waybill){
            $waybill->destination = ($destination=='Not Set'? NULL:$destination) ;
            if($waybill->save()){
                return 'saved';
            }else{
                return 'not saved';
            }
        }
    }

    public function change_city(){
        return view('operator.setting.change-city');
    }

    public function change_access_city(Request $request){
        $branch     = $request->branch;
        $user_id    = Auth::user()->id;
        $city_id    = branch($branch)['city_id'];

        $user = User::find($user_id);
        $user->branch_id    = $branch;
        $user->city_id      = $city_id;
        if($user->save()){
            $response['success'] = 1;
            $response['msg']     = 'city and branch are changed.';
        }else{
            $response['success'] = 0;
            $response['msg']     = 'somethine wrong.';
        }

        return response()->json($response);
    }

    public function config_routes($city_id){
        $routes = Transit::select('origin','destination')->where('origin',$city_id)->groupBy('origin','destination')->get();

        return view('config-routes',compact('routes','city_id'));
    }

    public function set_routes(){
        $origin         = $_GET['origin'];
        $destination    = $_GET['destination'];
        $routes = Transit::where('origin',$origin)->where('destination',$destination)->where('active',1)->get();


        return view('set-routes',compact('routes','origin','destination'));
    }

    public function changed_date(Request $request){
        $date = $request->set_date;
        setup_date($date);

        return response()->json($date);
    }

    public function marked_branch(Request $request){
        $response = array();

        $branch = $request->branch;
        $name   = branch($branch)['name'];

        setup_branch($branch);

        $response['branch_id']  = $branch;
        $response['branch_name']= $name;

        return response()->json($response);
    }

    public function clear_branch(Request $request){
        $response = array();
        clear_branch();

        $response['msg']  = 'clear branch';
        return response()->json($response);
    }
}
