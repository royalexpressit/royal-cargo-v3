<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use App\Branch;
use App\Waybill;
use App\ActionLog;
use App\Transit;
use App\BranchActionLog;
use Illuminate\Support\Facades\DB;

class AiController extends Controller{
	public function __construct(){
        $this->middleware('auth');
    }

	public function ai_outbound_received(){
		return view('ai.outbound-received');
	}

	public function scanned_outbound_received(Request $request){
		$response 	= array();
		$failed   	= array();
		if(date('A') == 'PM'){
        	$inbound_date = date('Y-m-d 00:00:00',strtotime("+1 days"));
        }else{
        	$inbound_date = date('Y-m-d 00:00:00');
        }

		$waybill 	= $request->waybill;
		$shortcode 	= substr($waybill, -3);
		$action_by 	= user($request->user_id)['name'];

		$city = City::where('shortcode',$shortcode)->first();
		if($city){
			$digital 	= Waybill::where('waybill_no',$request->waybill)->where('current_status',2)->first();
			
			if($digital){
				if($city->id == $request->city_id){
					$digital->current_status 	= 5;
					$digital->destination 		= $city->id;
					$digital->inbound_date 		= $inbound_date;

					if($digital->save()){
						$log 				= new ActionLog;
						$log->waybill_id	= $digital->id;
						$log->action_id 	= 3;
						$log->action_by     = $request->user_id;
						$log->action_date	= date('Y-m-d H:i:s');
						$log->action_type 	= 'outbound';
						$log->branch_id     = $request->branch_id;
						$log->city_id 		= $request->city_id;
						$log->action_log    = $action_by.' is received waybill.';
						$log->active = 1;
						$log->save();

						$log2 				= new ActionLog;
						$log2->waybill_id	= $digital->id;
						$log2->action_id 	= 4;
						$log2->action_by    = $request->user_id;
						$log2->action_date	= date('Y-m-d H:i:s');
						$log2->action_type 	= 'outbound';
						$log2->branch_id    = $request->branch_id;
						$log2->city_id 		= $request->city_id;
						$log2->action_log   = $action_by.' is branch out waybill.';
						$log2->active 		= 1;
						$log2->save();

						$log3 				= new ActionLog;
						$log3->waybill_id	= $digital->id;
						$log3->action_id 	= 5;
						$log3->action_by    = $request->user_id;
						$log3->action_date	= date('Y-m-d H:i:s');
						$log3->action_type 	= 'inbound';
						$log3->branch_id    = $request->branch_id;
						$log3->city_id 		= $request->city_id;
						$log3->action_log   = $action_by.' is branch in waybill.';
						$log3->active 		= 1;
						$log3->save();

						$response['success'] 	= 1;
						$response['voice'] 		= $shortcode.'.mp3';
						$response['message'] 	= '<li class="list-group-item text-success"><i class="fa fa-check"></i> '.$request->waybill.' <span class="badge badge-pill badge-success pull-right">'.$shortcode.' စာထွက် - စာဝင်</span></li>';
					}
				}else{
					$digital->current_status 	= 4;
					$digital->destination 		= $city->id;

					if($digital->save()){
						$log 				= new ActionLog;
						$log->waybill_id	= $digital->id;
						$log->action_id 	= 3;
						$log->action_by     = $request->user_id;
						$log->action_date	= date('Y-m-d H:i:s');
						$log->action_type 	= 'outbound';
						$log->branch_id     = $request->branch_id;
						$log->city_id 		= $request->city_id;
						$log->action_log    = $action_by.' is received waybill.';
						$log->active 		= 1;
						$log->save();

						$log2 				= new ActionLog;
						$log2->waybill_id	= $digital->id;
						$log2->action_id 	= 4;
						$log2->action_by    = $request->user_id;
						$log2->action_date	= date('Y-m-d H:i:s');
						$log2->action_type 	= 'outbound';
						$log2->branch_id    = $request->branch_id;
						$log2->city_id 		= $request->city_id;
						$log2->action_log   = $action_by.' is branch out waybill.';
						$log2->active 		= 1;
						$log2->save();

						$response['success'] 	= 1;
						$response['voice'] 		= $shortcode.'.mp3';
						$response['message'] 	= '<li class="list-group-item text-success"><i class="fa fa-check"></i> '.$request->waybill.' <span class="badge badge-pill badge-success pull-right">'.$shortcode.' စာထွက်</span></li>';
					}
				}
			}else{
				//saved failed waybill at logs
            	/* ------- continue codes ---------- */
            	$log_date 	= date('Y-m-d H:i:s');
            	$log_no 	= $waybill.'(action_by:'.$request->user_id.')';
            	$log_txt    = '['.$log_date.'] '.$log_no;
            	saved_outbound_received_failed($log_txt);

				$response['success'] 	= 0;
				$response['voice'] 		= $shortcode.'.mp3';
				$response['message'] 	= '<li class="list-group-item text-danger"><i class="fa fa-times"></i> '.$request->waybill.'<span class="badge badge-pill badge-danger pull-right">စာလွှဲစာရင်းထဲမတွေ့</span></li></li>';
			}
		}else{
			//saved failed waybill at logs
            /* ------- continue codes ---------- */
            $log_date 	= date('Y-m-d H:i:s');
            $log_no 	= $waybill.'(action_by:'.$request->user_id.')';
            $log_txt    = '['.$log_date.'] '.$log_no;
            saved_outbound_received_failed($log_txt);

			$response['success'] 	= 0;
			$response['voice'] 		= $shortcode.'.mp3';
			$response['message'] 	= '<li class="list-group-item text-danger"><i class="fa fa-times"></i> '.$request->waybill.'<span class="badge badge-pill badge-danger pull-right">နံပါတ်မှားနေသည်</span></li>';
		}

		return response()->json($response);
	}

	public function checked_handover(){
		
	}
}



