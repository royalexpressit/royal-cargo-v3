<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Waybill;
use App\ActionLog;
use App\Transit;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }

    /* report - form page */
    public function reports(){
        return view('operator.reports.reports');
    }

    /** report - search 1 view page **/
    public function search_action_one(Request $request){
        $city       = Auth::user()->city_id;
        $date       = $request->branch_out_date_1;
        $filtered   = $request->filtered_1;
        $json       = 'json/reports/action-1';

        return view('operator.reports.search-1',compact('city','waybills','date','filtered','json'));
    }

    /** report - search 2 view page **/
    public function search_action_two(Request $request){
        $city_id    = Auth::user()->city_id;
        $date       = $request->branch_in_date_2;
        $filtered   = $request->filtered_2;
        $json       = 'json/reports/action-2';
        
        return view('operator.reports.search-2',compact('city_id','waybills','date','filtered','json'));
    }

    /** report - search 3 view page **/
    public function search_action_three(Request $request){
        $to_city    = $request->to_city3;
        $date       = $request->outbound_date3;
        $filtered   = $request->filtered_3;
        $json       = 'json/reports/action-3';

        //print_r($_POST);

        return view('operator.reports.search-3',compact('to_city','date','filtered','json'));
    }

    /** report - search 4 view page **/
    public function search_action_four(Request $request){
        $from_city  = $request->from_city4;
        $date       = $request->inbound_date4;
        $filtered   = $request->filtered_4;
        $json       = 'json/reports/action-4';

        return view('operator.reports.search-4',compact('from_city','date','filtered','json'));
    }

    /** report - search 5 view page **/
    public function search_action_five(Request $request){
        $from_branch    = $request->from_branch5;
        $date           = $request->outbound_date5;
        $json           = 'json/reports/action-5';

        return view('operator.reports.search-5',compact('from_branch','date','json'));
    }

    /** report - search 6 view page **/
    public function search_action_six(Request $request){
        $to_branch      = $request->to_branch6;
        $date           = $request->handover_date5;
        $json           = 'json/reports/action-6';

        return view('operator.reports.search-6',compact('to_branch','date','json'));
    }

    /** report - search 7 view page **/
    public function search_action_seven(Request $request){
        $branch_id  = $request->from_branch_7;
        $date       = $request->date_7;
        $start      = date('H:i:s',strtotime($request->start_time_7));
        $end        = date('H:i:s',strtotime($request->end_time_7));
        $filtered   = $request->filtered_7;
        $json       = 'json/reports/action-7';
        //print_r($_POST);
        return view('operator.reports.search-7',compact('waybills','branch_id','date','filtered','start','end','json'));
    }

    /** report - search 1 json data **/
    public function json_search_action_one($params){
        $arr        = explode('&', $params);
        $city       = $arr['0']; 
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills (စာထွက်စာရင် နယ်စာမပါ)
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$city)
                ->where('waybills.outbound_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);

        }elseif($filtered == 2){
            //only transit waybills (စာထွက်စာရင်း နယ်စာသီးသန့်)
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','waybills.transit_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.transit',$city)
                ->where('waybills.transit_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);

        }else{
            //empty data
            $waybills =  array();
            $count = $waybills->count();
        }
        
        return response()->json($waybills);
    }

    /** report - search 2 json data **/
    public function json_search_action_two($params){
        $arr        = explode('&', $params);
        $city       = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills (စာဝင်စာရင် နယ်စာမပါ)
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.destination',$city)
                ->where('waybills.inbound_date','like',$date.'%')
                ->paginate(100);

        }elseif($filtered == 2){
            //only transit waybills (စာဝင်စာရင် နယ်စာသီးသန့်)
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.transit',$city)
                ->where('waybills.transit_date','like',$date.'%')
                ->paginate(100);

        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
        return response()->json($waybills);
    }

    /** report - search 3 json data **/
    public function json_search_action_three($params){
        $arr        = explode('&', $params);
        $from_city  = Auth::user()->city_id;
        $to_city    = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('action_logs.action_date','like',$date.'%')
                ->paginate(100);

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.transit',$from_city)
                ->where('waybills.destination',$to_city)
                //->where('waybills.transit_date','like',$date.'%')
                ->where('action_logs.action_date','like',$date.'%')
                ->paginate(100);
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
        return response()->json($waybills);
    }

    /** report - search 4 json data **/
    public function json_search_action_four($params){
        $arr        = explode('&', $params);
        $to_city    = Auth::user()->city_id;
        $from_city  = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('waybills.inbound_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',6)
                ->where('waybills.origin',$from_city)
                ->where('waybills.transit',$to_city)
                ->where('waybills.transit_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
        return response()->json($waybills);
    }

    /** report - search 6 json data **/
    public function json_search_action_five($params){
        $arr            = explode('&', $params);
        $to_city        = Auth::user()->city_id;
        $from_branch    = $arr['0'];
        $date           = $arr['1'];

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'waybills.user_id')
            ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as delivery','u2.name as collected_by','b.name AS branch'])
            ->where('action_logs.action_id',1)
            ->where('action_logs.branch_id',$from_branch)
            ->where('waybills.outbound_date','like',$date.'%')
            ->orderBy('waybills.waybill_no')
            ->paginate(100);

        return response()->json($waybills);
    }

    /** report - search 6 json data **/
    public function json_search_action_six($params){
        $arr        = explode('&', $params);
        $to_city    = Auth::user()->city_id;
        $to_branch  = $arr['0'];
        $date       = $arr['1'];

        $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'branch_action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','waybills.waybill_no','branch_action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('branch_action_logs.action_id',6)
                ->where('branch_action_logs.to_branch',$to_branch)
                ->where('branch_action_logs.action_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->paginate(100);

        
        return response()->json($waybills);
    }

    /** report - search 7 json data **/
    public function json_search_action_seven($params){
        $arr        = explode('&', $params);
        $from_city  = Auth::user()->city_id;
        $from_branch= $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];
        $start      = $date.' '.$arr['3'];
        $end        = $date.' '.$arr['4'];
        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'branch_action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','waybills.waybill_no','branch_action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as handover_by','b.name AS branch'])
                ->where('branch_action_logs.action_id',2)
                ->where('branch_action_logs.active',1)
                ->where('branch_action_logs.action_type','outbound')
                ->where('branch_action_logs.from_branch',$from_branch)
                ->where('branch_action_logs.action_date','>=',$start)
                ->where('branch_action_logs.action_date','<=',$end)
                ->where('waybills.outbound_date','like',$date.'%')
                ->paginate(100);
        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'branch_action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.id','waybills.waybill_no','branch_action_logs.action_date','waybills.batch_id','waybills.current_status','waybills.transit_status','c.shortcode as origin','c2.shortcode as destination','u.name as handover_by','b.name AS branch'])
                ->where('branch_action_logs.action_id',6)
                ->where('branch_action_logs.active',1)
                ->where('branch_action_logs.action_type','transit')
                ->where('branch_action_logs.to_branch',$from_branch)
                ->where('waybills.transit_date','like',$date.'%')
                ->paginate(100);
        }else{
            //empty
            $waybills   = array();
            $count      = 0;
        }

        return response()->json($waybills);
    }

    public function export_search_action_one(Request $request){
        $arr        = explode('&', $request->params);
        $city       = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$city)
                ->where('waybills.outbound_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->get();

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c1', 'c1.id', '=', 'waybills.transit')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.outbound_date','waybills.waybill_no','waybills.current_status','c.shortcode as origin','c1.shortcode as transit','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS from_branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.transit',$city)
                ->where('waybills.transit_date','like',$date.'%')
                ->get();
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
      
        return response()->json($waybills);
    }

    public function export_search_action_two(Request $request){
        $arr        = explode('&', $request->params);
        $city       = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.inbound_date','waybills.waybill_no','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.destination',$city)
                ->where('waybills.inbound_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->get();

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c1', 'c1.id', '=', 'waybills.transit')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.inbound_date','waybills.waybill_no','waybills.current_status','c.shortcode as origin','c1.shortcode as transit','c2.shortcode as destination','u.name as branch_in_by','b.name AS from_branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.transit',$city)
                ->where('waybills.transit_date','like',$date.'%')
                ->get();
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
      
        return response()->json($waybills);
    }

    public function export_search_action_three(Request $request){
        $arr        = explode('&', $request->params);
        $from_city  = Auth::user()->city_id;
        $to_city    = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c1', 'c1.id', '=', 'waybills.transit')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.outbound_date','waybills.waybill_no','waybills.current_status','c.shortcode as origin','c1.shortcode as transit','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS from_branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                //->where('waybills.outbound_date','like',$date.'%')
                ->where('action_logs.action_date','like',$date.'%')
                ->get();

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('users as u2', 'u2.id', '=', 'waybills.user_id')
                ->leftjoin('cities as c1', 'c1.id', '=', 'waybills.transit')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->select(['waybills.id','waybills.outbound_date','waybills.waybill_no','waybills.current_status','c.shortcode as origin','c1.shortcode as transit','c2.shortcode as destination','u.name as collected_by','u2.name as delivery','b.name AS from_branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.transit',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('waybills.transit_date','like',$date.'%')
                ->get();
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
      
        return response()->json($waybills);
    }

    public function export_search_action_four(Request $request){
        $arr        = explode('&', $request->params);
        $to_city    = Auth::user()->city_id;
        $from_city  = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('waybills.inbound_date','like',$date.'%')
                ->get();

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.transit')
                ->join('cities as c3', 'c3.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as transit','c3.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$from_city)
                ->where('waybills.transit',$to_city)
                ->where('waybills.outbound_date','like',$date.'%')
                ->get();
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
        return response()->json($waybills);
    }

    /** export - search 5 json data **/
    public function export_search_action_five(Request $request){
        $arr            = explode('&', $request->params);
        $to_city        = Auth::user()->city_id;
        $from_branch    = $arr['0'];
        $date           = $arr['1'];

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
            ->leftjoin('users as u', 'u.id', '=', 'waybills.user_id')
            ->leftjoin('users as u2', 'u2.id', '=', 'branch_action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->leftjoin('branches as b', 'b.id', '=', 'u.branch_id')
            ->select(['waybills.waybill_no','branch_action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as delivery','u2.name as collected_by','b.name AS branch'])
            ->where('action_logs.action_id',1)
            ->where('branch_action_logs.from_branch',$from_branch)
            ->where('branch_action_logs.action_date','like',$date.'%')
            ->orderBy('waybills.waybill_no')
            ->get();

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'waybills.user_id')
            ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->select(['waybills.id','waybills.waybill_no','waybills.outbound_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as delivery','u2.name as collected_by','b.name AS branch'])
            ->where('action_logs.action_id',1)
            ->where('action_logs.branch_id',$from_branch)
            ->where('waybills.outbound_date','like',$date.'%')
            ->orderBy('waybills.waybill_no')
            ->get();

        return response()->json($waybills);
    }

    /** export - search 5 json data **/
    public function export_search_action_six(Request $request){
        $arr        = explode('&', $request->params);
        $to_city    = Auth::user()->city_id;
        $to_branch  = $arr['0'];
        $date       = $arr['1'];

        $waybills = Waybill::join('branch_action_logs','waybills.id','=','branch_action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'branch_action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->leftjoin('branches as b1', 'b1.id', '=', 'branch_action_logs.from_branch')
                ->leftjoin('branches as b2', 'b2.id', '=', 'branch_action_logs.to_branch')
                ->select(['waybills.id','waybills.waybill_no','branch_action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b1.name AS from_branch','b2.name AS to_branch'])
                ->where('branch_action_logs.action_id',6)
                ->where('branch_action_logs.to_branch',$to_branch)
                ->where('branch_action_logs.action_date','like',$date.'%')
                ->orderBy('waybills.waybill_no')
                ->get();

        return response()->json($waybills);
    }

    public function export_search_action_seven(Request $request){
        $arr        = explode('&', $request->params);
        $to_city    = Auth::user()->city_id;
        $from_city  = $arr['0'];
        $date       = $arr['1'];
        $filtered   = $arr['2'];

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',5)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('waybills.inbound_date','like',$date.'%')
                ->get();

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.transit')
                ->join('cities as c3', 'c3.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as transit','c3.shortcode as destination','u.name as branch_in_by','b.name AS branch'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$from_city)
                ->where('waybills.transit',$to_city)
                ->where('waybills.outbound_date','like',$date.'%')
                ->get();
        }else{
            //empty
            $waybills =  array();
            $count = $waybills->count();
        }
        
        return response()->json($waybills);
    }

    public function search(){
        $term = $_GET['query'];

        //$waybills = Waybill::where('waybill_no','like','%'.$term.'%')->limit('10')->get();
        $waybills = Waybill::join('cities as c', 'c.id', '=', 'waybills.origin')
            ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
            ->select(['waybills.waybill_no','waybills.batch_id','waybills.current_status','c.shortcode as origin','c2.shortcode as destination'])
            ->where('waybill_no','like','%'.$term.'%')
            ->limit('10')
            ->get();

        //return $waybills;
        return view('search',compact('waybills'));
    }


    /** report - search 8 json data **/
    public function search_action_eight(Request $request){
        $from_city  = Auth::user()->city_id;

        $to_city    = $request->to_city8;
        $date       = $request->outbound_date8;
        $filtered   = $request->filtered_8;
        $pack       = $request->note;

        if($filtered == 1){
            //without transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.current_status'])
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybill_no','asc')
                ->get();

            $users = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->select('action_logs.action_by','u.name')
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('action_logs.action_date','like',$date.'%')
                ->groupBy('action_logs.action_by')
                ->get();

        }elseif($filtered == 2){
            //only transit waybills
            $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->select(['waybills.waybill_no','action_logs.action_date','waybills.current_status'])
                ->where('action_logs.action_id',4)
                ->where('waybills.transit',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('action_logs.action_date','like',$date.'%')
                ->orderBy('waybill_no','asc')
                ->get();

            $users = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->join('users as u', 'u.id', '=', 'action_logs.action_by')
                ->select('action_logs.action_by','u.name')
                ->where('action_logs.action_id',4)
                ->where('waybills.transit',$from_city)
                ->where('waybills.destination',$to_city)
                ->where('action_logs.action_date','like',$date.'%')
                ->groupBy('action_logs.action_by')
                ->get();
        }else{
            //empty
            $waybills =  array();
            //$count = $waybills->count();
        }
        $count = $waybills->count();

        //return response()->json($waybills);
        return view('operator.reports.print',compact('waybills','from_city','to_city','date','pack','count','users'));
    }

}
