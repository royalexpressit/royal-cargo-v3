<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;

class DocController extends Controller{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function index(){
    	return view('documentation.index');
    }

    public function api_lists($item){
    	if($item == 'login'){
    		return view('documentation.login');
    	}elseif($item == 'cities'){
    		return view('documentation.cities');
    	}elseif($item == 'branches'){
    		return view('documentation.branches');
    	}elseif($item == 'deliveries'){
    		return view('documentation.deliveries');
    	}elseif($item == 'outbound-collected'){
            return view('documentation.outbound-collected');
        }else{
    		//
    	}
    	
    }
}
