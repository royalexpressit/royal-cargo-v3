<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\City;
use App\Branch;
use App\Waybill;
use App\ActionLog;
use App\Transit;
use App\BranchActionLog;
use Illuminate\Support\Facades\DB;

class ApiController extends Controller{
    /* Outbound Action Collections */
    public function outbound_action(Request $request){
    	$response = array();
    	$failed   = array();
    	$raw      = $request->waybills;
    	$cargos   = array();

    	if($request->action == 'collected'){
    		$temp = explode(",",$raw);

    		$collected_by 	= user($request->user_id)['name'];
    		$delivery 		= user($request->delivery_id)['name'];

    		# @handover ကို checked ထာလားစစ်မယ် checked ထားရင် collected ကနေ handover အဆင့်ထိကျော်မယ်
    		if($request->handover == 1){
    			//skip is active (changed handover status)
	            for($i = 0; $i < count($temp); $i++) { 
	            	//check waybill is already collected
	            	# @waybill_no ရှိလား အရင်စစ် မရှိရင် အသစ် create လုပ်မယ်
	            	$check = Waybill::where('waybill_no',$temp[$i])->first();
	            	if(!$check){
	            		$new                    = new Waybill();
			            $new->waybill_no        = trim($temp[$i]);
			            $new->batch_id        	= batch_id();
			            $new->origin         	= $request->city_id;
			            $new->current_status    = 2;
			            $new->user_id  			= $request->delivery_id; //delivery man or counter
			            $new->outbound_date    	= date('Y-m-d H:i:s'); //marked start date for outbound

			            if($new->save()){
			            	//saved logs for collected action (action_id = 1)
			            	$action                 	= new ActionLog();
				            $action->waybill_id    		= $new->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'outbound';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->city_id;
				            $action->action_id      	= 1;
				            $action->action_log  		= $collected_by.' is collected waybill from '.$delivery .'.';
				            $action->save();

				            //saved next logs for handover action
				            $action2                 	= new ActionLog();
				            $action2->waybill_id     	= $new->id;
				            $action2->action_date      	= date('Y-m-d H:i:s');
				            $action2->action_id      	= 2;
				            $action2->action_by      	= $request->user_id;
				            $action2->action_type      	= 'outbound';
				            $action2->branch_id    		= $request->branch_id;
				            $action2->city_id  			= $request->city_id;
				            $action2->action_log  		= $collected_by.' is handover waybill.';
				            $action2->save();

				            //saved logs for handover branch action 
				            $branch_log                 = new BranchActionLog();
				            $branch_log->waybill_id     = $new->id;
				            $branch_log->from_branch    = $request->branch_id;
				            $branch_log->to_branch     	= $request->handover_id;
				            $branch_log->action_id    	= 2;
				            $branch_log->action_by      = $request->user_id;
				            $branch_log->action_type    = 'outbound';
				            $branch_log->action_date    = date('Y-m-d H:i:s');
				            $branch_log->save();
				        }
	            	}else{
	            		//return waybill_no for failed 
	            		array_push($failed, $temp[$i]);
	            	}
	            }
	        }else{
	        	for($i = 0; $i < count($temp); $i++) { 
	        		//check waybill is already collected
	            	$check = Waybill::where('waybill_no',$temp[$i])->first();
	            	if(!$check){
	            		$new                    = new Waybill();
			            $new->waybill_no        = trim($temp[$i]);
			            $new->batch_id        	= batch_id();
			            $new->origin         	= $request->city_id;
			            $new->current_status    = 1;
			            $new->user_id  			= $request->delivery_id;
			            $new->outbound_date    	= date('Y-m-d H:i:s');
			            //action skipped for 'service_point_area = 1'
			            if($request->skip == 1){
			            	//skipped to received
	            			$new->current_status    = 3;
	            		}else{
	            			$new->current_status    = 1;
	            		}
	            	
			            if($new->save()){
			            	//saved logs for collected action
			            	$action                 	= new ActionLog();
				            $action->waybill_id    		= $new->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'outbound';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->city_id;
				            $action->action_id      	= 1;
				            $action->action_log  		= $collected_by.' is collected waybill from '.$delivery.'.';
				            $action->save();

				            if($request->skip == 1){
				            	$action_skip                 	= new ActionLog();
					            $action_skip->waybill_id    	= $new->id;
					            $action_skip->action_date      	= date('Y-m-d H:i:s');
					            $action_skip->action_by      	= $request->user_id;
					            $action_skip->action_type      	= 'outbound';
					            $action_skip->branch_id    		= $request->branch_id;
					            $action_skip->city_id  			= $request->city_id;
					            $action_skip->action_id      	= 3;
					            $action_skip->action_log  		=  $collected_by.' is received waybill.';
					            $action_skip->save();
				            }
				        }
	            	}else{
	            		//return waybills for action failed
	            		array_push($failed, $temp[$i]);
	            	}
	            }
	        }

            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'handover'){
        	$temp = explode("," , $raw);

        	$handover_by 	= user($request->user_id)['name'];

            for ($i=0; $i < count($temp); $i++) { 
            	$waybill = Waybill::select('id')->where('waybill_no',$temp[$i])->where('current_status',1)->first();
            	
            	if($waybill){
            		$waybill->batch_id        		= batch_id();
            		$waybill->current_status    	= 2;
		            if($waybill->save()){
		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $waybill->id;//$temp[$i];
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 2;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'outbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $handover_by.' is handover waybill.';
			            $action->save();

			            $branch_log                 = new BranchActionLog();
			            $branch_log->waybill_id     = $waybill->id;
			            $branch_log->from_branch    = $request->branch_id;
			            $branch_log->to_branch     	= $request->handover_branch;
			            $branch_log->action_id    	= 2;
			            $branch_log->action_by      = $request->user_id;
			            $branch_log->action_type    = 'outbound';
			            $branch_log->action_date    = date('Y-m-d H:i:s');
			            $branch_log->save();

		            }
            	}else{
            		array_push($failed, $temp[$i]);
            	}
            	$response['success'] 	= 1;
            	$response['failed'] 	= $failed;
            }
        }elseif($request->action == 'received'){
        	$temp = explode("," , $raw);

        	$received_by = user($request->user_id)['name'];

            for ($i=0; $i < count($temp); $i++) { 
            	/** received waybill for transit **/
            	$transit = Waybill::select('id')->where('waybill_no',$temp[$i])->where('transit',$request->city_id)->where('transit_status',2)->first();
            	
            	/** received waybill for handover **/
            	$waybill = Waybill::select('id')->where('waybill_no',$temp[$i])->where('origin',$request->city_id)->where('current_status',2)->first();
            	
            	if($waybill){
            		$waybill->batch_id        = batch_id();
		            $waybill->current_status  = 3;
		            if($waybill->save()){
		            	//temp data to sent odoo
		            	array_push($cargos,trim($temp[$i]));

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $waybill->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 3;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'outbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $received_by.' is received waybill.';
			            $action->save();
			            

			            $response['success'] = 1;
			            $response['message'] = 'successful received';	
		            }
            	}elseif($transit){
            		$transit->batch_id        = batch_id();
		            $transit->transit_status  = 3;
		            if($transit->save()){
		            	//temp data to sent odoo
		            	array_push($cargos,trim($temp[$i]));

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $transit->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 3;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'transit';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $received_by.' is received transit waybill.';
			            $action->save();
			            
			            $response['success'] = 1;
			            $response['message'] = 'successful received';	
		            }
            	}else{
            		array_push($failed, $temp[$i]);

            		//saved failed waybill at logs
            		/* ------- continue codes ---------- */
            		$log_date 	= date('Y-m-d H:i:s');
            		$log_no 	= $temp[$i].'(action_by:'.$request->user_id.')';
            		$log_txt    = '['.$log_date.'] '.$log_no;
            		saved_outbound_received_failed($log_txt);
            	}

            	$response['cargos'] 	= $cargos;
            	$response['success'] 	= 1;
            	$response['failed'] 	= $failed;
            }
        }elseif($request->action == 'branch-out'){
        	$temp = explode("," , $raw);

        	if(date('A') == 'PM'){
        		$inbound_date = date('Y-m-d 00:00:00',strtotime("+1 days"));
        	}else{
        		$inbound_date = date('Y-m-d 00:00:00');
        	}

        	$action_by = user($request->user_id)['name'];

            for ($i=0; $i < count($temp); $i++) {
            	/* branch out for received transit waybill */ 
            	$transit = Waybill::select('id','destination')->where('waybill_no',$temp[$i])->where('transit',$request->origin)->where('transit_status',3)->first();
            	
            	/* branch out for received waybill */
            	$waybill = Waybill::select('id')->where('waybill_no',$temp[$i])->where('origin',$request->origin)->where('current_status',3)->first();

            	if($waybill){
            		/* auto branch in for same city's waybill branch out */
            		if($request->origin == $request->destination){
            			$waybill->batch_id        	= batch_id();
			            //$waybill->transit         = $request->transit;
			            $waybill->destination       = $request->destination;
			            $waybill->inbound_date    	= $inbound_date; // + 1 day for inbound
			            $waybill->current_status    = 5;
			            if($waybill->save()){
			            	//temp data to sent odoo
		            		array_push($cargos,trim($temp[$i]));

			            	$action                 	= new ActionLog();
				            $action->waybill_id     	= $waybill->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_id      	= 4;
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'outbound';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->origin;
				            $action->action_log  		= $action_by.' is branch out waybill.';
				            $action->save();

				            $action2                 	= new ActionLog();
				            $action2->waybill_id     	= $waybill->id;
				            $action2->action_date      	= $inbound_date;
				            $action2->action_id      	= 5;
				            $action2->action_by      	= $request->user_id;
				            $action2->action_type      	= 'inbound';
				            $action2->branch_id    		= $request->branch_id;
				            $action2->city_id  			= $request->origin;
				            $action2->action_log  		= $action_by.' is branch in waybill.';
				            $action2->save();
				            
				            //called method data sent to odoo
				        }	
            		}else{
            			$waybill->batch_id        	= batch_id();
			            $waybill->transit         	= $request->transit;
			            $waybill->destination       = $request->destination;
			            $waybill->current_status    = 4;
			            $waybill->transit_status    = 0;
			            if($waybill->save()){
			            	//temp data to sent odoo
		            		array_push($cargos,trim($temp[$i]));

			            	$action                 	= new ActionLog();
				            $action->waybill_id     	= $waybill->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_id      	= 4;
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'outbound';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->origin;
				            $action->action_log  		= $action_by.' is branch out waybill.';
				            $action->save();
				            
				            //called method data sent to odoo
				        }
            		}
            		
            	}elseif($transit){
            		if($transit->destination == ''){
	            		$transit->batch_id        = batch_id();
			            $transit->transit_status  = 4;
			            $transit->destination     = $request->destination;       
			            
			            if($transit->save()){
			            	//temp data to sent odoo
		            		array_push($cargos,trim($temp[$i]));

			            	$action                 	= new ActionLog();
				            $action->waybill_id     	= $transit->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_id      	= 4;
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'transit';
				            $action->action_date      	= date('Y-m-d');
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->origin;
				            $action->action_log  		= $action_by.' is branch out for transit waybill.';
				            $action->save();

				            $response['success'] = 1;
				            $response['message'] = 'successful received';	
			            }
			        }else{
			        	if($transit->destination == $request->destination){
			        		$transit->batch_id        = batch_id();
				            $transit->transit_status  = 4;
				            
				            if($transit->save()){
				            	//temp data to sent odoo
		            			array_push($cargos,trim($temp[$i]));

				            	$action                 	= new ActionLog();
					            $action->waybill_id     	= $transit->id;
					            $action->action_date      	= date('Y-m-d H:i:s');
					            $action->action_id      	= 4;
					            $action->action_by      	= $request->user_id;
					            $action->action_type      	= 'transit';
					            $action->action_date      	= date('Y-m-d');
					            $action->branch_id    		= $request->branch_id;
					            $action->city_id  			= $request->city_id;
					            $action->action_log  		= $action_by.' is branch out waybill.';
					            $action->save();

					            $response['success'] = 1;
					            $response['message'] = 'successful received';	
				            }
			        	}else{
			        		array_push($failed, $temp[$i]);
			        	}
			        }
            	}else{
            		array_push($failed, $temp[$i]);

            		//saved failed waybill at logs
            		/* ------- continue codes ---------- */
            		$log_date 	= date('Y-m-d H:i:s');
            		$log_no 	= $temp[$i].'(action_by:'.$request->user_id.')';
            		$log_txt    = '['.$log_date.'] '.$log_no;
            		saved_outbound_branch_out_failed($log_txt);
            	}
            }

            $response['cargos'] 	= $cargos;
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'single-received'){
        	$received_by = user($request->user_id)['name'];
            $transit = Waybill::select('id','current_status')->where('waybill_no',$request->waybill)->where('transit',$request->city_id)->first();
            $waybill = Waybill::select('id','current_status')->where('waybill_no',$request->waybill)->where('origin',$request->city_id)->first();
            	
            if($waybill){
            	//where('current_status',2)
            	if($waybill->current_status == 1){
            		$response['success'] = 0;
				    $response['message'] = '<li class="list-group-item text-danger"><i class="fa fa-times"></i> '.$request->waybill.' (စာမလွှဲရသေးပါ)</li>';
            	}elseif($waybill->current_status == 2){
            		$waybill->batch_id        = batch_id();
			        $waybill->current_status  = 3;
			        if($waybill->save()){
			            $action                 	= new ActionLog();
				        $action->waybill_id     	= $waybill->id;
				        $action->action_date      	= date('Y-m-d H:i:s');
				        $action->action_id      	= 3;
				        $action->action_by      	= $request->user_id;
				        $action->action_type      	= 'outbound';
				        $action->branch_id    		= $request->branch_id;
				        $action->city_id  			= $request->city_id;
				        $action->action_log  		= $received_by.' is received waybill.';
				        $action->save();
				            
				        $response['success'] = 1;
				        $response['message'] = '<li class="list-group-item text-success"><i class="fa fa-check"></i> '.$request->waybill.' (စာလက်ခံ)</li>';	
			        }
            	}elseif($waybill->current_status == 3){
            		$response['success'] = 0;
				    $response['message'] = '<li class="list-group-item text-danger"><i class="fa fa-times"></i> '.$request->waybill.' (စာလက်ခံပြီးသား)</li>';
            	}else{
            		$response['success'] = 0;
				    $response['message'] = '<li class="list-group-item text-danger"><i class="fa fa-times"></i> '.$request->waybill.' (စာထွက်ထားပြီး)</li>';
            	}
            	
            }elseif($transit){
            	//where('transit_status',2)
            	$transit->batch_id        = time();
		        $transit->transit_status  = 3;
		        if($transit->save()){
		            $action                 	= new ActionLog();
			        $action->waybill_id     	= $transit->id;
			        $action->action_date      	= date('Y-m-d H:i:s');
			        $action->action_id      	= 3;
			        $action->action_by      	= $request->user_id;
			        $action->action_type      	= 'transit';
			        $action->branch_id    		= $request->branch_id;
			        $action->city_id  			= $request->city_id;
			        $action->action_log  		= $received_by.' is received waybill.';
			        $action->save();

			        $response['success'] = 1;
			        $response['message'] = 'successful received';	
		        }
            }else{
            	$response['success'] = 0;
			    $response['message'] = '<li class="list-group-item text-danger"><i class="fa fa-times"></i> '.$request->waybill.'(စာမတွေ့ပါ)</li>';
            }
        }else{
        	$response['success'] = 0;
		    $response['message'] = 'action not allowed';
        }

        return response()->json($response);
    }

    /** Inbound Action Collections **/
	public function inbound_action(Request $request){
		$response = array();
    	$failed   = array();
    	$raw      = $request->waybills;
    	$cargos   = array();

    	if($request->active_time){
    		// set custom datetime
    		$tomorrow = date("Y-m-d", strtotime("+ 1 day"));
    		$action_date = $tomorrow.' 00:00:00';
    	}else{
    		// set default datetime
    		$action_date = date('Y-m-d H:i:s');
    	}

    	if($request->action == 'branch-in'){
    		$temp = explode("," , $raw);
    		$branch_in_by = user($request->user_id)['name'];

            for($i=0; $i < count($temp); $i++){ 
            	// 1#first check for transit
            	$transit = Waybill::where('waybill_no',$temp[$i])->where('origin',$request->from_city)->where('transit',$request->city_id)->where('transit_status',0)->first();
            	
            	// 2#next check for destination
            	$inbound = Waybill::where('waybill_no',$temp[$i])->where('origin',$request->from_city)->where('destination',$request->city_id)->where('current_status',4)->first();
            	
            	// 3#last check no record
            	$check = Waybill::where('waybill_no',$temp[$i])->first();
            	
            	// 4#not use transit check
            	$inbound2 = Waybill::where('waybill_no',$temp[$i])->where('origin',$request->from_city)->where('current_status',4)->first();

            	if($transit){
            		//transit waybill
		            $transit->batch_id        	= batch_id();
		            $transit->transit_status    = 1; //branch in for transit
		            $transit->transit_date    	= date('Y-m-d H:i:s');
		            if($transit->save()){
		            	//temp data to sent odoo
		            	array_push($cargos,trim($temp[$i]));

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $transit->id;
			            $action->action_date      	= $action_date;
			            $action->action_id      	= 5;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'transit';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $branch_in_by.' is branch in transit waybill';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}elseif($inbound){
            		//inbound waybill
		            $inbound->batch_id        	= batch_id();
		            $inbound->current_status    = 5;
		            $inbound->inbound_date    	= date('Y-m-d H:i:s');
		            if($inbound->save()){
		            	//temp data to sent odoo
		            	array_push($cargos,trim($temp[$i]));

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $inbound->id;
			            $action->action_date      	= $action_date;
			            $action->action_id      	= 5;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $branch_in_by.' is branch in waybill';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}elseif($inbound2){
            		//inbound waybill
		            $inbound2->batch_id        	= batch_id();
		            $inbound2->transit   		= $request->city_id;
		            $inbound2->transit_status   = 1;
		            $inbound2->transit_date    	= date('Y-m-d H:i:s');
		            if($inbound2->save()){
		            	//temp data to sent odoo
		            	array_push($cargos,trim($temp[$i]));

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $inbound2->id;
			            $action->action_date      	= $action_date;
			            $action->action_id      	= 5;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'transit';
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $branch_in_by.' is branch in waybill';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}elseif(!$check){
            		//new waybill
            		$new        			= new Waybill;
            		$new->waybill_no        = $temp[$i];
		           	$new->batch_id      	= batch_id();
		           	$new->origin      		= $request->from_city;
		           	$new->destination   	= $request->city_id;
		           	$new->current_status    = 5;
		           	$new->inbound_date    	= date('Y-m-d H:i:s');
		            if($new->save()){
		            	//temp data to sent odoo
		            	array_push($cargos,trim($temp[$i]));

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $new->id;
			            $action->action_date      	= $action_date;
			            $action->action_id      	= 5;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $branch_in_by.' is branch in waybill';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}else{
            		array_push($failed, $temp[$i]);

            		//saved failed waybill at logs
            		/* ------- continue codes ---------- */
            		$log_date 	= date('Y-m-d H:i:s');
            		$log_no 	= $temp[$i].'(action_by:'.$request->user_id.')';
            		$log_txt    = '['.$log_date.']['.$request->from_city.'] '.$log_no;
            		saved_inbound_branch_in_failed($log_txt);
            	}
            }
            $response['cargos'] 	= $cargos;
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'handover'){
        	$temp = explode("," , $raw);

        	$handover_by = user($request->user_id)['name'];

            for($i = 0; $i < count($temp); $i++){ 
            	if($request->transit == 1){
            		//transit branch handover
            		$handover_branch = $request->transit_branch;
            		$transit 	= Waybill::where('waybill_no',$temp[$i])->where('transit',$request->city_id)->where('transit_status',1)->first();
            		$handover 	= Waybill::where('waybill_no',$temp[$i])->where('destination',$request->city_id)->where('current_status',5)->first();

            		if($transit){
            			/* server know waybill is transit coz transit same with city_id */
			            $transit->batch_id        	= batch_id();
			            $transit->transit_status    = 2; //handover for transit
			            if($transit->save()){
			            	$action                 	= new ActionLog();
				            $action->waybill_id     	= $transit->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_id      	= 6;
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'transit';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->city_id;
				            $action->action_log  		= $handover_by.' is handover transit waybill.';
				            $action->save();

				            $branch_log                 = new BranchActionLog();
				            $branch_log->waybill_id     = $transit->id;
				            $branch_log->from_branch    = $request->branch_id;
				            $branch_log->to_branch     	= $request->transit_branch;
				            $branch_log->action_id    	= 6;
				            $branch_log->action_by    	= $request->user_id;
				            $branch_log->action_type    = 'transit';
				            $branch_log->action_date    = date('Y-m-d H:i:s');
				            $branch_log->save();
				            
				            //called method data sent to odoo
				        }
	            	}elseif($handover){
	            		/* destination change to transit if direct branch-in for transit waybill */
		            	$transit_area = branch($request->transit_branch)['is_transit_area'];

		            	if($transit_area != $transit){
		            		if($handover->origin != $request->city_id){
		            			$handover->batch_id        	 = batch_id();
			            		$handover->current_status    = 4;
					            $handover->transit_status    = 2; //handover for transit
					            $handover->transit       	 = $request->city_id;
				            	$handover->transit_date      = date('Y-m-d H:i:s');
				            	$handover->inbound_date      = NULL;
					            $handover->destination       = NULL;
					            if($handover->save()){
					            	//updated old logs to 'action_type=transit'
					        		DB::table('action_logs')->where('action_id',5)->where('waybill_id', $handover->id)->update(['action_type' => 'transit']);

					            	$action                 	= new ActionLog();
						            $action->waybill_id     	= $handover->id;
						            $action->action_date      	= date('Y-m-d H:i:s');
						            $action->action_id      	= 6;
						            $action->action_by      	= $request->user_id;
						            $action->action_type      	= 'transit';
						            $action->branch_id    		= $request->branch_id;
						            $action->city_id  			= $request->city_id;
						            $action->action_log  		= $handover_by.' is handover transit waybill.';
						            $action->save();

						            $branch_log                 = new BranchActionLog();
						            $branch_log->waybill_id     = $handover->id;
						            $branch_log->from_branch    = $request->branch_id;
						            $branch_log->to_branch     	= $request->transit_branch;
						            $branch_log->action_id    	= 6;
						            $branch_log->action_by    	= $request->user_id;
						            $branch_log->action_type    = 'transit';
						            $branch_log->action_date    = date('Y-m-d H:i:s');
						            $branch_log->save();
						            
						            //called method data sent to odoo
						        }
		            		}else{
		            			$response['msg'] 	= 'same city cannot use transit.';
					    		array_push($failed, $temp[$i]);
			            	}	
					    }else{
					    	$response['msg'] 	= 'we found some waybills handover to transit.';
					    	array_push($failed, $temp[$i]);
					    }
	            	}else{
	            		array_push($failed, $temp[$i]);

	            		//saved failed waybill at logs
            			/* ------- continue codes ---------- */
            			$log_date 	= date('Y-m-d H:i:s');
            			$log_no 	= $temp[$i].'(action_by:'.$request->user_id.')';
            			$log_txt    = '['.$log_date.']['.$request->transit_branch.'] '.$log_no;
            			saved_inbound_handover_failed($log_txt);
	            	}
            	}else{
            		//same city branch handover
            		$handover_branch = $request->handover_branch;
            		$waybill 	= Waybill::where('waybill_no',$temp[$i])->where('destination',$request->city_id)->where('current_status',5)->first();
            		
            		// return handover for wrong waybills
            		$handover 	= Waybill::where('waybill_no',$temp[$i])->where('destination',$request->city_id)->where('current_status',7)->first();
            		
            		if($waybill){
				        $waybill->batch_id        	= batch_id();
				        $waybill->current_status    = 6;
				        
				        if($waybill->save()){
				            $action                 	= new ActionLog();
					        $action->waybill_id     	= $waybill->id;
					        $action->action_date      	= date('Y-m-d H:i:s');
					        $action->action_id      	= 6;
					        $action->action_by      	= $request->user_id;
					        $action->action_type      	= 'inbound';
					        $action->branch_id    		= $request->branch_id;
					        $action->city_id  			= $request->city_id;
					        $action->action_log  		= $handover_by.' is handover transit waybill.';
					        $action->save();

					        $branch_log                 = new BranchActionLog();
					        $branch_log->waybill_id     = $waybill->id;
					        $branch_log->from_branch    = $request->branch_id;
					        $branch_log->to_branch     	= $request->handover_branch;
					        $branch_log->action_id    	= 6;
					        $branch_log->action_by    	= $request->user_id;
					        $branch_log->action_type    = 'inbound';
					        $branch_log->action_date    = date('Y-m-d H:i:s');
					        $branch_log->save();
					            
					        //called method data sent to odoo
					    }
		            }elseif($handover){
		            	$handover->batch_id        	= time();
				        $handover->current_status    = 6;
				        
				        if($handover->save()){
				        	//updated old logs to 'active=0'
				        	DB::table('action_logs')->where('action_id',6)->where('waybill_id', $handover->id)->update(['active' => 0]);
				        	DB::table('branch_action_logs')->where('action_id',6)->where('waybill_id', $handover->id)->update(['active' => 0]);
				        	
				        	//created new log to 'active=1'
				            $action                 	= new ActionLog();
					        $action->waybill_id     	= $handover->id;
					        $action->action_date      	= date('Y-m-d H:i:s');
					        $action->action_id      	= 6;
					        $action->action_by      	= $request->user_id;
					        $action->action_type      	= 'inbound';
					        $action->branch_id    		= $request->branch_id;
					        $action->city_id  			= $request->city_id;
					        $action->action_log  		= $handover_by.' is handover for received waybill.';
					        $action->active    			= 1;
					        $action->save();

					        $branch_log                 = new BranchActionLog();
					        $branch_log->waybill_id     = $handover->id;
					        $branch_log->from_branch    = $request->branch_id;
					        $branch_log->to_branch     	= $request->handover_branch;
					        $branch_log->action_by    	= $request->user_id;
					        $branch_log->action_date    = date('Y-m-d H:i:s');
					        $branch_log->action_id    	= 6;
					        $branch_log->action_type    = 'inbound';
					        $branch_log->active    		= 1;
					        $branch_log->save();
					            
					        //called method data sent to odoo
					    }
		            }else{
		            	array_push($failed, $temp[$i]);

		            	//saved failed waybill at logs
            			/* ------- continue codes ---------- */
            			$log_date 	= date('Y-m-d H:i:s');
            			$log_no 	= $temp[$i].'(action_by:'.$request->user_id.')';
            			$log_txt    = '['.$log_date.']['.$request->transit_branch.'] '.$log_no;
            			saved_inbound_handover_failed($log_txt);
		            }
            	}
            }

            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'received'){     	
        	$temp 			= explode("," , $raw);
        	$received_by 	= user($request->user_id)['name'];

            for ($i=0; $i < count($temp); $i++) { 
            	$waybill 	= Waybill::where('waybill_no',$temp[$i])->where('current_status',6)->where('destination',$request->city_id)->first();
            	
            	$postponed 	= Waybill::where('waybill_no',$temp[$i])->where('current_status',8)->where('destination',$request->city_id)->first();
            	
            	if($waybill){
		            $waybill->batch_id        	= batch_id();
		            $waybill->current_status    = 7;
		            if($waybill->save()){
		            	//updated actual received branch
		            	$branch_action                 	= BranchActionLog::where('waybill_id',$waybill->id)->where('action_type','inbound')->first();
		            	if($branch_action){
		            		$branch_action->to_branch   = $request->branch_id;
			            	$branch_action->action_by	= $request->user_id;
			            	$branch_action->action_date = date('Y-m-d H:i:s');
			            	$branch_action->save();
		            	}
			            
		            	//updated old logs to 'active=0'
				        DB::table('action_logs')->where('action_id',7)->where('waybill_id', $waybill->id)->update(['active' => 0]);
				        //DB::table('branch_action_logs')->where('action_id',6)->where('waybill_id', $waybill->id)->update(['active' => 0]);

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $waybill->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 7;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $received_by.' is received waybill.';
			            $action->active    			= 1;
			            $action->save();

			            //called method data sent to odoo
			        }
            	}elseif($postponed){
		            $postponed->batch_id        	= batch_id();
		            $postponed->current_status    = 7;
		            if($postponed->save()){
		            	//updated actual received branch
		            	$branch_action   = BranchActionLog::where('waybill_id',$postponed->id)->first();
		            	if($branch_action){
		            		$branch_action->to_branch     	= $request->branch_id;
			            	$branch_action->save();
		            	}
			            
		            	//updated old logs to 'active=0'
				        DB::table('action_logs')->where('action_id',7)->where('waybill_id', $postponed->id)->update(['active' => 0]);
				        //DB::table('branch_action_logs')->where('action_id',6)->where('waybill_id', $waybill->id)->update(['active' => 0]);

		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $postponed->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 7;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $received_by.' received for postponed waybill.';
			            $action->active    			= 1;
			            $action->save();

			            //called method data sent to odoo
			        }
            	}else{
            		array_push($failed, $temp[$i]);

            		//saved failed waybill at logs
            		/* ------- continue codes ---------- */
            		$log_date 	= date('Y-m-d H:i:s');
            		$log_no 	= $temp[$i].'(action_by:'.$request->user_id.')';
            		$log_txt    = '['.$log_date.'] '.$log_no;
            		saved_inbound_received_failed($log_txt);
            	}
            }
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'postponed'){
        	$temp = explode("," , $raw);
        	$received_by = user($request->user_id)['name'];
            for ($i=0; $i < count($temp); $i++) { 
            	$waybill = Waybill::where('waybill_no',$temp[$i])->where('current_status',7)->where('destination',$request->city_id)->first();
            	if($waybill){
		            $waybill->batch_id        	= time();
		            $waybill->current_status    = 8;
		            if($waybill->save()){
		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $waybill->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 8;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= $received_by.' is postponed waybill.';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}else{
            		array_push($failed, $temp[$i]);
            	}
            }
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'rejected'){
        	$temp = explode("," , $raw);
        	$rejected_by = user($request->user_id)['name'];

            for ($i=0; $i < count($temp); $i++) { 
            	$waybill = Waybill::where('waybill_no',$temp[$i])->where('current_status',8)->where('destination',$request->city_id)->first();
            	if($waybill){
            		if($request->transferred == 1){
            			$waybill->batch_id        	= batch_id();
			            $waybill->current_status    = 10;
			            if($waybill->save()){
			            	//saved rejected log
			            	$action                 	= new ActionLog();
				            $action->waybill_id     	= $waybill->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_id      	= 9;
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'inbound';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->city_id;
				            $action->action_log  		= $rejected_by.' is rejected waybill.';
				            $action->save();
				            
				            //saved transferred log
				            $action2                 	= new ActionLog();
				            $action2->waybill_id     	= $waybill->id;
				            $action2->action_date      	= date('Y-m-d H:i:s');
				            $action2->action_id      	= 10;
				            $action2->action_by      	= $request->user_id;
				            $action2->action_type      	= 'inbound';
				            $action2->branch_id    		= $request->branch_id;
				            $action2->city_id  			= $request->city_id;
				            $action2->action_log  		= $rejected_by.' is transferred waybill.';
				            $action2->save();
				        }
            		}else{
            			$waybill->batch_id        	= batch_id();
			            $waybill->current_status    = 9;
			            if($waybill->save()){
			            	$action                 	= new ActionLog();
				            $action->waybill_id     	= $waybill->id;
				            $action->action_date      	= date('Y-m-d H:i:s');
				            $action->action_id      	= 9;
				            $action->action_by      	= $request->user_id;
				            $action->action_type      	= 'inbound';
				            $action->branch_id    		= $request->branch_id;
				            $action->city_id  			= $request->city_id;
				            $action->action_log  		= $rejected_by.' is rejected waybill.';
				            $action->save();
				            
				            //called method data sent to odoo
				        }
            		}
            	}else{
            		array_push($failed, $temp[$i]);
            	}
            }
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'transferred'){
        	$temp = explode("," , $raw);
            for ($i=0; $i < count($temp); $i++) { 
            	$waybill = Waybill::where('waybill_no',$temp[$i])->where('current_status',9)->where('destination',$request->city_id)->first();
            	if($waybill){
		            $waybill->batch_id        	= time();
		            $waybill->current_status    = 10;
		            if($waybill->save()){
		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $waybill->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 10;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= user($request->user_id)['name'].' is transferred waybill.';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}else{
            		array_push($failed, $temp[$i]);
            	}
            }
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }elseif($request->action == 'accepted'){
        	$temp = explode("," , $raw);
            for ($i=0; $i < count($temp); $i++) { 
            	$waybill = Waybill::where('waybill_no',$temp[$i])->where('current_status',10)->first();
            	if($waybill){
		            $waybill->batch_id        	= time();
		            $waybill->current_status    = 11;
		            if($waybill->save()){
		            	$action                 	= new ActionLog();
			            $action->waybill_id     	= $waybill->id;
			            $action->action_date      	= date('Y-m-d H:i:s');
			            $action->action_id      	= 11;
			            $action->action_by      	= $request->user_id;
			            $action->action_type      	= 'inbound';
			            $action->branch_id    		= $request->branch_id;
			            $action->city_id  			= $request->city_id;
			            $action->action_log  		= user($request->user_id)['name'].' is accepted for rejected waybill.';
			            $action->save();
			            
			            //called method data sent to odoo
			        }
            	}else{
            		array_push($failed, $temp[$i]);
            	}
            }
            $response['success'] 	= 1;
            $response['failed'] 	= $failed;
        }else{
        	$response['success'] = 0;
		    $response['message'] = 'action not allowed';
        }

        return response()->json($response);
    }

    public function cities(){
    	$cities = City::orderBy('shortcode','asc')->paginate(20);

    	return response()->json($cities);
    }

    public function branches(){
    	//$branches = Branch::orderBy('name','asc')->paginate(20);
    	$branches = Branch::select('branches.name AS branch_name','branches.is_main_office','branches.is_transit_area','cities.name AS city_name','cities.mm_name AS city_mm_name')
		->join('cities','branches.city_id','=','cities.id')
		->paginate(20);

    	return response()->json($branches);
    }

    public function city_branches($id){
    	$branches = Branch::where('city_id',$id)->get();

    	return response()->json($branches);
    }

    public function check_transit(Request $request){
    	$data 	= array();
    	$cities = array();
    	$text 	= '';


    	$transits = Transit::where('origin',$request->origin)->where('destination',$request->destination)->where('active',1)->get();
    	//$transits = Transit::where('origin',102)->where('destination',28)->get();
    	if(!$transits->isEmpty()){
    		foreach ($transits as $key => $transit) {
    			$city['city_name'] 	= city($transit->transit)['name'].'('.city($transit->transit)['mm_name'].')';
    			$city['city_id'] 	= $transit->transit;

	    		$origin 			= city($request->origin)['name'];
	    		$transit 			= city($transit->transit)['name'];
	    		$destination 		= city($request->destination)['name'];
	    		
	    		$text 			   .= ($key > 0? ', ':'').$transit;

	    		array_push($cities, $city);


	    		
	    		
	    		//$data['transit'] 	= $transit->transit;
	    		//$data['city_name'] 	= $transit;
	    		//$data['msg']		= $transit.' မှ transit ဖြင့်သွားမည်။';
	    		//$data['description']= '<span>'.$origin.'</span> မှ <span>'.$destination.'</span> သို့ပို့ပါက transit ဖြင့် <span>'.$transit.'</span> ကိုဖြတ်ပြီး ခန့်မှန်ခြေ <span>'.$transit->duration.'</span> ခန့် ကြာနိုင်ပါသည်။';

	    		
    		}
    		$data['description']= '<span class="text-danger">'.$text.'</span> အား transit ဖြတ်သွားမည်ဖြစ်သည်။ မိမိနှစ်သက်ရာ transit route အားရွေးချယ်နိုင်ပါသည်။ transit အသုံးမပြုလိုပါက <span class="text-danger">Transit အား အသုံးမပြုပါ</span> ကိုရွေးချယ်နိုင်ပါသည်။';

    		$c['city_id'] 		= '';
    		$c['city_name'] 	= 'Transit အား အသုံးမပြုပါ';
			array_push($cities, $c);

    		$data['success'] 	= 1;
    		$data['cities'] 	= $cities;
    		
    	}else{
    		$data['success'] 	= 0;
    	}
    	return response()->json($data);
    }

    public function transit_routes(){
    	//$branches = Branch::orderBy('name','asc')->paginate(20);
    	/*
    	$cities = DB::table('transits')
		->select('transits.id','transits.origin','transits.transit','transits.destination','cities.name as origin')
		->join('cities','transits.origin','=','cities.id')
		->paginate(20);

		$tasks = App\Task::join('users as u1', 'u1.id', '=', 'tasks.assigned_by')
	    ->join('users as u2', 'u2.id', '=', 'tasks.assigned_to')
	    ->select(['tasks.id', 'tasks.title', 'tasks.description', 'u1.name as assigned_by', 'u2.name as assigned_to', 'tasks.created_at'])
	    ->get();
		*/

		
		$cities = Transit::join('cities as u1', 'u1.id', '=', 'transits.origin')
	    ->join('cities as u2', 'u2.id', '=', 'transits.transit')
	    ->join('cities as u3', 'u3.id', '=', 'transits.destination')
	    ->select(['transits.duration','u1.name as origin','u1.shortcode as origin_shortcode', 'u2.name as transit','u2.shortcode as transit_shortcode','u3.name as destination','u3.shortcode as destination_shortcode'])
	    ->paginate(100);

		return response()->json($cities);
    }

    public function search_transit(Request $request){
    	$term = Transit::where('route',$request->term)
    	->join('transits as u1', 'u1.origin', '=', 'cities.id')
	    ->first();

    	return response()->json($term);
    }

    public function view_logs($id){
    	
    	$logs = ActionLog::join('users as u', 'u.id', '=', 'action_logs.action_by')
    	->join('branches as b', 'b.id', '=','action_logs.branch_id')
    	->join('actions as a', 'a.id', '=','action_logs.action_id')
    	->select(['action_logs.waybill_no','action_logs.action_log','action_logs.action_date','a.action as action','u.name as action_by','b.name as branch'])
    	->get();
    	
    	//$logs = ActionLog::get();

    	return response()->json($logs);
    }

    public function json_delivery_by($id){
    	if(isset($_GET['filtered-date'])){
            $date = $_GET['filtered-date'];
        }else{
            $date = date('Y-m-d');
        }

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name AS delivery_by','b.name AS branch','u2.name AS collected_by'])
                    ->where('waybills.user_id',$id)
                    ->where('action_logs.action_id',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->orderBy('waybills.waybill_no')
                    ->paginate(100);
        

        return response()->json($waybills);
    }

    public function search_waybill(Request $request){
    	$response = array();

    	$waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.delivery_id')
                ->join('action_logs', 'action_logs.waybill_no', '=', 'waybills.waybill_no')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_type','outbound')
                ->where('waybills.waybill_no','like','%'.$request->term.'%')
                ->where('waybills.current_status',4)
                ->select(['action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name','b.name AS branch'])
                ->paginate(100);

       	return response()->json($waybills);
    }

    public function sent_odoo_api_callback(Request $request){
		sent_to_odoo($request->cargos,$request->city,$request->status);

		return response()->json(['status' => 'sent']);
	}

	public function scanned_handover(Request $request){
		$waybill 	= $request->waybill;
		$response 	= array();

		$cargo = Waybill::where('waybill_no',$waybill)->first();

		if($cargo){
			if($cargo->current_status > 1 ){
				$response['success'] = 0;
				$response['message'] 	= '<li class="list-group-item text-danger"><i class="fa fa-times"></i>'.$waybill.'<span class="badge badge-pill badge-danger pull-right"> စာလွှဲပြောင်းထားပြီးသား</span></li>';
			}else{
				$cargo->current_status = 2;

				if($cargo->save()){
					$response['success'] = 1;
					$response['message'] 	= '<li class="list-group-item text-success"><i class="fa fa-check"></i>'.$waybill.'<span class="badge badge-pill badge-danger pull-right"> စာလွှဲပြောင်းလိုက်ပြီ</span></li>';
				}else{
					//push failed
					$response['success'] = 0;
					$response['message'] 	= '<li class="list-group-item text-danger"><i class="fa fa-times"></i>'.$waybill.'<span class="badge badge-pill badge-danger pull-right"> လုပ်ဆောင်ချက်မအောင်မြင်ပါ</span></li>';
				}
			}
		}else{
			//saved failed log
			$response['success'] = 0;
			$response['message'] 	= '<li class="list-group-item text-danger"><i class="fa fa-times"></i>'.$waybill.'<span class="badge badge-pill badge-danger pull-right"> စာအမှတ်မတွေ့ပါ</span></li>';
		}

		return response()->json($response);
	}
    
}
