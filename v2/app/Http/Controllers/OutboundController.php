<?php

namespace App\Http\Controllers;
use Mail;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Waybill;
use App\ActionLog;
use App\City;

class OutboundController extends Controller
{
    public function __construct(){
        $this->middleware('auth');
    }
    
    public function json_outbound_by_status($status){
        $date       = get_date();
        $city_id    = Auth::user()->city_id;
        $response   = array();

        if($status == 'collected'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',1)
                    ->where('waybills.current_status',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',1)
                    ->where('waybills.current_status',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }
        }elseif($status == 'handover'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->where('action_logs.action_id',2)
                ->where('waybills.current_status',2)
                ->where('action_logs.action_type','outbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.outbound_date','like',$date.'%')
                ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                ->orderBy('waybills.waybill_no','asc')
                ->paginate(100);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->where('action_logs.action_id',2)
                ->where('waybills.current_status',2)
                ->where('action_logs.action_type','outbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.outbound_date','like',$date.'%')
                ->where('waybills.origin',$city_id)
                ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                ->orderBy('waybills.waybill_no','asc')
                ->paginate(100);
            }
        }elseif($status == 'received'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status',3)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status',3)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }
        }elseif($status == 'branch-out'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id','>=',4)
                    ->where('waybills.current_status','>=',4)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','waybills.transit','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id','>=',4)
                    ->where('waybills.current_status','>=',4)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','waybills.transit','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }
        }elseif($status == 'finished-received'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status','>=',3)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','waybills.transit','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status','>=',3)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','waybills.transit','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }
        }else{
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',1)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);
            }else{
               $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',1)
                    ->where('action_logs.action_type','outbound')
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100); 
            }
            
        }
        return response()->json($waybills);
    }

    public function outbound_by_status($status){
        if($status == 'collected'){
            $keyword    = 'Collected';
            $search     = 'collected';
            $mm_keyword = 'စာကောက်';
            $json       = 'json/outbound/collected';
        }elseif($status == 'handover'){
            $keyword    = 'Handover';
            $search     = 'handover';
            $mm_keyword = 'စာလွှဲပြောင်း';
            $json       = 'json/outbound/handover';
        }elseif($status == 'received'){
            $keyword    = 'Received';
            $search     = 'received';
            $mm_keyword = 'စာလက်ခံ';
            $json       = 'json/outbound/received';
        }elseif($status == 'branch-out'){
            $keyword    = 'Branch Out';
            $search     = 'branch-out';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/outbound/branch-out';
        }elseif($status == 'finished-received'){
            $keyword    = 'Received';
            $search     = 'finished-received';
            $mm_keyword = 'စာလက်ခံပြီး';
            $json       = 'json/outbound/finished-received';
        }else{
            $keyword    = 'All';
            $mm_keyword = '';
            $search     = '';
            $json       = 'json/outbound/all';
        }
        
        if(role() == 1){
            return view('admin.outbound.lists',compact('json','keyword','mm_keyword','search'));
        }elseif(role() == 2){
            return view('operator.outbound.lists',compact('json','keyword','mm_keyword','search'));
        }else{
            return view('layouts.errors.404');
        }
        
    }

    /*
    public function outbound_transit_status($status){
        if($status == 'inbound'){
            $keyword    = 'Inbound';
            $mm_keyword = 'စာကောက်';
            $json       = 'json/outbound/collected';
        }elseif($status == 'branch-in'){
            $keyword    = 'Branch In';
            $mm_keyword = 'စာလွှဲပြောင်း';
            $json       = 'json/outbound/handover';
        }elseif($status == 'handover'){
            $keyword    = 'Handover';
            $mm_keyword = 'စာလက်ခံ';
            $json       = 'json/outbound/received';
        }elseif($status == 'received'){
            $keyword    = 'Received';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/outbound/branch-out';
        }elseif($status == 'branch-out'){
            $keyword    = 'Branch Out';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/outbound/branch-out';
        }elseif($status == 'outbound'){
            $keyword    = 'Outbound';
            $mm_keyword = 'စာထွက်';
            $json       = 'json/outbound/branch-out';
        }else{
            $keyword    = '';
            $mm_keyword = '';
            $json       = '';
        }

        return view('operator.outbound.lists',compact('json','keyword','mm_keyword'));
    }
    */

    public function outbound_scan(Request $request){
        if($request->action == 'collected'){
        	return view('operator.outbound.action-collected');
        }elseif($request->action == 'handover'){
            return view('operator.outbound.action-handover');
        }elseif($request->action == 'received'){
            if(branch(Auth::user()->branch_id)['is_main_office'] == 1){
                return view('operator.outbound.action-received');
            }else{
                return view('layouts.errors.404');
            }
        }elseif($request->action == 'branch-out'){
            if(branch(Auth::user()->branch_id)['is_main_office'] == 1){
                return view('operator.outbound.action-branch-out');
            }else{
                return view('layouts.errors.404');
            }
        }else{
            return 'action not allowed';
        }
    }

    public function single_outbound_scan(Request $request){
        if($request->action == 'received'){
            return view('operator.outbound.single-action-received');
        }else{
            return 'action not allowed';
        }
    }

    public function fetch_waybills(Request $request){
        $response = array();

        foreach ($request->waybills as $key => $waybill_no) {
            //$waybill = Outbound::where('waybill_no',$waybill_no)->first();
            $waybill = DB::table('cargos')->where('waybill_no',$waybill_no)->first();
            
            //$data['status'] = $waybill->status;
            $data['id']         = $waybill->id;
            $data['from_city']  = city($waybill->from_city,'city');

            array_push($response,$data);
        }

        return response()->json($response);
    }

    public function view_logs($id){
        $waybill = Waybill::find($id);
        $logs = Waybill::join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
            ->join('users as u', 'u.id', '=', 'action_logs.action_by')
            ->join('branches as b', 'b.id', '=','action_logs.branch_id')
            ->join('actions as a', 'a.id', '=','action_logs.action_id')
            ->select(['action_logs.waybill_id','action_logs.action_log','action_logs.action_date','action_logs.branch_id','a.action as action','u.name as action_by','b.name as branch'])
            ->where('action_logs.waybill_id',$id)
            ->get();
        
        return view('operator.outbound.view-logs',compact('id','logs','waybill'));
    }

    public function delivery_by($id){
        $json = 'json/delivery-by/'.$id;
        
        return view('operator.outbound.delivery-by',compact('id','json'));
    }

    public function json_delivery_by($id){
        $date = get_date();

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
            ->join('users as u', 'u.id', '=', 'waybills.user_id')
            ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
            ->join('cities as c', 'c.id', '=', 'waybills.origin')
            ->join('branches as b', 'b.id', '=', 'u.branch_id')
            ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name AS delivery_by','b.name AS branch','u2.name AS collected_by'])
            ->where('waybills.user_id',$id)
            ->where('action_logs.action_id',1)
            ->where('action_logs.action_date','like',$date.'%')
            ->orderBy('waybills.waybill_no')
            ->paginate(100);
        
        return response()->json($waybills);
    }

    public function branch_out_by_city($id){
        $json = 'json/branch-out-by-city/'.$id;
        if(role() == 1){
            return view('admin.outbound.branch-out-by-city',compact('id','json'));
        }elseif(role() == 2){
            return view('operator.outbound.branch-out-by-city',compact('id','json'));
        }else{
            return view('layouts.errors.404');
        }
        
    }

    public function transit_out_by_city($id){
        $json = 'json/transit-out-by-city/'.$id;
        if(role() == 1){
            return view('admin.outbound.branch-out-by-city',compact('id','json'));
        }elseif(role() == 2){
            return view('operator.outbound.branch-out-by-city',compact('id','json'));
        }else{
            return view('layouts.errors.404');
        }
        
    }

    public function json_branch_out_by_city($city_id){
        $origin    = Auth::user()->city_id;
        $date       = get_date();

        if(role() == 1){
            $waybills   = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_id',4)
                ->where('waybills.destination',$city_id)
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.outbound_date','like',$date.'%')
                ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit','c.name as origin','c2.name as destination','u.name AS delivery_by','b.name AS branch','u2.name AS action_by'])
                ->paginate(100);
        }else{
            $waybills   = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$origin)
                ->where('waybills.destination',$city_id)
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.outbound_date','like',$date.'%')
                ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit','c.name as origin','c2.name as destination','u.name AS delivery_by','b.name AS branch','u2.name AS action_by'])
                ->paginate(100);
        }
        
        return response()->json($waybills);
    }

    public function json_transit_out_by_city($city_id){
        $origin    = Auth::user()->city_id;
        $date       = get_date();

        if(role() == 1){
            $waybills   = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_id',4)
                ->where('waybills.destination',$city_id)
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.transit_date','like',$date.'%')
                ->select(['waybill_id','action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit','c.name as origin','c2.name as destination','u.name AS delivery_by','b.name AS branch','u2.name AS action_by'])
                ->paginate(100);
        }else{
            $waybills   = Waybill::join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->leftjoin('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->leftjoin('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->leftjoin('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_id',4)
                ->where('action_logs.action_type','transit')
                ->where('waybills.transit',$origin)
                ->where('waybills.destination',$city_id)
                ->where('waybills.transit_date','like',$date.'%')
                ->select(['waybill_id','action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit','c.shortcode as origin','c2.shortcode as destination','u.name AS delivery_by','b.name AS branch','u2.name AS action_by'])
                ->paginate(100);

        }
        
        return response()->json($waybills);
    }

    public function view_waybill($id){
        $response   = array();
        $waybill    = Waybill::find($id);

        if($waybill){
            $response['outbound_date']  = $waybill->outbound_date;
            $response['id']             = $waybill->id;
            $response['waybill_no']     = $waybill->waybill_no;
            $response['origin']         = city($waybill->origin)['name'];
            $response['transit']        = city($waybill->transit)['name'];
            $response['destination']    = city($waybill->destination)['name'];
            $response['current_status'] = '<span class="badge badge-pill badge-'.$waybill->current_status.'">'.current_status($waybill->current_status).'</span>';
            $response['transit_status'] = $waybill->transit_status;
            $response['delivery']       = user($waybill->user_id)['name'];
        }

        return response()->json($response);

    }

    public function filtered_outbound_by_branches(){
        
        return view('operator.outbound.filtered-outbound-by-branch');
    }

    public function to_receive_lists(){
        $date   = get_date();
        $city_id= Auth::user()->city_id;

        $branches = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                ->select('action_logs.branch_id',DB::raw('count(*) as total'))
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.current_status',2)
                ->where('waybills.destination',$city_id)
                ->groupBy('action_logs.branch_id')
                ->get();

        return view('operator.outbound.to-receive-lists',compact('branches'));
    }

    public function inform_to_mail($id){
        $city_id    = Auth::user()->city_id;
        $date       = get_date();
        $delivery   = User::find($id);
        $sender     = User::find(Auth::id());

        $waybills = Waybill::join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->where('action_logs.action_id',1)
                ->where('waybills.user_id',$id)
                ->where('waybills.outbound_date','like',$date.'%')
                ->select(['waybills.waybill_no','c.name as origin','action_logs.action_date'])
                ->get();

        //return view('operator.outbound.mail-template',compact('waybills','sender'));   
        
        $data = array( 
            'date'          => $date, 
            'delivery'      => $delivery, 
            'waybills'      => $waybills, 
            'sender'        => $sender,
            'from'          => 'royalexpress2019@gmail.com', 
            'from_name'     => 'Royal Cargo Team' 
        );

        
        Mail::send(['html'=>'operator.outbound.mail-template'], $data, function($message) use ($data){
            $message->to('yekyawaung@royalx.biz')->cc(['yekyawaung1991@gmail.com'])->from( $data['from'], $data['from_name'])->subject('Royal Cargo Team');
        });
        
        return redirect()->back()->with('success','You informed for outbound.');
    }


    public function outbound_search(){
        $date       = get_date();
        $city_id    = Auth::user()->city_id;
        $response   = array();
        $status     = $_GET['page'];
        $term       = $_GET['term'];

        if($status == 'collected'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',1)
                    ->where('waybills.current_status',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',1)
                    ->where('waybills.current_status',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }
        }elseif($status == 'handover'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',2)
                    ->where('waybills.current_status',2)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',2)
                    ->where('waybills.current_status',2)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }
        }elseif($status == 'received'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status',3)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',3)
                    ->where('waybills.current_status',3)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }
        }elseif($status == 'branch-out'){
            if(role() == 1){
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',4)
                    ->where('waybills.current_status',4)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }else{
                $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('action_logs.action_id',4)
                    ->where('waybills.current_status',4)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.outbound_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->where('waybills.origin',$city_id)
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(10);
            }
        }else{
            $waybills = array();
        }
        return response()->json($waybills);
    }

    public function search_json_delivery_by(){
        $term           = $_GET['term'];
        $delivery_id    = $_GET['delivery_id'];
        $date           = get_date();

        $waybills = Waybill::join('action_logs','waybills.id','=','action_logs.waybill_id')
                    ->join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name','b.name AS branch'])
                    ->where('waybills.user_id',$delivery_id)
                    ->where('action_logs.action_id',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('waybills.waybill_no','like','%'.$term.'%')
                    ->orderBy('waybills.waybill_no')
                    ->paginate(100);
        
        return response()->json($waybills);
    }

    public function search_branch_out_by_city(Request $request){
        $origin    = Auth::user()->city_id;
        $date      = get_date();
        $to_city   = $request->to_city;
        $term      = $request->term;

        if(role() == 1){
            $waybills   = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_id',4)
                ->where('waybills.destination',$to_city)
                ->where('waybills.waybill_no','like','%'.$term.'%')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.outbound_date','like',$date.'%')
                ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit','c.name as origin','c2.name as destination','u.name AS delivery_by','b.name AS branch','u2.name AS action_by'])
                ->paginate(100);
        }else{
            $waybills   = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                ->join('cities as c', 'c.id', '=', 'waybills.origin')
                ->join('cities as c2', 'c2.id', '=', 'waybills.destination')
                ->join('branches as b', 'b.id', '=', 'u.branch_id')
                ->where('action_logs.action_id',4)
                ->where('waybills.origin',$origin)
                ->where('waybills.destination',$to_city)
                ->where('waybills.waybill_no','like','%'.$term.'%')
                ->where('action_logs.action_date','like',$date.'%')
                ->where('waybills.outbound_date','like',$date.'%')
                ->select(['action_logs.action_date','waybills.id','waybills.waybill_no','waybills.batch_id','waybills.current_status','waybills.transit','c.name as origin','c2.name as destination','u.name AS delivery_by','b.name AS branch','u2.name AS action_by'])
                ->paginate(100);
        }
        

        return response()->json($waybills);
    }

    public function branch_out_summary(){
        return view('operator.outbound.summary');
    }

    public function branch_out_summary_by_branch($branch_id){
        $city_id        = Auth::user()->city_id;
        $same_cities    = 'json/branch-out-summary/'.$branch_id.'/same-city';
        $other_cities   = 'json/branch-out-summary/'.$branch_id.'/other-cities';

        return view('operator.outbound.summary-by-branch',compact('city_id','same_cities','other_cities','branch_id'));
    }

    public function json_branch_out_summary_by_branch($branch_id,$filtered){
        $date       = get_date();
        $city_id    = Auth::user()->city_id;
        $response   = array();


        if($filtered == 'same-city'){
            $waybills = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
                ->join('cities','cities.id','=','waybills.destination')
                ->select('cities.id AS city_id','cities.shortcode','cities.name',DB::raw('count(*) as total'))
                ->where('action_logs.branch_id',$branch_id)
                ->where('waybills.current_status','>=',4)
                ->where('action_logs.action_id',1)
                ->where('waybills.destination',$city_id)
                ->where('action_logs.action_type','outbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->groupBy('cities.id','cities.shortcode','cities.name')
                ->paginate(10);
        }elseif($filtered == 'other-cities'){
            $waybills = Waybill::join('action_logs','action_logs.waybill_id', '=', 'waybills.id')
                ->join('cities','cities.id','=','waybills.destination')
                ->select('cities.id AS city_id','cities.shortcode','cities.name',DB::raw('count(*) as total'))
                ->where('action_logs.branch_id',$branch_id)
                ->where('waybills.current_status','>=',4)
                ->where('action_logs.action_id',1)
                ->where('waybills.destination','!=',$city_id)
                ->where('action_logs.action_type','outbound')
                ->where('action_logs.action_date','like',$date.'%')
                ->groupBy('cities.id','cities.shortcode','cities.name')
                ->paginate(10);
        }else{
            $waybills = array();
        }

        return response()->json($waybills);
    }

    public function branch_out_to_city_by_branch($branch_id,$city_id){
        $json = 'json/branch-out-summary/'.$branch_id.'/to/'.$city_id.'/';
        return view('operator.outbound.branch-out-summary-to-city',compact('branch_id','city_id','json'));
    }

    public function json_branch_out_to_city_by_branch($branch_id,$city_id){
        $response = array();
        $date = get_date();

        $waybills = Waybill::join('users as u', 'u.id', '=', 'waybills.user_id')
                    ->join('action_logs', 'action_logs.waybill_id', '=', 'waybills.id')
                    ->join('cities as c', 'c.id', '=', 'waybills.origin')
                    ->join('branches as b', 'b.id', '=', 'u.branch_id')
                    ->join('users as u2', 'u2.id', '=', 'action_logs.action_by')
                    ->where('waybills.destination',$city_id)
                    ->where('action_logs.branch_id',$branch_id)
                    ->where('waybills.current_status','>=',4)
                    ->where('action_logs.action_id',1)
                    ->where('action_logs.action_date','like',$date.'%')
                    ->where('action_logs.action_type','outbound')
                    ->select(['waybills.id','action_logs.action_date','waybills.waybill_no','waybills.batch_id','waybills.current_status','c.name as origin','u.name as delivery','u2.name as action_by','b.name AS branch'])
                    ->orderBy('waybills.waybill_no','asc')
                    ->paginate(100);

        return response()->json($waybills);
    }

    public function collected_by_branch($branch_id){
        $link = 'sec-2';
        return view('operator.pages.block-7',compact('branch_id','link'));
    }
}
