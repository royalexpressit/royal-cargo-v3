$(document).ready(function(){
	//declared first loaded json data
	var load_url = 'http://cargo-2.test/json/inbounds/received';

	$.ajax({
	    url: load_url,
	    type: 'GET',
	    data: {},
	    success: function(data){
	        console.log(data);
	        $.each( data.data, function( key, value ) {
				$("#fetched-data").append("<tr><td>"+value.waybill_no+"</td></tr>");
			});
				
			if(data.prev_page_url === null){
				$("#prev-btn").attr('disabled',true);
			}else{
				$("#prev-btn").attr('disabled',false);
			}
			if(data.next_page_url === null){
				$("#next-btn").attr('disabled',true);
			}else{
				$("#next-btn").attr('disabled',false);
			}
			$("#prev-btn").val(data.prev_page_url);
			$("#next-btn").val(data.next_page_url);
	    }
	});
});

$('.pagination-btn').click(function(){
	//clicked url json data
	var clicked_url = $(this).val();


	

	$.ajax({
	    url: clicked_url,
	    type: 'GET',
	    data: {},
	    success: function(data){
	        console.log(data);
	        $("#fetched-data").empty();
	        $.each( data.data, function( key, value ) {
				$("#fetched-data").append("<tr><td>"+value.waybill_no+"</td></tr>");
			});
			if(data.prev_page_url === null){
				$("#prev-btn").attr('disabled',true);
			}else{
				$("#prev-btn").attr('disabled',false);
			}
			if(data.next_page_url === null){
				$("#next-btn").attr('disabled',true);
			}else{
				$("#next-btn").attr('disabled',false);
			}
			$("#prev-btn").val(data.prev_page_url);
			$("#next-btn").val(data.next_page_url);
	    }
	});
});