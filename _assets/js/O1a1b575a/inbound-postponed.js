$(document).ready(function(){
    var url     = $("#url").val();
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var raw     = '';

    $('#from_city').select2({
        placeholder: "Select From City"
    });

    $("#form").submit(function(event){
        event.preventDefault();  
    });

    $("#waybill").on("keydown",function search(e) {
        if(e.keyCode == 13) {
            ++scanned;
            $("#scanned").text(scanned);
            $("#success").text(0);
            $("#failed").text(0);
                
            $("#failed-lists").empty();
            $("#scanned-lists").show(); 

            if(scanned < 11){
                waybill   = $("#waybill").val().toUpperCase();
                $(".scan-btn").show();
                $("#scanned-lists").prepend('<li class="list-group-item"><i class="fa fa-qrcode"></i>  '+waybill+'</li>');
                if(scanned > 1){
                    raw = waybill+',';
                }else{
                    raw = waybill;
                }
                $(this).val('');
                $('#multi_scanned_waybills').prepend(raw);
                console.log(raw);
            }else{
                alert('limit');
            }
        }
    });

    $(".scan-btn").on("click",function search(e) {
        waybills        = $('#multi_scanned_waybills').val();
        user_id         = $("#user_id").val();
        package_id      = $("#package_id").val();
        city_id         = $("#city_id").val();
        branch_id       = $("#branch_id").val();
        from_city       = $("#from_city").val();
            //check           = waybill.length;
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/api/action/inbound',
            dataType:'json',
            data: {
                'waybills'      :waybills,
                'user_id'       :user_id,
                'city_id'       :city_id,
                'branch_id'     :branch_id,
                'from_city'     :from_city,
                'package_id'    :package_id,
                'action'        :'postponed'
            },
            success: function(data) { 
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();


                success = $("#scanned").text() - data.failed.length;
                failed  = data.failed.length;

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
                    for (i = 0; i < data.failed.length; i++) {
                        $("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-times text-danger"></i> '+data.failed[i]+'</li>');
                    }
                }else{
                    $("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-check text-success"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                }
            },
        });
        $(this).val(''); 

        $('#multi_scanned_waybills').empty();
        $('.scan-btn').hide();
        scanned = 0;
    });

    $("#switch-postponed").change(function(){
        if($(this).prop("checked") == true){
            //run code
            $(".action-received").hide();
            $(".action-postponed").show();
        }else{
            $(".action-postponed").hide();
            $(".action-received").show();
        }
    });
});
