$(document).ready(function(){
	var url     = $("#url").val();
	var scanned = 0;
	var success = 0;
	var failed  = 0;
	var raw     = '';
    var voice   = 'alert-1.mp3';
    var city    = $("#city").val();
    var _token  = $("#_token").val();

	//temp scanned and received count
    tmp_scanned 	= parseInt($("#temp-scanned").text());
    tmp_received 	= parseInt($("#temp-received").text());

    var check = $('#check-marked-branch').val();
    if(check == ''){
    	$(".marked-lists").hide();
    }

    $('#marked-branch').select2({
        placeholder: "Select Branch"
    });

    $("#form").submit(function(event){
		event.preventDefault();  
	});

    $("#waybill").on("keydown",function search(e) {
    	if(e.keyCode == 13) {
    		waybill   = $("#waybill").val().toUpperCase();
            if(waybill.length > 10){
                //valid length && continue
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                        
                $("#failed-lists").empty();
                $("#scanned-lists").show();
                
                $(".scan-btn").show();
                $("#scanned-lists").prepend('<li class="list-group-item"><i class="fa fa-qrcode"></i>  '+waybill+'</li>');
                if(scanned > 1){
                    raw = waybill+',';
                }else{
                    raw = waybill;
                }
                $(this).val('');
                $(".check-number").addClass('hide');

                $('#multi_scanned_waybills').prepend(raw); 
                $("#scanned-lists").addClass('scanned-panel');

                //limit scanned count with 30
                if(scanned == 30){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    $('#exampleModal').modal({show:true});
                    $(".continue-action").show();
                    setTimeout(function(){
                        $('#exampleModal').modal('hide');
                        $("#continue-action").trigger('focus');
                    },5000);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
    	}
    });

    $(".scan-btn").on("click",function search(e) {
    	//call api sent to server function
        data_send();
    });

    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                data_send();
            }else{
                $("#continue-action").val('');
            }
        }
    });

    var data_send = function(){
        KTApp.block('#blockui_content_1', {
            overlayColor: '#000000',
            type: 'v2',
            state: 'danger',
            message: 'လုပ်ဆောင်နေပါသည် ...'
        });
        
        //prepare input for api request
        waybills 		= $('#multi_scanned_waybills').val();
        user_id         = $("#user_id").val();
        delivery_id     = $('select[id=delivery]').val()
        package_id      = $("#package_id").val();
        city_id      	= $("#city_id").val();
        branch_id  		= $("#branch_id").val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/api/action/outbound',
            dataType:'json',
            data: {
                'waybills'		:waybills,
                'user_id'		:user_id,
                'city_id'		:city_id,
                'branch_id'		:branch_id,
                'delivery_id'	:delivery_id,
                'package_id'	:package_id,
                'action'		:'received'
            },
            success: function(data) { 
            	KTApp.unblock('#blockui_content_1');
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();
                	
                success = $("#scanned").text() - data.failed.length;
        		failed  = data.failed.length;

        		//add scroll max size for item > 10
                if(failed > 10){
                   $("#failed-lists").addClass('scanned-panel'); 
                }

                $("#success").text(success);
                $("#failed").text(failed);

                //added old and new count for tmp 
                tmp_scanned 	= tmp_scanned+parseInt($("#scanned").text());
                tmp_received 	= tmp_received+parseInt(success);
                	
                //updated count
                $("#temp-scanned").text(tmp_scanned);
                $("#temp-received").text(tmp_received);

                if(data.failed.length > 0 ){
	                for (i = 0; i < data.failed.length; i++) {
						$("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-times text-danger"></i> '+data.failed[i]+'</li>');
					}
				}else{
					$("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-check text-success"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
				}

                //callback action to sent odoo api
                if(data.cargos){
                    if(data.cargos.length > 0){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        //callback odoo api sent
                        $.ajax({
                            type: 'post',
                            url: url+"/sent-odoo-api-callback",
                            dataType:'json',
                            data: {
                                'cargos':data.cargos,
                                'city':city,
                                'status':'in',
                                '_token':_token
                            },
                            success: function(data) { 

                            }
                        });
                    }else{
                        console.log('empyt');
                    }
                }
            },
        });
        $(this).val(''); 


        $('#multi_scanned_waybills').empty();
        $('.scan-btn').hide();
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    }

    //saved for marked waybill count for scanned and success waybills
    $('body').delegate(".selected-branch","click",function () {
        var branch = $('select[id=marked-branch]').val();
        _token 		= $('#_token').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/marked-branch',
            dataType:'json',
            data: {
                'branch'	:branch,
                '_token' 	: _token
            },
            success: function(data) { 
                console.log(data);
                $("#choice-branch").text(data.branch_name);
                $('.marked-branch').removeClass('btn-danger');
                $('.marked-branch').addClass('btn-success');
                $('.marked-lists').show();
            },
        });
    });

    //removed for marked waybill count for scanned and success waybills
    $('body').delegate(".clear-branch","click",function () {
        _token 		= $('#_token').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/clear-branch',
            dataType:'json',
            data: {
                '_token' 	: _token
            },
            success: function(data) { 
                console.log(data);
                $("#choice-branch").text('Set Branch');
                $('.marked-branch').removeClass('btn-success');
                $('.marked-branch').addClass('btn-danger');
                $('.marked-lists').hide();

                //clear count for scanned & success tmp
                $("#temp-scanned").text(0);
                $("#temp-received").text(0);
            },
        });
    });

    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/_assets/'+voice);

    $(".set-voice").on("click",function search(e) {
        $('#voiceModal').modal({show:true});
    });

    $(".choice-voice").on("click",function search(e) {
        voice = $(this).val();

        audioElement.setAttribute('src', url+'/_assets/'+voice);
        audioElement.load();
        console.log(voice);
    });
});