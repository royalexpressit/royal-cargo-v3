$(document).ready(function(){
	var url     = $("#url").val();
	var scanned = 0;
	var success = 0;
	var failed  = 0;
	var raw     = '';
    var voice   = 'alert-1.mp3';

	//temp scanned and received count
    tmp_scanned 	= 0;
    tmp_received 	= 0;

    var check = $('#check-marked-branch').val();
    if(check == ''){
    	$(".marked-lists").hide();
    }

    $('#marked-branch').select2({
        placeholder: "Select Branch"
    });

    $("#form").submit(function(event){
		event.preventDefault();  
	});

    $("#scanned_waybill").on("keydown",function search(e) {
    	if(e.keyCode == 13) {
    		var scanned_waybill   = $("#scanned_waybill").val().toUpperCase();
            if(scanned_waybill.length > 10){
                //valid length && continue
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);

                data_send();
                        
                //limit scanned count with 30
                if(scanned == 30){
                    $("#scanned_waybill").attr("disabled", true);
                    audioElement.play();
                    $('#exampleModal').modal({show:true});
                    $(".continue-action").show();
                    $(".continue-action-btn").show();
                    setTimeout(function(){
                        $('#exampleModal').modal('hide');
                        $("#continue-action").trigger('focus');
                    },5000);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#scanned_waybill").val('');
            }

    	}
    });

    $(".continue-action-btn").on("click",function search(e) {
        //reset form
        $("#scanned_waybill").attr("disabled", false);
        $("#scanned_waybill").trigger('focus');
        $(".continue-action").hide();
        $("#continue-action").val('');
        $("#scanned-lists").empty();
        $(".continue-action-btn").hide();
        scanned     = 0;
        failed      = 0;
        success     = 0;
    });

    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                //reset form
                $("#scanned_waybill").attr("disabled", false);
                $("#scanned_waybill").trigger('focus');
                $(".continue-action").hide();
                $("#continue-action").val('');
                $("#scanned-lists").empty();
                scanned     = 0;
                failed      = 0;
                success     = 0;
            }else{
                $("#continue-action").val('');
            }
        }
    });

    var data_send = function(){
        KTApp.block('#blockui_content_1', {
            overlayColor: '#000000',
            type: 'v2',
            state: 'danger',
            message: 'လုပ်ဆောင်နေပါသည် ...'
        });
        
        //prepare input for api request
        waybill         = $('#scanned_waybill').val();
        user_id         = $("#user_id").val();
        delivery_id     = $('select[id=delivery]').val()
        package_id      = $("#package_id").val();
        city_id      	= $("#city_id").val();
        branch_id  		= $("#branch_id").val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/api/action/outbound',
            dataType:'json',
            data: {
                'waybill'		:waybill,
                'user_id'		:user_id,
                'city_id'		:city_id,
                'branch_id'		:branch_id,
                'delivery_id'	:delivery_id,
                'package_id'	:package_id,
                'action'		:'single-received'
            },
            success: function(data) { 
                if(data.success == 1){
                    //added old and new count for tmp 
                    ++tmp_scanned;
                    ++tmp_received;
                    ++success;
                    $("#success").text(success);
                
                }else{
                    ++tmp_scanned;
                    ++failed;
                    $("#failed").text(failed);
                }
                console.log(scanned);
            	KTApp.unblock('#blockui_content_1');
                $("#scanned-lists").removeClass('scanned-panel');
                	
                
                //added old and new count for tmp 
                //tmp_scanned 	= tmp_scanned+parseInt($("#scanned").text());
                //tmp_received 	= tmp_received+parseInt(success);
                	
                //updated count
                $("#temp-scanned").text(tmp_scanned);
                $("#temp-received").text(tmp_received);

                $("#scanned-lists").prepend(data.message);
            },
        });
        

        $('.scan-btn').hide();
        $('.continue-action').hide();
        $('#scanned_waybill').val('');
        $('#scanned-waybill').trigger('focus');
    }

    //saved for marked waybill count for scanned and success waybills
    $('body').delegate(".selected-branch","click",function () {
        var branch = $('select[id=marked-branch]').val();
        _token 		= $('#_token').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/setting/marked-branch',
            dataType:'json',
            data: {
                'branch'	:branch,
                '_token' 	: _token
            },
            success: function(data) { 
                $("#choice-branch").text(data.branch_name);
                $('.marked-branch').removeClass('btn-danger');
                $('.marked-branch').addClass('btn-success');
                $('.marked-lists').show();
            },
        });
    });

    //removed for marked waybill count for scanned and success waybills
    $('body').delegate(".clear-branch","click",function () {
        _token 		= $('#_token').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/setting/clear-branch',
            dataType:'json',
            data: {
                '_token' 	: _token
            },
            success: function(data) { 
                console.log(data);
                $("#choice-branch").text('Set Branch');
                $('.marked-branch').removeClass('btn-success');
                $('.marked-branch').addClass('btn-danger');
                $('.marked-lists').hide();

                //clear count for scanned & success tmp
                $("#temp-scanned").text(0);
                $("#temp-received").text(0);
            },
        });
    });

    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/assets/'+voice);

    $(".set-voice").on("click",function search(e) {
        $('#voiceModal').modal({show:true});
    });

    $(".choice-voice").on("click",function search(e) {
        voice = $(this).val();

        audioElement.setAttribute('src', url+'/assets/'+voice);
        audioElement.load();
        console.log(voice);
    });
});