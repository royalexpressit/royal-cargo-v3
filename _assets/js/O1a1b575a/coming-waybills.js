$(document).ready(function(){
	var url 		= $("#url").val();
	var json    	= $("#json").val();
	var transit    	= $("#transit").val();

	//declared first loaded json data
	var load_json = url+'/'+json;
	var waybills = [];
	var item     = 0;
	$.ajax({
		url: load_json,
		type: 'GET',
		data: {},
		success: function(data){
			if(data.total > 0){
			    $(".alert-box").hide();
			    console.log(data);
			    		
				$.each( data.data, function( key, value ) {
				++item;
				waybills.push(value.city_id);
				$("#fetched-data").append('<a href="coming-waybills/'+value.city_id+'"><div class="kt-widget-4__item coming-city-list">'
                	+ '<div class="kt-widget-4__item-content">'
                    	+ '<div class="kt-widget-4__item-section">'
                        	+ '<div class="kt-widget-4__item-pic">'
                            + '<img class="" src="assets/images/favicon.png" alt="">'
				            + '</div>'
				            + '<div class="kt-widget-4__item-info">'
				            + '<span class="kt-widget-4__item-username text-primary">'+value.city_name+' - '+value.city_mm_name+'</span>'
				            + '<div class="kt-widget-4__item-desc text-danger">ဝင်လာမည့်စာအရေအတွက်</div>'
				            + '</div>'
				            + '</div>'
				            + '</div>'
				            + '<div class="kt-widget-4__item-content">'
				            + '<div class="kt-widget-4__item-price">'
				            + '<span class="kt-widget-4__item-number"><span class="badge badge-warning badge-pill badge-sm">'+value.total+'</span></span>'
				            + '</div>'
				        + '</div>'
				    + '</div></a>'
				);	
			});

			$(".pagination").show();
			$("#to-records").text(data.to);
			$("#total-records").text(data.total);
							
			if(data.prev_page_url === null){
				$("#prev-btn").attr('disabled',true);
			}else{
				$("#prev-btn").attr('disabled',false);
			}
			if(data.next_page_url === null){
				$("#next-btn").attr('disabled',true);
			}else{
				$("#next-btn").attr('disabled',false);
			}
			$("#prev-btn").val(data.prev_page_url);
			$("#next-btn").val(data.next_page_url);
			}else{
			    $(".pagination").hide();
			    $(".alert-box").show();
			}
		}
	});

			$('.pagination-btn').click(function(){
				//clicked url json data
				var clicked_url = $(this).val();
				

				$(this).siblings().removeClass('active')
				$(this).addClass('active');
				$.ajax({
				    url: clicked_url,
				    type: 'GET',
				    data: {},
				    success: function(data){
				        //console.log(data);
				        $("#fetched-data").empty();
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="coming-waybills/'+value.city_id+'"><div class="kt-widget-4__item coming-city-list">'
                			+ '<div class="kt-widget-4__item-content">'
                    		+ '<div class="kt-widget-4__item-section">'
                        	+ '<div class="kt-widget-4__item-pic">'
                            + '<img class="" src="assets/images/favicon.png" alt="">'
				            + '</div>'
				            + '<div class="kt-widget-4__item-info">'
				            + '<span class="kt-widget-4__item-username text-primary">'+value.city_name+' - '+value.city_mm_name+'</span>'
				            + '<div class="kt-widget-4__item-desc text-danger">ဝင်လာမည့်စာအရေအတွက်</div>'
				            + '</div>'
				            + '</div>'
				            + '</div>'
				            + '<div class="kt-widget-4__item-content">'
				            + '<div class="kt-widget-4__item-price">'
				            + '<span class="kt-widget-4__item-number"><span class="badge badge-warning badge-pill badge-sm">'+value.total+'</span></span>'
				            + '</div>'
				            + '</div>'
				            + '</div></a>');
						});

				        $("#to-records").text(data.to);
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
				    }
				});
			});

			//transit loaded data
			var load_transit = url+'/'+transit;
			var waybills = [];
			var item     = 0;
			$.ajax({
			    url: load_transit,
			    type: 'GET',
			    data: {},
			    success: function(data){
			    	if(data.total > 0){
			    		$(".alert-box").hide();
			    		//console.log(data);
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.city_id);
							$("#fetched-data2").append('<a href="transit-coming-waybills/'+value.city_id+'"><div class="kt-widget-4__item coming-city-list">'
                			+ '<div class="kt-widget-4__item-content">'
                    		+ '<div class="kt-widget-4__item-section">'
                        	+ '<div class="kt-widget-4__item-pic">'
                            + '<img class="" src="assets/images/favicon.png" alt="">'
				            + '</div>'
				            + '<div class="kt-widget-4__item-info">'
				            + '<span class="kt-widget-4__item-username text-primary">'+value.city_name+' - '+value.city_mm_name+'</span>'
				            + '<div class="kt-widget-4__item-desc text-danger">ဝင်လာမည့်စာအရေအတွက်</div>'
				            + '</div>'
				            + '</div>'
				            + '</div>'
				            + '<div class="kt-widget-4__item-content">'
				            + '<div class="kt-widget-4__item-price">'
				            + '<span class="kt-widget-4__item-number"><span class="badge badge-warning badge-pill badge-sm">'+value.total+'</span></span>'
				            + '</div>'
				            + '</div>'
				            + '</div></a>');
							
						});

						//console.log(waybills);
						//$(".pagination").show();
				        $("#to-records").text(data.to);
						$("#total-records").text(data.total);
							
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
			    	}else{
			    		$(".pagination").hide();
			    		$(".alert-box").show();
			    	}
			    }
			});

		});