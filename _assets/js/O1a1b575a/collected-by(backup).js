$(document).ready(function(){
	var url 	= $("#url").val();
	var json 	= $("#json").val();
	var load    = url+'/'+json;
	var _token  = $("#_token").val();
			
	var load_url = load;
	var waybills = [];
	var item     = 0;
	
	$.ajax({
		url: load_url,
		type: 'GET',
		data: {},
		success: function(data){
			$.each( data.data, function( key, value ) {
				++item;
				waybills.push(value.waybill_no);
				$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
			    + '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-success"></i>'+value.branch+')</h5>'
			    + '<small>'+value.action_date+'</small></div>'
			    + '<small> စာပို့သမား/ကောင်တာ - '+value.name+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
			});				

			$("#to-records").text(data.to);
			$("#total-records").text(data.total);
						
			if(data.prev_page_url === null){
				$("#prev-btn").attr('disabled',true);
			}else{
				$("#prev-btn").attr('disabled',false);
			}
			if(data.next_page_url === null){
				$("#next-btn").attr('disabled',true);
			}else{
				$("#next-btn").attr('disabled',false);
			}
			$("#prev-btn").val(data.prev_page_url);
			$("#next-btn").val(data.next_page_url);
		}
	});

	$('.pagination-btn').click(function(){
		//clicked url json data
		var clicked_url = $(this).val();
				
		$(this).siblings().removeClass('active')
		$(this).addClass('active');
		$.ajax({
			url: clicked_url,
			type: 'GET',
			data: {},
			success: function(data){
				//console.log(data);
				$("#fetched-data").empty();
				$.each( data.data, function( key, value ) {
					++item;
					waybills.push(value.waybill_no);
					$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
				    + '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-success"></i>'+value.branch+')</h5>'
				    + '<small>'+value.action_date+'</small></div>'
				    + '<small> စာပို့သမား/ကောင်တာ - '+value.name+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
				});	

					
				$("#to-records").text(data.to);
				if(data.prev_page_url === null){
					$("#prev-btn").attr('disabled',true);
				}else{
					$("#prev-btn").attr('disabled',false);
				}
				if(data.next_page_url === null){
					$("#next-btn").attr('disabled',true);
				}else{
					$("#next-btn").attr('disabled',false);
				}
				$("#prev-btn").val(data.prev_page_url);
				$("#next-btn").val(data.next_page_url);
			}
		});
	});

	$('body').delegate(".load-modal","click",function () {
		var id = $(this).attr('id');
		$.ajax({
			url: '/outbound/'+id+'/view',
			type: 'POST',
			data: {
				'id':id,
				'_token': _token
			},
			success: function(data){
				console.log(data);
				$("#waybill_label").text(data.waybill_no);
				$("#created_at").text(data.created_at);
				$("#from_city").text('_ '+data.origin);
				$("#transit_city").text('_ '+data.transit);
				$("#to_city").text('_ '+data.destination);
				$("#delivery").text('_ '+data.delivery);
				$("#current_status").text(data.current_status);
				$(".view-logs").attr("href",'/view/'+data.id+'/logs')
			}
		});
	});
});