$(document).ready(function(){
	var url 		= $("#url").val();
	var no_secure = $("#no_secure").val();
	var hours = $("#hours").val();
	$("input[type='checkbox']").not(this).prop("checked",false);
	if(no_secure == 1){
	   	$('#no-secure-alert').modal({show:true});
	}

	//daily check lists modal
	if(hours < 10){
		//$('#check-alert').modal({show:true});
	}


	$("#kt_datepicker_2").on("change", function(e) {
	   	var date = $("#kt_datepicker_2").val();
	   	$("#set_date").text(date);
	   	$("#config_date").val(date);
		$('#exampleModal').modal({show:true});
	});

	$(".set-date").on("click",function search(e) {
	    set_date 	= $('#config_date').val();
	    _token 		= $('#_token').val();
	    $.ajaxSetup({
	        headers: {
	            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	        }
	    });
	                
	    $.ajax({
	        type: 'post',
	        url: url+'/changed-date',
	        dataType:'json',
	        data: {
	            'set_date'	:set_date,
	            '_token' 	: _token
	        },
	        success: function(data) { 
	        	KTApp.block('#blockui_content_1', {
		            overlayColor: '#000000',
		            type: 'v2',
		            state: 'danger',
		            message: 'လုပ်ဆောင်နေပါသည် ...'
		        });
	            location.reload();
	        },
	    });
	});

	var fixed_height = $("#check-tb-height").val();
	var fixed_height2 = $("#check-tb-height2").val();

	if(fixed_height > 10){
	   	$(".fixed-height").addClass('add-scroll');
	}
	if(fixed_height2 > 10){
	   	$(".fixed-height2").addClass('add-scroll');
	}
	/*
	$("#inbound-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".inbound-item").hide();
			$(".inbound-transit").show();
		}else{
			$(".inbound-transit").hide();
			$(".inbound-item").show();
		}
	});
	*/

	$("#inbound-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".inbound-switch").hide();
			$(".inbound-transit-switch").show();
		}else{
			$(".inbound-transit-switch").hide();
			$(".inbound-switch").show();
		}
	});

	$("#outbound-city-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".to-city-branch-switch").hide();
			$(".to-city-transit-switch").show();
		}else{
			$(".to-city-transit-switch").hide();
			$(".to-city-branch-switch").show();
		}
	});
   		
	$("#inbound-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".inbound-switch").hide();
			$(".inbound-transit-switch").show();
		}else{
			$(".inbound-transit-switch").hide();
			$(".inbound-switch").show();
		}
	});

	$("#handover-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".handover-handover-switch").hide();
			$(".handover-transit-switch").show();
		}else{
			$(".handover-handover-switch").show();
			$(".handover-transit-switch").hide();
		}
	});




	$("#inbound-city-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".inbound-city-branch-switch").hide();
			$(".inbound-city-transit-switch").show();
		}else{
			$(".inbound-city-transit-switch").hide();
			$(".inbound-city-branch-switch").show();
		}
	});

	$("#outbound-transit").change(function(){
		if($(this).prop("checked") == true){
			//run code
			$(".outbound-switch").hide();
			$(".outbound-transit-switch").show();
		}else{
			$(".outbound-transit-switch").hide();
			$(".outbound-switch").show();
		}
	});

	$(".filtered-outbound-by-branch").click(function(){
		var total = collected = handover = received = 0;
		var id = $(this).attr('id');
		
	   	$('.removed-outbound-filtered').removeClass('hide');
	   	$('.filtered-show-1').removeClass('hide');
		var branch = $(this).attr('id');
		
		$('.branch').hide();
		$('.branch-'+branch).show();
		$('.checked-item').addClass('hide');
		$('.checked-item-'+branch).removeClass('hide');
		$('.filtered-hide-1').hide();


	    $(".total-"+id).each(function(){
	        total 		+= +$(this).text();
	    });
	    $(".collected-"+id).each(function(){
	        collected 	+= +$(this).text();
	    });
	    $(".handover-"+id).each(function(){
	        handover 	+= +$(this).text();
	    });
	    $(".received-"+id).each(function(){
	        received 	+= +$(this).text();
	    });


		$('#filtered-total').text(total);
		$('#filtered-collected').text(collected);
		$('#filtered-handover').text(handover);
		$('#filtered-received').text(received);
	});

	$(".removed-outbound-filtered").click(function(){
	   	$('.removed-outbound-filtered').addClass('hide');
	   	$('.filtered-show-1').addClass('hide');
	   	$('.branch').show();
	   	$('.filtered-hide-1').show();
	   	$('.checked-item').addClass('hide');
	});
});

