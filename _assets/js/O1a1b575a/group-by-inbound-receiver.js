$(document).ready(function(){

			//declared first loaded json data
			var load_url = '{{ url('').'/' }}';
			var waybills = [];
			var item     = 0;
			$.ajax({
			    url: load_url,
			    type: 'GET',
			    data: {},
			    success: function(data){
			    	if(data.total > 0){
			    		$(".alert-box").hide();
			    		//console.log(data);
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
			    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<span class=""><i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+'</span> - <i class="flaticon-placeholder-3 text-success"></i>'+value.destination+' )</h5> <h5><i class="flaticon2-location text-primary"></i>'+value.branch+' - <i class="flaticon2-location text-success"></i>'+value.handover_branch+'</h5>'
			    			+ '<small>'+value.action_date+'</small></div>'
			    			+ '<small> စာဝင်လုပ်ဆောင်သူ - '+value.branch_in_by+' ('+value.branch+')</small><span class="badge badge-pill badge-'+(value.current_status == 5? 'danger':(value.current_status == 6? 'warning':(value.current_status == 7? 'success':(value.current_status == 8? 'primary':'danger'))))+' pull-right">'+(value.current_status == 5? 'Branch In':(value.current_status == 6? 'Handover':(value.current_status == 7? 'Received':(value.current_status == 8? 'Postponed':'Rejected'))))+'</span></a>');
						});

						//console.log(waybills);
						//$(".pagination").show();
				        $("#to-records").text(data.to);
						$("#total-records").text(data.total);
							
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
			    	}else{
			    		$(".pagination").hide();
			    		$(".alert-box").show();
			    	}
			    }
			});

			$('.pagination-btn').click(function(){
				//clicked url json data
				var clicked_url = $(this).val();
				

				$(this).siblings().removeClass('active')
				$(this).addClass('active');
				$.ajax({
				    url: clicked_url,
				    type: 'GET',
				    data: {},
				    success: function(data){
				        //console.log(data);
				        $("#fetched-data").empty();
				        $.each( data.data, function( key, value ) {
				        	++item;
				        	waybills.push(value.waybill_no);
							$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start" data-toggle="modal" data-target="#exampleModalLong">'
			    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1 "><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<span class="text-danger"><i class="flaticon-placeholder-3 "></i>'+value.branch+'</span>)</h5>'
			    			+ '<small>'+value.action_date+'</small></div>'
			    			+ '<small> စာဝင်လုပ်ဆောင်သူ - '+value.branch_in_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 5? 'danger':(value.current_status == 6? 'warning':(value.current_status == 7? 'success':'primary')))+' pull-right">'+(value.current_status == 5? 'Branch In':(value.current_status == 6? 'Handover':(value.current_status == 7? 'Received':'Postponed')))+'</span></a>');
						});

						

				        $("#to-records").text(data.to);
						if(data.prev_page_url === null){
							$("#prev-btn").attr('disabled',true);
						}else{
							$("#prev-btn").attr('disabled',false);
						}
						if(data.next_page_url === null){
							$("#next-btn").attr('disabled',true);
						}else{
							$("#next-btn").attr('disabled',false);
						}
						$("#prev-btn").val(data.prev_page_url);
						$("#next-btn").val(data.next_page_url);
				    }
				});
			});
		});