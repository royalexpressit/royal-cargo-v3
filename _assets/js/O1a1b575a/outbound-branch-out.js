$(document).ready(function(){
	var base_url = $("#url").val();
	var scanned = 0;
    var success = 0;
    var failed  = 0;
    var raw     = '';
    var voice   = 'alert-1.mp3';
    var city    = $("#city").val();
    var _token  = $("#_token").val();

    $('#to_city').select2({
        placeholder: "Select To City"
    });

    $("#form").submit(function(event){
		event.preventDefault();  
	});


    $("#waybill").on("keydown",function search(e) {
    	if(e.keyCode == 13) {
    		waybill   = $("#waybill").val().toUpperCase();
        	if(waybill.length > 10){
                //valid length && continue
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                        
                $("#failed-lists").empty();
                $("#scanned-lists").show();

                
                $(".scan-btn").show();
                $("#scanned-lists").prepend('<li class="list-group-item"><i class="fa fa-qrcode"></i>  '+waybill+'</li>');
                if(scanned > 1){
                    raw = waybill+',';
                }else{
                    raw = waybill;
                }
                $(this).val('');
                $(".check-number").addClass('hide');

                $('#multi_scanned_waybills').prepend(raw); 
                $("#scanned-lists").addClass('scanned-panel');

                //limit scanned count with 30
                if(scanned == 30){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    $('#exampleModal').modal({show:true});
                    $(".continue-action").show();
                    setTimeout(function(){
                        $('#exampleModal').modal('hide');
                        $("#continue-action").trigger('focus');
                    },5000);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');


            
    	}
    });

    $(".scan-btn").on("click",function search(e) {
        //call api sent to server function
        data_send();
    });

    var data_send = function(){
    	KTApp.block('#blockui_content_1', {
            overlayColor: '#000000',
            type: 'v2',
            state: 'danger',
            message: 'လုပ်ဆောင်နေပါသည် ...'
        });

    	//prepare input for api request
    	waybills 		= $('#multi_scanned_waybills').val();
        user_id         = $("#user_id").val();
        origin      	= $("#city_id").val();
        transit         = $("#transit").val();
        destination     = $('select[id=to_city]').val();
        package_id      = $("#package_id").val();
        branch_id  		= $("#branch_id").val();
        transit  		= $("input[name='transit']:checked"). val();
        
        $.ajaxSetup({
           	headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: base_url+'/api/action/outbound',
            dataType:'json',
            data: {
                'waybills'		:waybills,
                'user_id'		:user_id,
                'origin'    	:origin,
                'transit'		:transit,
                'destination'   :destination,
                'branch_id'		:branch_id,
                'package_id'	:package_id,
                'action'		:'branch-out'
            },
            success: function(data) { 
            	KTApp.unblock('#blockui_content_1');
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();

                success = $("#scanned").text() - data.failed.length;
        		failed  = data.failed.length;

        		//add scroll max size for item > 10
                if(failed > 10){
                   $("#failed-lists").addClass('scanned-panel'); 
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
	                for (i = 0; i < data.failed.length; i++) {
						$("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-times text-danger"></i> '+data.failed[i]+'</li>');
					}
				}else{
					$("#failed-lists").prepend('<li class="list-group-item"><i class="fa fa-check text-success"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
				}

                //callback action to sent odoo api
                if(data.cargos){
                    if(data.cargos.length > 0){
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        //callback odoo api sent
                        $.ajax({
                            type: 'post',
                            url: url+"/sent-odoo-api-callback",
                            dataType:'json',
                            data: {
                                'cargos':data.cargos,
                                'city':city,
                                'status':'out',
                                '_token':_token
                            },
                            success: function(data) { 

                            }
                        });
                    }else{
                        console.log('empyt');
                    }
                }
            },
        });
        $(this).val(''); 

        $('#multi_scanned_waybills').empty();
        $('.scan-btn').hide();
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    };


    $('#to_city').on("change",function(e) { 
		destination = $('#to_city option:selected').val();
		city_id 	= $("#city_id").val();

		$.ajax({
            type: 'post',
            url: base_url+'/api/check-transit',
            dataType:'json',
            data: {
                'origin':city_id,
                'destination':destination
            },
            success: function(data) {  
                console.log(data); 

                if(data.success == 1){
                    $("#routes").empty();
                    $("#description").html(data.description);
                    $("#transit").val(data.transit); 
                    $(".transit-msg").hide();
                    console.log(data.cities);
                    
                    if(data.cities.length > 0 ){
                    	$('#transit-modal').modal({show:true});
                    	for (i = 0; i < data.cities.length; i++) {
                    		if(i > 1){
                    			$("#h-64").addClass("h-64");
                    		}
                    		$("#routes").append('<label class="kt-radio kt-radio--bold kt-radio--success">'
							+'<input type="radio" name="transit" value="'+data.cities[i].city_id+'" '+(i==0 ? 'checked':'')+'>'+data.cities[i].city_name+'<span></span>'
							+'</label>&nbsp;&nbsp;');
                    	};
                    }else{
                    	$("#h-64").removeClass("h-64");
                    	$("#routes").empty();
                    	$(".transit-msg").show();
                    }
                }else{
                    $("#h-64").removeClass("h-64");
                    $("#routes").empty();
                    $(".transit-msg").show();
                }
            },
        });
	});

    //get updated routes from server
	$('.reload-routes').on("click",function(e){
		destination = $('#to_city option:selected').val();
		city_id 	= $("#city_id").val();

		$.ajax({
            type: 'post',
            url: base_url+'/api/check-transit',
            dataType:'json',
            data: {
                'origin':city_id,
                'destination':destination
            },
            success: function(data) {  
                console.log(data); 

                if(data.success == 1){
                    $("#routes").empty();
                    $("#description").html(data.description);
                    $("#transit").val(data.transit); 
                    $(".transit-msg").hide();
                    
                    console.log(data.cities);
                    if(data.cities.length > 0 ){
                    	$('#transit-modal').modal({show:true});
                    	for (i = 0; i < data.cities.length; i++) {
                    		if(i > 1){
                    			$("#h-64").addClass("h-64");
                    		}
                    		$("#routes").append('<label class="kt-radio kt-radio--bold kt-radio--success">'
							+'<input type="radio" name="transit" value="'+data.cities[i].city_id+'" '+(i==0 ? 'checked':'')+'>'+data.cities[i].city_name+'<span></span>'
							+'</label>&nbsp;&nbsp;');
                    	};
                    }else{
                    	$("#h-64").removeClass("h-64");
                    	$("#routes").empty();
                    	$(".transit-msg").show();
                    }
                }else{
                    $("#h-64").removeClass("h-64");
                    $("#routes").empty();
                    $(".transit-msg").show();
                }
            },
        });
	})

	var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', base_url+'/_assets/'+voice);

    $(".set-voice").on("click",function search(e) {
        $('#voiceModal').modal({show:true});
    });

    $(".choice-voice").on("click",function search(e) {
        voice = $(this).val();

        audioElement.setAttribute('src', base_url+'/_assets/'+voice);
        audioElement.load();
        console.log(voice);
    });
});