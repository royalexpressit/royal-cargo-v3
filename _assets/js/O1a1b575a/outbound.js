$(document).ready(function(){
	var url 		= $("#url").val();
	var json    	= $("#json").val();
	var search    	= $("#search").val();

   	//declared first loaded json data
	var load_json 	= url+'/'+json;
	var waybills 	= [];
	var item     	= 0;
	var _token   	= $("#_token").val();

	/** first data load function  **/
    $.ajax({
			url: load_json,
			type: 'GET',
			data: {},
			success: function(data){
				if(data.total > 0){
					$(".alert-box").hide();
					$('#fetched-data').empty();
					$.each( data.data, function( key, value ) {
						++item;
						waybills.push(value.waybill_no);
						$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
					    	+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
					    	+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
					    	+ '<small class="fs-15">'+value.action_date+'</small></div>'
					    	+ '<small class="fs-15"> စာပို့သမား/ကောင်တာ - '+value.delivery+' , စာရင်းသွင်းသူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>'
					    );
					});

					//console.log(waybills);
					//$(".pagination").show();
					$("#to-records").text(data.to);
					$("#total-records").text(data.total);
									
					if(data.prev_page_url === null){
						$("#prev-btn").attr('disabled',true);
					}else{
						$("#prev-btn").attr('disabled',false);
					}
					if(data.next_page_url === null){
						$("#next-btn").attr('disabled',true);
					}else{
						$("#next-btn").attr('disabled',false);
					}

					$("#prev-btn").val(data.prev_page_url);
					$("#next-btn").val(data.next_page_url);
				}else{
					$(".pagination").hide();
					$(".alert-box").show();
				}
			}
	});

	$('.pagination-btn').click(function(){
		//clicked url json data
		var clicked_url = $(this).val();

		$(this).siblings().removeClass('active')
		$(this).addClass('active');
		$.ajax({
			url: clicked_url,
			type: 'GET',
			data: {},
			success: function(data){
				$("#fetched-data").empty();
				$.each( data.data, function( key, value ) {
				    ++item;
				    $("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
					    + '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
					    + (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
					    + '<small class="fs-15">'+value.action_date+'</small></div>'
					    + '<small class="fs-15"> စာပို့သမား/ကောင်တာ - '+value.delivery+' , စာရင်းသွင်းသူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>'
					);
				});

						

				$("#to-records").text(data.to);
				if(data.prev_page_url === null){
					$("#prev-btn").attr('disabled',true);
				}else{
					$("#prev-btn").attr('disabled',false);
				}
				if(data.next_page_url === null){
					$("#next-btn").attr('disabled',true);
				}else{
					$("#next-btn").attr('disabled',false);
				}
				$("#prev-btn").val(data.prev_page_url);
				$("#next-btn").val(data.next_page_url);
			}
		});
	});

   

    	/** load search function **/
    	var load_search_data = function(data) {
    		var waybill = data;
			if(waybill.length > 5){
			    $('.search-spinner').show();
			    $('.keyword').hide();
			    $('.search').css('display', 'inline-block');
			    $('#term').text(data);
			    $('#fetched-data').hide();
			    $.ajax({
					url: url+'/outbound-search',
					type: 'GET',
					data: {
					    'term':waybill,
					    'page':search
					},
					success: function(data){
					    $('.search-spinner').hide();
					    if(data.total > 0){
					        $(".alert-box").hide();
					        $("#search-data").empty();
						    $.each( data.data, function( key, value ) {
						        ++item;
						    	waybills.push(value.waybill_no);
									$("#search-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
					    			+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon2-protection text-danger"></i>'+value.branch+')</h5>'
					    			+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
					    			+ '<small>'+value.action_date+'</small></div>'
					    			+ '<small> စာပို့သမား/ကောင်တာ - '+value.delivery+' , စာရင်းသွင်းသူ - '+value.action_by+'</small><span class="badge badge-pill badge-'+(value.current_status == 1? 'danger':(value.current_status == 2? 'warning':(value.current_status == 3? 'success':'primary')))+' pull-right">'+(value.current_status == 1? 'Collected':(value.current_status == 2? 'Handover':(value.current_status == 3? 'Received':'Branch Out')))+'</span></a>');
							});
							console.log(item);
							$(".pagination").hide();
						}else{
							$("#search-data").empty();
				    		$(".alert-box").show();
						}
					}
				});
			}else{
			    $("#search-data").empty();
			    $('#fetched-data').show();
			    $('.keyword').show();
			    $('.search').hide();
			    load_data();
			   }
    	}

    	
			
    

		/** search waybill from search box **/
		$('#search-waybill').keyup(function(e) {
			//need input min 5 digits
			if(e.target.value.length > 5){
				load_search_data(e.target.value);
			}else{
				$(".pagination").show();
				$("#search-data").empty();
				$('#fetched-data').show();
			}	
		});

		/** view waybill detail with modal **/
		$('body').delegate(".load-modal","click",function () {
			var id = $(this).attr('id');
			$.ajax({
				url: url+'/outbound/'+id+'/view',
				type: 'POST',
				data: {
					'id':id,
					'_token': _token
				},
				success: function(data){
					console.log(data);
					$("#waybill_label").text(data.waybill_no);
					$("#outbound_date").text(data.outbound_date);
					$("#from_city").text('_ '+data.origin);
					$("#transit_city").text('_ '+data.transit);
					$("#to_city").text('_ '+data.destination);
					$("#delivery").text('_ '+data.delivery);
					$("#current_status").html(data.current_status);
					$(".view-logs").attr("href",url+'/outbound/view/'+data.id+'/logs')
				}
			});
		});
});