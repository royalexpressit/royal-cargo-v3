$(document).ready(function(){
			var url 		= $("#url").val();
			var json    	= $("#json").val();
			var to_branch   = $("#to_branch").val();
			var date        = $("#date").val();
			var params   	= to_branch+'&'+date;

		   	//declared first loaded json data
			var load_json 	= url+'/'+json+'/'+params;
			var waybills 	= [];
			var item     	= 0;
			var _token   	= $("#_token").val();

			/** first data load function  **/
		    var load_data = function() { 
			    $.ajax({
					url: load_json,
					type: 'get',
					data: {},
					success: function(data){
						if(data.total > 0){
							$(".alert-box").hide();
							$('#fetched-data').empty();
							$.each( data.data, function( key, value ) {
								++item;
								waybills.push(value.waybill_no);
								$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
							    	+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon-placeholder-3 text-danger"></i>'+value.destination+')</h5>'
							    	+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
							    	+ '<small class="fs-15">'+value.action_date+'</small></div>'
							    	+ '<small class="fs-15"> နောက်ဆုံးလုပ်ဆောင်သူ - '+value.branch_in_by+'</small><span class="badge badge-pill badge-'+value.current_status+' pull-right">'+cargo_status(value.current_status)+'</span></a>'
							    );
							});

							//console.log(waybills);
							//$(".pagination").show();
							$("#to-records").text(data.to);
							$("#total-records").text(data.total);
											
							if(data.prev_page_url === null){
								$("#prev-btn").attr('disabled',true);
							}else{
								$("#prev-btn").attr('disabled',false);
							}
							if(data.next_page_url === null){
								$("#next-btn").attr('disabled',true);
							}else{
								$("#next-btn").attr('disabled',false);
							}

							$("#prev-btn").val(data.prev_page_url);
							$("#next-btn").val(data.next_page_url);
						}else{
							$(".pagination").hide();
							$(".alert-box").show();
						}
					}
				});
		    };

		    //called function when page loading
    		load_data();	

		    /** pagination button click event **/
			$('.pagination-btn').click(function(){
				//clicked url json data
				var clicked_url = $(this).val();
				$(this).siblings().removeClass('active')
				$(this).addClass('active');

				$.ajax({
					url: clicked_url,
					type: 'get',
					data: {},
					success: function(data){
						if(data.total > 0){
							$(".alert-box").hide();
							$('#fetched-data').empty();
							$.each( data.data, function( key, value ) {
								++item;
								waybills.push(value.waybill_no);
								$("#fetched-data").append('<a href="#" class="list-group-item list-group-item-action flex-column align-items-start load-modal" data-toggle="modal" data-target="#exampleModalLong" id="'+value.id+'">'
							    	+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="fa fa-qrcode"></i> '+value.waybill_no+' (<i class="flaticon-placeholder-3 text-danger"></i>'+value.origin+' , <i class="flaticon-placeholder-3 text-danger"></i>'+value.destination+')</h5>'
							    	+ (value.transit != null? '<span class="badge badge-pill badge-danger">Transit</span>':'')
							    	+ '<small class="fs-15">'+value.action_date+'</small></div>'
							    	+ '<small class="fs-15"> နောက်ဆုံးလုပ်ဆောင်သူ - '+value.branch_in_by+'</small><span class="badge badge-pill badge-'+value.current_status+' pull-right">'+cargo_status(value.current_status)+'</span></a>'
							    );
							});

							//console.log(waybills);
							//$(".pagination").show();
							$("#to-records").text(data.to);
							$("#total-records").text(data.total);
											
							if(data.prev_page_url === null){
								$("#prev-btn").attr('disabled',true);
							}else{
								$("#prev-btn").attr('disabled',false);
							}
							if(data.next_page_url === null){
								$("#next-btn").attr('disabled',true);
							}else{
								$("#next-btn").attr('disabled',false);
							}

							$("#prev-btn").val(data.prev_page_url);
							$("#next-btn").val(data.next_page_url);
						}else{
							$(".pagination").hide();
							$(".alert-box").show();
						}
					}
				});		
			});

		    /** load pagination function **/
		    var load_pagination_data = function() { 
		    	//clicked url json data
				
			};

			$('body').delegate(".load-modal","click",function () {
				var id = $(this).attr('id');
						
				$.ajax({
					url: url+'/inbound/'+id+'/view',
					type: 'POST',
					data: {
						'id':id,
						'_token': _token
					},
					success: function(data){
						$("#waybill_label").text(data.waybill_no);
						$("#action_date").text(data.action_date);
						$("#from_city").text('_ '+data.origin);
						$("#transit_city").text('_ '+data.transit);
						$("#to_city").text('_ '+data.destination);
						$("#branch_in_by").text('_ '+data.branch_in_by);
						$("#current_status").html(data.current_status);
						$(".view-logs").attr("href",url+'/inbound/view/'+data.id+'/logs')	
					}
				});		
			});

			$('.export-csv').click(function(){
				$('#export-modal').modal({show:true});
				var params = $('#params').val();
				$.ajax({
					url: url+'/export/reports/action-6/',
					type: 'post',
					data: {
						params:params,
						_token:_token
					},
					success: function(data){
						if(data != ''){
							json = JSON.stringify(data);
							$("#raw").val(json);
							$('.confirmed').show();
						}else{
							$('.confirmed').hide();
						}
						
					}

				});
			});

		});