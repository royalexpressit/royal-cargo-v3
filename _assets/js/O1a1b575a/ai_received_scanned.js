$(document).ready(function(){
    var url     = $("#url").val();
    var _token  = $("#_token").val();
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var raw     = '';
    var voice   = 'alert-1.mp3';

    $("#scanned_waybill").on("keydown",function search(e) {
        if(e.keyCode == 13) {
            var scanned_waybill   = $("#scanned_waybill").val().toUpperCase();
            if(scanned_waybill.length == 17){
                    //valid length && continue
                    ++scanned;
                    $("#scanned").text(scanned);
                    $("#success").text(0);
                    $("#failed").text(0);
                    $(".check-number").addClass('hide');

                    data_send();
                              
                    //limit scanned count with 30
                if(scanned == 10){
                    $("#scanned_waybill").attr("disabled", true);
                    audioElement.play();
                    $('#exampleModal').modal({show:true});
                    $(".continue-action").show();
                    $(".continue-action-btn").show();
                    setTimeout(function(){
                    	$('#exampleModal').modal('hide');
                    	$("#continue-action").trigger('focus');
                    },5000);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#scanned_waybill").val('');
            }
        }
    });

    $(".continue-action-btn").on("click",function search(e) {
        //reset form
        $("#scanned_waybill").attr("disabled", false);
        $("#scanned_waybill").trigger('focus');
        $(".continue-action").hide();
        $("#continue-action").val('');
        $("#scanned-lists").empty();
        $(".continue-action-btn").hide();
        scanned     = 0;
        failed      = 0;
        success     = 0;
    });

    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                //reset form
                $("#scanned_waybill").attr("disabled", false);
                $("#scanned_waybill").trigger('focus');
                $(".continue-action").hide();
                $("#continue-action").val('');
                $("#scanned-lists").empty();
                scanned     = 0;
                failed      = 0;
                success     = 0;
            }else{
                $("#continue-action").val('');
            }
        }
    });

    var data_send = function(){
        //prepare input for api request
        waybill         = $('#scanned_waybill').val();
        user_id         = $("#user_id").val();
        city_id         = $("#city_id").val();
        branch_id       = $("#branch_id").val();
        
        $.ajaxSetup({
            headers: {
               'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                
        $.ajax({
            type: 'post',
            url: url+'/ai-outbound-received',
            dataType:'json',
            data: {
               	'waybill'     :waybill,
               	'user_id'     :user_id,
               	'city_id'     :city_id,
               	'branch_id'   :branch_id,
               	'_token'      : _token,
            },
            success: function(data) { 
               	if(data.success == 1){
                    //added old and new count for tmp 
                    ++success;
               	}else{
                    ++failed;
                }
                const audio = new Audio("_assets/voices/"+data.voice);
                audio.play();

                $("#success").text(success);
                $("#failed").text(failed);
                $("#scanned-lists").prepend(data.message);
            },
        });
        

        $('.scan-btn').hide();
        $('.continue-action').hide();
        $('#scanned_waybill').val('');
        $('#scanned-waybill').trigger('focus');
    }

    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/_assets/'+voice);
});