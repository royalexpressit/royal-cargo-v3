$(document).ready(function(){
    var url 		= $("#url").val();

	//declared first loaded json data
	var load_url = url+'/api/cities';

	$.ajax({
		url: load_url,
		type: 'GET',
		data: {},
		success: function(data){
			console.log(data);
			$.each( data.data, function( key, value ) {
				$("#fetched-data").append('<div class="list-group-item list-group-item-action flex-column align-items-start">'
		    		+ '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="flaticon-placeholder-3 text-danger"></i> <span class="text-primary">'+value.name+' ('+value.mm_name+')</span></h5>'
		    		+ '<span>'+(value.is_service_point == 1? '<i class="flaticon-placeholder-2"></i> Service Point':'<i class="flaticon-placeholder"></i> Offices')+'</span></div>'
		    		+ '<span> Shortcode - <strong class="text-danger">'+value.shortcode+'<strong></span> <a href="config-routes/'+value.id+'"><i class="flaticon-cogwheel-2"></i></a></div>'
		    	);
			});

			$("#to-records").text(data.to);
			$("#total-records").text(data.total);
						
			if(data.prev_page_url === null){
				$("#prev-btn").attr('disabled',true);
			}else{
				$("#prev-btn").attr('disabled',false);
			}
			if(data.next_page_url === null){
				$("#next-btn").attr('disabled',true);
			}else{
				$("#next-btn").attr('disabled',false);
			}
			$("#prev-btn").val(data.prev_page_url);
			$("#next-btn").val(data.next_page_url);
		}
	});

	//pagination btn click action
	$('.pagination-btn').click(function(){
		//clicked url json data
		var clicked_url = $(this).val();
		$(this).siblings().removeClass('active')
		$(this).addClass('active');
		$.ajax({
			url: clicked_url,
			type: 'GET',
			data: {},
			success: function(data){
				console.log(data);
				$("#fetched-data").empty();
				$.each( data.data, function( key, value ) {
					$("#fetched-data").append('<div class="list-group-item list-group-item-action flex-column align-items-start">'
					    + '<div class="d-flex w-100 justify-content-between"><h5 class="mb-1"><i class="flaticon-placeholder-3 text-danger"></i> <span class="text-primary">'+value.name+' ('+value.mm_name+')</span></h5>'
					    + '<span>'+(value.is_service_point == 1? '<i class="flaticon-placeholder-2"></i> Service Point':'<i class="flaticon-placeholder"></i> Offices')+'</span></div>'
					    + '<span> Shortcode - <strong class="text-danger">'+value.shortcode+'<strong></span> <a href="config-routes/'+value.id+'"><i class="flaticon-cogwheel-2"></i></a></div>'
					);
				});
				$("#to-records").text(data.to);
				if(data.prev_page_url === null){
					$("#prev-btn").attr('disabled',true);
				}else{
					$("#prev-btn").attr('disabled',false);
				}
				if(data.next_page_url === null){
					$("#next-btn").attr('disabled',true);
				}else{
					$("#next-btn").attr('disabled',false);
				}
				$("#prev-btn").val(data.prev_page_url);
				$("#next-btn").val(data.next_page_url);
			}
		});
	});
});