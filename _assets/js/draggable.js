var url         = $("#url").val();
var origin      = $("#origin").val();
var destination = $("#destination").val();
var _token      = $("#_token").val();

"use strict";
var KTPortletDraggable = function () {

    return {
        //main function to initiate the module
        init: function () {
            $("#kt_sortable_portlets").sortable({
                connectWith: ".kt-portlet__head",
                items: ".kt-portlet",
                opacity: 0.8,
                handle : '.kt-portlet__head',
                coneHelperSize: true,
                placeholder: 'kt-portlet--sortable-placeholder',
                forcePlaceholderSize: true,
                tolerance: "pointer",
                helper: "clone",
                cancel: ".kt-portlet--sortable-empty", // cancel dragging if portlet is in fullscreen mode
                revert: 250, // animation in milliseconds
                update: function(b, c) {
                    if (c.item.prev().hasClass("kt-portlet--sortable-empty")) {
                        c.item.prev().before(c.item);
                        console.log('item');  
                    }
                    
                    KTApp.block('#blockui_content_1', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'danger',
                        message: 'လုပ်ဆောင်နေပါသည် ...'
                    });

                    var item = c.item[0].id;
                    $(".drag-item-"+item).addClass('drag-item-active');
                    console.log(item); 
                    
                    $.ajax({
                        url: url+'/setting/setup-routes',
                        type: 'POST',
                        data: {
                            origin:origin,
                            destination:destination,
                            transit:item,
                            _token:_token
                        },
                        success: function(data){ 
                            //console.log(data);
                            if(data.active == 0){
                                $(".drag-item-"+item).removeClass('drag-item-active');
                                $(".drag-item-"+item).addClass('drag-item-inactive');
                            }else{
                                $(".drag-item-"+item).addClass('drag-item-active');
                                $(".drag-item-"+item).removeClass('drag-item-inactive');
                            }
                            setTimeout(function() {
                                KTApp.unblock('#blockui_content_1');
                            }, 1000);
                        }
                    });

                }
            });
        }
    };
}();

jQuery(document).ready(function() {
    KTPortletDraggable.init();
});