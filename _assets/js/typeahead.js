"use strict";
// Class definition
var KTTypeahead = function() {
    var cities = [
        'STE-YGN','MDY-MLM','STE-ATG','STE-ALN','STE-APN','STE-BGN','STE-BGO',
        'STE-BHO','STE-BKK','STE-BLN','STE-BGL','STE-CHK','STE-CTR','STE-DKU',
        'STE-DNB','STE-DWI','STE-GBG','STE-HKA','STE-HTD','STE-HAN','STE-HKN',
        'STE-HPW','STE-JGO','STE-KLW','STE-KLY','STE-KTA','STE-KKR','STE-KLN',
        'STE-KTG','STE-KGT'
    ];


    // Private functions
    var demo1 = function() {
        var substringMatcher = function(strs) {
            return function findMatches(q, cb) {
                var matches, substringRegex;

                // an array that will be populated with substring matches
                matches = [];

                // regex used to determine if a string contains the substring `q`
                var substrRegex = new RegExp(q, 'i');

                // iterate through the pool of strings and for any string that
                // contains the substring `q`, add it to the `matches` array
                $.each(strs, function(i, str) {
                    if (substrRegex.test(str)) {
                        matches.push(str);
                    }
                });

                cb(matches);
            };
        };

        $('#kt_typeahead_1, #kt_typeahead_1_modal, #kt_typeahead_1_validate, #kt_typeahead_2_validate, #kt_typeahead_3_validate').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        }, {
            name: 'cities',
            source: substringMatcher(cities)
        });
    }

    return {
        // public functions
        init: function() {
            demo1();
        }
    };
}();

jQuery(document).ready(function() {
    KTTypeahead.init();
});
