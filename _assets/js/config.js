const loaderEl = document.getElementsByClassName('fullpage-loader')[0];
			document.addEventListener('readystatechange', (event) => {
			// const readyState = "interactive";
			const readyState = "complete";
		    
			if(document.readyState == readyState) {
				// when document ready add lass to fadeout loader
				loaderEl.classList.add('fullpage-loader--invisible');
				
				// when loader is invisible remove it from the DOM
				setTimeout(()=>{
					loaderEl.parentNode.removeChild(loaderEl);
				}, 1000)
			}
		});
var KTAppOptions = {
			    "colors": {
			        "state": {
			            "brand": "#385aeb",
			            "metal": "#c4c5d6",
			            "light": "#ffffff",
			            "accent": "#00c5dc",
			            "primary": "#5867dd",
			            "success": "#34bfa3",
			            "info": "#36a3f7",
			            "warning": "#ffb822",
			            "danger": "#fd3995",
			            "focus": "#9816f4"
			        },
			        "base": {
			            "label": [
			                "#c5cbe3",
			                "#a1a8c3",
			                "#3d4465",
			                "#3e4466"
			            ],
			            "shape": [
			                "#f0f3ff",
			                "#d9dffa",
			                "#afb4d4",
			                "#646c9a"
			            ]
			        }
			    }
		};



function cargo_status(status){
	if(status == 1){
		return '[Ori]Collected';
	}else if(status == 2){
		return '[Ori]Handover';
	}else if(status == 3){
		return '[Ori]Received';
	}else if(status == 4){
		return '[Ori]Branch Out';
	}else if(status == 5){
		return '[Des]Branch In';
	}else if(status == 6){
		return '[Des]Handover';
	}else if(status == 7){
		return '[Des]Received';
	}else if(status == 8){
		return '[Des]Postponed';
	}else if(status == 9){
		return '[Des]Rejected';
	}else if(status == 10){
		return '[Des]Transferred';
	}else{
		return '[Des]Branch In';
	}
}

function transit_status(status){
	if(status == 1){
		return 'Branch In';
	}else if(status == 2){
		return 'Handover';
	}else if(status == 3){
		return 'Received';
	}else if(status == 4){
		return 'Branch Out';
	}else{
		return 'Undefined';
	}
}


