function status(status){
    		if(status == 1 || status == 5){
    			badge = '<div class="chip mr-1 border-danger">'
              	+'<div class="chip-body">'
                	+'<div class="avatar bg-danger">'
                  		+'<i class="bx bx-chevrons-'+(status == 1 ? 'right':'left')+'"></i>'
                	+'</div>'
                	+'<span class="chip-text">'+(status == 1 ? 'Collected':'Branch In')+'</span>'
              	+'</div>'
            	+'</div>';
    		}else if(status == 2 || status == 6){
    			badge = '<div class="chip mr-1 border-warning">'
              	+'<div class="chip-body">'
                	+'<div class="avatar bg-warning">'
                  		+'<i class="bx bx-chevrons-'+(status == 2 ? 'right':'left')+'"></i>'
                	+'</div>'
                	+'<span class="chip-text">Handover</span>'
              	+'</div>'
            	+'</div>';
    		}else if(status == 3 || status == 7){
    			badge = '<div class="chip mr-1 border-success">'
              	+'<div class="chip-body">'
                	+'<div class="avatar bg-success">'
                  		+'<i class="bx bx-chevrons-'+(status == 3 ? 'right':'left')+'"></i>'
                	+'</div>'
                	+'<span class="chip-text">Received</span>'
              	+'</div>'
            	+'</div>';
    		}else{
    			badge = '<div class="chip mr-1 border-primary">'
              	+'<div class="chip-body">'
                	+'<div class="avatar bg-primary">'
                  		+'<i class="bx bx-chevrons-'+(status == 4 ? 'right':'left')+'"></i>'
                	+'</div>'
                	+'<span class="chip-text">Branch Out</span>'
              	+'</div>'
            	+'</div>';
    		}

    		return badge;
}

$('body').delegate(".load-modal","click",function () {
        var id = $(this).attr('id');
            
        $.ajax({
          url: url+'/inbound/'+id+'/view',
          type: 'POST',
          data: {
            'id':id,
            '_token': _token
          },
          success: function(data){
            $("#waybill_label").text(data.waybill_no);
            $("#action_date").text(data.action_date);
            $("#from_city").text('_ '+data.origin);
            $("#transit_city").text('_ '+data.transit);
            $("#to_city").text('_ '+data.destination);
            $("#branch_in_by").text('_ '+data.branch_in_by);
            $("#current_status").html(data.current_status);
            $(".view-logs").attr("href",url+'/inbound/view/'+data.id+'/logs')  
          }
        });    
      });