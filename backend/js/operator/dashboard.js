var url     = $("#url").val();
var _token = $("#_token").val();
    var term = '';

  $("#term").on("keyup", function(e) {
       var val = $("#term").val();
      term = val;
      console.log(term);
  });

    jQuery(function($) {
      $(".format-picker1").pickadate({
          format: 'yyyy-mm-dd'
        });
    });

  

    $(".format-picker1").on("change", function(e) {
        var date = $(".format-picker1").val();
        $("#set_date").text(date);
        $("#config_date").val(date);
        $('#warning').modal({show:true});

        console.log(date);
    });

    $(".set-date").on("click",function search(e) {
        set_date  = $('#config_date').val();
        _token    = $('#_token').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                  
        $.ajax({
            type: 'post',
            url: url+'/changed-date',
            dataType:'json',
            data: {
                'set_date'  :set_date,
                '_token'  : _token
            },
            success: function(data) { 
              console.log(data);
              location.reload();
            },
        });
    });


    // Custom Message
    $('.btn-set').on('click', function () {
      var block_ele = $(this).closest('.reloading');
      $(block_ele).block({
          message: '<span class="semibold"> လုပ်ဆောင်နေပါသည်.....</span>',
          timeout: 2000, //unblock after 2 seconds
          overlayCSS: {
            backgroundColor: '#ffffff',
            opacity: 0.8,
            cursor: 'wait'
          },
          css: {
            border: 0,
            padding: 0,
            backgroundColor: 'transparent'
          }
      });
    });