$(document).ready(function(){
    var url     = $("#url").val();
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var raw     = '';
    var voice   = 'alert-1.mp3';
    //var _token  = $("#_token").val();


    $("#form").submit(function(event){
        event.preventDefault();  
    });

    $("#waybill").on("keydown",function search(e){
        if(e.keyCode == 13) {
            waybill   = $("#waybill").val().toUpperCase();
            if(waybill.length > 10){
                //valid length && continue
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                            
                $("#failed-lists").empty();
                $("#scanned-lists").show();
   
                $(".scan-btn").removeAttr('disabled');
                $("#scanned-lists").prepend('<li class="list-group-item sm-item"><i class="bx bxs-search text-primary v-middle"></i> '+waybill+'</li>');
                if(scanned > 1){
                    raw = waybill+',';
                }else{
                    raw = waybill;
                }
                $(this).val('');
                $(".check-number").addClass('hide');

                $('#multi_scanned_waybills').prepend(raw); 
                $("#scanned-lists").addClass('scanned-panel');
                    
                //limit scanned count with 25
                if(scanned == 25){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    $('#warning-modal').modal({show:true});
                    $(".continue-action").show();
                    setTimeout(function(){
                        $('#warning-modal').modal('hide');
                        $("#continue-action").trigger('focus');
                    },5000);
                    console.log(voice);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
        }
    });

    $(".scan-btn").on("click",function search(e) {
        //call api sent to server function
        $('.data-loading').show();
        data_send();
    });

    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                data_send();
            }else{
                $("#continue-action").val('');
            }
        }
    });

    $("#switch-postponed").change(function(){
        if($(this).prop("checked") == true){
            //run code
            $(".action-received").hide();
            $(".action-postponed").show();
        }else{
            $(".action-postponed").hide();
            $(".action-received").show();
        }
    });

    var data_send = function(){
        waybills        = $('#multi_scanned_waybills').val();
        user_id         = $("#user_id").val();
        package_id      = $("#package_id").val();
        city_id         = $("#city_id").val();
        branch_id       = $("#branch_id").val();
        active_time     = $("#active_time").val();
        handover_branch = $("#handover_branch").val();
        transit_branch  = $('#transit_branch').val()

        if($("#checkbox1").prop("checked") == true){
            transit = 1;
        }else{
            transit = 0;
        }
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
               
        $.ajax({
            type: 'post',
            url: url+'/api/action/inbound',
            dataType:'json',
            data: {
                'waybills'          :waybills,
                'user_id'           :user_id,
                'destination'       :city_id,
                'branch_id'         :branch_id,
                'package_id'        :package_id,
                'action'            :'postponed'
            },
            success: function(data) { 
                finished();
                $('.data-loading').hide();
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();

                success = $("#scanned").text() - data.failed.length;
                failed  = data.failed.length;

                //add scroll max size for item > 10
                if(failed > 10){
                    $("#failed-lists").addClass('scanned-panel'); 
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
                    for (i = 0; i < data.failed.length; i++) {
                        $("#failed-lists").prepend('<li class="list-group-item sm-item"><i class="bx bx-x text-danger v-middle"></i> '+data.failed[i]+'</li>');
                    }
                }else{
                    $("#failed-lists").prepend('<li class="list-group-item sm-item"><i class="bx bx-check text-success v-middle"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
                }
            },
        });
        $(this).val(''); 


        $('#multi_scanned_waybills').empty();
        $('.scan-btn').attr('disabled',true);
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    }
        
    //limit alert audio background
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/backend/alerts/'+voice);

    $(".set-voice").on("click",function search(e) {
        $('#voiceModal').modal({show:true});
    });

    $(".choice-voice").on("click",function search(e) {
        voice = $(this).val();

        $(".choice-voice").removeClass('btn-success btn-primary');
        $(".choice-voice").addClass('btn-primary');
        $(this).addClass('btn-success');

        audioElement.setAttribute('src', url+'/backend/alerts/'+voice);
        audioElement.load();
        console.log(voice);
    });

    function finished() {
        $('.reloading').block({
            timeout: 500, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#ffffff',
                opacity: 0.8,
                cursor: 'wait'
              },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    };

    $('.scan-btn').on('click', function () {
        var block_ele = $(this).closest('.reloading');
        $(block_ele).block({
            message: '<span class="semibold"> လုပ်ဆောင်နေပါသည်.....</span>',
            overlayCSS: {
                backgroundColor: '#ffffff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    });

    //transit switch option
    $("#checkbox1").change(function(){
        if($(this).prop("checked") == true){
            //run code
            $(".action-transit").show();
            $(".action-no-transit").hide();
        }else{
            $(".action-no-transit").show();
            $(".action-transit").hide();
        }
    });
}); 