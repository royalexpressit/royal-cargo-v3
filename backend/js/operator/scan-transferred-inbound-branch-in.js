$(document).ready(function(){
    var url     = $("#url").val();
    var scanned = 0;
    var success = 0;
    var failed  = 0;
    var raw     = '';
    var voice   = 'alert-1.mp3';

    $('#delivery').select2({
        placeholder: "Select Delivery/Counter"
    });

    $("#form").submit(function(event){
    	event.preventDefault();  
    });

    $("#waybill").on("keydown",function search(e) {
        if(e.keyCode == 13) {
            waybill   = $("#waybill").val().toUpperCase();
            if(waybill.length > 10 && (waybill != 'CONTINUE-ACTION')){
                //valid length && continue
                console.log(waybill);
                ++scanned;
                $("#scanned").text(scanned);
                $("#success").text(0);
                $("#failed").text(0);
                        
                $("#failed-lists").empty();
                $("#scanned-lists").show();

                
                $(".scan-btn").removeAttr('disabled');
                $("#scanned-lists").prepend('<li class="list-group-item sm-item"><i class="bx bxs-search text-primary v-middle"></i>  '+waybill+'</li>');
                if(scanned > 1){
                    raw = waybill+',';
                }else{
                    raw = waybill;
                }
                $(this).val('');
                $(".check-number").addClass('hide');

                $('#multi_scanned_waybills').prepend(raw); 
                if(scanned > 10){
                    $("#scanned-lists").addClass('scanned-panel');
                }

                //limit scanned count with 25
                if(scanned == 25){
                    $("#waybill").attr("disabled", true);
                    audioElement.play();
                    $('#exampleModal').modal({show:true});
                    $(".continue-action").show();
                    setTimeout(function(){
                        $('#exampleModal').modal('hide');
                        $("#continue-action").trigger('focus');
                    },5000);
                }
            }else{
                //invalid length && try again
                $(".check-number").removeClass('hide');
                $("#waybill").val('');
            }

            //removed fixed height for error lists
            $("#failed-lists").removeClass('scanned-panel');
        }
    });

    $(".scan-btn").on("click",function search(e) {
        //call api sent to server function
        $('.data-loading').show();
        data_send();
    });

    //scan code for continue action
    $("#continue-action").on("keydown",function search(e) {
        var code = $("#continue-action").val();
        if(e.keyCode == 13) {
            if(code == 'continue-action'){
                data_send();
            }else{
                $("#continue-action").val('');
            }
        }
    });


    var data_send = function(){
        //prepare input for api request
        waybills        = $('#multi_scanned_waybills').val();
        user_id         = $("#user_id").val();
        package_id      = $("#package_id").val();
        city_id         = $("#city_id").val();
        origin          = $('select[id=origin]').val();
        branch_id       = $("#branch_id").val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
                    
        $.ajax({
            type: 'post',
            url: url+'/api/action/transferred',
            dataType:'json',
            data: {
                'waybills'        :waybills,
                'user_id'         :user_id,
                'origin'          :origin,
                'destination'     :city_id,
                'branch_id'       :branch_id,
                'package_id'      :package_id,
                'action'          :'branch-in',
                'type'            :'inbound'
            },
            success: function(data) { 
                finished();
                $('.data-loading').hide();
                $("#scanned-lists").removeClass('scanned-panel');
                $("#scanned-lists").empty(); 
                $("#failed-lists").empty();


                success = $("#scanned").text() - data.failed.length;
            	failed  = data.failed.length;

                //add scroll max size for item > 10
                if(failed > 10){
                   $("#failed-lists").addClass('scanned-panel'); 
                }
                if(failed == 0){
                    $('.great').show();
                }

                $("#success").text(success);
                $("#failed").text(failed);

                if(data.failed.length > 0 ){
    	            for (i = 0; i < data.failed.length; i++) {
    					$("#failed-lists").prepend('<li class="list-group-item sm-item"><i class="bx bx-x text-danger v-middle"></i> '+data.failed[i]+'</li>');
    				}
    			}else{
    				$("#failed-lists").prepend('<li class="list-group-item sm-item"><i class="bx bx-check text-success v-middle"></i> စာရင်းအားလုံး သိမ်းလိုက်ပါပြီ။</li>');
    			}
            },
        });
        $(this).val(''); 

        $('#multi_scanned_waybills').empty();
        $('.scan-btn').attr('disabled',true);
        $('.continue-action').hide();
        $("#waybill").attr("disabled", false);
        $('#waybill').trigger('focus');
        scanned = 0;
    }

    //limit alert audio background
    var audioElement = document.createElement('audio');
    audioElement.setAttribute('src', url+'/backend/alerts/'+voice);

    $(".set-voice").on("click",function search(e) {
        $('#voiceModal').modal({show:true});
    });

    $(".choice-voice").on("click",function search(e) {
        voice = $(this).val();

        audioElement.setAttribute('src', url+'/backend/alerts/'+voice);
        audioElement.load();
        console.log(voice);
    });

    function finished() {
        $('.reloading').block({
            timeout: 100, //unblock after 2 seconds
            overlayCSS: {
                backgroundColor: '#ffffff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    };

    $('.scan-btn').on('click', function () {
        var block_ele = $(this).closest('.reloading');
        $(block_ele).block({
            message: '<span class="semibold"> လုပ်ဆောင်နေပါသည်.....</span>',
            overlayCSS: {
                backgroundColor: '#ffffff',
                opacity: 0.8,
                cursor: 'wait'
            },
            css: {
                border: 0,
                padding: 0,
                backgroundColor: 'transparent'
            }
        });
    });
});           